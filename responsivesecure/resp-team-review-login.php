<?php
// Include the configuration file:

require_once ('includes/configuration-responsivesecure.php');
//session_start();

//require('includes/db_sessions.inc.php');
$page_title = 'Team review login';
$page_metadesc = '';
$loginnotok = 99;
$loginnotentered = 99;

if (isset($_POST['submitted'])) {
  require_once (MYSQLI);

  // Trim all the incoming data:
  $trimmed = array_map('trim', $_POST);


  $sid = intval($trimmed['teamsurveyid']);
  $p = $trimmed['pswd'];
	$url = '';
	
	var_dump($sid);
	var_dump($p);

  // Check for a survey ID and password:

  if ((!empty($sid)) && (!empty($p))) { // if survey ID and password present, authenticate and redirect to appropriate questionnaire depending on password
    
    $sql = "SELECT teamsurveyid, url_hash, leaderpassword, teampassword, stakeholderpassword, bosspassword FROM teamsurveys WHERE teamsurveyid='" . $sid . "' AND leaderpassword = '" . $p . "'";
    var_dump($sql);
    
    //Check if this is a person checking in to complete an individual review of a team by checking the password used and if so re-direct to the leader team questionnaire and exit
    if (!($stmt = $dbc->prepare("SELECT teamsurveyid, url_hash, leaderpassword, teampassword, stakeholderpassword, bosspassword FROM teamsurveys WHERE (teamsurveyid=? AND leaderpassword = ?)"))) {
      die("prepare() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    $val1 = $sid;
    $val2 = $p;


    if (!$stmt->bind_param('is', $val1, $val2)) {
      die("bind_param() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    if (!$stmt->execute()) {
      die("execute() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    if (!($stmt->store_result())) {
      die("store_result() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    $numrows = $stmt->num_rows;
    $stmt->bind_result($teamsurveyid, $url_hash, $leaderpassword, $teampassword, $stakeholderpassword, $bosspassword);
		$stmt->fetch();
    $stmt->free_result();
    $stmt->close();
    		
    if ($numrows === 1) {// A match was made
      print 'leader';
      $url = 'resp-quest-team-leader.php?a=' . $url_hash . '&b='. $teamsurveyid . '&c=t';
      header("Location: $url");
    }


    //Check if this is a team member checking in to complete a team review by checking the password used and if so re-direct to the team member questionnaire and exit
    if (!($stmt = $dbc->prepare("SELECT teamsurveyid, url_hash, leaderpassword, teampassword, stakeholderpassword, bosspassword FROM teamsurveys WHERE (teamsurveyid=? AND teampassword = ?)"))) {
      die("prepare() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    $val1 = $sid;
    $val2 = $p;

    if (!$stmt->bind_param('is', $val1, $val2)) {
      die("bind_param() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    if (!$stmt->execute()) {
      die("execute() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    if (!($stmt->store_result())) {
      die("store_result() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    $numrows = $stmt->num_rows;
    $stmt->bind_result($teamsurveyid, $url_hash, $leaderpassword, $teampassword, $stakeholderpassword, $bosspassword);
    $stmt->fetch();
    $stmt->free_result();
    $stmt->close();

    if ($numrows === 1) {// A match was made
      $url = 'resp-quest-team-member.php?a=' . $url_hash . '&b='. $teamsurveyid . '&c=t'; // Define the URL for the team member questionnaire
      header("Location: $url");
    }


    //Check if this is a stakeholder checking in to complete the survey and if so re-direct to the stakeholder questionnaire and exit
    if (!($stmt = $dbc->prepare("SELECT teamsurveyid, url_hash, leaderpassword, teampassword, stakeholderpassword, bosspassword FROM teamsurveys WHERE (teamsurveyid=? AND stakeholderpassword = ?)"))) {
      die("prepare() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    $val1 = $sid;
    $val2 = $p;

    if (!$stmt->bind_param('is', $val1, $val2)) {
      die("bind_param() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    if (!$stmt->execute()) {
      die("execute() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    if (!($stmt->store_result())) {
      die("store_result() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    $numrows = $stmt->num_rows;
    $stmt->bind_result($teamsurveyid, $url_hash, $leaderpassword, $teampassword, $stakeholderpassword, $bosspassword);
    $stmt->fetch();
    $stmt->free_result();
    $stmt->close();

    if ($numrows === 1) {// A match was made
      $url = 'resp-quest-team-stakeholder.php?a=' . $url_hash . '&b='. $teamsurveyid . '&c=t'; // Define the URL for the team stakeholder questionnaire
      header("Location: $url");
    }

    //ID and passwords were input in suitable format but not correct
    $loginnotok = 1;
  }
  else { //ID and passwords not input
    $loginnotentered = 1;
  }

  mysqli_close($dbc);
} // End of SUBMIT conditional.

?>

<?php require('includes/_header.php'); ?>

        <div class="limiter">
						<?php require('includes/_banner.php'); ?>

            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">

                    <h1>Log in to complete a team review</h1>
                    <br />
                    <?php
                    if (($loginnotentered === 1) || ($loginnotok === 1)) {
                      echo '<p class="alert alert-danger">The review identification number or password do not match those on file. <br />Please try again or check them with the person who commissioned the review. Thank you.</p><br />';
                    }
                    ?>
                    <form role="form" action="resp-team-review-login.php" method="post">
                        <div class="form-group">
                            <label for="teamsurveyid">Team review ID</label>
                            <input type="text" class="form-control" id="teamsurveyid" name="teamsurveyid">
                        </div>
                        <div class="form-group">
                            <label for="pswd">Team review password</label>
                            <input type="password" class="form-control" id="pswd" name="pswd">
                        </div>
                        <br />
                        <button type="submit" class="btn btn-danger">Login to complete the review</button>
                        <input type="hidden" name="submitted" value="TRUE" />
                    </form>

                    <!--
                                        <form action="team-review-login.php" method="post">
                                            <table class="table table-responsive">
                                                <thead><th>Review ID:</thead>
                                                <td><input type="text" name="teamsurveyid" /></td>
                                                <thead><th style="width: 5%;">Your password:</thead>
                                                <td style="width: 10%;"><input type="password" name="pswd" /></td></tr>
                                            </table>
                                            <br />
                                            <div><button type="submit" class="btn btn-danger">Login to complete the review</button></div>
                                            <p><input type="hidden" name="submitted" value="TRUE" /></p>

                                        </form>
                    -->

                </div>
                <div class="col-sm-2"></div>

            </div>
        </div>
        
<?php 
require('includes/_footer.php');
include_once("includes/analyticstracking.php"); 
?>


