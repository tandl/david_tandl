<?php
require_once ('includes/configuration-responsivesecure.php');
require_once ('includes/authenticate.php');

$page_title = 'Team review questionnaire';
$page_metadesc = '';
?>

<?php require('includes/_header.php'); ?>

        <div class="limiter">
						<?php require('includes/_banner.php'); ?>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">

                    <?php echo' <h1>Team review:  ' . $tsn . '</h1>'; ?>
                    <h3>Confidentiality</h3>
                    <p>Thank you for completing this team effectiveness review - it should take about five minutes.</p>
                    <p>By completing the questionnaire you are agreeing that your responses can be entered into our confidential and anonymized database. Your responses are not identifiable in the report as coming from you.</p>
                    <p>We make the report available only to the person who set up this review - if that is not you, please approach them for further information.</p>

                    <h3>How to complete</h3>
                    <ol>
					<li>Rate the team on each statement  - from <br /> 'They're very poor at this' <span class="icon-vpoor"></span>&nbsp;&nbsp;to&nbsp;&nbsp; 'They're very good at this' <span class="icon-vgood"></span></li>
                        <li>Then, please tick three 'Development Priority?' boxes to show the three areas you think are development priorities for the team and it most needs to work on.</li>
                        <li>At the end of the questionnaire there are a couple of boxes for your comments on the team. These will be reproduced exactly in the report, so be careful not to identify yourself in these comments unless you want to.</li>
                        <li>If you want to keep a record of your own answers use your browser to print out the form before you submit it.</li>
                        <li>When you are happy with your responses click 'Submit your review'.</li>
                    </ol>

                    <h3>For the team as a whole:</h3>

                    <form  action="resp-thanks-for-input-team.php" method="post" class="form-questions">
                        <p>
                        		<input type="hidden" name="sid" value="<?php print $sid; ?>" />
                            <input type="hidden" name="d1" value="99" />
                            <input type="hidden" name="d2" value="99" />
                            <input type="hidden" name="d3" value="99" />
                            <input type="hidden" name="d4" value="99" />
                            <input type="hidden" name="d5" value="99" />
                            <input type="hidden" name="d6" value="99" />
                            <input type="hidden" name="d7" value="99" />
                            <input type="hidden" name="d8" value="99" />
                            <input type="hidden" name="d9" value="99" />
                            <input type="hidden" name="d10" value="99" />
                            <input type="hidden" name="d11" value="99" />
                            <input type="hidden" name="d12" value="99" />
                            <input type="hidden" name="d13" value="99" />
                            <input type="hidden" name="d14" value="99" />
                            <input type="hidden" name="d15" value="99" />
                            <input type="hidden" name="d16" value="99" />
                            <input type="hidden" name="d17" value="99" />
                            <input type="hidden" name="d18" value="99" />
                            <input type="hidden" name="d19" value="99" />
                            <input type="hidden" name="d20" value="99" />
                            <input type="hidden" name="d21" value="99" />
                            <input type="hidden" name="d22" value="99" />
                            <input type="hidden" name="d23" value="99" />
                            <input type="hidden" name="d24" value="99" />
                            <input type="hidden" name="d25" value="99" />
                            <input type="hidden" name="d26" value="99" />

                            <input type="hidden" name="dev1" value="0" />
                            <input type="hidden" name="dev2" value="0" />
                            <input type="hidden" name="dev3" value="0" />
                            <input type="hidden" name="dev4" value="0" />
                            <input type="hidden" name="dev5" value="0" />
                            <input type="hidden" name="dev6" value="0" />
                            <input type="hidden" name="dev7" value="0" />
                            <input type="hidden" name="dev8" value="0" />
                            <input type="hidden" name="dev9" value="0" />
                            <input type="hidden" name="dev10" value="0" />
                            <input type="hidden" name="dev11" value="0" />
                            <input type="hidden" name="dev12" value="0" />
                            <input type="hidden" name="dev13" value="0" />
                            <input type="hidden" name="dev14" value="0" />
                            <input type="hidden" name="dev15" value="0" />
                            <input type="hidden" name="dev16" value="0" />
                            <input type="hidden" name="dev17" value="0" />
                            <input type="hidden" name="dev18" value="0" />
                            <input type="hidden" name="dev19" value="0" />
                            <input type="hidden" name="dev20" value="0" />
                            <input type="hidden" name="dev21" value="0" />
                            <input type="hidden" name="dev22" value="0" />
                            <input type="hidden" name="dev23" value="0" />
                            <input type="hidden" name="dev24" value="0" />
                            <input type="hidden" name="dev25" value="0" />
                            <input type="hidden" name="dev26" value="0" />

                            <input type="hidden" name="teamrole" value="4" />
                        </p>


<table class="tablesaw tablesaw-stack table-questions" data-tablesaw-mode="stack">
                            <thead>

                            <th style="width:58%"></th>
                            <th style="min-width:320px"></span></th>
                            <!--
                            <th class="equal-width"><span class="icon-poor"></span></th>
                            <th class="equal-width"><span class="icon-ok"></span></th>
                            <th class="equal-width"><span class="icon-good"></span></th>
                            
                            <th class="equal-width"></span></th>
                            -->
                            <th>Development<br />Priority?</th>

                            </thead>

                            <tr>
                                <td>Team members seem very clear about what they are trying to achieve together</td>
                                <td>
                                	<input class="radio_item" type="radio" name="d1" id="d1-1" value="0" <?php if ((isset($_POST['d1'])) && ($_POST['d1'] == 0)) echo 'checked="checked"'; ?> />
                                	<label class="label_item" for="d1-1"><span class="icon-vpoor"></span></label>
                                	<input class="radio_item" type="radio" name="d1" id="d1-2" value="1" <?php if ((isset($_POST['d1'])) && ($_POST['d1'] == 1)) echo 'checked="checked"'; ?> />
                                	<label class="label_item" for="d1-2"><span class="icon-poor"></span></label>
                                	<input class="radio_item" type="radio" name="d1" id="d1-3" value="2" <?php if ((isset($_POST['d1'])) && ($_POST['d1'] == 2)) echo 'checked="checked"'; ?> />
                                	<label class="label_item" for="d1-3"><span class="icon-ok"></span></label>
                                	<input class="radio_item" type="radio" name="d1" id="d1-4" value="3" <?php if ((isset($_POST['d1'])) && ($_POST['d1'] == 3)) echo 'checked="checked"'; ?> />
                                	<label class="label_item" for="d1-4"><span class="icon-good"></span></label>
                                	<input class="radio_item" type="radio" name="d1" id="d1-5" value="4" <?php if ((isset($_POST['d1'])) && ($_POST['d1'] == 4)) echo 'checked="checked"'; ?> />
                                	<label class="label_item" for="d1-5"><span class="icon-vgood"></span></label>
                                </td>
                                <td style="text-align:center;"><input type="checkbox" name="dev1" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>People in the team seem to support each other very well</td>
                                <td>
                                	<input class="radio_item" type="radio" name="d18" id="d18-1" value="0" <?php if ((isset($_POST['d18'])) && ($_POST['d18'] == 0)) echo 'checked="checked"'; ?> />
                                	<label class="label_item" for="d18-1"><span class="icon-vpoor"></span></label>
                                	<input class="radio_item" type="radio" name="d18" id="d18-2" value="1" <?php if ((isset($_POST['d18'])) && ($_POST['d18'] == 1)) echo 'checked="checked"'; ?> />
                                	<label class="label_item" for="d18-2"><span class="icon-poor"></span></label>
                                	<input class="radio_item" type="radio" name="d18" id="d18-3" value="2" <?php if ((isset($_POST['d18'])) && ($_POST['d18'] == 2)) echo 'checked="checked"'; ?> />
                                	<label class="label_item" for="d18-3"><span class="icon-ok"></span></label>
                                	<input class="radio_item" type="radio" name="d18" id="d18-4" value="3" <?php if ((isset($_POST['d18'])) && ($_POST['d18'] == 3)) echo 'checked="checked"'; ?> />
                                	<label class="label_item" for="d18-4"><span class="icon-good"></span></label>
                                	<input class="radio_item" type="radio" name="d18" id="d18-5" value="4" <?php if ((isset($_POST['d18'])) && ($_POST['d18'] == 4)) echo 'checked="checked"'; ?> />
                                	<label class="label_item" for="d18-5"><span class="icon-vgood"></span></label>
                                </td>
                                <td style="text-align:center;"><input type="checkbox" name="dev18" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                         
                            <tr>
                                <td>The team seems skilful and well-resourced</td>
                                <td>
                                	<input class="radio_item" type="radio" name="d8" id="d8-1" value="0" <?php if ((isset($_POST['d8'])) && ($_POST['d8'] == 0)) echo 'checked="checked"'; ?> />
                                	<label class="label_item" for="d8-1"><span class="icon-vpoor"></span></label>
                                	<input class="radio_item" type="radio" name="d8" id="d8-2" value="1" <?php if ((isset($_POST['d8'])) && ($_POST['d8'] == 1)) echo 'checked="checked"'; ?> />
                                	<label class="label_item" for="d8-2"><span class="icon-poor"></span></label>
                                	<input class="radio_item" type="radio" name="d8" id="d8-3" value="2" <?php if ((isset($_POST['d8'])) && ($_POST['d8'] == 2)) echo 'checked="checked"'; ?> />
                                	<label class="label_item" for="d8-3"><span class="icon-ok"></span></label>
                                	<input class="radio_item" type="radio" name="d8" id="d8-4" value="3" <?php if ((isset($_POST['d8'])) && ($_POST['d8'] == 3)) echo 'checked="checked"'; ?> />
                                	<label class="label_item" for="d8-4"><span class="icon-good"></span></label>
                                	<input class="radio_item" type="radio" name="d8" id="d8-5" value="4" <?php if ((isset($_POST['d8'])) && ($_POST['d8'] == 4)) echo 'checked="checked"'; ?> />
                                	<label class="label_item" for="d8-5"><span class="icon-vgood"></span></label>
                                </td>
                                <td style="text-align:center;"><input type="checkbox" name="dev8" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            
                            
                            <tr>
                                <td>The team has an ambitious vision</td>
								<td>
								<input class="radio_item" type="radio" name="d3" id="d3-1" value="0" <?php if ((isset($_POST['d3'])) && ($_POST['d3'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d3-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d3" id="d3-2" value="1" <?php if ((isset($_POST['d3'])) && ($_POST['d3'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d3-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d3" id="d3-3" value="2" <?php if ((isset($_POST['d3'])) && ($_POST['d3'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d3-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d3" id="d3-4" value="3" <?php if ((isset($_POST['d3'])) && ($_POST['d3'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d3-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d3" id="d3-5" value="4" <?php if ((isset($_POST['d3'])) && ($_POST['d3'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d3-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev3" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
								 </td>
                            </tr>
                            <tr>
                               <td>People in the team seem very motivated to get things done</td>
							   <td>
                                <input class="radio_item" type="radio" name="d16" id="d16-1" value="0" <?php if ((isset($_POST['d16'])) && ($_POST['d16'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d16-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d16" id="d16-2" value="1" <?php if ((isset($_POST['d16'])) && ($_POST['d16'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d16-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d16" id="d16-3" value="2" <?php if ((isset($_POST['d16'])) && ($_POST['d16'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d16-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d16" id="d16-4" value="3" <?php if ((isset($_POST['d16'])) && ($_POST['d16'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d16-4"><span class="icon-good"></span></label>
								<input class="radio_item" type="radio" name="d16" id="d16-5" value="4" <?php if ((isset($_POST['d16'])) && ($_POST['d16'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d16-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev16" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>We can always get decisions out of the team when we need them</td>
								<td>
                                <input class="radio_item" type="radio" name="d21" id="d21-1" value="0" <?php if ((isset($_POST['d21'])) && ($_POST['d21'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d21-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d21" id="d21-2" value="1" <?php if ((isset($_POST['d21'])) && ($_POST['d21'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d21-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d21" id="d21-3" value="2" <?php if ((isset($_POST['d21'])) && ($_POST['d21'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d21-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d21" id="d21-4" value="3" <?php if ((isset($_POST['d21'])) && ($_POST['d21'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d21-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d21" id="d21-5" value="4" <?php if ((isset($_POST['d21'])) && ($_POST['d21'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d21-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev21" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>The team puts great emphasis on delivering what customers want</td>
								<td>
                                <input class="radio_item" type="radio" name="d2" id="d2-1" value="0" <?php if ((isset($_POST['d2'])) && ($_POST['d2'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d2-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d2" id="d2-2" value="1" <?php if ((isset($_POST['d2'])) && ($_POST['d2'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d2-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d2" id="d2-3" value="2" <?php if ((isset($_POST['d2'])) && ($_POST['d2'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d2-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d2" id="d2-4" value="3" <?php if ((isset($_POST['d2'])) && ($_POST['d2'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d2-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d2" id="d2-5" value="4" <?php if ((isset($_POST['d2'])) && ($_POST['d2'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d2-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev2" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>The team seems very well organised</td>
								<td>
                                <input class="radio_item" type="radio" name="d10" id="d10-1" value="0" <?php if ((isset($_POST['d10'])) && ($_POST['d10'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d10-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d10" id="d10-2" value="1" <?php if ((isset($_POST['d10'])) && ($_POST['d10'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d10-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d10" id="d10-3" value="2" <?php if ((isset($_POST['d10'])) && ($_POST['d10'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d10-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d10" id="d10-4" value="3" <?php if ((isset($_POST['d10'])) && ($_POST['d10'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d10-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d10" id="d10-5" value="4" <?php if ((isset($_POST['d10'])) && ($_POST['d10'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d10-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev10" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>The team communicates its vision and priorities in an inspiring way</td>
								<td>
                                <input class="radio_item" type="radio" name="d4" id="d4-1" value="0" <?php if ((isset($_POST['d4'])) && ($_POST['d4'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d4-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d4" id="d4-2" value="1" <?php if ((isset($_POST['d4'])) && ($_POST['d4'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d4-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d4" id="d4-3" value="2" <?php if ((isset($_POST['d4'])) && ($_POST['d4'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d4-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d4" id="d4-4" value="3" <?php if ((isset($_POST['d4'])) && ($_POST['d4'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d4-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d4" id="d4-5" value="4" <?php if ((isset($_POST['d4'])) && ($_POST['d4'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d4-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev4" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>They are very good at analysing where exactly where things need improving</td>
								<td>
                                <input class="radio_item" type="radio" name="d23" id="d23-1" value="0" <?php if ((isset($_POST['d23'])) && ($_POST['d23'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d23-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d23" id="d23-2" value="1" <?php if ((isset($_POST['d23'])) && ($_POST['d23'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d23-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d23" id="d23-3" value="2" <?php if ((isset($_POST['d23'])) && ($_POST['d23'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d23-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d23" id="d23-4" value="3" <?php if ((isset($_POST['d23'])) && ($_POST['d23'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d23-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d23" id="d23-5" value="4" <?php if ((isset($_POST['d23'])) && ($_POST['d23'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d23-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev23" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>Team members speak with one voice - they are very consistent</td>
								<td>
                                <input class="radio_item" type="radio" name="d14" id="d14-1" value="0" <?php if ((isset($_POST['d14'])) && ($_POST['d14'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d14-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d14" id="d14-2" value="1" <?php if ((isset($_POST['d14'])) && ($_POST['d14'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d14-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d14" id="d14-3" value="2" <?php if ((isset($_POST['d14'])) && ($_POST['d14'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d14-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d14" id="d14-4" value="3" <?php if ((isset($_POST['d14'])) && ($_POST['d14'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d14-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d14" id="d14-5" value="4" <?php if ((isset($_POST['d14'])) && ($_POST['d14'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d14-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev14" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                               <td>Team members can make decisions without having to refer back to the boss</td>
							   <td>
                                <input class="radio_item" type="radio" name="d12" id="d12-1" value="0" <?php if ((isset($_POST['d12'])) && ($_POST['d12'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d12-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d12" id="d12-2" value="1" <?php if ((isset($_POST['d12'])) && ($_POST['d12'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d12-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d12" id="d12-3" value="2" <?php if ((isset($_POST['d12'])) && ($_POST['d12'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d12-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d12" id="d12-4" value="3" <?php if ((isset($_POST['d12'])) && ($_POST['d12'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d12-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d12" id="d12-5" value="4" <?php if ((isset($_POST['d12'])) && ($_POST['d12'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d12-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev12" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>Team members are very good at making change happen</td>
								<td>
                                <input class="radio_item" type="radio" name="d24" id="d24-1" value="0" <?php if ((isset($_POST['d24'])) && ($_POST['d24'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d24-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d24" id="d24-2" value="1" <?php if ((isset($_POST['d24'])) && ($_POST['d24'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d24-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d24" id="d24-3" value="2" <?php if ((isset($_POST['d24'])) && ($_POST['d24'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d24-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d24" id="d24-4" value="3" <?php if ((isset($_POST['d24'])) && ($_POST['d24'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d24-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d24" id="d24-5" value="4" <?php if ((isset($_POST['d24'])) && ($_POST['d24'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d24-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev24" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>The team seems to have an all-for-one and one-for-all attitude</td>
								<td>
                                <input class="radio_item" type="radio" name="d15" id="d15-1" value="0" <?php if ((isset($_POST['d15'])) && ($_POST['d15'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d15-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d15" id="d15-2" value="1" <?php if ((isset($_POST['d15'])) && ($_POST['d15'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d15-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d15" id="d15-3" value="2" <?php if ((isset($_POST['d15'])) && ($_POST['d15'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d15-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d15" id="d15-4" value="3" <?php if ((isset($_POST['d15'])) && ($_POST['d15'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d15-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d15" id="d15-5" value="4" <?php if ((isset($_POST['d15'])) && ($_POST['d15'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d15-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev15" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>Team members always seem to have the right information at their fingertips</td>
								<td>
                                <input class="radio_item" type="radio" name="d20" id="d20-1" value="0" <?php if ((isset($_POST['d20'])) && ($_POST['d20'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d20-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d20" id="d20-2" value="1" <?php if ((isset($_POST['d20'])) && ($_POST['d20'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d20-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d20" id="d20-3" value="2" <?php if ((isset($_POST['d20'])) && ($_POST['d20'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d20-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d20" id="d20-4" value="3" <?php if ((isset($_POST['d20'])) && ($_POST['d20'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d20-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d20" id="d20-5" value="4" <?php if ((isset($_POST['d20'])) && ($_POST['d20'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d20-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev20" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>The team generates optimism and confidence</td>
								<td>
                                <input class="radio_item" type="radio" name="d5" id="d5-1" value="0" <?php if ((isset($_POST['d5'])) && ($_POST['d5'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d5-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d5" id="d5-2" value="1" <?php if ((isset($_POST['d5'])) && ($_POST['d5'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d5-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d5" id="d5-3" value="2" <?php if ((isset($_POST['d5'])) && ($_POST['d5'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d5-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d5" id="d5-4" value="3" <?php if ((isset($_POST['d5'])) && ($_POST['d5'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d5-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d5" id="d5-5" value="4" <?php if ((isset($_POST['d5'])) && ($_POST['d5'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d5-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev5" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                               <td>People's roles and responsibilities in the team are very clear</td>
							   <td>
                                <input class="radio_item" type="radio" name="d11" id="d11-1" value="0" <?php if ((isset($_POST['d11'])) && ($_POST['d11'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d11-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d11" id="d11-2" value="1" <?php if ((isset($_POST['d11'])) && ($_POST['d11'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d11-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d11" id="d11-3" value="2" <?php if ((isset($_POST['d11'])) && ($_POST['d11'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d11-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d11" id="d11-4" value="3" <?php if ((isset($_POST['d11'])) && ($_POST['d11'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d11-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d11" id="d11-5" value="4" <?php if ((isset($_POST['d11'])) && ($_POST['d11'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d11-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev11" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>There seem to be very good relationships between team members</td>
								<td>
                                <input class="radio_item" type="radio" name="d17" id="d17-1" value="0" <?php if ((isset($_POST['d17'])) && ($_POST['d17'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d17-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d17" id="d17-2" value="1" <?php if ((isset($_POST['d17'])) && ($_POST['d17'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d17-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d17" id="d17-3" value="2" <?php if ((isset($_POST['d17'])) && ($_POST['d17'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d17-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d17" id="d17-4" value="3" <?php if ((isset($_POST['d17'])) && ($_POST['d17'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d17-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d17" id="d17-5" value="4" <?php if ((isset($_POST['d17'])) && ($_POST['d17'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d17-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev17" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>Team members take responsibility if things go wrong and try to sort out problems</td>
								<td>
                                <input class="radio_item" type="radio" name="d19" id="d19-1" value="0" <?php if ((isset($_POST['d19'])) && ($_POST['d19'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d19-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d19" id="d19-2" value="1" <?php if ((isset($_POST['d19'])) && ($_POST['d19'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d19-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d19" id="d19-3" value="2" <?php if ((isset($_POST['d19'])) && ($_POST['d19'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d19-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d19" id="d19-4" value="3" <?php if ((isset($_POST['d19'])) && ($_POST['d19'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d19-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d19" id="d19-5" value="4" <?php if ((isset($_POST['d19'])) && ($_POST['d19'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d19-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev19" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>The team leads by example - they do what they say they will do</td>
								<td>
                                <input class="radio_item" type="radio" name="d6" id="d6-1" value="0" <?php if ((isset($_POST['d6'])) && ($_POST['d6'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d6-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d6" id="d6-2" value="1" <?php if ((isset($_POST['d6'])) && ($_POST['d6'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d6-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d6" id="d6-3" value="2" <?php if ((isset($_POST['d6'])) && ($_POST['d6'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d6-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d6" id="d6-4" value="3" <?php if ((isset($_POST['d6'])) && ($_POST['d6'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d6-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d6" id="d6-5" value="4" <?php if ((isset($_POST['d6'])) && ($_POST['d6'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d6-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev6" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>The team comes up with very creative solutions</td>
								<td>
                                <input class="radio_item" type="radio" name="d22" id="d22-1" value="0" <?php if ((isset($_POST['d22'])) && ($_POST['d22'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d22-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d22" id="d22-2" value="1" <?php if ((isset($_POST['d22'])) && ($_POST['d22'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d22-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d22" id="d22-3" value="2" <?php if ((isset($_POST['d22'])) && ($_POST['d22'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d22-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d22" id="d22-4" value="3" <?php if ((isset($_POST['d22'])) && ($_POST['d22'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d22-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d22" id="d22-5" value="4" <?php if ((isset($_POST['d22'])) && ($_POST['d22'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d22-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev22" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>Communication between team members seems excellent</td>
								<td>
                                <input class="radio_item" type="radio" name="d13" id="d13-1" value="0" <?php if ((isset($_POST['d13'])) && ($_POST['d13'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d13-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d13" id="d13-2" value="1" <?php if ((isset($_POST['d13'])) && ($_POST['d13'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d13-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d13" id="d13-3" value="2" <?php if ((isset($_POST['d13'])) && ($_POST['d13'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d13-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d13" id="d13-4" value="3" <?php if ((isset($_POST['d13'])) && ($_POST['d13'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d13-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d13" id="d13-5" value="4" <?php if ((isset($_POST['d13'])) && ($_POST['d13'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d13-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev13" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>The team is good at negotiating</td>
								<td>
                                <input class="radio_item" type="radio" name="d9" id="d9-1" value="0" <?php if ((isset($_POST['d9'])) && ($_POST['d9'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d9-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d9" id="d9-2" value="1" <?php if ((isset($_POST['d9'])) && ($_POST['d9'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d9-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d9" id="d9-3" value="2" <?php if ((isset($_POST['d9'])) && ($_POST['d9'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d9-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d9" id="d9-4" value="3" <?php if ((isset($_POST['d9'])) && ($_POST['d9'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d9-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d9" id="d9-5" value="4" <?php if ((isset($_POST['d9'])) && ($_POST['d9'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d9-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev9" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>Team members build good relationships with key stakeholders</td>
								<td>
                                <input class="radio_item" type="radio" name="d7" id="d7-1" value="0" <?php if ((isset($_POST['d7'])) && ($_POST['d7'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d7-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d7" id="d7-2" value="1" <?php if ((isset($_POST['d7'])) && ($_POST['d7'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d7-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d7" id="d7-3" value="2" <?php if ((isset($_POST['d7'])) && ($_POST['d7'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d7-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d7" id="d7-4" value="3" <?php if ((isset($_POST['d7'])) && ($_POST['d7'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d7-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d7" id="d7-5" value="4" <?php if ((isset($_POST['d7'])) && ($_POST['d7'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d7-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev7" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>The team delivers great results for us</td>
								<td>
                                <input class="radio_item" type="radio" name="d25" id="d25-1" value="0" <?php if ((isset($_POST['d25'])) && ($_POST['d25'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d25-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d25" id="d25-2" value="1" <?php if ((isset($_POST['d25'])) && ($_POST['d25'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d25-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d25" id="d25-3" value="2" <?php if ((isset($_POST['d25'])) && ($_POST['d25'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d25-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d25" id="d25-4" value="3" <?php if ((isset($_POST['d25'])) && ($_POST['d25'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d25-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d25" id="d25-5" value="4" <?php if ((isset($_POST['d25'])) && ($_POST['d25'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d25-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev25" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>
                            <tr>
                                <td>Team members seem to enjoy their work</td>
								<td>
                                <input class="radio_item" type="radio" name="d26" id="d26-1" value="0" <?php if ((isset($_POST['d26'])) && ($_POST['d26'] == 0)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d26-1"><span class="icon-vpoor"></span></label>
                                <input class="radio_item" type="radio" name="d26" id="d26-2" value="1" <?php if ((isset($_POST['d26'])) && ($_POST['d26'] == 1)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d26-2"><span class="icon-poor"></span></label>
                                <input class="radio_item" type="radio" name="d26" id="d26-3" value="2" <?php if ((isset($_POST['d26'])) && ($_POST['d26'] == 2)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d26-3"><span class="icon-ok"></span></label>
                                <input class="radio_item" type="radio" name="d26" id="d26-4" value="3" <?php if ((isset($_POST['d26'])) && ($_POST['d26'] == 3)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d26-4"><span class="icon-good"></span></label>
                                <input class="radio_item" type="radio" name="d26" id="d26-5" value="4" <?php if ((isset($_POST['d26'])) && ($_POST['d26'] == 4)) echo 'checked="checked"'; ?> />
								<label class="label_item" for="d26-5"><span class="icon-vgood"></span></label>
								</td>
                                 <td style="text-align:center;"><input type="checkbox" name="dev26" value="1" /><b class="tablesaw-cell-label">Development Priority?</b></td>
                            </tr>

</table>

                        <h3>What does the team do really well?</h3>
                        <textarea name="goodcomments" style="width:90%" rows="4"></textarea>
                        <h3>What could the team usefully improve?</h3>
                        <textarea name="badcomments" style="width:90%" rows="4"></textarea>
                        <br />
                        <div><button type="submit" class="btn btn-danger">Submit your review</button></div>
                        <p><input type="hidden" name="submitted" value="TRUE" /></p>

                    </form>
                </div>
                <div class="col-sm-1"></div>
            </div>
        </div>
        
<?php 
require('includes/_footer.php');
include_once("includes/analyticstracking.php"); 
?>
