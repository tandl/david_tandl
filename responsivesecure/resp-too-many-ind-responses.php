<?php
require_once ('includes/configuration-responsivesecure.php');

// Initialize a session:
//session_start();
$page_title = 'Too many responses';
$page_metadesc = '';

$num_resp_for_report = $_SESSION['numrespondents'];
$survey_name_for_report = $_SESSION['reportsurveyname'];
?>

<?php require('includes/_header.php'); ?>

        <div class="limiter">
						<?php require('includes/_banner.php'); ?>
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">

                    <h1>You seem to have exceeded the number of reviewers allowed for this report.</h1>
                    <br />
                    <?php echo" <p><span style='font-weight:bold'>Report requested:</span>&nbsp;&nbsp;&nbsp;" . $survey_name_for_report . "</p>
                    <p><span style='font-weight:bold'>Responses to date:</span>&nbsp;&nbsp;" . $num_resp_for_report . '</p>'; ?>
                    <br />
                    <p>The type of review that was set up for you is 'Individual review - self-assessment only'. Only one review is permitted for this type of review. There seem to have been more than one review completed, so the report is no longer available I'm afraid.</p>
                    <p>You have a couple of options to choose between - both very easy to do.</p>
                    <ul>
                    <li>You can set up another 'Individual review - self-assessment only' and make sure the review is only completed once</li>
                    <li> Or you could set up an Individual 360 review. With a 360 review you can invite as many contributors as you like to contribute to your leadership profile.</li>
                    </ul>
                    <p>Just return to Your Dashboard to set up the review you prefer.</p>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>
<?php 
require('includes/_footer.php');
include_once("includes/analyticstracking.php"); 
?>
