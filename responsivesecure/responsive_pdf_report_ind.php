<?php

// Include the configuration file:
require_once ('includes/configuration-responsivesecure.php');

//require('includes/db_sessions.inc.php');
require ('fpdf/fpdf.php');
//require_once('fpdf/src/autoload.php');

$memberid = $user->uid;
if ($memberid <= 0) {
  echo "Access denied: not logged in";
  exit();
}

$reportrequested = $_GET["indsurveyid"];

require_once (MYSQLI);
if (!$dbc) {
  die('Could not connect you up to the database: ');
}

/*  Testing pick up of registering person using Andrew Cooper's reports
 *   Comment out block below if we are using this test
  $memberid = 175;
  $reportrequested = 590412;
  $reportrequested = 283716;
  $result1 = $dbc->prepare("SELECT indsurveyname,surveytype FROM indsurveys WHERE indsurveyid=? AND memberid=?");
  $result1->bind_param("ii", $reportrequested, $memberid);
 *
 */

// Beginning of block to be commented out if we are testing pick up of registered person
// if we are admin, allow viewing any report
if ($memberid == 1) {
  $result1 = $dbc->prepare("SELECT indsurveyname,surveytype FROM indsurveys WHERE indsurveyid=?");
  $result1->bind_param("i", $reportrequested);
}
else {
  // limit report to match current member id
  $result1 = $dbc->prepare("SELECT indsurveyname,surveytype FROM indsurveys WHERE indsurveyid=? AND memberid=?");
  $result1->bind_param("ii", $reportrequested, $memberid);
}
// End of block to be commented out if we are testing pick up of registered person

$result1->execute();
//number of rows returned from query
$result1->store_result();
$rows = $result1->num_rows;

//populates teamsurveyname and surveytype variables
$result1->bind_result($indsurveyname, $surveytype);
$result1->fetch();

if ($rows != 1) {
  echo "Access denied: no rows returned for survey and member";
  exit();
}

$_SESSION['reportsurveyname'] = $indsurveyname;
// Check if 3 people have responded to multi-reviewer questionnaire, or 1 person to single-reviewer questionnaire

$result = mysqli_query($dbc, "SELECT indsurveyid, COUNT(indrespno) FROM inddata WHERE indsurveyid = '$reportrequested' && (indrole = '1') ");
$row = mysqli_fetch_array($result, MYSQLI_BOTH);
$numindrespondents = $row['COUNT(indrespno)'];

$result2 = mysqli_query($dbc, "SELECT indsurveyid, COUNT(indrespno) FROM inddata WHERE indsurveyid = '$reportrequested'  && (indrole = '2' || indrole = '3') ");
$row2 = mysqli_fetch_array($result2, MYSQLI_BOTH);
$numstakerespondents = $row2['COUNT(indrespno)'];

$numrespondents = $numindrespondents + $numstakerespondents;

$_SESSION['numindrespondents'] = $numindrespondents;
$_SESSION['numstakerespondents'] = $numstakerespondents;
$_SESSION['numrespondents'] = $numrespondents;

#$base_url = 'https://teamsandleadership.net';

if ((($numrespondents < 3) && ($surveytype != 9)) || (($numrespondents === 0) && ($surveytype === 9))) {
  $url = '/responsivesecure/resp-too-few-responses.php'; // Define the URL.
  header("Location: $url");
}
elseif (($numrespondents > 1) && ($surveytype === 9)) {
  $url = '/responsivesecure/resp-too-many-ind-responses.php'; // Define the URL.
  header("Location: $url");
}
else {

  include ('functions/extract_ind.php');

  //Finding commissioner's  (ie member's) Drupal sub-domain prefix from their email address to enable report links to take them to their branded website rather than the default teamsandleadership.net
  /* create a prepared statement */
  if ($stmt = $dbc->prepare("SELECT memberemail FROM members WHERE memberid=?")) {

    /* bind parameters for markers */
    $stmt->bind_param("i", $memberid);

    /* execute query */
    $stmt->execute();

    /* bind result variables */
    $stmt->bind_result($client_email);

    /* fetch value and extract email domain */
    $stmt->fetch();

    // Initialising CLIENT VARIABLES to default
    $subdomainprefix = 'default';
    $client_logo = 'Jigsaw table.png';
    $client_logo_width = 25;
    $client_logo_height = 25;
    $client_email_domain = 'change-fx.com';

    // Generating client variable for report customisation
    $client_email_domain = substr(strrchr($client_email, "@"), 1);

    //Testing
    // client_email_domain = 'redcross.org.uk';

    for ($i = 0; $i < count($client_details); $i++) {
      if ($client_details[$i][1] == $client_email_domain) {
        $subdomainprefix = $client_details[$i][2];
        $client_logo = $client_details[$i][3];
        $client_logo_width = ($client_details[$i][4] * 25 / $client_details[$i][5]);
      }
    }

    // Reducing logo size if too big at 25 high, to balance T&L logo
    if ($client_logo_width > 40) {
      $new_client_logo_width = 40;
      $new_client_logo_height = (40 / $client_logo_width) * 25;
      $client_logo_height = $new_client_logo_height;
      $client_logo_width = $new_client_logo_width;
    }

    // Setting customised client link, retruning always to their sub-domain
    if ($subdomainprefix === 'default') {
      $customised_link = 'https://';
    }
    else {
      $customised_link = 'https://' . $subdomainprefix . '.';
    }

//echo '<br/>Client\'s subdomain prefix after the loop is: '.$subdomainprefix;

    /* close statement */
    $stmt->close();
  }
  else {
    echo "ERROR: Could not prepare query:";
  }

//End of setting  commissioner's  (ie member's) Drupal sub-domain prefix

  class PDF extends FPDF {
    /* function Header() {
      $this-> SetLineWidth(0);
      $this-> Line( 20, 12, 184, 12);
      } */

    function Footer() {
      $this->SetX(-1); // negative arg means relative to right of page
      //  $page_width = $this->GetX() + 1;
      $this->SetY(-1); // negative arg means relative to bottom of page
      $page_height = $this->GetY() + 1;
      $this->SetLineWidth(.1);
      $this->SetDrawColor(0, 0, 0);
      $this->Line(20, $page_height - 12, 190, $page_height - 12);
      $this->SetFont('Helvetica', 'I', 8);
      $this->Text(96, $page_height - 6, 'Page ' .
          $this->PageNo() . ' of {nb}');
      $this->Text(20, $page_height - 6, 'Report generated: ' . date('d.m.Y'));
      $this->Text(127, $page_height - 6, chr(169) . '  2018 david.mathew@teamsandleadership.com');
    }

    function Circle($x, $y, $r, $style = 'D') {
      $this->Ellipse($x, $y, $r, $r, $style);
    }

    function Ellipse($x, $y, $rx, $ry, $style = 'D') {
      if ($style == 'F')
        $op = 'f';
      elseif ($style == 'FD' || $style == 'DF')
        $op = 'B';
      else
        $op = 'S';
      $lx = 4 / 3 * (M_SQRT2 - 1) * $rx;
      $ly = 4 / 3 * (M_SQRT2 - 1) * $ry;
      $k = $this->k;
      $h = $this->h;
      $this->_out(sprintf('%.2F %.2F m %.2F %.2F %.2F %.2F %.2F %.2F c', ($x + $rx) * $k, ($h - $y) * $k, ($x + $rx) * $k, ($h - ($y - $ly)) * $k, ($x + $lx) * $k, ($h - ($y - $ry)) * $k, $x * $k, ($h - ($y - $ry)) * $k));
      $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c', ($x - $lx) * $k, ($h - ($y - $ry)) * $k, ($x - $rx) * $k, ($h - ($y - $ly)) * $k, ($x - $rx) * $k, ($h - $y) * $k));
      $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c', ($x - $rx) * $k, ($h - ($y + $ly)) * $k, ($x - $lx) * $k, ($h - ($y + $ry)) * $k, $x * $k, ($h - ($y + $ry)) * $k));
      $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c %s', ($x + $lx) * $k, ($h - ($y + $ry)) * $k, ($x + $rx) * $k, ($h - ($y + $ly)) * $k, ($x + $rx) * $k, ($h - $y) * $k, $op));
    }

  }

  $pdf = new PDF();
  $pdf->SetTitle("Teams and Leadership Review");
  $pdf->AliasNbPages();

  $pdf->SetFont('Helvetica', '', 9);
  $pdf->SetLeftMargin(20);
  $pdf->SetRightMargin(20);
  $pdf->SetTopMargin(15);

  /* FRONT PAGE ---------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();

// Front page header
// Add client logo
  $client_logo_top = 18 - (0.5 * $client_logo_height);
  $pdf->Image("images/v3/" . $client_logo, 20, $client_logo_top, $client_logo_width, $client_logo_height);

  // Add T&L logo
  $pdf->Image("images/v3/T&L Logo - Navy.png", 145, 6, 44, 25);

  // Draw Report Title box and Overall score highlight circle
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(224, 230, 255);
  $pdf->Line(33, 50, 180, 50);
  $pdf->SetFillColor(255, 249, 82);
  $pdf->Circle(174, 49, 10, 'F');

  // Formatting for different length Team Name so as not to upset page formatting
  // Test names
  // $indsurveyname = '14 characters ';
  // $indsurveyname = '84 characters 84 characters 84 characters 84 characters 84 characters 84 characters ';

  $indsurveyname_strlen = strlen($indsurveyname);
  $pdf->SetRightMargin(50);
  if (strlen($indsurveyname) > 80) {
    $indsurveyname = substr($indsurveyname, 0, 77) . '...';
  }
  if ($indsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 39);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $indsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 39);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $indsurveyname . "\n");
  }
  // End of Formatting for different length Team Name so as not to upset page formatting
  // Write this report details: Overall score and responses to date
  $pdf->SetRightMargin(20);
  $pdf->SetFont('Helvetica', '', 24);
  $pdf->SetXY(165.5, 47);
  $pdf->Write(5, $iandsgrandavefm);
  $pdf->SetFont('Helvetica', '', 20);
  $pdf->Write(5, "%");
  $pdf->SetXY(20, 56);
  $pdf->SetFont('Helvetica', 'I', 9);
  // Next line checks the sub-domain data has got through OK
  //  $pdf->Write(5, 'Client email: ' . $client_email . '  Member ID: ' . $memberid . '  Subdomain prefix: ' . $subdomainprefix . '  Email domain: ' . $client_email_domain . ' Client logo: ' .$client_logo .' ');
  $pdf->Write(5, 'Responses to date: ' . $numindrespondents . ' self-assessment and ' . $numstakerespondents . ' contributor responses' . "\n" . "\n" . "\n");

  //   $pdf->SetXY(160, 50);
  // End of Front page header

  $pdf->SetLineWidth(65);
  $pdf->SetDrawColor(240, 240, 240);
  $pdf->Line(51, 103, 163, 103);
  $pdf->Image("images/v3/analytics-1.png", 24, 88, 30, 30);
  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, "\n" . 'Executive summary' . "\n" . "\n");
  $pdf->SetLeftMargin(60);

  include ('functions/pdf_execsummary_ind.php');

  $pdf->SetLeftMargin(20);
  $pdf->SetRightMargin(20);
  $pdf->SetFont('Helvetica', '', 16);
  $pdf->SetXY(38, 133);
  $pdf->Write(5, "\n" . "\n" . 'What\'s in this report' . "\n" . "\n");
  $pdf->Image("images/v3/Folders light blue.png", 24, 150, 30, 30);
  $pdf->SetLeftMargin(60);
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, '* A detailed personal profile, benchmarking you against our database.' . "\n");
  $pdf->Write(5, '* Your signature strengths and challenges.' . "\n");
  $pdf->Write(5, '* A list of potential development needs pinpointed by you and your contributors.' . "\n");
  $pdf->Write(5, '* Links to our recommended exercises based on your report.' . "\n");
  $pdf->Write(5, '* Comments from people who completed the questionnaire.' . "\n" . "\n");

  $pdf->SetLineWidth(70);
  $pdf->SetDrawColor(240, 240, 240);
  $pdf->Line(55, 240, 68, 240);
  $pdf->SetLineWidth(70);
  $pdf->SetDrawColor(240, 240, 240);
  $pdf->Line(142, 240, 155, 240);
  $pdf->SetLeftMargin(20);
  $pdf->SetRightMargin(20);
  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, "\n" . "\n" . 'Resources to use alongside this report' . "\n" . "\n");
  $pdf->SetFont('Helvetica', '', 11);

  $pdf->Image("images/v3/Asset 2 copy 2 (1).png", 52, 212, 20, 20);
  $pdf->SetXY(37, 236);

  $pdf->SetFont('Helvetica', 'B', 14);
  $pdf->Write(5, "Individual 360 FAQs" . "\n" . "\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->SetLeftMargin(33);
  $pdf->SetRightMargin(118);
  $Title = 'A concise guide to the individual 360: why individual 360s are great to do, and how the process works.';
  $pdf->Write(5, $Title . "\n" . "\n");
  $pdf->SetX(46);
  $pdf->SetFont('', 'U');
  $link = $pdf->AddLink();
  $pdf->Write(5, 'Individual 360 Guide', $customised_link . 'teamsandleadership.net/system/files/related-files/Individual 360 Guide v3.pdf');
  //$pdf->SetXY(48, 242);
//  $pdf->Cell(0,10,$Title,0,0,'C');

  $pdf->Image("images/v3/Expert Guidance.png", 137, 212, 20, 20);
  $pdf->SetXY(127, 236);
  $pdf->SetLeftMargin(20);
  $pdf->SetRightMargin(20);
  $pdf->SetFont('Helvetica', 'B', 14);
  $pdf->Write(5, "Expert Guidance" . "\n" . "\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->SetLeftMargin(120);
  $pdf->SetRightMargin(33);
  $Title = 'Comprehensive briefings on the skills you need to transform your leadership skills and your team\'s performance.';
  $pdf->Write(5, $Title . "\n" . "\n");
  $pdf->SetX(134);
  $pdf->SetFont('', 'U');
  $link = $pdf->AddLink();

  $pdf->Write(5, 'Expert Guidance', $customised_link . 'teamsandleadership.net/expert-guidance');

  $pdf->SetLeftMargin(20);
  $pdf->SetRightMargin(20);

  /* PAGE 2 ------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();
// Add client logo
  $client_logo_top = 18 - (0.5 * $client_logo_height);
  $pdf->Image("images/v3/" . $client_logo, 20, $client_logo_top, $client_logo_width, $client_logo_height);

  // Add T&L logo
  $pdf->Image("images/v3/T&L Logo - Navy.png", 145, 6, 44, 25);

  // Page 2 header
  // Draw Report Title box and Overall score highlight circle
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(224, 230, 255);
  $pdf->Line(33, 50, 180, 50);
  $pdf->SetFillColor(255, 249, 82);
  $pdf->Circle(174, 49, 10, 'F');

  // Adapting Front page header for this page
  $pdf->SetRightMargin(50);
  if ($indsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 39);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $indsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 39);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $indsurveyname . "\n");
  }
  // End of Formatting for different length Team Name so as not to upset page formatting
  // Write this report details: Overall score and responses to date
  $pdf->SetRightMargin(20);
  $pdf->SetFont('Helvetica', '', 24);
  $pdf->SetXY(165.5, 47);
  $pdf->Write(5, $iandsgrandavefm);
  $pdf->SetFont('Helvetica', '', 20);
  $pdf->Write(5, "%");
  $pdf->SetXY(20, 56);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numindrespondents . ' self-assessment and ' . $numstakerespondents . ' contributor responses' . "\n" . "\n" . "\n");

// End of Page 2 header


  $pdf->SetLeftMargin(20);
  $pdf->SetRightMargin(25);
  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, "\n" . 'How to use this report' . "\n" . "\n" );
  $pdf->Image("images/v3/Step 1.png", 24, 89, 20, 20);
  $pdf->SetLeftMargin(55);
  $pdf->SetXY(55,89);
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Step 1: Review your 360 report.' . "\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, 'What themes have been brought out? What do your contributors think are the most important things for you to work on? What do they see as your main strengths? Do these tally with your own view?' . "\n" . "\n" . "\n");
  $pdf->Image("images/v3/Step 2.png", 24, 119, 20, 20);
  $pdf->SetLeftMargin(55);
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Step 2: Think about what you are going to do with the feedback in your 360.' . "\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, 'You might have set up the 360 for a specific reason, or part of a process. If not, would it help to have someone to talk it through with? Maybe a coach or your supervisor? Or a close friend or colleague to act as a sounding board? The key is to find a person who will have your interests at heart, will listen well, and help you reflect on your best next steps.' . "\n" . "\n");
  $pdf->Image("images/v3/Step 3.png", 24, 149, 20, 20);
  $pdf->SetLeftMargin(55);
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Step 3: Think about your development priorities.' . "\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, 'Look for aspects of your review that you feel show potential for you. These may well be things you are already good at but want to take to a higher level. Low scores only need addressing if they are critical to the role that you play now or want to play in the future. Focus on developing a style and skills that feel true to you.' . "\n" . "\n");
  $pdf->Image("images/v3/Step 4.png", 24, 179, 20, 20);
  $pdf->SetLeftMargin(55);
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Step 4: Click the links to the recommended exercise(s) in your 360 report,' . "\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, 'or use the ');
  $pdf->SetFont('', 'U');
  $link = $pdf->AddLink();
  $pdf->Write(5, 'Expert Guidance', $customised_link . 'teamsandleadership.net/expert-guidance');
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, ' section of the website to find out more about your chosen areas of potential development.' . "\n" . "\n" . "\n"."\n");
  $pdf->Image("images/v3/Step 5.png", 24, 209, 20, 20);
  $pdf->SetLeftMargin(55);
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Step 5: Work on the recommended exercise, on your own or alongside your coach or colleague.' . "\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, 'See where the exercise takes you. If you can, adopt an open and reflective state of mind to give yourself the maximum chance of learning from it.' . "\n" . "\n" . "\n");
  $pdf->Image("images/v3/Step 6.png", 24, 239, 20, 20);
  $pdf->SetLeftMargin(55);
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Step 6: Decide on your next actions.' . "\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, 'Come up with a plan of how you can improve. Write down some targets and deadlines and, most important, set yourslf some actions ');
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'for the next seven days ');
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, 'that will give you some momentum and move your plan forward.'."\n"."\n");
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5,'Good luck!'."\n");
      $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5,'PS: you might also think about sharing your 360 feedback with your team.');
  $pdf->SetLeftMargin(20);
  $pdf->SetRightMargin(20);


  include ('functions/pdf_headlines_chart_ind.php');

  /* PAGE 3 ----------------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();

  // Front page header adapted for this page
  // Draw Report Title box and Overall score highlight circle
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(224, 230, 255);
  $pdf->Line(33, 20, 180, 20);
  $pdf->SetFillColor(255, 249, 82);
  $pdf->Circle(174, 20, 10, 'F');

  // Print Individual Report details
  $pdf->SetRightMargin(50);
  if ($indsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $indsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $indsurveyname . "\n");
  }
  // End of Page 3 header
  // Write this report details: Overall score and responses to date
  $pdf->SetRightMargin(20);
  $pdf->SetFont('Helvetica', '', 24);
  $pdf->SetXY(165.5, 18);
  $pdf->Write(5, $iandsgrandavefm);
  $pdf->SetFont('Helvetica', '', 20);
  $pdf->Write(5, "%");
  $pdf->SetXY(20, 27);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numindrespondents . ' self-assessment and ' . $numstakerespondents . ' contributor responses' . "\n" . "\n" . "\n");

  $pdf->SetFont('Helvetica', '', 16);
  $pdf->setXY(20, 40);
  $pdf->Write(5, 'Your Detailed Personal Profile' . "\n" . "\n");

  include ('functions/pdf_full_chart_ind.php');
  /* Original page -add back in to check calculations are right
    $pdf->AddPage();
    $pdf->SetFont('Helvetica', '', 14);
    $pdf->Write(10, $teamsurveyname . "\n");
    $pdf->Write(10, 'Your Leadership Quartet Team Review ORIGINAL' . "\n" . "\n");
    $pdf->SetlineWidth(0);
    $pdf->SetDrawColor(35, 134, 243);
    $pdf->Line(20, 40, 182, 40);
    $pdf->SetFont('Helvetica', '', 12);
    $pdf->setXY(20, 45);
    $pdf->Write(5, 'Your Detailed Team Profile - how do you perform?' . "\n" . "\n");

    include ('functions/ok_pdf_full_chart_team.php');
   */

  /* PAGE 4 --------------------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();
// Front page header adapted for this page
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(255, 255, 255);
  $pdf->Line(33, 20, 180, 20);
  // Print Team Report details
  $pdf->SetRightMargin(50);
  if ($indsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $indsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 13);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $indsurveyname . "\n");
  }
  $pdf->SetRightMargin(20);
  $pdf->SetXY(20, 27);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numindrespondents . ' self-assessment and ' . $numstakerespondents . ' contributor responses' . "\n" . "\n" . "\n");
  $pdf->SetLineWidth(.1);
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->Line(20, 35, 184, 35);

  // End of Page 4 header

  include ('functions/pdf_sandw_ind.php');

  /* PAGE 5 -------------------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();
// Front page header adapted for this page
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(255, 255, 255);
  $pdf->Line(33, 20, 180, 20);
  // Print Team Report details
  $pdf->SetRightMargin(50);
  if ($indsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $indsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 13);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $indsurveyname . "\n");
  }
  $pdf->SetRightMargin(20);
  $pdf->SetXY(20, 27);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numindrespondents . ' self-assessment and ' . $numstakerespondents . ' contributor responses' . "\n" . "\n" . "\n");
  $pdf->SetLineWidth(.1);
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->Line(20, 35, 184, 35);

  // End of Page 5 header

  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, 'Suggestions for Development' . "\n" . "\n" . "\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, 'The questionnaire asked each respondent to pick up to three areas for the team to prioritise for development.' . "\n" . "\n");

  include ('functions/pdf_voterank_ind.php');

  /* PAGE 6 --------------------------------------------------------------------------------------------------------------------------------- */

  $pdf->AddPage();
// Front page header adapted for this page
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(255, 255, 255);
  $pdf->Line(33, 20, 180, 20);
  // Print Team Report details
  $pdf->SetRightMargin(50);
  if ($indsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $indsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 13);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $indsurveyname . "\n");
  }
  $pdf->SetRightMargin(20);
  $pdf->SetXY(20, 27);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numindrespondents . ' self-assessment and ' . $numstakerespondents . ' contributor responses' . "\n" . "\n" . "\n");
  $pdf->SetLineWidth(.1);
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->Line(20, 35, 184, 35);

  // End of Page 6 header

  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, 'Comments: What you say you do well' . "\n" . "\n");

  $result = mysqli_query($dbc, "SELECT  goodcomments FROM inddata  WHERE indsurveyid = '$reportrequested' && (indrole = '1') ");
// Print out comments
  $pdf->SetXY(20, 65);
  $pdf->SetFont('Helvetica', '', 9);
  while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
    $goodc = $row['goodcomments'];
    if ($goodc != '') {    // stripping out nil comments
      $pdf->Write(5, '"' . $goodc . '"' . "\n" . "\n");
    }
  }

  /* PAGE 7 -------------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();
// Front page header adapted for this page
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(255, 255, 255);
  $pdf->Line(33, 20, 180, 20);
  // Print Team Report details
  $pdf->SetRightMargin(50);
  if ($indsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $indsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 13);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $indsurveyname . "\n");
  }
  $pdf->SetRightMargin(20);
  $pdf->SetXY(20, 27);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numindrespondents . ' self-assessment and ' . $numstakerespondents . ' contributor responses' . "\n" . "\n" . "\n");
  $pdf->SetLineWidth(.1);
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->Line(20, 35, 184, 35);

  // End of Page 7 header

  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, 'Comments: What you say you could improve ' . "\n" . "\n");

  $result1 = mysqli_query($dbc, "SELECT  badcomments FROM inddata  WHERE indsurveyid = '$reportrequested' && (indrole = '1') ");
// Print out comments
  $pdf->SetXY(20, 65);
  $pdf->SetFont('Helvetica', '', 9);
  while ($row = mysqli_fetch_array($result1, MYSQLI_BOTH)) {
    $badc = $row['badcomments'];
    if ($badc != '') {  // stripping out nil comments
      $pdf->Write(5, '"' . $badc . '"' . "\n" . "\n");
    }
  }

  /* PAGE 8 -------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();
// Front page header adapted for this page
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(255, 255, 255);
  $pdf->Line(33, 20, 180, 20);
  // Print Team Report details
  $pdf->SetRightMargin(50);
  if ($indsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $indsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 13);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $indsurveyname . "\n");
  }
  $pdf->SetRightMargin(20);
  $pdf->SetXY(20, 27);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numindrespondents . ' self-assessment and ' . $numstakerespondents . ' contributor responses' . "\n" . "\n" . "\n");
  $pdf->SetLineWidth(.1);
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->Line(20, 35, 184, 35);

  // End of Front page header adapted for this page

  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, 'What your 360 contributors say you do well:' . "\n" . "\n");

  $result2 = mysqli_query($dbc, "SELECT  goodcomments FROM inddata  WHERE indsurveyid = '$reportrequested'  && (indrole = '2' || indrole = '3')");
// Print out comments
  $pdf->SetXY(20, 65);
  $pdf->SetFont('Helvetica', '', 9);
  while ($row = mysqli_fetch_array($result2, MYSQLI_BOTH)) {
    $goodc = $row['goodcomments'];
    if ($goodc != '') {    // stripping out nil comments
      $pdf->Write(5, '"' . $goodc . '"' . "\n" . "\n");
    }
  }

  /* PAGE 9 -------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();
// Front page header adapted for this page
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(255, 255, 255);
  $pdf->Line(33, 20, 180, 20);
  // Print Team Report details
  $pdf->SetRightMargin(50);
  if ($indsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $indsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 13);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Individual 360: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $indsurveyname . "\n");
  }
  $pdf->SetRightMargin(20);
  $pdf->SetXY(20, 27);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numindrespondents . ' self-assessment and ' . $numstakerespondents . ' contributor responses' . "\n" . "\n" . "\n");
  $pdf->SetLineWidth(.1);
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->Line(20, 35, 184, 35);

  // End of Front page header adapted for this page

  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, 'What your 360 contributors say you could improve:' . "\n" . "\n");
  $result3 = mysqli_query($dbc, "SELECT  badcomments FROM inddata  WHERE indsurveyid = '$reportrequested'  && (indrole = '2' || indrole = '3')");
// Print out comments
  $pdf->SetXY(20, 65);
  $pdf->SetFont('Helvetica', '', 9);
  while ($row = mysqli_fetch_array($result3, MYSQLI_BOTH)) {
    $badc = $row['badcomments'];
    if ($badc != '') {  // stripping out nil comments
      $pdf->Write(5, '"' . $badc . '"' . "\n" . "\n");
    }
  }

  $reportname = $indsurveyname . ' 360 .pdf';
  $pdf->Output($reportname, 'I');
  include("includes/analyticstracking.php");
  include("includes/downloadstracking.php");
}

mysqli_close($dbc);

// Include the HTML footer file:
// Write and close the session:
//session_write_close();
//include ('includes/footercfx.php');
?>
