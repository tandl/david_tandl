$( document ).ready(function() {
    $(".form-questions").submit( function(e) {	
	
			//check the number of checked radio buttons equal the number of rows in the table			
   	 	if($('.form-questions input:radio:checked').length < 1) {
    		alert('Please ensure you have answered at least 1 question.');
    		e.preventDefault();
    		return false;
    	} 
    	
    	//check the three checkboxes have been checked
    	if($('.form-questions input:checkbox:checked').length <1) {
    		alert('Please tick at least 1 area which you feel needs work');
    		e.preventDefault();
    		return false;
    	}
    	    	
    });
});
