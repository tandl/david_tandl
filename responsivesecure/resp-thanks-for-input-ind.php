<?php
// Include the configuration file:
require_once ('includes/configuration-responsivesecure.php');

// Set the page title and include the HTML header:
$page_title = 'Teams and Leadership';
$page_metadesc = 'Theory, tips and free Development Exercises on Inspiring Leadership and Great Teamwork?';

include ('functions/sanitize_ind.php');
?>

<?php require('includes/_header.php'); ?>

        <div class="limiter">
						<?php require('includes/_banner.php'); ?>
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <?php
                    if (($d1 === "99") && ($d2 === "99") && ($d3 === "99") && ($d4 === "99") && ($d5 === "99") && ($d6 === "99") && ($d7 === "99") && ($d8 === "99") && ($d9 === "99") && ($d10 === "99") && ($d11 === "99") && ($d12 === "99") && ($d13 === "99") && ($d14 === "99") && ($d15 === "99") && ($d16 === "99") && ($d17 === "99") && ($d18 === "99") && ($d19 === "99") && ($d20 === "99") && ($d21 === "99") && ($d22 === "99") && ($d23 === "99") && ($d24 === "99") && ($d25 === "99") && ($d26 === "99") && (($dev1 === "0") && ($dev2 === "0") && ($dev3 === "0") && ($dev4 === "0") && ($dev5 === "0") && ($dev6 === "0") && ($dev7 === "0") && ($dev8 === "0") && ($dev9 === "0") && ($dev10 === "0") && ($dev11 === "0") && ($dev12 === "0") && ($dev13 === "0") && ($dev14 === "0") && ($dev15 === "0") && ($dev16 === "0") && ($dev17 === "0") && ($dev18 === "0") && ($dev19 === "0") && ($dev20 === "0") && ($dev21 === "0") && ($dev22 === "0") && ($dev23 === "0") && ($dev24 === "0") && ($dev25 === "0") && ($dev26 === "0")) && empty($gc) && empty($bc)) { // the form is empty
                      echo'
     <h1>Oops! Our computer seems to think you didn\'t complete the form.</h1>
     <br />
     <div>
     <p>Please try again, or <a href="mailto:support@teamsandleadership.net"> contact us </a> and we\'ll sort it out for you.</p>
     </div>
     ';
                    }
                    else { // the form isn't empty
                      require_once (MYSQLI);
                      if (!$dbc) {
                        die('Could not connect you up to the database: ');
                      }

// Add questionnaire responses to the inddata database:
                      if (!($stmt = $dbc->prepare("INSERT INTO inddata(indsurveyid, indrole, d1, d2, d3, d4, d5, d6, d7,d8, d9, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, dev1, dev2, dev3, dev4, dev5,dev6, dev7, dev8, dev9, dev10, dev11, dev12, dev13, dev14, dev15, dev16, dev17, dev18, dev19, dev20, dev21, dev22, dev23, dev24, dev25, dev26, goodcomments, badcomments) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"))) {
                        die("prepare() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
                      }

                      $val1 = $sid;
                      $val2 = $tr;
                      $val3 = $d1;
                      $val4 = $d2;
                      $val5 = $d3;
                      $val6 = $d4;
                      $val7 = $d5;
                      $val8 = $d6;
                      $val9 = $d7;
                      $val10 = $d8;
                      $val11 = $d9;
                      $val12 = $d10;
                      $val13 = $d11;
                      $val14 = $d12;
                      $val15 = $d13;
                      $val16 = $d14;
                      $val17 = $d15;
                      $val18 = $d16;
                      $val19 = $d17;
                      $val20 = $d18;
                      $val21 = $d19;
                      $val22 = $d20;
                      $val23 = $d21;
                      $val24 = $d22;
                      $val25 = $d23;
                      $val26 = $d24;
                      $val27 = $d25;
                      $val28 = $d26;
                      $val29 = $dev1;
                      $val30 = $dev2;
                      $val31 = $dev3;
                      $val32 = $dev4;
                      $val33 = $dev5;
                      $val34 = $dev6;
                      $val35 = $dev7;
                      $val36 = $dev8;
                      $val37 = $dev9;
                      $val38 = $dev10;
                      $val39 = $dev11;
                      $val40 = $dev12;
                      $val41 = $dev13;
                      $val42 = $dev14;
                      $val43 = $dev15;
                      $val44 = $dev16;
                      $val45 = $dev17;
                      $val46 = $dev18;
                      $val47 = $dev19;
                      $val48 = $dev20;
                      $val49 = $dev21;
                      $val50 = $dev22;
                      $val51 = $dev23;
                      $val52 = $dev24;
                      $val53 = $dev25;
                      $val54 = $dev26;
                      $val55 = $gc;
                      $val56 = $bc;


                      if (!$stmt->bind_param('iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiss', $val1, $val2, $val3, $val4, $val5, $val6, $val7, $val8, $val9, $val10, $val11, $val12, $val13, $val14, $val15, $val16, $val17, $val18, $val19, $val20, $val21, $val22, $val23, $val24, $val25, $val26, $val27, $val28, $val29, $val30, $val31, $val32, $val33, $val34, $val35, $val36, $val37, $val38, $val39, $val40, $val41, $val42, $val43, $val44, $val45, $val46, $val47, $val48, $val49, $val50, $val51, $val52, $val53, $val54, $val55, $val56)) {
                        die("bind_param() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
                      }

                      if (!$stmt->execute()) {
                        die("execute() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
                      }

                      $affectedrows = $stmt->affected_rows;
                      $stmt->close();

                      if ($affectedrows === 1) { // One row inserted
                        echo'
                        <h1>Thank you for completing the confidential review.</h1>
                        <div><p>Your anonymised data has successfully been processed into our database. For further confidentiality, the person who commissioned the review will receive a copy of their report only when at least three people have responded (except for reviews set up for a single reviewer).</p></div>
                        <p><a href="/review/dashboard" class="button">Go to review dashboard</a></p>
                        ';
                      }
                      else { // If it did not run OK.
                        echo'
                        <h1>Oops! Apologies - for some reason we could not accept your data.</h1>
                        <div><p>This is very unusual and we are looking into it. Please try again in 24 hours. Sorry for the inconvenience.</p></div>
                        ';
                      }
                    }
                    ?>

                </div>
                <div class="col-sm-2"></div>

            </div>
        </div>
        
<?php 
require('includes/_footer.php');
include_once("includes/analyticstracking.php"); 
?>

