<?php
require_once ('includes/configuration-responsivesecure.php');

// Initialize a session:
//session_start();
$page_title = 'Too few responses';
$page_metadesc = '';

$num_resp_for_report = $_SESSION['numrespondents'];
$survey_name_for_report = $_SESSION['reportsurveyname'];
?>

<?php require('includes/_header.php'); ?>

        <div class="limiter">
						<?php require('includes/_banner.php'); ?>
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <h1>So far there have been too few responses to give you a report.</h1>
                    <br />
                    <?php echo" <p class='alert alert-danger'><span style='font-weight:bold'>Report requested:</span>&nbsp;&nbsp;&nbsp;" . $survey_name_for_report . "</p>
                    <p class='alert alert-danger'><span style='font-weight:bold'>Responses to date:</span>&nbsp;&nbsp;" . $num_resp_for_report . '</p>'; ?>
                    <br />
                    <p>You might like to send out a reminder to people you asked to complete the review.</p>
                    <p>For reviews with multiple reviewers, reports are not available until at least three people have submitted an online questionnaire. This is to retain the anonymity of people who have responded.</p>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>
        
<?php 
require('includes/_footer.php');
include_once("includes/analyticstracking.php"); 
?>
