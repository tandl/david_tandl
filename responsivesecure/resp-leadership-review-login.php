<?php
// Include the configuration file:

require_once ('includes/configuration-responsivesecure.php');

//session_start();
//require('includes/db_sessions.inc.php');
$page_title = 'Leadership review login';
$page_metadesc = '';
$loginnotok = 99;
$loginnotentered = 99;

if (isset($_POST['submitted'])) {
  require_once (MYSQLI);

  // Trim all the incoming data:
  $trimmed = array_map('trim', $_POST);


  $sid = intval($trimmed['indsurveyid']);
  $p = $trimmed['pswd'];

  // Check for a survey ID and password:

  if ((!empty($sid)) && (!empty($p))) { // if survey ID and password present, authenticate and redirect to appropriate questionnaire depending on password
    //Check if this is a person checking in to complete an individual self-assessment by checking the password used and if so re-direct to the individual self-assessment questionnaire and exit
    if (!($stmt = $dbc->prepare("SELECT indsurveyid, url_hash, indpassword, bosspassword, contribpassword FROM indsurveys WHERE (indsurveyid=? AND indpassword = ?)"))) {
      die("prepare() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    $val1 = $sid;
    $val2 = $p;

    if (!$stmt->bind_param('is', $val1, $val2)) {
      die("bind_param() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    if (!$stmt->execute()) {
      die("execute() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    if (!($stmt->store_result())) {
      die("store_result() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    $numrows = $stmt->num_rows;
    $stmt->bind_result($indsurveyid, $url_hash, $indpassword, $bosspassword, $contribpassword);
    $stmt->fetch();
    $stmt->free_result();
    $stmt->close();
		
    if ($numrows === 1) {// A match was made
    		
      $url = 'resp-quest-ind-self.php?a=' . $url_hash . '&b='. $indsurveyid . '&c=i'; // Define the URL:
      header("Location: $url");
    }

    //Check if this is the boss checking in to complete a 360 survey by checking the password used and if so re-direct to the contributor questionnaire and exit
    if (!($stmt = $dbc->prepare("SELECT indsurveyid, url_hash, indpassword, bosspassword, contribpassword  FROM indsurveys WHERE (indsurveyid=? AND bosspassword = ?)"))) {
      die("prepare() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    $val1 = $sid;
    $val2 = $p;

    if (!$stmt->bind_param('is', $val1, $val2)) {
      die("bind_param() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    if (!$stmt->execute()) {
      die("execute() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    if (!($stmt->store_result())) {
      die("store_result() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    $numrows = $stmt->num_rows;
    $stmt->bind_result($indsurveyid, $url_hash, $indpassword, $bosspassword, $contribpassword);
    $stmt->fetch();
    $stmt->free_result();
    $stmt->close();

    if ($numrows === 1) {// A match was made
      $url = 'resp-quest-ind-contrib.php?a=' . $url_hash . '&b='. $indsurveyid . '&c=i';
      header("Location: $url");
    }


    //Check if this is a stakeholder checking in to complete the survey and if so re-direct to the stakeholder questionnaire and exit
    if (!($stmt = $dbc->prepare("SELECT indsurveyid, url_hash, indpassword, bosspassword, contribpassword FROM indsurveys WHERE (indsurveyid=? AND contribpassword = ?)"))) {
      die("prepare() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    $val1 = $sid;
    $val2 = $p;

    if (!$stmt->bind_param('is', $val1, $val2)) {
      die("bind_param() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    if (!$stmt->execute()) {
      die("execute() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    if (!($stmt->store_result())) {
      die("store_result() failed: (" . htmlspecialchars($dbc->errno) . ") " . htmlspecialchars($dbc->error));
    }

    $numrows = $stmt->num_rows;
    $stmt->bind_result($indsurveyid, $url_hash, $indpassword, $bosspassword, $contribpassword);
    $stmt->fetch();
    $stmt->free_result();
    $stmt->close();

    if ($numrows === 1) {// A match was made
      $_SESSION['indsurveyid'] = $sid;
      $cleared = TRUE;
      $_SESSION['cleared'] = $cleared;
      $url = 'resp-quest-ind-contrib.php?a=' . $url_hash . '&b='. $indsurveyid . '&c=i';
      header("Location: $url");
    }

    //ID and passwords were input in suitable format but not correct
    $loginnotok = 1;
  }
  else { //ID and passwords not input
    $loginnotentered = 1;
  }

  mysqli_close($dbc);
} // End of SUBMIT conditional.
?>

<?php require('includes/_header.php'); ?>

        <div class="limiter">
						<?php require('includes/_banner.php'); ?>
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">

                    <h1>Log in to complete an individual leadership review</h1>
                    <br />
                    <?php
                    if (($loginnotentered === 1) || ($loginnotok === 1)) {
                      echo '<p class="alert alert-danger">The review identification number or password do not match those on file. <br />Please try again or check them with the person who commissioned the review. Thank you.</p><br />';
                    }
                    ?>
                    <form role="form" action="resp-leadership-review-login.php" method="post">
                        <div class="form-group">
                            <label for="indsurveyid">Leadership review ID</label>
                            <input type="text" class="form-control" id="indsurveyid" name="indsurveyid">
                        </div>
                        <div class="form-group">
                            <label for="pswd">Leadership review password</label>
                            <input type="password" class="form-control" id="pswd" name="pswd">
                        </div>
                        <br />
                        <button type="submit" class="btn btn-danger">Login to complete the review</button>
                        <input type="hidden" name="submitted" value="TRUE" />
                    </form>

                </div>
                <div class="col-sm-2"></div>

            </div>
        </div>
        
<?php 
require('includes/_footer.php');
include_once("includes/analyticstracking.php"); 
?>


