
<?php

//Producing the headlines report bar charts for the Individual Review
//Specifying the labels and values used in the reports
//Each line on the report shows data for three variables - the self score, database average, and contributor score (if requested in report set up)
//Items are the individual questions from the review
//Grand average is the average of all scored items on the questionnaire

//NB database averages and stakeholder averages have same variable names as the corresponding variables for teams.
//This is OK (but should be changed at some stage) because the data is stored in different tables
//and the report generation queries the team table for the team report and the in table for the ind report

//Specifying labels for the bar charts in the report

$topics = array('Clarity of purpose', 'Focus on key business goals', 'High expectations of self and others', 'Inspires others', 'Generates optimism', 'Leads by example', 'Builds productive relationships', 'Manages priorities and workload well', 'Negotiates well', 'Is well organized', 'Is clear about roles and responsibilities', 'Delegates well when necessary', 'Communicates well', 'Handles disagreement constructively', 'Is a good team player', 'Has a can-do attitude', 'Expresses trust and confidence in people', 'Encourages  and supports others', 'Delivers what they say they will', 'Listens to feedback and acts on it', 'Makes decisions well', 'Adaptable and innovative', 'Continually strives to improve performance', 'Good at making change happen', 'Gets great results from self and others', 'Empowers people around them');

//Specifying individual item values for the bar charts in the report
$indavefm = array($indavgd1, $indavgd2, $indavgd3, $indavgd4, $indavgd5, $indavgd6, $indavgd7, $indavgd8, $indavgd9, $indavgd10, $indavgd11, $indavgd12, $indavgd13, $indavgd14, $indavgd15, $indavgd16, $indavgd17, $indavgd18, $indavgd19, $indavgd20, $indavgd21, $indavgd22, $indavgd23, $indavgd24, $indavgd25, $indavgd26);

$dbavefm = array($dbavgd1, $dbavgd2, $dbavgd3, $dbavgd4, $dbavgd5, $dbavgd6, $dbavgd7, $dbavgd8, $dbavgd9, $dbavgd10, $dbavgd11, $dbavgd12, $dbavgd13, $dbavgd14, $dbavgd15, $dbavgd16, $dbavgd17, $dbavgd18, $dbavgd19, $dbavgd20, $dbavgd21, $dbavgd22, $dbavgd23, $dbavgd24, $dbavgd25, $dbavgd26);

$stakeavefm = array($stakeavgd1, $stakeavgd2, $stakeavgd3, $stakeavgd4, $stakeavgd5, $stakeavgd6, $stakeavgd7, $stakeavgd8, $stakeavgd9, $stakeavgd10, $stakeavgd11, $stakeavgd12, $stakeavgd13, $stakeavgd14, $stakeavgd15, $stakeavgd16, $stakeavgd17, $stakeavgd18, $stakeavgd19, $stakeavgd20, $stakeavgd21, $stakeavgd22, $stakeavgd23, $stakeavgd24, $stakeavgd25, $stakeavgd26);

$ivotedev = array($ivotedev1, $ivotedev2, $ivotedev3, $ivotedev4, $ivotedev5, $ivotedev6, $ivotedev7, $ivotedev8, $ivotedev9, $ivotedev10, $ivotedev11, $ivotedev12, $ivotedev13, $ivotedev14, $ivotedev15, $ivotedev16, $ivotedev17, $ivotedev18, $ivotedev19, $ivotedev20, $ivotedev21, $ivotedev22, $ivotedev23, $ivotedev24, $ivotedev25, $ivotedev26);



// Put database top and bottom 10 percentile figs for items, calculated periodically using SPSS in arrays for benchmarking
$itemindtoptenpc = array(85, 84, 81, 76, 80, 89, 92, 86, 79, 82, 81, 80, 83, 79, 92, 93, 87, 94, 77, 79, 82, 87, 81, 84, 84, 92);
$itemindbottenpc = array(29, 37, 26, 24, 29, 36, 42, 26, 29, 30, 27, 27, 25, 26, 35, 35, 30, 40, 23, 20, 30, 32, 23, 31, 41, 31);
// Put database top and bottom 10 percentile figs for aspects, calculated periodically using SPSS in arrays for benchmarking
$aspectsindtoptenpc = array(79, 77, 79, 74, 78, 84, 71, 76);
$aspectsindbottenpc = array(39, 34, 42, 34, 35, 42, 35, 35);
// Put database top and bottom 10 percentile figs for faces, calculated periodically using SPSS in arrays for benchmarking
$facesindtoptenpc = array(76, 73, 77, 72);
$facesindbottenpc = array(38, 41, 40, 37);
// Put database top and bottom 10 percentile figs for grand average, calculated periodically using SPSS in arrays for benchmarking
$indgrandtoptenpc = 72;
$indgrandbottenpc = 41;

?>


