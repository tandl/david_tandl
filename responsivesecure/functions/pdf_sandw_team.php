<?php

    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(5, 'Your Signature Strengths' . "\n" . "\n");

//Printing out top 5 scores identified by the team - ie their "Signature Strengths"

$teamss = array();

/* These were the items re-worded slightly for the report - an idea I reverted on in April 2017
$teamss['%      The team is very clear about what it is trying to achieve'] = $teamavgd1;
$teamss['%      The team puts great emphasis on delivering what customers want'] = $teamavgd2;
$teamss['%      The team has ambitious and measurable team targets'] = $teamavgd3;
$teamss['%      The team communicates its vision and priorities in an inspiring way'] = $teamavgd4;
$teamss['%      The team generates optimism and confidence internally and externally'] = $teamavgd5;
$teamss['%      The team leads by example - actions match words'] = $teamavgd6;
$teamss['%      The team builds good relationships with key stakeholders'] = $teamavgd7;
$teamss['%      The team has all the skills and resources it needs to achieve its goals'] = $teamavgd8;
$teamss['%      The team is good at negotiating to get what it needs'] = $teamavgd9;
$teamss['%      Team meetings are effective and their purpose is clear'] = $teamavgd10;
$teamss['%      The team is clear about everyone\'s role and responsibilities'] = $teamavgd11;
$teamss['%      Authority and responsibilities are delegated to the right people in the team'] = $teamavgd12;
$teamss['%      In team discussions no-one is too dominant or too quiet'] = $teamavgd13;
$teamss['%      The team talks about everything it needs to and disagreements get resolved'] = $teamavgd14;
$teamss['%      People put collective achievement above their individual priorities'] = $teamavgd15;
$teamss['%      The team has a very can-do culture'] = $teamavgd16;
$teamss['%      Individuals in the team feel understood and accepted by each other'] = $teamavgd17;
$teamss['%      Team members openly thank and support each other'] = $teamavgd18;
$teamss['%      Team members hold each other to account for delivering on agreements'] = $teamavgd19;
$teamss['%      The team has accurate and comprehensive performance data and reports'] = $teamavgd20;
$teamss['%      The team makes decisions well together'] = $teamavgd21;
$teamss['%      The team is very strong in creativity and innovation'] = $teamavgd22;
$teamss['%      The team systematically analyzes data to improve results'] = $teamavgd23;
$teamss['%      The team has a great track record in delivering change and improvements'] = $teamavgd24;
$teamss['%      The team actually achieves the results it sets itself'] = $teamavgd25;
$teamss['%      It is personally and professionally rewarding working in the team'] = $teamavgd26;
*/

/*
This is the actual wording of the stakeholder questions in the online review
*/
$teamss['%      '.$topics_team_review[1][3]] = $teamavgd1;
$teamss['%      '.$topics_team_review[2][3]] = $teamavgd2;
$teamss['%      '.$topics_team_review[3][3]] = $teamavgd3;
$teamss['%      '.$topics_team_review[4][3]] = $teamavgd4;
$teamss['%      '.$topics_team_review[5][3]] = $teamavgd5;
$teamss['%      '.$topics_team_review[6][3]] = $teamavgd6;
$teamss['%      '.$topics_team_review[7][3]] = $teamavgd7;
$teamss['%      '.$topics_team_review[8][3]] = $teamavgd8;
$teamss['%      '.$topics_team_review[9][3]] = $teamavgd9;
$teamss['%      '.$topics_team_review[10][3]] = $teamavgd10;
$teamss['%      '.$topics_team_review[11][3]] = $teamavgd11;
$teamss['%      '.$topics_team_review[12][3]] = $teamavgd12;
$teamss['%      '.$topics_team_review[13][3]] = $teamavgd13;
$teamss['%      '.$topics_team_review[14][3]] = $teamavgd14;
$teamss['%      '.$topics_team_review[15][3]] = $teamavgd15;
$teamss['%      '.$topics_team_review[16][3]] = $teamavgd16;
$teamss['%      '.$topics_team_review[17][3]] = $teamavgd17;
$teamss['%      '.$topics_team_review[18][3]] = $teamavgd18;
$teamss['%      '.$topics_team_review[19][3]] = $teamavgd19;
$teamss['%      '.$topics_team_review[20][3]] = $teamavgd20;
$teamss['%      '.$topics_team_review[21][3]] = $teamavgd21;
$teamss['%      '.$topics_team_review[22][3]] = $teamavgd22;
$teamss['%      '.$topics_team_review[23][3]] = $teamavgd23;
$teamss['%      '.$topics_team_review[24][3]] = $teamavgd24;
$teamss['%      '.$topics_team_review[25][3]] = $teamavgd25;
$teamss['%      '.$topics_team_review[26][3]] = $teamavgd26;

$pdf->SetFont('Helvetica', '', 10);
$pdf->Write(5, 'Your team\'s view - highest 4 scores (see previous page) as rated by your team: ' . "\n" ."\n");
$pdf->SetFont('Helvetica', '', 9);




//Printing out any Signature Strengths identified by team members

if (($teamavgd1 === '-') && ($teamavgd2 === '-') && ($teamavgd3 === '-') && ($teamavgd4 === '-') && ($teamavgd5 === '-') && ($teamavgd6 === '-') && ($teamavgd7 === '-') && ($teamavgd8 === '-') && ($teamavgd9 === '-') && ($teamavgd10 === '-') && ($teamavgd11 === '-') && ($teamavgd12 === '-') && ($teamavgd13 === '-') && ($teamavgd14 === '-') && ($teamavgd15 === '-') && ($teamavgd16 === '-') && ($teamavgd17 === '-') && ($teamavgd18 === '-') && ($teamavgd19 === '-') && ($teamavgd20 === '-') && ($teamavgd21 === '-') && ($teamavgd22 === '-') && ($teamavgd23 === '-') && ($teamavgd24 === '-') && ($teamavgd25 === '-') && ($teamavgd26 === '-')) {
    $pdf->Write(3, '       Your team didn\'t identify any signature strengths ' . "\n" . "\n");
} else {
    arsort($teamss);
    $top4 = array_slice($teamss, 0, 4);
    foreach ($top4 as $key => $value) {
        if ($value != '-') {
            $pdf->Write(3, '     ' . $value . ' ' . $key . "\n" . "\n");
        }
    }
}


$stakess = array();
/* These were the items re-worded slightly for the report - an idea I reverted on in April 2017
$stakess['%      The team is very clear about what it is trying to achieve'] = $stakeavgd1;
$stakess['%      The team puts great emphasis on delivering what customers want'] = $stakeavgd2;
$stakess['%      The team has ambitious and measurable team targets'] = $stakeavgd3;
$stakess['%      The team communicates its vision and priorities in an inspiring way'] = $stakeavgd4;
$stakess['%      The team generates optimism and confidence internally and externally'] = $stakeavgd5;
$stakess['%      The team leads by example - actions match words'] = $stakeavgd6;
$stakess['%      The team builds good relationships with key stakeholders'] = $stakeavgd7;
$stakess['%      The team has all the skills and resources it needs to achieve its goals'] = $stakeavgd8;
$stakess['%      The team is good at negotiating to get what it needs'] = $stakeavgd9;
$stakess['%      Team meetings are effective and their purpose is clear'] = $stakeavgd10;
$stakess['%      The team is clear about everyone\'s role and responsibilities'] = $stakeavgd11;
$stakess['%      Authority and responsibilities are delegated to the right people in the team'] = $stakeavgd12;
$stakess['%      In team discussions no-one is too dominant or too quiet'] = $stakeavgd13;
$stakess['%      The team talks about everything it needs to and disagreements get resolved'] = $stakeavgd14;
$stakess['%      People put collective achievement above their individual priorities'] = $stakeavgd15;
$stakess['%      The team has a very can-do culture'] = $stakeavgd16;
$stakess['%      Individuals in the team feel understood and accepted by each other'] = $stakeavgd17;
$stakess['%      Team members openly thank and support each other'] = $stakeavgd18;
$stakess['%      Team members hold each other to account for delivering on agreements'] = $stakeavgd19;
$stakess['%      The team has accurate and comprehensive performance data and reports'] = $stakeavgd20;
$stakess['%      The team makes decisions well together'] = $stakeavgd21;
$stakess['%      The team is very strong in creativity and innovation'] = $stakeavgd22;
$stakess['%      The team systematically analyzes data to improve results'] = $stakeavgd23;
$stakess['%      The team has a great track record in delivering change and improvements'] = $stakeavgd24;
$stakess['%      The team actually achieves the results it sets itself'] = $stakeavgd25;
$stakess['%      It is personally and professionally rewarding working in the team'] = $stakeavgd26;
*/
/*
This is the actual wording of the stakeholder questions in the online review
*/
$stakess['%      '.$topics_team_review[1][4]] = $stakeavgd1;
$stakess['%      '.$topics_team_review[2][4]] = $stakeavgd2;
$stakess['%      '.$topics_team_review[3][4]] = $stakeavgd3;
$stakess['%      '.$topics_team_review[4][4]] = $stakeavgd4;
$stakess['%      '.$topics_team_review[5][4]] = $stakeavgd5;
$stakess['%      '.$topics_team_review[6][4]] = $stakeavgd6;
$stakess['%      '.$topics_team_review[7][4]] = $stakeavgd7;
$stakess['%      '.$topics_team_review[8][4]] = $stakeavgd8;
$stakess['%      '.$topics_team_review[9][4]] = $stakeavgd9;
$stakess['%      '.$topics_team_review[10][4]] = $stakeavgd10;
$stakess['%      '.$topics_team_review[11][4]] = $stakeavgd11;
$stakess['%      '.$topics_team_review[12][4]] = $stakeavgd12;
$stakess['%      '.$topics_team_review[13][4]] = $stakeavgd13;
$stakess['%      '.$topics_team_review[14][4]] = $stakeavgd14;
$stakess['%      '.$topics_team_review[15][4]] = $stakeavgd15;
$stakess['%      '.$topics_team_review[16][4]] = $stakeavgd16;
$stakess['%      '.$topics_team_review[17][4]] = $stakeavgd17;
$stakess['%      '.$topics_team_review[18][4]] = $stakeavgd18;
$stakess['%      '.$topics_team_review[19][4]] = $stakeavgd19;
$stakess['%      '.$topics_team_review[20][4]] = $stakeavgd20;
$stakess['%      '.$topics_team_review[21][4]] = $stakeavgd21;
$stakess['%      '.$topics_team_review[22][4]] = $stakeavgd22;
$stakess['%      '.$topics_team_review[23][4]] = $stakeavgd23;
$stakess['%      '.$topics_team_review[24][4]] = $stakeavgd24;
$stakess['%      '.$topics_team_review[25][4]] = $stakeavgd25;
$stakess['%      '.$topics_team_review[26][4]] = $stakeavgd26;

$pdf->SetFont('Helvetica', '', 10);
$pdf->Write(5, "\n" . 'Your stakeholders\' view - highest 4 scores (see previous page) as rated by your stakeholders: ' . "\n" ."\n");
$pdf->SetFont('Helvetica', '', 9);


//Printing out any Signature Strengths identified by stakeholders

if (($stakeavgd1 === '-') && ($stakeavgd2 === '-') && ($stakeavgd3 === '-') && ($stakeavgd4 === '-') && ($stakeavgd5 === '-') && ($stakeavgd6 === '-') && ($stakeavgd7 === '-') && ($stakeavgd8 === '-') && ($stakeavgd9 === '-') && ($stakeavgd10 === '-') && ($stakeavgd11 === '-') && ($stakeavgd12 === '-') && ($stakeavgd13 === '-') && ($stakeavgd14 === '-') && ($stakeavgd15 === '-') && ($stakeavgd16 === '-') && ($stakeavgd17 === '-') && ($stakeavgd18 === '-') && ($stakeavgd19 === '-') && ($stakeavgd20 === '-') && ($stakeavgd21 === '-') && ($stakeavgd22 === '-') && ($stakeavgd23 === '-') && ($stakeavgd24 === '-') && ($stakeavgd25 === '-') && ($stakeavgd26 === '-')) {
    $pdf->Write(3, '       No stakeholders contibuted to this section of the review ' . "\n" . "\n");
} else {
    arsort($stakess);
    $top4 = array_slice($stakess, 0, 4);
    foreach ($top4 as $key => $value) {
        if ($value != '-') {
            $pdf->Write(3, '     ' . $value . ' ' . $key . "\n" . "\n");
        }
    }
}

    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(5,  "\n" . "\n" . 'Your Challenges' . "\n");

//Printing out bottom 5 scoresidentified by the team - ie their "Challenges"

$teamff = array();
/* These were the items re-worded slightly for the report - an idea I reverted on in April 2017
$teamff['%      The team is not very clear what it is trying to achieve'] = $teamavgd1;
$teamff['%      The team does not put enough emphasis on delivering what customers want'] = $teamavgd2;
$teamff['%      The team doesn\'t have ambitious and measurable team targets'] = $teamavgd3;
$teamff['%      The team doesn\'t communicate its vision and priorities in an inspiring way'] = $teamavgd4;
$teamff['%      The team doesn\'t generate optimism and confidence internally and externally'] = $teamavgd5;
$teamff['%      The team doesn\'t lead by example - actions don\'t match words'] = $teamavgd6;
$teamff['%      The team doesn\'t build good enough relationships with key stakeholders'] = $teamavgd7;
$teamff['%      The team doesn\'t have all the skills and resources it needs to achieve its goals'] = $teamavgd8;
$teamff['%      The team is not good at negotiating to get what it needs'] = $teamavgd9;
$teamff['%      Team meetings are not effective with a clear purpose'] = $teamavgd10;
$teamff['%      The team is not clear about everyone\'s role and responsibilities'] = $teamavgd11;
$teamff['%      Authority and responsibilities are not delegated to the right people in the team'] = $teamavgd12;
$teamff['%      In team discussions some people are too dominant or too quiet'] = $teamavgd13;
$teamff['%      The team doesn\'t talk about everything it needs to and resolve disagreements'] = $teamavgd14;
$teamff['%      People don\'t put collective achievement above their individual priorities'] = $teamavgd15;
$teamff['%      The team doesn\'t have a very can-do culture'] = $teamavgd16;
$teamff['%      Individuals in our team don\'t feel understood and accepted by each other'] = $teamavgd17;
$teamff['%      Team members don\'t openly thank and support each other'] = $teamavgd18;
$teamff['%      Team members don\'t hold each other to account for delivering on agreements'] = $teamavgd19;
$teamff['%      The team doesn\'t have accurate and comprehensive performance data and reports'] = $teamavgd20;
$teamff['%      The team doesn\'t make decisions well together'] = $teamavgd21;
$teamff['%      The team is not very strong in creativity and innovation'] = $teamavgd22;
$teamff['%      The team doesn\'t systematically analyze data to improve results'] = $teamavgd23;
$teamff['%      The team doesn\'t have a great track record in delivering change and improvements'] = $teamavgd24;
$teamff['%      The team doesn\'t achieve the results it sets itself'] = $teamavgd25;
$teamff['%      It is not personally and professionally rewarding working in the team'] = $teamavgd26;
*/

$teamff['%      '.$topics_team_review[1][3]] = $teamavgd1;
$teamff['%      '.$topics_team_review[2][3]] = $teamavgd2;
$teamff['%      '.$topics_team_review[3][3]] = $teamavgd3;
$teamff['%      '.$topics_team_review[4][3]] = $teamavgd4;
$teamff['%      '.$topics_team_review[5][3]] = $teamavgd5;
$teamff['%      '.$topics_team_review[6][3]] = $teamavgd6;
$teamff['%      '.$topics_team_review[7][3]] = $teamavgd7;
$teamff['%      '.$topics_team_review[8][3]] = $teamavgd8;
$teamff['%      '.$topics_team_review[9][3]] = $teamavgd9;
$teamff['%      '.$topics_team_review[10][3]] = $teamavgd10;
$teamff['%      '.$topics_team_review[11][3]] = $teamavgd11;
$teamff['%      '.$topics_team_review[12][3]] = $teamavgd12;
$teamff['%      '.$topics_team_review[13][3]] = $teamavgd13;
$teamff['%      '.$topics_team_review[14][3]] = $teamavgd14;
$teamff['%      '.$topics_team_review[15][3]] = $teamavgd15;
$teamff['%      '.$topics_team_review[16][3]] = $teamavgd16;
$teamff['%      '.$topics_team_review[17][3]] = $teamavgd17;
$teamff['%      '.$topics_team_review[18][3]] = $teamavgd18;
$teamff['%      '.$topics_team_review[19][3]] = $teamavgd19;
$teamff['%      '.$topics_team_review[20][3]] = $teamavgd20;
$teamff['%      '.$topics_team_review[21][3]] = $teamavgd21;
$teamff['%      '.$topics_team_review[22][3]] = $teamavgd22;
$teamff['%      '.$topics_team_review[23][3]] = $teamavgd23;
$teamff['%      '.$topics_team_review[24][3]] = $teamavgd24;
$teamff['%      '.$topics_team_review[25][3]] = $teamavgd25;
$teamff['%      '.$topics_team_review[26][3]] = $teamavgd26;

$pdf->SetFont('Helvetica', '', 10);
$pdf->Write(5, "\n" . 'Your team\'s view - lowest 4 scores (see previous page) as rated by your team: ' . "\n" ."\n");
$pdf->SetFont('Helvetica', '', 9);


//Printing out the Challenges identified by team members

if (($teamavgd1 === '-') && ($teamavgd2 === '-') && ($teamavgd3 === '-') && ($teamavgd4 === '-') && ($teamavgd5 === '-') && ($teamavgd6 === '-') && ($teamavgd7 === '-') && ($teamavgd8 === '-') && ($teamavgd9 === '-') && ($teamavgd10 === '-') && ($teamavgd11 === '-') && ($teamavgd12 === '-') && ($teamavgd13 === '-') && ($teamavgd14 === '-') && ($teamavgd15 === '-') && ($teamavgd16 === '-') && ($teamavgd17 === '-') && ($teamavgd18 === '-') && ($teamavgd19 === '-') && ($teamavgd20 === '-') && ($teamavgd21 === '-') && ($teamavgd22 === '-') && ($teamavgd23 === '-') && ($teamavgd24 === '-') && ($teamavgd25 === '-') && ($teamavgd26 === '-')) {
    $pdf->Write(3, '       Your team didn\'t identify any challenges ' . "\n" . "\n");
} else {
    $teamff = array_diff($teamff, array('-'));
    asort($teamff);
    $bottom4 = array_slice($teamff, 0, 4);
	arsort($bottom4);
    foreach ($bottom4 as $key => $value) {
        $pdf->Write(3, '     ' . $value . ' ' . $key . "\n" . "\n");
    }
}

$stakeff = array();

/* These were the items re-worded slightly for the report - an idea I reverted on in April 2017
$stakeff['%      The team is not very clear what it is trying to achieve'] = $stakeavgd1;
$stakeff['%      The team does not put enough emphasis on delivering what customers want'] = $stakeavgd2;
$stakeff['%      The team doesn\'t have ambitious and measurable team targets'] = $stakeavgd3;
$stakeff['%      The team doesn\'t communicate its vision and priorities in an inspiring way'] = $stakeavgd4;
$stakeff['%      The team doesn\'t generate optimism and confidence internally and externally'] = $stakeavgd5;
$stakeff['%      The team doesn\'t lead by example - actions don\'t match words'] = $stakeavgd6;
$stakeff['%      The team doesn\'t build good enough relationships with key stakeholders'] = $stakeavgd7;
$stakeff['%      The team doesn\'t have all the skills and resources it needs to achieve its goals'] = $stakeavgd8;
$stakeff['%      The team is not good at negotiating to get what it needs'] = $stakeavgd9;
$stakeff['%      Team meetings are not effective with a clear purpose'] = $stakeavgd10;
$stakeff['%      The team is not clear about everyone\'s role and responsibilities'] = $stakeavgd11;
$stakeff['%      Authority and responsibilities are not delegated to the right people in the team'] = $stakeavgd12;
$stakeff['%      In team discussions some people are too dominant or too quiet'] = $stakeavgd13;
$stakeff['%      The team doesn\'t talk about everything it needs to and resolve disagreements'] = $stakeavgd14;
$stakeff['%      People don\'t put collective achievement above their individual priorities'] = $stakeavgd15;
$stakeff['%      The team doesn\'t have a very can-do culture'] = $stakeavgd16;
$stakeff['%      Individuals in our team don\'t feel understood and accepted by each other'] = $stakeavgd17;
$stakeff['%      Team members don\'t openly thank and support each other'] = $stakeavgd18;
$stakeff['%      Team members don\'t hold each other to account for delivering on agreements'] = $stakeavgd19;
$stakeff['%      The team doesn\'t have accurate and comprehensive performance data and reports'] = $stakeavgd20;
$stakeff['%      The team doesn\'t make decisions well together'] = $stakeavgd21;
$stakeff['%      The team is not very strong in creativity and innovation'] = $stakeavgd22;
$stakeff['%      The team doesn\'t systematically analyze data to improve results'] = $stakeavgd23;
$stakeff['%      The team doesn\'t have a great track record in delivering change and improvements'] = $stakeavgd24;
$stakeff['%      The team doesn\'t achieve the results it sets itself'] = $stakeavgd25;
$stakeff['%      It is not personally and professionally rewarding working in the team'] = $stakeavgd26;
*/

$stakeff['%      '.$topics_team_review[1][4]] = $stakeavgd1;
$stakeff['%      '.$topics_team_review[2][4]] = $stakeavgd2;
$stakeff['%      '.$topics_team_review[3][4]] = $stakeavgd3;
$stakeff['%      '.$topics_team_review[4][4]] = $stakeavgd4;
$stakeff['%      '.$topics_team_review[5][4]] = $stakeavgd5;
$stakeff['%      '.$topics_team_review[6][4]] = $stakeavgd6;
$stakeff['%      '.$topics_team_review[7][4]] = $stakeavgd7;
$stakeff['%      '.$topics_team_review[8][4]] = $stakeavgd8;
$stakeff['%      '.$topics_team_review[9][4]] = $stakeavgd9;
$stakeff['%      '.$topics_team_review[10][4]] = $stakeavgd10;
$stakeff['%      '.$topics_team_review[11][4]] = $stakeavgd11;
$stakeff['%      '.$topics_team_review[12][4]] = $stakeavgd12;
$stakeff['%      '.$topics_team_review[13][4]] = $stakeavgd13;
$stakeff['%      '.$topics_team_review[14][4]] = $stakeavgd14;
$stakeff['%      '.$topics_team_review[15][4]] = $stakeavgd15;
$stakeff['%      '.$topics_team_review[16][4]] = $stakeavgd16;
$stakeff['%      '.$topics_team_review[17][4]] = $stakeavgd17;
$stakeff['%      '.$topics_team_review[18][4]] = $stakeavgd18;
$stakeff['%      '.$topics_team_review[19][4]] = $stakeavgd19;
$stakeff['%      '.$topics_team_review[20][4]] = $stakeavgd20;
$stakeff['%      '.$topics_team_review[21][4]] = $stakeavgd21;
$stakeff['%      '.$topics_team_review[22][4]] = $stakeavgd22;
$stakeff['%      '.$topics_team_review[23][4]] = $stakeavgd23;
$stakeff['%      '.$topics_team_review[24][4]] = $stakeavgd24;
$stakeff['%      '.$topics_team_review[25][4]] = $stakeavgd25;
$stakeff['%      '.$topics_team_review[26][4]] = $stakeavgd26;

$pdf->SetFont('Helvetica', '', 10);
$pdf->Write(5, "\n" . 'Your stakeholders\' view - lowest 4 scores (see previous page) as rated by your stakeholders: ' . "\n" ."\n");
$pdf->SetFont('Helvetica', '', 9);


//Printing out the Challenges identified by team members

if (($stakeavgd1 === '-') && ($stakeavgd2 === '-') && ($stakeavgd3 === '-') && ($stakeavgd4 === '-') && ($stakeavgd5 === '-') && ($stakeavgd6 === '-') && ($stakeavgd7 === '-') && ($stakeavgd8 === '-') && ($stakeavgd9 === '-') && ($stakeavgd10 === '-') && ($stakeavgd11 === '-') && ($stakeavgd12 === '-') && ($stakeavgd13 === '-') && ($stakeavgd14 === '-') && ($stakeavgd15 === '-') && ($stakeavgd16 === '-') && ($stakeavgd17 === '-') && ($stakeavgd18 === '-') && ($stakeavgd19 === '-') && ($stakeavgd20 === '-') && ($stakeavgd21 === '-') && ($stakeavgd22 === '-') && ($stakeavgd23 === '-') && ($stakeavgd24 === '-') && ($stakeavgd25 === '-') && ($stakeavgd26 === '-')) {
    $pdf->Write(3, '       No stakeholders contibuted to this section of the review ' . "\n" . "\n");
} else {
    $stakeff = array_diff($stakeff, array('-'));
    asort($stakeff);
    $bottom4 = array_slice($stakeff, 0, 4);
	arsort($bottom4);
    foreach ($bottom4 as $key => $value) {
        $pdf->Write(3, '     ' . $value . ' ' . $key . "\n" . "\n");
    }
}


?>