
<?php

//NB NB NB I have made the calculations highly restrictive - input must be between 0 and 4 inclusive. This can easily be changed by altering the SELECT criteria. Restriction then is just the SQL type in the database.
// This included process extracts team and database averages and votes

/* Assume connect and disconnect details are on the including page. If not, add:

  require_once (MYSQLI);
  if (!$dbc)
  {
  die('Could not connect you up to the database: ');
  }

  before the script below, and:

  mysqli_close($dbc);

  at the end
 */

// Initializing variables

//Team Topics & Page links array
$topics_team_review = array(
  array('Topic ID', 'Topic Heading', 'Topic Description', 'Team Question', 'Stakeholder Question', 'Exercise Link', 'Bar Chart Heading'),
  array(1, 'Clarifying Objectives', 'Being clearer about what the team is trying to achieve', 'We are all very clear about what the team is trying to achieve', 'Team members seem very clear about what they are trying to achieve together', 'https://teamsandleadership.net/content/objectives-and-key-results', 'Clarity of team objectives'  ),
  array(2, 'Delivering what customers want', 'Putting more emphasis on delivering what customers want', 'Our team puts great emphasis on delivering what customers want', 'The team puts great emphasis on delivering what customers want', 'https://teamsandleadership.net/content/what-customers-want', 'Focus on what customers want'  ),
  array(3, 'Developing team targets', 'Having ambitious and measurable team targets', 'We have ambitious and measurable team targets', 'The team has an ambitious vision', 'https://teamsandleadership.net/content/clarifying-your-objectives', 'Ambitious and measurable team targets'  ),
  array(4, 'Inspiring people', 'Communicating the vision and priorities in an inspiring way', 'Our team communicates its vision and priorities in an inspiring way', 'The team communicates its vision and priorities in an inspiring way', 'https://teamsandleadership.net/content/inspiring-communication', 'Inspiring communication'  ),
  array(5, 'Generating optimism', 'Generating optimism and confidence', 'The team generates optimism and confidence internally and externally', 'The team generates optimism and confidence', 'https://teamsandleadership.net/content/optimism',  'Generating optimism'  ),
  array(6, 'Leading by example', 'Leading by example', 'The team leads by example – we do what we say we will do', 'The team leads by example - they do what they say they will do', 'https://teamsandleadership.net/content/leading-by-example', 'Leading by example'   ),
  array(7, 'Managing stakeholders', 'Building good relationships with key stakeholders', 'The team builds good relationships with key stakeholders', 'Team members build good relationships with key stakeholders', 'https://teamsandleadership.net/content/managing-stakeholders',  'Managing stakeholders'  ),
  array(8, 'Securing skills and resources', 'Getting the skills and resources the team needs to achieve its goals', 'Our team has all the skills and resources it needs to achieve its goals', 'The team seems skilful and well-resourced', 'https://teamsandleadership.net/content/Influencing-people', 'Securing skills and resources'  ),
  array(9, 'Improving negotiating skills', 'Negotiating', 'The team is good at negotiating to get what it needs', 'The team is good at negotiating', 'https://teamsandleadership.net/content/negotiating', 'Negotiating skill'  ),
  array(10, 'Running effective meetings', 'Improving the effectiveness and clarifying the purpose of team meetings', 'Team meetings are effective and their purpose is clear', 'The team seems very well organized', 'https://teamsandleadership.net/content/running-effective-meetings',  'Effective team meetings'   ),
  array(11, 'Clarifying roles and responsibilities', 'Being clear about everyone\'s role and responsibilities', 'The team is clear about everyone\'s role and responsibilities', 'People\'s roles and responsibilities in the team are very clear', 'https://teamsandleadership.net/content/roles-and-responsibilities', 'Clear roles and responsibilities'   ),
  array(12, 'Improving delegation', 'Delegating authority and responsibilities to the right people', 'Authority and responsibilities are delegated to the right people in the team', 'Team members can make decisions without having to refer back to the boss', 'https://teamsandleadership.net/content/consultation-and-delegation', 'Effective delegation'   ),
  array(13, 'Ensuring full contribution to team discussions', 'Ensuring no-one is too dominant or too quiet in team meetings', 'In team discussions no-one is too dominant or too quiet', 'Communication between team members seems excellent', 'https://teamsandleadership.net/content/effective-teamwork',  'Full contribution to team discussions'  ),
  array(14, 'Handling disagreements', 'Having open conversations and resolving disagreements', 'The team talks about everything it needs to and disagreements get resolved', 'Team members speak with one voice – they are very consistent', 'https://teamsandleadership.net/content/managing-conflict', 'Disagreements in the open and resolved'  ),
  array(15, 'Putting collective achievement first', 'Putting collective achievement above individual priorities', 'People put collective achievement above their individual priorities', 'The team seems to have an all-for-one and one-for-all attitude', 'https://teamsandleadership.net/content/team-objectives', 'Collective achievement put above individual priorities'  ),
  array(16, 'Developing a can-do culture', 'Generating a can-do culture', 'The team has a very can-do culture', 'People in the team seem very motivated to get things done', 'https://teamsandleadership.net/content/can-do-culture', 'Can-do culture'  ),
  array(17, 'Emotional intelligence', 'Understanding and accepting each other', 'Individuals in our team feel understood and accepted by each other', 'There seem to be very good relationships between team members', 'https://teamsandleadership.net/content/emotional-intelligence',   'Emotional intelligence' ),
  array(18, 'Increasing appreciation and support', 'Thanking and supporting each other', 'Team members openly thank and support each other', 'People in the team seem to support each other very well', 'https://teamsandleadership.net/content/can-do-culture',  'Appreciation and support'  ),
  array(19, 'Managing performance as a team', 'Holding each other to account for delivering on agreements', 'Team members hold each other to account for delivering on agreements', 'Team members take responsibility if things go wrong and try to sort out problems', 'https://teamsandleadership.net/content/managing-performance-as-a-team', 'Collective performance management'  ),
  array(20, 'Quality of performance data and reports', 'Having accurate and comprehensive performance data and reports', 'The team has accurate and comprehensive performance data and reports', 'Team members always seem to have the right information at their fingertips', 'https://teamsandleadership.net/content/reviewing-progress', 'Quality of performance data and reports'  ),
  array(21, 'Making decisions as a team', 'Making decisions better as a team', 'We make decisions well as a team', 'We can always get decisions out of the team when we need them', 'https://teamsandleadership.net/content/making-decisions',  'Team decision-making' ),
  array(22, 'Encouraging creativity and innovation', 'Being more creative and innovative', 'Our team is very strong in creativity and innovation', 'The team comes up with very creative solutions', 'https://teamsandleadership.net/content/creativity-innovation',  'Creativity and innovation'  ),
  array(23, 'Delivering continuous improvement', 'Systematically analyzing data to improve results', 'Our team systematically analyzes data to improve results', 'They are very good at analyzing where exactly where things need improving', 'https://teamsandleadership.net/content/continuous-improvement',  'Systematic performance improvement based on data'  ),
  array(24, 'Leading and delivering change', 'Delivering change and improvements', 'The team has a great track record in delivering change and improvements', 'Team members are very good at making change happen', 'https://teamsandleadership.net/content/leading-change-programmes', 'Leading and delivering change'  ),
  array(25, 'Meeting targets', 'Achieving the results the team sets itself', 'How well does your team actually achieve the results it sets itself?', 'The team delivers great results for us', 'https://teamsandleadership.net/content/reviewing-progress',  'Meeting targets'  ),
  array(26, 'Personal and professional fulfilment', 'Making the team more personally and professionally rewarding', 'How personally and professionally rewarding is working in the team?', 'Team members seem to enjoy their work', 'https://teamsandleadership.net/content/happiness-at-work', 'Personal and professional fulfilment'  )
);

$teamavgd1 = 99;
$teamavgd2 = 99;
$teamavgd3 = 99;
$teamavgd4 = 99;
$teamavgd5 = 99;
$teamavgd6 = 99;
$teamavgd7 = 99;
$teamavgd8 = 99;
$teamavgd9 = 99;
$teamavgd10 = 99;
$teamavgd11 = 99;
$teamavgd12 = 99;
$teamavgd13 = 99;
$teamavgd14 = 99;
$teamavgd15 = 99;
$teamavgd16 = 99;
$teamavgd17 = 99;
$teamavgd18 = 99;
$teamavgd19 = 99;
$teamavgd20 = 99;
$teamavgd21 = 99;
$teamavgd22 = 99;
$teamavgd23 = 99;
$teamavgd24 = 99;
$teamavgd25 = 99;
$teamavgd26 = 99;

$stakeavgd1 = 99;
$stakeavgd2 = 99;
$stakeavgd3 = 99;
$stakeavgd4 = 99;
$stakeavgd5 = 99;
$stakeavgd6 = 99;
$stakeavgd7 = 99;
$stakeavgd8 = 99;
$stakeavgd9 = 99;
$stakeavgd10 = 99;
$stakeavgd11 = 99;
$stakeavgd12 = 99;
$stakeavgd13 = 99;
$stakeavgd14 = 99;
$stakeavgd15 = 99;
$stakeavgd16 = 99;
$stakeavgd17 = 99;
$stakeavgd18 = 99;
$stakeavgd19 = 99;
$stakeavgd20 = 99;
$stakeavgd21 = 99;
$stakeavgd22 = 99;
$stakeavgd23 = 99;
$stakeavgd24 = 99;
$stakeavgd25 = 99;
$stakeavgd26 = 99;

// Periodically (eg annually) re-calculate benchmark stats. Last calculated 28.10.2014

$dbavgd1 = 57;
$dbavgd2 = 60;
$dbavgd3 = 53;
$dbavgd4 = 50;
$dbavgd5 = 55;
$dbavgd6 = 62;
$dbavgd7 = 67;
$dbavgd8 = 56;
$dbavgd9 = 54;
$dbavgd10 = 56;
$dbavgd11 = 54;
$dbavgd12 = 54;
$dbavgd13 = 54;
$dbavgd14 = 53;
$dbavgd15 = 64;
$dbavgd16 = 64;
$dbavgd17 = 59;
$dbavgd18 = 67;
$dbavgd19 = 50;
$dbavgd20 = 50;
$dbavgd21 = 56;
$dbavgd22 = 59;
$dbavgd23 = 52;
$dbavgd24 = 58;
$dbavgd25 = 62;
$dbavgd26 = 61;

$dbfocus = 0;
$dbfocusdenom = 0;
$dbfocusave = 59;
$dbfocusavefm = 59;

$dbcommunicate = 0;
$dbcommunicatedenom = 0;
$dbcommunicateave = 55;
$dbcommunicateavefm = 55;

$dbsecure = 0;
$dbsecuredenom = 0;
$dbsecureave = 60;
$dbsecureavefm = 60;

$dborganize = 0;
$dborganizedenom = 0;
$dborganizeave = 54;
$dborganizeavefm = 54;

$dbunite = 0;
$dbunitedenom = 0;
$dbuniteave = 57;
$dbuniteavefm = 57;

$dbempower = 0;
$dbempowerdenom = 0;
$dbempowerave = 63;
$dbempoweravefm = 63;

$dbdeliver = 0;
$dbdeliverdenom = 0;
$dbdeliverave = 53;
$dbdeliveravefm = 53;

$dbimprove = 0;
$dbimprovedenom = 0;
$dbimproveave = 56;
$dbimproveavefm = 56;

$dbvisionaryave = 57;
$dbvisionaryavefm = 57;

$dbguardianave = 57;
$dbguardianavefm = 57;

$dbpsychologistave = 59;
$dbpsychologistavefm = 59;

$dbdirectorave = 54;
$dbdirectoravefm = 54;

$dbgrandave = 57;
$dbgrandavefm = 57;

$teamfocus = 0;
$teamfocusdenom = 0;
$teamfocusave = 0;
$teamfocusavefm = 0;

$teamcommunicate = 0;
$teamcommunicatedenom = 0;
$teamcommunicateave = 0;
$teamcommunicateavefm = 0;

$teamsecure = 0;
$teamsecuredenom = 0;
$teamsecureave = 0;
$teamsecureavefm = 0;

$teamorganize = 0;
$teamorganizedenom = 0;
$teamorganizeave = 0;
$teamorganizeavefm = 0;

$teamunite = 0;
$teamunitedenom = 0;
$teamuniteave = 0;
$teamuniteavefm = 0;

$teamempower = 0;
$teamempowerdenom = 0;
$teamempowerave = 0;
$teamempoweravefm = 0;

$teamdeliver = 0;
$teamdeliverdenom = 0;
$teamdeliverave = 0;
$teamdeliveravefm = 0;

$teamimprove = 0;
$teamimprovedenom = 0;
$teamimproveave = 0;
$teamimproveavefm = 0;

$teamvisionaryave = 0;
$teamvisionaryavefm = 0;

$teamguardianave = 0;
$teamguardianavefm = 0;

$teampsychologistave = 0;
$teampsychologistavefm = 0;

$teamdirectorave = 0;
$teamdirectoravefm = 0;

$teamgrandave = 0;
$teamgrandavefm = 0;

$stakefocus = 0;
$stakefocusdenom = 0;
$stakefocusave = 0;
$stakefocusavefm = 0;

$stakecommunicate = 0;
$stakecommunicatedenom = 0;
$stakecommunicateave = 0;
$stakecommunicateavefm = 0;

$stakesecure = 0;
$stakesecuredenom = 0;
$stakesecureave = 0;
$stakesecureavefm = 0;

$stakeorganize = 0;
$stakeorganizedenom = 0;
$stakeorganizeave = 0;
$stakeorganizeavefm = 0;

$stakeunite = 0;
$stakeunitedenom = 0;
$stakeuniteave = 0;
$stakeuniteavefm = 0;

$stakeempower = 0;
$stakeempowerdenom = 0;
$stakeempowerave = 0;
$stakeempoweravefm = 0;

$stakedeliver = 0;
$stakedeliverdenom = 0;
$stakedeliverave = 0;
$stakedeliveravefm = 0;

$stakeimprove = 0;
$stakeimprovedenom = 0;
$stakeimproveave = 0;
$stakeimproveavefm = 0;

$stakevisionaryave = 0;
$stakevisionaryavefm = 0;

$stakeguardianave = 0;
$stakeguardianavefm = 0;

$stakepsychologistave = 0;
$stakepsychologistavefm = 0;

$stakedirectorave = 0;
$stakedirectoravefm = 0;

$stakegrandave = 0;
$stakegrandavefm = 0;

$tandsfocusave = 0;
$tandscommunicateave = 0;
$tandssecureave = 0;
$tandsorganizeave = 0;
$tandsuniteave = 0;
$tandsempowerave = 0;
$tandsdeliverave = 0;
$tandsimproveave = 0;

$tandsvisionaryave = 0;
$tandsguardianave = 0;
$tandspsychologistave = 0;
$tandsdirectorave = 0;
$tandsgrandave = 0;

$focushigh = 0;
$communicatehigh = 0;
$securehigh = 0;
$organizehigh = 0;
$unitehigh = 0;
$empowerhigh = 0;
$deliverhigh = 0;
$improvehigh = 0;


$focuscomment = '';
$communicatecomment = '';
$securecomment = '';
$organizecomment = '';
$unitecomment = '';
$empowercomment = '';
$delivercomment = '';
$improvecomment = '';
$visionarycomment = '';
$guardiancomment = '';
$psychologistcomment = '';
$directorcomment = '';
$grandcomment = '';



//Query database for team averages and votes  for development
//Getting team votes for development
$teamvotes = mysqli_query($dbc, "SELECT  teamsurveyid, SUM(dev1), SUM(dev2), SUM(dev3), SUM(dev4), SUM(dev5) , SUM(dev6), SUM(dev7) ,SUM(dev8),SUM(dev9), SUM(dev10), SUM(dev11), SUM(dev12), SUM(dev13) , SUM(dev14), SUM(dev15) ,SUM(dev16),SUM(dev17), SUM(dev18), SUM(dev19), SUM(dev20), SUM(dev21) , SUM(dev22), SUM(dev23) ,SUM(dev24),SUM(dev25), SUM(dev26) FROM teamdata  WHERE ((teamsurveyid = '$reportrequested') AND ((teamrole=1) OR (teamrole=2)))");
$getteamvotes = mysqli_fetch_array($teamvotes) or die(mysqli_error());

//Getting stakeholder votes for development
$stakevotes = mysqli_query($dbc, "SELECT  teamsurveyid, SUM(dev1), SUM(dev2), SUM(dev3), SUM(dev4), SUM(dev5) , SUM(dev6), SUM(dev7) ,SUM(dev8),SUM(dev9), SUM(dev10), SUM(dev11), SUM(dev12), SUM(dev13) , SUM(dev14), SUM(dev15) ,SUM(dev16),SUM(dev17), SUM(dev18), SUM(dev19), SUM(dev20), SUM(dev21) , SUM(dev22), SUM(dev23) ,SUM(dev24),SUM(dev25), SUM(dev26) FROM teamdata  WHERE ((teamsurveyid = '$reportrequested') AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$getstakevotes = mysqli_fetch_array($stakevotes) or die(mysqli_error());

//Extract team averages and summary stats
//Query database for d1 averages
$d1result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d1) FROM teamdata WHERE  (((d1>=0) AND (d1<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d1score = mysqli_fetch_array($d1result) or die(mysqli_error());
if (!$d1score['AVG(d1)']) {
    $teamavgd1 = '-';
} else {
    $teamavgd1 = number_format(25 * $d1score['AVG(d1)'], 0);
    $teamfocus = $teamfocus + $d1score['AVG(d1)'];
    $teamfocusdenom++;
}
//echo "After d1 team focus = $teamfocus and teamfocusdenom = $teamfocusdenom <br />"  ;

$d1stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d1) FROM teamdata WHERE  (((d1>=0) AND (d1<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d1stakescore = mysqli_fetch_array($d1stake) or die(mysqli_error());
if (!$d1stakescore['AVG(d1)']) {
    $stakeavgd1 = '-';
} else {
    $stakeavgd1 = number_format(25 * $d1stakescore['AVG(d1)'], 0);
    $stakefocus = $stakefocus + $d1stakescore['AVG(d1)'];
    $stakefocusdenom++;
}
//echo "After d1 stake focus = $stakefocus and stakefocusdenom = $stakefocusdenom<br />"  ;


//Query database for d2 averages
$d2result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d2) FROM teamdata WHERE  (((d2>=0) AND (d2<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d2score = mysqli_fetch_array($d2result) or die(mysqli_error());
if (!$d2score['AVG(d2)']) {
    $teamavgd2 = '-';
} else {
    $teamavgd2 = number_format(25 * $d2score['AVG(d2)'], 0);
    $teamfocus = $teamfocus + $d2score['AVG(d2)'];
    $teamfocusdenom++;
}
//echo "After d2 team focus = $teamfocus and teamfocusdenom = $teamfocusdenom<br />"  ;

$d2stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d2) FROM teamdata WHERE  (((d2>=0) AND (d2<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d2stakescore = mysqli_fetch_array($d2stake) or die(mysqli_error());
if (!$d2stakescore['AVG(d2)']) {
    $stakeavgd2 = '-';
} else {
    $stakeavgd2 = number_format(25 * $d2stakescore['AVG(d2)'], 0);
    $stakefocus = $stakefocus + $d2stakescore['AVG(d2)'];
    $stakefocusdenom++;
}
//echo "After d2 stake focus = $stakefocus and stakefocusdenom = $stakefocusdenom<br />"  ;


//Query database for d3 averages
$d3result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d3) FROM teamdata WHERE  (((d3>=0) AND (d3<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d3score = mysqli_fetch_array($d3result) or die(mysqli_error());
if (!$d3score['AVG(d3)']) {
    $teamavgd3 = '-';
} else {
    $teamavgd3 = number_format(25 * $d3score['AVG(d3)'], 0);
    $teamfocus = $teamfocus + $d3score['AVG(d3)'];
    $teamfocusdenom++;
}
//echo "After d3 team focus = $teamfocus and teamfocusdenom = $teamfocusdenom<br />"  ;


$d3stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d3) FROM teamdata WHERE  (((d3>=0) AND (d3<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d3stakescore = mysqli_fetch_array($d3stake) or die(mysqli_error());
if (!$d3stakescore['AVG(d3)']) {
    $stakeavgd3 = '-';
} else {
    $stakeavgd3 = number_format(25 * $d3stakescore['AVG(d3)'], 0);
    $stakefocus = $stakefocus + $d3stakescore['AVG(d3)'];
    $stakefocusdenom++;
}
//echo "After d3 stake focus = $stakefocus and stakefocusdenom = $stakefocusdenom<br />"  ;

//Calculating stakefocus
if ($stakefocusdenom === 0) {
    $stakefocusave = '-';
    $stakefocusavefm = '-';
} else {
    $stakefocusave = 25 * $stakefocus / $stakefocusdenom;
    $stakefocusavefm = number_format($stakefocusave, 0);
}
//echo" stakefocusave = $stakefocusave" ;

//Calculating teamfocus
if ($teamfocusdenom === 0) {
    $teamfocusave = '-';
    $teamfocusavefm = '-';
} else {
    $teamfocusave = 25 * $teamfocus / $teamfocusdenom;
    $teamfocusavefm = number_format($teamfocusave, 0);
}

// Calculating tandsfocus for star-rating
$tandsfocusdenom = $teamfocusdenom + $stakefocusdenom;
$tandsfocus = $teamfocus + $stakefocus;
if ($tandsfocusdenom === 0) {
    $focuscomment = '';
} else {
    $tandsfocusave = 25 * ($tandsfocus / $tandsfocusdenom);
    if ($tandsfocusave >= 69.64) {
        $focushigh = 1;
        $focuscomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
    } elseif ($tandsfocusave >= 62.33 && $tandsfocusave < 69.64) {
        $focushigh = 1;
        $focuscomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
    } else {
        $focuscomment = '';
    };
}

//Query database for d4 averages
$d4result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d4) FROM teamdata WHERE  (((d4>=0) AND (d4<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d4score = mysqli_fetch_array($d4result) or die(mysqli_error());
if (!$d4score['AVG(d4)']) {
    $teamavgd4 = '-';
} else {
    $teamavgd4 = number_format(25 * $d4score['AVG(d4)'], 0);
    $teamcommunicate = $teamcommunicate + $d4score['AVG(d4)'];
    $teamcommunicatedenom++;
}
//echo "After d4 team communicate = $teamcommunicate and teamcommunicatedenom = $teamcommunicatedenom <br />"  ;

$d4stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d4) FROM teamdata WHERE  (((d4>=0) AND (d4<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d4stakescore = mysqli_fetch_array($d4stake) or die(mysqli_error());
if (!$d4stakescore['AVG(d4)']) {
    $stakeavgd4 = '-';
} else {
    $stakeavgd4 = number_format(25 * $d4stakescore['AVG(d4)'], 0);
    $stakecommunicate = $stakecommunicate + $d4stakescore['AVG(d4)'];
    $stakecommunicatedenom++;
}
//echo "After d4 stake communicate = $stakecommunicate and stakecommunicatedenom = $stakecommunicatedenom<br />"  ;

//Query database for d5 averages
$d5result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d5) FROM teamdata WHERE  (((d5>=0) AND (d5<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d5score = mysqli_fetch_array($d5result) or die(mysqli_error());
if (!$d5score['AVG(d5)']) {
    $teamavgd5 = '-';
} else {
    $teamavgd5 = number_format(25 * $d5score['AVG(d5)'], 0);
    $teamcommunicate = $teamcommunicate + $d5score['AVG(d5)'];
    $teamcommunicatedenom++;
}
//echo "After d5 team communicate = $teamcommunicate and teamcommunicatedenom = $teamcommunicatedenom <br />"  ;

$d5stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d5) FROM teamdata WHERE  (((d5>=0) AND (d5<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d5stakescore = mysqli_fetch_array($d5stake) or die(mysqli_error());
if (!$d5stakescore['AVG(d5)']) {
    $stakeavgd5 = '-';
} else {
    $stakeavgd5 = number_format(25 * $d5stakescore['AVG(d5)'], 0);
    $stakecommunicate = $stakecommunicate + $d5stakescore['AVG(d5)'];
    $stakecommunicatedenom++;
}
//echo "After d5 stake communicate = $stakecommunicate and stakecommunicatedenom = $stakecommunicatedenom<br />"  ;

//Query database for d6 averages
$d6result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d6) FROM teamdata WHERE  (((d6>=0) AND (d6<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d6score = mysqli_fetch_array($d6result) or die(mysqli_error());
if (!$d6score['AVG(d6)']) {
    $teamavgd6 = '-';
} else {
    $teamavgd6 = number_format(25 * $d6score['AVG(d6)'], 0);
    $teamcommunicate = $teamcommunicate + $d6score['AVG(d6)'];
    $teamcommunicatedenom++;
}
//echo "After d6 team communicate = $teamcommunicate and teamcommunicatedenom = $teamcommunicatedenom <br />"  ;

$d6stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d6) FROM teamdata WHERE  (((d6>=0) AND (d6<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d6stakescore = mysqli_fetch_array($d6stake) or die(mysqli_error());
if (!$d6stakescore['AVG(d6)']) {
    $stakeavgd6 = '-';
} else {
    $stakeavgd6 = number_format(25 * $d6stakescore['AVG(d6)'], 0);
    $stakecommunicate = $stakecommunicate + $d6stakescore['AVG(d6)'];
    $stakecommunicatedenom++;
}
//echo "After d6 stake communicate = $stakecommunicate and stakecommunicatedenom = $stakecommunicatedenom<br />"  ;

//Calculating stakecommunicate
if ($stakecommunicatedenom === 0) {
    $stakecommunicateave = '-';
    $stakecommunicateavefm = '-';
} else {
    $stakecommunicateave = 25 * $stakecommunicate / $stakecommunicatedenom;
    $stakecommunicateavefm = number_format($stakecommunicateave, 0);
}
//echo" stakecommunicateave = $stakecommunicateave" ;


//Calculating teamcommunicate
if ($teamcommunicatedenom === 0) {
    $teamcommunicateave = '-';
    $teamcommunicateavefm = '-';
} else {
    $teamcommunicateave = 25 * $teamcommunicate / $teamcommunicatedenom;
    $teamcommunicateavefm = number_format($teamcommunicateave, 0);
}

// Calculating tandscommunicate for star-rating
$tandscommunicatedenom = $teamcommunicatedenom + $stakecommunicatedenom;
$tandscommunicate = $teamcommunicate + $stakecommunicate;
if ($tandscommunicatedenom === 0) {
    $communicatecomment = '';
} else {
    $tandscommunicateave = 25 * ($tandscommunicate / $tandscommunicatedenom);
    if ($tandscommunicateave >= 74.17) {
        $communicatehigh = 1;
        $communicatecomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
    } elseif ($tandscommunicateave >= 66.87 && $tandscommunicateave < 74.17) {
        $communicatehigh = 1;
        $communicatecomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
    } else {
        $communicatecomment = '';
    };
}

// Calculating teamvisionary
if (($teamfocusdenom === 0) && ($teamcommunicatedenom === 0)) {
    $teamvisionaryave = '-';
    $teamvisionaryavefm = '-';
}
if (($teamfocusdenom != 0) && ($teamcommunicatedenom === 0)) {
    $teamvisionaryave = $teamfocusave;
    $teamvisionaryavefm = $teamfocusavefm;
}
if (($teamfocusdenom === 0) && ($teamcommunicatedenom != 0)) {
    $teamvisionaryave = $teamcommunicateave;
    $teamvisionaryavefm = $teamcommunicateavefm;
}
if (($teamfocusdenom != 0) && ($teamcommunicatedenom != 0)) {
    $teamvisionaryave = 0.5 * ($teamfocusave + $teamcommunicateave);
    $teamvisionaryavefm = number_format($teamvisionaryave, 0);
}

// Calculating stakevisionary
if (($stakefocusdenom === 0) && ($stakecommunicatedenom === 0)) {
    $stakevisionaryave = '-';
    $stakevisionaryavefm = '-';
}
if (($stakefocusdenom != 0) && ($stakecommunicatedenom === 0)) {
    $stakevisionaryave = $stakefocusave;
    $stakevisionaryavefm = $stakefocusavefm;
}
if (($stakefocusdenom === 0) && ($stakecommunicatedenom != 0)) {
    $stakevisionaryave = $stakecommunicateave;
    $stakevisionaryavefm = $stakecommunicateavefm;
}
if (($stakefocusdenom != 0) && ($stakecommunicatedenom != 0)) {
    $stakevisionaryave = 0.5 * ($stakefocusave + $stakecommunicateave);
    $stakevisionaryavefm = number_format($stakevisionaryave, 0);
}

//Calculating tandsvisionary for star-rating
if (($tandsfocusdenom === 0) && ($tandscommunicatedenom === 0)) {

//just for the sake of calculation - actually it does not exist in reality
    $tandsvisionaryave = '-';
} elseif (($tandsfocusdenom != 0) && ($tandscommunicatedenom === 0)) {
    $tandsvisionaryave = $tandsfocusave;
} elseif (($tandsfocusdenom === 0) && ($tandscommunicatedenom != 0)) {
    $tandsvisionaryave = $tandscommunicateave;
} else {
$tandsvisionaryave = 0.5 * ($tandsfocusave + $tandscommunicateave);
}

if ( ($tandsvisionaryave != '-')   && ($tandsvisionaryave >= 70.02)  )  {
    $visionarycomment = "<img src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
} elseif  ( ($tandsvisionaryave != '-')   &&  ($tandsvisionaryave >= 63.60 && $tandsvisionaryave < 70.02) )      {
    $visionarycomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
} else {
    $visionarycomment = '';
};

//Query database for d7 averages
$d7result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d7) FROM teamdata WHERE  (((d7>=0) AND (d7<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d7score = mysqli_fetch_array($d7result) or die(mysqli_error());
if (!$d7score['AVG(d7)']) {
    $teamavgd7 = '-';
} else {
    $teamavgd7 = number_format(25 * $d7score['AVG(d7)'], 0);
    $teamsecure = $teamsecure + $d7score['AVG(d7)'];
    $teamsecuredenom++;
}
//echo "After d7 team secure = $teamsecure and teamsecuredenom = $teamsecuredenom<br />"  ;

$d7stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d7) FROM teamdata WHERE  (((d7>=0) AND (d7<=4))  AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d7stakescore = mysqli_fetch_array($d7stake) or die(mysqli_error());
if (!$d7stakescore['AVG(d7)']) {
    $stakeavgd7 = '-';
} else {
    $stakeavgd7 = number_format(25 * $d7stakescore['AVG(d7)'], 0);
    $stakesecure = $stakesecure + $d7stakescore['AVG(d7)'];
    $stakesecuredenom++;
}
//echo "After d7 stake secure = $stakesecure and stakesecuredenom = $stakesecuredenom<br />"  ;

//Query database for d8 averages
$d8result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d8) FROM teamdata WHERE  (((d8>=0) AND (d8<=4))  AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d8score = mysqli_fetch_array($d8result) or die(mysqli_error());
if (!$d8score['AVG(d8)']) {
    $teamavgd8 = '-';
} else {
    $teamavgd8 = number_format(25 * $d8score['AVG(d8)'], 0);
    $teamsecure = $teamsecure + $d8score['AVG(d8)'];
    $teamsecuredenom++;
}
//echo "After d8 team secure = $teamsecure and teamsecuredenom = $teamsecuredenom<br />"  ;

$d8stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d8) FROM teamdata WHERE  (((d8>=0) AND (d8<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d8stakescore = mysqli_fetch_array($d8stake) or die(mysqli_error());
if (!$d8stakescore['AVG(d8)']) {
    $stakeavgd8 = '-';
} else {
    $stakeavgd8 = number_format(25 * $d8stakescore['AVG(d8)'], 0);
    $stakesecure = $stakesecure + $d8stakescore['AVG(d8)'];
    $stakesecuredenom++;
}
//echo "After d8 stake secure = $stakesecure and stakesecuredenom = $stakesecuredenom<br />"  ;

//Query database for d9 averages
$d9result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d9) FROM teamdata WHERE  (((d9>=0) AND (d9<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d9score = mysqli_fetch_array($d9result) or die(mysqli_error());
if (!$d9score['AVG(d9)']) {
    $teamavgd9 = '-';
} else {
    $teamavgd9 = number_format(25 * $d9score['AVG(d9)'], 0);
    $teamsecure = $teamsecure + $d9score['AVG(d9)'];
    $teamsecuredenom++;
}
//echo "After d9 team secure = $teamsecure and teamsecuredenom = $teamsecuredenom<br />"  ;

$d9stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d9) FROM teamdata WHERE  (((d9>=0) AND (d9<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d9stakescore = mysqli_fetch_array($d9stake) or die(mysqli_error());
if (!$d9stakescore['AVG(d9)']) {
    $stakeavgd9 = '-';
} else {
    $stakeavgd9 = number_format(25 * $d9stakescore['AVG(d9)'], 0);
    $stakesecure = $stakesecure + $d9stakescore['AVG(d9)'];
    $stakesecuredenom++;
}
//echo "After d9 stake secure = $stakesecure and stakesecuredenom = $stakesecuredenom<br />"  ;

//Calculating stakesecure
if ($stakesecuredenom === 0) {
    $stakesecureave = '-';
    $stakesecureavefm = '-';
} else {
    $stakesecureave = 25 * $stakesecure / $stakesecuredenom;
    $stakesecureavefm = number_format($stakesecureave, 0);
}
//echo" stakesecureave = $stakesecureave" ;

//Calculating teamsecure
if ($teamsecuredenom === 0) {
    $teamsecureave = '-';
    $teamsecureavefm = '-';
} else {
    $teamsecureave = 25 * $teamsecure / $teamsecuredenom;
    $teamsecureavefm = number_format($teamsecureave, 0);
}

// Calculating tandssecure for star-rating
$tandssecuredenom = $teamsecuredenom + $stakesecuredenom;
$tandssecure = $teamsecure + $stakesecure;
if ($tandssecuredenom === 0) {
    $securecomment = '';
} else {
    $tandssecureave = 25 * ($tandssecure / $tandssecuredenom);
    if ($tandssecureave >= 72.56) {
        $securehigh = 1;
        $securecomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
    } elseif ($tandssecureave >= 65.26 && $tandssecureave < 72.56) {
        $securehigh = 1;
        $securecomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
    } else {
        $securecomment = '';
    };
}

//Query database for d10 averages
$d10result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d10) FROM teamdata WHERE  (((d10>=0) AND (d10<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d10score = mysqli_fetch_array($d10result) or die(mysqli_error());
if (!$d10score['AVG(d10)']) {
    $teamavgd10 = '-';
} else {
    $teamavgd10 = number_format(25 * $d10score['AVG(d10)'], 0);
    $teamorganize = $teamorganize + $d10score['AVG(d10)'];
    $teamorganizedenom++;
}
//echo "After d10 team organize = $teamorganize and teamorganizedenom = $teamorganizedenom<br />"  ;

$d10stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d10) FROM teamdata WHERE  (((d10>=0) AND (d10<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d10stakescore = mysqli_fetch_array($d10stake) or die(mysqli_error());
if (!$d10stakescore['AVG(d10)']) {
    $stakeavgd10 = '-';
} else {
    $stakeavgd10 = number_format(25 * $d10stakescore['AVG(d10)'], 0);
    $stakeorganize = $stakeorganize + $d10stakescore['AVG(d10)'];
    $stakeorganizedenom++;
}
//echo "After d10 stake organize = $stakeorganize and stakeorganizedenom = $stakeorganizedenom<br />"  ;

//Query database for d11 averages
$d11result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d11) FROM teamdata WHERE  (((d11>=0) AND (d11<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d11score = mysqli_fetch_array($d11result) or die(mysqli_error());
if (!$d11score['AVG(d11)']) {
    $teamavgd11 = '-';
} else {
    $teamavgd11 = number_format(25 * $d11score['AVG(d11)'], 0);
    $teamorganize = $teamorganize + $d11score['AVG(d11)'];
    $teamorganizedenom++;
}
//echo "After d11 team organize = $teamorganize and teamorganizedenom = $teamorganizedenom<br />"  ;

$d11stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d11) FROM teamdata WHERE  (((d11>=0) AND (d11<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d11stakescore = mysqli_fetch_array($d11stake) or die(mysqli_error());
if (!$d11stakescore['AVG(d11)']) {
    $stakeavgd11 = '-';
} else {
    $stakeavgd11 = number_format(25 * $d11stakescore['AVG(d11)'], 0);
    $stakeorganize = $stakeorganize + $d11stakescore['AVG(d11)'];
    $stakeorganizedenom++;
}
//echo "After d11 stake organize = $stakeorganize and stakeorganizedenom = $stakeorganizedenom<br />"  ;

//Query database for d12 averages
$d12result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d12) FROM teamdata WHERE  (((d12>=0) AND (d12<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d12score = mysqli_fetch_array($d12result) or die(mysqli_error());
if (!$d12score['AVG(d12)']) {
    $teamavgd12 = '-';
} else {
    $teamavgd12 = number_format(25 * $d12score['AVG(d12)'], 0);
    $teamorganize = $teamorganize + $d12score['AVG(d12)'];
    $teamorganizedenom++;
}
//echo "After d12 team organize = $teamorganize and teamorganizedenom = $teamorganizedenom<br />"  ;

$d12stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d12) FROM teamdata WHERE  (((d12>=0) AND (d12<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d12stakescore = mysqli_fetch_array($d12stake) or die(mysqli_error());
if (!$d12stakescore['AVG(d12)']) {
    $stakeavgd12 = '-';
} else {
    $stakeavgd12 = number_format(25 * $d12stakescore['AVG(d12)'], 0);
    $stakeorganize = $stakeorganize + $d12stakescore['AVG(d12)'];
    $stakeorganizedenom++;
}
//echo "After d12 stake organize = $stakeorganize and stakeorganizedenom = $stakeorganizedenom<br />"  ;

//Calculating stakeorganize
if ($stakeorganizedenom === 0) {
    $stakeorganizeave = '-';
    $stakeorganizeavefm = '-';
} else {
    $stakeorganizeave = 25 * $stakeorganize / $stakeorganizedenom;
    $stakeorganizeavefm = number_format($stakeorganizeave, 0);
}
//echo" stakeorganizeave = $stakeorganizeave" ;

//Calculating teamorganize
if ($teamorganizedenom === 0) {
    $teamorganizeave = '-';
    $teamorganizeavefm = '-';
} else {
    $teamorganizeave = 25 * $teamorganize / $teamorganizedenom;
    $teamorganizeavefm = number_format($teamorganizeave, 0);
}

// Calculating tandsorganize for star-rating
$tandsorganizedenom = $teamorganizedenom + $stakeorganizedenom;
$tandsorganize = $teamorganize + $stakeorganize;
if ($tandsorganizedenom === 0) {
    $organizecomment = '';
} else {
    $tandsorganizeave = 25 * ($tandsorganize / $tandsorganizedenom);
    if ($tandsorganizeave >= 72.86) {
        $organizehigh = 1;
        $organizecomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
    } elseif ($tandsorganizeave >= 65.56 && $tandsorganizeave < 72.86) {
        $organizehigh = 1;
        $organizecomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
    } else {
        $organizecomment = '';
    };
}

// Calculating teamguardian
if (($teamsecuredenom === 0) && ($teamorganizedenom === 0)) {
    $teamguardianave = '-';
    $teamguardianavefm = '-';
}
if (($teamsecuredenom != 0) && ($teamorganizedenom === 0)) {
    $teamguardianave = $teamsecureave;
    $teamguardianavefm = $teamsecureavefm;
}
if (($teamsecuredenom === 0) && ($teamorganizedenom != 0)) {
    $teamguardianave = $teamorganizeave;
    $teamguardianavefm = $teamorganizeavefm;
}
if (($teamsecuredenom != 0) && ($teamorganizedenom != 0)) {
    $teamguardianave = 0.5 * ($teamsecureave + $teamorganizeave);
    $teamguardianavefm = number_format($teamguardianave, 0);
}

// Calculating stakeguardian
if (($stakesecuredenom === 0) && ($stakeorganizedenom === 0)) {
    $stakeguardianave = '-';
    $stakeguardianavefm = '-';
}
if (($stakesecuredenom != 0) && ($stakeorganizedenom === 0)) {
    $stakeguardianave = $stakesecureave;
    $stakeguardianavefm = $stakesecureavefm;
}
if (($stakesecuredenom === 0) && ($stakeorganizedenom != 0)) {
    $stakeguardianave = $stakeorganizeave;
    $stakeguardianavefm = $stakeorganizeavefm;
}
if (($stakesecuredenom != 0) && ($stakeorganizedenom != 0)) {
    $stakeguardianave = 0.5 * ($stakesecureave + $stakeorganizeave);
    $stakeguardianavefm = number_format($stakeguardianave, 0);
}

//Calculating tandsguardian for star-rating
if (($tandssecuredenom === 0) && ($tandsorganizedenom === 0)) {

//just for the sake of calculation - actually it does not exist in reality
    $tandsguardianave = '-';
} elseif (($tandssecuredenom != 0) && ($tandsorganizedenom === 0)) {
    $tandsguardianave = $tandssecureave;
} elseif (($tandssecuredenom === 0) && ($tandsorganizedenom != 0)) {
    $tandsguardianave = $tandsorganizeave;
} else {
$tandsguardianave = 0.5 * ($tandssecureave + $tandsorganizeave);
}

if ( ($tandsguardianave != '-')   && ($tandsguardianave >= 70.82)  )  {
    $guardiancomment = "<img src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
} elseif  ( ($tandsguardianave != '-')   &&  ($tandsguardianave >= 64.41 && $tandsguardianave < 70.82) )      {
    $guardiancomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
} else {
    $guardiancomment = '';
};

//Query database for d13 averages
$d13result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d13) FROM teamdata WHERE  (((d13>=0) AND (d13<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d13score = mysqli_fetch_array($d13result) or die(mysqli_error());
if (!$d13score['AVG(d13)']) {
    $teamavgd13 = '-';
} else {
    $teamavgd13 = number_format(25 * $d13score['AVG(d13)'], 0);
    $teamunite = $teamunite + $d13score['AVG(d13)'];
    $teamunitedenom++;
}
//echo "After d13 team unite = $teamunite and teamunitedenom = $teamunitedenom<br />"  ;

$d13stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d13) FROM teamdata WHERE  (((d13>=0) AND (d13<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d13stakescore = mysqli_fetch_array($d13stake) or die(mysqli_error());
if (!$d13stakescore['AVG(d13)']) {
    $stakeavgd13 = '-';
} else {
    $stakeavgd13 = number_format(25 * $d13stakescore['AVG(d13)'], 0);
    $stakeunite = $stakeunite + $d13stakescore['AVG(d13)'];
    $stakeunitedenom++;
}
//echo "After d13 stake unite = $stakeunite and stakeunitedenom = $stakeunitedenom<br />"  ;

//Query database for d14 averages
$d14result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d14) FROM teamdata WHERE  (((d14>=0) AND (d14<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d14score = mysqli_fetch_array($d14result) or die(mysqli_error());
if (!$d14score['AVG(d14)']) {
    $teamavgd14 = '-';
} else {
    $teamavgd14 = number_format(25 * $d14score['AVG(d14)'], 0);
    $teamunite = $teamunite + $d14score['AVG(d14)'];
    $teamunitedenom++;
}
//echo "After d14 team unite = $teamunite and teamunitedenom = $teamunitedenom<br />"  ;

$d14stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d14) FROM teamdata WHERE  (((d14>=0) AND (d14<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d14stakescore = mysqli_fetch_array($d14stake) or die(mysqli_error());
if (!$d14stakescore['AVG(d14)']) {
    $stakeavgd14 = '-';
} else {
    $stakeavgd14 = number_format(25 * $d14stakescore['AVG(d14)'], 0);
    $stakeunite = $stakeunite + $d14stakescore['AVG(d14)'];
    $stakeunitedenom++;
}
//echo "After d14 stake unite = $stakeunite and stakeunitedenom = $stakeunitedenom<br />"  ;

//Query database for d15 averages
$d15result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d15) FROM teamdata WHERE  (((d15>=0) AND (d15<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d15score = mysqli_fetch_array($d15result) or die(mysqli_error());
if (!$d15score['AVG(d15)']) {
    $teamavgd15 = '-';
} else {
    $teamavgd15 = number_format(25 * $d15score['AVG(d15)'], 0);
    $teamunite = $teamunite + $d15score['AVG(d15)'];
    $teamunitedenom++;
}
//echo "After d15 team unite = $teamunite and teamunitedenom = $teamunitedenom<br />"  ;

$d15stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d15) FROM teamdata WHERE  (((d15>=0) AND (d15<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d15stakescore = mysqli_fetch_array($d15stake) or die(mysqli_error());
if (!$d15stakescore['AVG(d15)']) {
    $stakeavgd15 = '-';
} else {
    $stakeavgd15 = number_format(25 * $d15stakescore['AVG(d15)'], 0);
    $stakeunite = $stakeunite + $d15stakescore['AVG(d15)'];
    $stakeunitedenom++;
}
//echo "After d15 stake unite = $stakeunite and stakeunitedenom = $stakeunitedenom<br />"  ;

//Calculating stakeunite
if ($stakeunitedenom === 0) {
    $stakeuniteave = '-';
    $stakeuniteavefm = '-';
} else {
    $stakeuniteave = 25 * $stakeunite / $stakeunitedenom;
    $stakeuniteavefm = number_format($stakeuniteave, 0);
}
//echo" stakeuniteave = $stakeuniteave" ;

//Calculating teamunite
if ($teamunitedenom === 0) {
    $teamuniteave = '-';
    $teamuniteavefm = '-';
} else {
    $teamuniteave = 25 * $teamunite / $teamunitedenom;
    $teamuniteavefm = number_format($teamuniteave, 0);
}

// Calculating tandsunite for star-rating
$tandsunitedenom = $teamunitedenom + $stakeunitedenom;
$tandsunite = $teamunite + $stakeunite;
if ($tandsunitedenom === 0) {
    $unitecomment = '';
} else {
    $tandsuniteave = 25 * ($tandsunite / $tandsunitedenom);
    if ($tandsuniteave >= 75.28) {
        $unitehigh = 1;
        $unitecomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
    } elseif ($tandsuniteave >= 67.98 && $tandsuniteave < 75.28) {
        $unitehigh = 1;
        $unitecomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
    } else {
        $unitecomment = '';
    };
}

//echo" teamuniteave = $teamuniteave<br /><br />" ;
//Query database for d16 averages
$d16result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d16) FROM teamdata WHERE  (((d16>=0) AND (d16<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d16score = mysqli_fetch_array($d16result) or die(mysqli_error());
if (!$d16score['AVG(d16)']) {
    $teamavgd16 = '-';
} else {
    $teamavgd16 = number_format(25 * $d16score['AVG(d16)'], 0);
    $teamempower = $teamempower + $d16score['AVG(d16)'];
    $teamempowerdenom++;
}
//echo "After d16 team empower = $teamempower and teamempowerdenom = $teamempowerdenom<br />"  ;

$d16stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d16) FROM teamdata WHERE  (((d16>=0) AND (d16<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d16stakescore = mysqli_fetch_array($d16stake) or die(mysqli_error());
if (!$d16stakescore['AVG(d16)']) {
    $stakeavgd16 = '-';
} else {
    $stakeavgd16 = number_format(25 * $d16stakescore['AVG(d16)'], 0);
    $stakeempower = $stakeempower + $d16stakescore['AVG(d16)'];
    $stakeempowerdenom++;
}
//echo "After d16 stake empower = $stakeempower  and stakeempowerdenom = $stakeempowerdenom<br />"  ;

//Query database for d17 averages
$d17result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d17) FROM teamdata WHERE  (((d17>=0) AND (d17<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d17score = mysqli_fetch_array($d17result) or die(mysqli_error());
if (!$d17score['AVG(d17)']) {
    $teamavgd17 = '-';
} else {
    $teamavgd17 = number_format(25 * $d17score['AVG(d17)'], 0);
    $teamempower = $teamempower + $d17score['AVG(d17)'];
    $teamempowerdenom++;
}
//echo "After d17 team empower = $teamempower and teamempowerdenom = $teamempowerdenom<br />"  ;

$d17stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d17) FROM teamdata WHERE  (((d17>=0) AND (d17<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d17stakescore = mysqli_fetch_array($d17stake) or die(mysqli_error());
if (!$d17stakescore['AVG(d17)']) {
    $stakeavgd17 = '-';
} else {
    $stakeavgd17 = number_format(25 * $d17stakescore['AVG(d17)'], 0);
    $stakeempower = $stakeempower + $d17stakescore['AVG(d17)'];
    $stakeempowerdenom++;
}
//echo "After d17 stake empower = $stakeempower  and stakeempowerdenom = $stakeempowerdenom<br />"  ;

//Query database for d18 averages
$d18result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d18) FROM teamdata WHERE  (((d18>=0) AND (d18<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d18score = mysqli_fetch_array($d18result) or die(mysqli_error());
if (!$d18score['AVG(d18)']) {
    $teamavgd18 = '-';
} else {
    $teamavgd18 = number_format(25 * $d18score['AVG(d18)'], 0);
    $teamempower = $teamempower + $d18score['AVG(d18)'];
    $teamempowerdenom++;
}
//echo "After d18 team empower = $teamempower and teamempowerdenom = $teamempowerdenom<br />"  ;

$d18stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d18) FROM teamdata WHERE  (((d18>=0) AND (d18<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d18stakescore = mysqli_fetch_array($d18stake) or die(mysqli_error());
if (!$d18stakescore['AVG(d18)']) {
    $stakeavgd18 = '-';
} else {
    $stakeavgd18 = number_format(25 * $d18stakescore['AVG(d18)'], 0);
    $stakeempower = $stakeempower + $d18stakescore['AVG(d18)'];
    $stakeempowerdenom++;
}
//echo "After d18 stake empower = $stakeempower  and stakeempowerdenom = $stakeempowerdenom<br />"  ;

//Query database for d19 averages
$d19result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d19) FROM teamdata WHERE  (((d19>=0) AND (d19<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d19score = mysqli_fetch_array($d19result) or die(mysqli_error());
if (!$d19score['AVG(d19)']) {
    $teamavgd19 = '-';
} else {
    $teamavgd19 = number_format(25 * $d19score['AVG(d19)'], 0);
    $teamdeliver = $teamdeliver + $d19score['AVG(d19)'];
    $teamdeliverdenom++;
}
//echo "After d19 team deliver = $teamdeliver and teamdeliverdenom = $teamdeliverdenom<br />"  ;

$d19stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d19) FROM teamdata WHERE  (((d19>=0) AND (d19<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d19stakescore = mysqli_fetch_array($d19stake) or die(mysqli_error());
if (!$d19stakescore['AVG(d19)']) {
    $stakeavgd19 = '-';
} else {
    $stakeavgd19 = number_format(25 * $d19stakescore['AVG(d19)'], 0);
    $stakedeliver = $stakedeliver + $d19stakescore['AVG(d19)'];
    $stakedeliverdenom++;
}
//echo "After d19 stake deliver = $stakedeliver  and stakedeliverdenom = $stakedeliverdenom<br />"  ;

//Query database for d20 averages
$d20result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d20) FROM teamdata WHERE  (((d20>=0) AND (d20<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d20score = mysqli_fetch_array($d20result) or die(mysqli_error());
if (!$d20score['AVG(d20)']) {
    $teamavgd20 = '-';
} else {
    $teamavgd20 = number_format(25 * $d20score['AVG(d20)'], 0);
    $teamdeliver = $teamdeliver + $d20score['AVG(d20)'];
    $teamdeliverdenom++;
}
//echo "After d20 team deliver = $teamdeliver and teamdeliverdenom = $teamdeliverdenom<br />"  ;

$d20stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d20) FROM teamdata WHERE  (((d20>=0) AND (d20<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d20stakescore = mysqli_fetch_array($d20stake) or die(mysqli_error());
if (!$d20stakescore['AVG(d20)']) {
    $stakeavgd20 = '-';
} else {
    $stakeavgd20 = number_format(25 * $d20stakescore['AVG(d20)'], 0);
    $stakedeliver = $stakedeliver + $d20stakescore['AVG(d20)'];
    $stakedeliverdenom++;
}
//echo "After d20 stake deliver = $stakedeliver  and stakedeliverdenom = $stakedeliverdenom<br />"  ;

//Query database for d21 averages
$d21result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d21) FROM teamdata WHERE  (((d21>=0) AND (d21<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d21score = mysqli_fetch_array($d21result) or die(mysqli_error());
if (!$d21score['AVG(d21)']) {
    $teamavgd21 = '-';
} else {
    $teamavgd21 = number_format(25 * $d21score['AVG(d21)'], 0);
    $teamdeliver = $teamdeliver + $d21score['AVG(d21)'];
    $teamdeliverdenom++;
}
//echo "After d21 team deliver = $teamdeliver and teamdeliverdenom = $teamdeliverdenom<br />"  ;

$d21stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d21) FROM teamdata WHERE  (((d21>=0) AND (d21<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d21stakescore = mysqli_fetch_array($d21stake) or die(mysqli_error());
if (!$d21stakescore['AVG(d21)']) {
    $stakeavgd21 = '-';
} else {
    $stakeavgd21 = number_format(25 * $d21stakescore['AVG(d21)'], 0);
    $stakedeliver = $stakedeliver + $d21stakescore['AVG(d21)'];
    $stakedeliverdenom++;
}
//echo "After d21 stake deliver = $stakedeliver  and stakedeliverdenom = $stakedeliverdenom<br />"  ;

//Query database for d22 averages
$d22result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d22) FROM teamdata WHERE  (((d22>=0) AND (d22<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d22score = mysqli_fetch_array($d22result) or die(mysqli_error());
if (!$d22score['AVG(d22)']) {
    $teamavgd22 = '-';
} else {
    $teamavgd22 = number_format(25 * $d22score['AVG(d22)'], 0);
    $teamimprove = $teamimprove + $d22score['AVG(d22)'];
    $teamimprovedenom++;
}
//echo "After d22 team improve = $teamimprove and teamimprovedenom = $teamimprovedenom<br />"  ;

$d22stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d22) FROM teamdata WHERE  (((d22>=0) AND (d22<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d22stakescore = mysqli_fetch_array($d22stake) or die(mysqli_error());
if (!$d22stakescore['AVG(d22)']) {
    $stakeavgd22 = '-';
} else {
    $stakeavgd22 = number_format(25 * $d22stakescore['AVG(d22)'], 0);
    $stakeimprove = $stakeimprove + $d22stakescore['AVG(d22)'];
    $stakeimprovedenom++;
}
//echo "After d22 stake improve = $stakeimprove  and stakeimprovedenom = $stakeimprovedenom<br />"  ;

//Query database for d23 averages
$d23result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d23) FROM teamdata WHERE  (((d23>=0) AND (d23<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d23score = mysqli_fetch_array($d23result) or die(mysqli_error());
if (!$d23score['AVG(d23)']) {
    $teamavgd23 = '-';
} else {
    $teamavgd23 = number_format(25 * $d23score['AVG(d23)'], 0);
    $teamimprove = $teamimprove + $d23score['AVG(d23)'];
    $teamimprovedenom++;
}
//echo "After d23 team improve = $teamimprove and teamimprovedenom = $teamimprovedenom<br />"  ;

$d23stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d23) FROM teamdata WHERE  (((d23>=0) AND (d23<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d23stakescore = mysqli_fetch_array($d23stake) or die(mysqli_error());
if (!$d23stakescore['AVG(d23)']) {
    $stakeavgd23 = '-';
} else {
    $stakeavgd23 = number_format(25 * $d23stakescore['AVG(d23)'], 0);
    $stakeimprove = $stakeimprove + $d23stakescore['AVG(d23)'];
    $stakeimprovedenom++;
}
//echo "After d23 stake improve = $stakeimprove  and stakeimprovedenom = $stakeimprovedenom<br />"  ;

//Query database for d24 averages
$d24result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d24) FROM teamdata WHERE  (((d24>=0) AND (d24<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d24score = mysqli_fetch_array($d24result) or die(mysqli_error());
if (!$d24score['AVG(d24)']) {
    $teamavgd24 = '-';
} else {
    $teamavgd24 = number_format(25 * $d24score['AVG(d24)'], 0);
    $teamimprove = $teamimprove + $d24score['AVG(d24)'];
    $teamimprovedenom++;
}
//echo "After d24 team improve = $teamimprove and teamimprovedenom = $teamimprovedenom<br />"  ;

$d24stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d24) FROM teamdata WHERE  (((d24>=0) AND (d24<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d24stakescore = mysqli_fetch_array($d24stake) or die(mysqli_error());
if (!$d24stakescore['AVG(d24)']) {
    $stakeavgd24 = '-';
} else {
    $stakeavgd24 = number_format(25 * $d24stakescore['AVG(d24)'], 0);
    $stakeimprove = $stakeimprove + $d24stakescore['AVG(d24)'];
    $stakeimprovedenom++;
}
//echo "After d24 stake improve = $stakeimprove  and stakeimprovedenom = $stakeimprovedenom<br />"  ;

//Calculating stakeimprove
if ($stakeimprovedenom === 0) {
    $stakeimproveave = '-';
    $stakeimproveavefm = '-';
} else {
    $stakeimproveave = 25 * $stakeimprove / $stakeimprovedenom;
    $stakeimproveavefm = number_format($stakeimproveave, 0);
}
//echo" stakeimproveave = $stakeimproveave" ;


//Calculating teamimprove
if ($teamimprovedenom === 0) {
    $teamimproveave = '-';
    $teamimproveavefm = '-';
} else {
    $teamimproveave = 25 * $teamimprove / $teamimprovedenom;
    $teamimproveavefm = number_format($teamimproveave, 0);
}

// Calculating tandsimprove for star-rating
$tandsimprovedenom = $teamimprovedenom + $stakeimprovedenom;
$tandsimprove = $teamimprove + $stakeimprove;
if ($tandsimprovedenom === 0) {
    $improvecomment = '';
} else {
    $tandsimproveave = 25 * ($tandsimprove / $tandsimprovedenom);
    if ($tandsimproveave >= 73.52) {
        $improvehigh = 1;
        $improvecomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
    } elseif ($tandsimproveave >= 66.62 && $tandsimproveave < 73.52) {
        $improvehigh = 1;
        $improvecomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
    } else {
        $improvecomment = '';
    };
}

//Query database for d25 averages
$d25result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d25) FROM teamdata WHERE  (((d25>=0) AND (d25<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d25score = mysqli_fetch_array($d25result) or die(mysqli_error());
if (!$d25score['AVG(d25)']) {
    $teamavgd25 = '-';
} else {
    $teamavgd25 = number_format(25 * $d25score['AVG(d25)'], 0);
    $teamdeliver = $teamdeliver + $d25score['AVG(d25)'];
    $teamdeliverdenom++;
}
//echo "After d25 team deliver = $teamdeliver and teamdeliverdenom = $teamdeliverdenom<br />"  ;

$d25stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d25) FROM teamdata WHERE  (((d25>=0) AND (d25<=4))  AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d25stakescore = mysqli_fetch_array($d25stake) or die(mysqli_error());
if (!$d25stakescore['AVG(d25)']) {
    $stakeavgd25 = '-';
} else {
    $stakeavgd25 = number_format(25 * $d25stakescore['AVG(d25)'], 0);
    $stakedeliver = $stakedeliver + $d25stakescore['AVG(d25)'];
    $stakedeliverdenom++;
}
//echo "After d25 stake deliver = $stakedeliver  and stakedeliverdenom = $stakedeliverdenom<br />"  ;

//Calculating stakedeliver
if ($stakedeliverdenom === 0) {
    $stakedeliverave = '-';
    $stakedeliveravefm = '-';
} else {
    $stakedeliverave = 25 * $stakedeliver / $stakedeliverdenom;
    $stakedeliveravefm = number_format($stakedeliverave, 0);
}
//echo" stakedeliverave = $stakedeliverave" ;

//Calculating teamdeliver
if ($teamdeliverdenom === 0) {
    $teamdeliverave = '-';
    $teamdeliveravefm = '-';
} else {
    $teamdeliverave = 25 * $teamdeliver / $teamdeliverdenom;
    $teamdeliveravefm = number_format($teamdeliverave, 0);
}

// Calculating tandsdeliver for star-rating
$tandsdeliverdenom = $teamdeliverdenom + $stakedeliverdenom;
$tandsdeliver = $teamdeliver + $stakedeliver;
if ($tandsdeliverdenom === 0) {
    $delivercomment = '';
} else {
    $tandsdeliverave = 25 * ($tandsdeliver / $tandsdeliverdenom);
    if ($tandsdeliverave >= 68.42) {
        $deliverhigh = 1;
        $delivercomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
    } elseif ($tandsdeliverave >= 61.11 && $tandsdeliverave < 68.42) {
        $deliverhigh = 1;
        $delivercomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
    } else {
        $delivercomment = '';
    };
}

// Calculating teamdirector
if (($teamdeliverdenom === 0) && ($teamimprovedenom === 0)) {
    $teamdirectorave = '-';
    $teamdirectoravefm = '-';
}
if (($teamdeliverdenom != 0) && ($teamimprovedenom === 0)) {
    $teamdirectorave = $teamdeliverave;
    $teamdirectoravefm = $teamdeliveravefm;
}
if (($teamdeliverdenom === 0) && ($teamimprovedenom != 0)) {
    $teamdirectorave = $teamimproveave;
    $teamdirectoravefm = $teamimproveavefm;
}
if (($teamdeliverdenom != 0) && ($teamimprovedenom != 0)) {
    $teamdirectorave = 0.5 * ($teamdeliverave + $teamimproveave);
    $teamdirectoravefm = number_format($teamdirectorave, 0);
}

// Calculating stakedirector
if (($stakedeliverdenom === 0) && ($stakeimprovedenom === 0)) {
    $stakedirectorave = '-';
    $stakedirectoravefm = '-';
}
if (($stakedeliverdenom != 0) && ($stakeimprovedenom === 0)) {
    $stakedirectorave = $stakedeliverave;
    $stakedirectoravefm = $stakedeliveravefm;
}
if (($stakedeliverdenom === 0) && ($stakeimprovedenom != 0)) {
    $stakedirectorave = $stakeimproveave;
    $stakedirectoravefm = $stakeimproveavefm;
}
if (($stakedeliverdenom != 0) && ($stakeimprovedenom != 0)) {
    $stakedirectorave = 0.5 * ($stakedeliverave + $stakeimproveave);
    $stakedirectoravefm = number_format($stakedirectorave, 0);
}

//Calculating tandsdirector for star-rating
if (($tandsdeliverdenom === 0) && ($tandsimprovedenom === 0)) {

//just for the sake of calculation - actually it does not exist in reality
    $tandsdirectorave = '-';
} elseif (($tandsdeliverdenom != 0) && ($tandsimprovedenom === 0)) {
    $tandsdirectorave = $tandsdeliverave;
} elseif (($tandsdeliverdenom === 0) && ($tandsimprovedenom != 0)) {
    $tandsdirectorave = $tandsimproveave;
} else {
$tandsdirectorave = 0.5 * ($tandsdeliverave + $tandsimproveave);
}

if ( ($tandsdirectorave != '-')   && ($tandsdirectorave >= 69.08)  )  {
    $directorcomment = "<img src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
} elseif  ( ($tandsdirectorave != '-')   &&  ($tandsdirectorave >= 62.67 && $tandsdirectorave < 69.08) )      {
    $directorcomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
} else {
    $directorcomment = '';
}

//Query database for d26 averages
$d26result = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d26) FROM teamdata WHERE  (((d26>=0) AND (d26<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=1) OR (teamrole=2)))");
$d26score = mysqli_fetch_array($d26result) or die(mysqli_error());
if (!$d26score['AVG(d26)']) {
    $teamavgd26 = '-';
} else {
    $teamavgd26 = number_format(25 * $d26score['AVG(d26)'], 0);
    $teamempower = $teamempower + $d26score['AVG(d26)'];
    $teamempowerdenom++;
}
//echo "After d26 team empower = $teamempower and teamempowerdenom = $teamempowerdenom<br />"  ;

$d26stake = mysqli_query($dbc, "SELECT  teamsurveyid, AVG(d26) FROM teamdata WHERE  (((d26>=0) AND (d26<=4)) AND teamsurveyid = '$reportrequested' AND ((teamrole=3) OR (teamrole=4)  OR (teamrole=99)))");
$d26stakescore = mysqli_fetch_array($d26stake) or die(mysqli_error());
if (!$d26stakescore['AVG(d26)']) {
    $stakeavgd26 = '-';
} else {
    $stakeavgd26 = number_format(25 * $d26stakescore['AVG(d26)'], 0);
    $stakeempower = $stakeempower + $d26stakescore['AVG(d26)'];
    $stakeempowerdenom++;
}
//echo "After d26 stake empower = $stakeempower  and stakeempowerdenom = $stakeempowerdenom<br />"  ;

// Calculating 8 facet averages

//Calculating stakeempower
if ($stakeempowerdenom === 0) {
    $stakeempowerave = '-';
    $stakeempoweravefm = '-';
} else {
    $stakeempowerave = 25 * $stakeempower / $stakeempowerdenom;
    $stakeempoweravefm = number_format($stakeempowerave, 0);
}
//echo" stakeempowerave = $stakeempowerave" ;

//Calculating teamempower
if ($teamempowerdenom === 0) {
    $teamempowerave = '-';
    $teamempoweravefm = '-';
} else {
    $teamempowerave = 25 * $teamempower / $teamempowerdenom;
    $teamempoweravefm = number_format($teamempowerave, 0);
}

// Calculating tandsempower for star-rating
$tandsempowerdenom = $teamempowerdenom + $stakeempowerdenom;
$tandsempower = $teamempower + $stakeempower;
if ($tandsempowerdenom === 0) {
    $empowercomment = '';
} else {
    $tandsempowerave = 25 * ($tandsempower / $tandsempowerdenom);
    if ($tandsempowerave >= 78.07) {
        $empowerhigh = 1;
        $empowercomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
    } elseif ($tandsempowerave >= 70.76 && $tandsempowerave < 78.07) {
        $empowerhigh = 1;
        $empowercomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
    } else {
        $empowercomment = '';
    };
}

// Calculating teampsychologist
if (($teamunitedenom === 0) && ($teamempowerdenom === 0)) {
    $teampsychologistave = '-';
    $teampsychologistavefm = '-';
}
if (($teamunitedenom != 0) && ($teamempowerdenom === 0)) {
    $teampsychologistave = $teamuniteave;
    $teampsychologistavefm = $teamuniteavefm;
}
if (($teamunitedenom === 0) && ($teamempowerdenom != 0)) {
    $teampsychologistave = $teamempowerave;
    $teampsychologistavefm = $teamempoweravefm;
}
if (($teamunitedenom != 0) && ($teamempowerdenom != 0)) {
    $teampsychologistave = 0.5 * ($teamuniteave + $teamempowerave);
    $teampsychologistavefm = number_format($teampsychologistave, 0);
}

// Calculating stakepsychologist
if (($stakeunitedenom === 0) && ($stakeempowerdenom === 0)) {
    $stakepsychologistave = '-';
    $stakepsychologistavefm = '-';
}
if (($stakeunitedenom != 0) && ($stakeempowerdenom === 0)) {
    $stakepsychologistave = $stakeuniteave;
    $stakepsychologistavefm = $stakeuniteavefm;
}
if (($stakeunitedenom === 0) && ($stakeempowerdenom != 0)) {
    $stakepsychologistave = $stakeempowerave;
    $stakepsychologistavefm = $stakeempoweravefm;
}
if (($stakeunitedenom != 0) && ($stakeempowerdenom != 0)) {
    $stakepsychologistave = 0.5 * ($stakeuniteave + $stakeempowerave);
    $stakepsychologistavefm = number_format($stakepsychologistave, 0);
}

//Calculating tandspsychologist for star-rating
if (($tandsunitedenom === 0) && ($tandsempowerdenom === 0)) {

//just for the sake of calculation - actually it does not exist in reality
    $tandspsychologistave = '-';
} elseif (($tandsunitedenom != 0) && ($tandsempowerdenom === 0)) {
    $tandspsychologistave = $tandsuniteave;
} elseif (($tandsunitedenom === 0) && ($tandsempowerdenom != 0)) {
    $tandspsychologistave = $tandsempowerave;
} else {
$tandspsychologistave = 0.5 * ($tandsuniteave + $tandsempowerave);
}

if ( ($tandspsychologistave != '-')   && ($tandspsychologistave >= 74.49)  )  {
    $psychologistcomment = "<img src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
} elseif  ( ($tandspsychologistave != '-')   &&  ($tandspsychologistave >= 68.38 && $tandspsychologistave < 74.49) )      {
    $psychologistcomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
} else {
    $psychologistcomment = '';
}


// Calculating grand total averages for Headlines page


if (($teamvisionaryave === '-') && ($teamguardianave === '-') && ($teampsychologistave === '-') && ($teamdirectorave === '-')) {
    $teamgrandave = '-';
    $teamgrandavefm = '-';
}
if (($teamvisionaryave != '-') && ($teamguardianave != '-') && ($teampsychologistave != '-') && ($teamdirectorave != '-')) {
    $teamgrandave = ($teamvisionaryave + $teamguardianave + $teampsychologistave + $teamdirectorave) / 4;
    $teamgrandavefm = number_format($teamgrandave, 0);
}

if (($teamvisionaryave != '-') && ($teamguardianave === '-') && ($teampsychologistave === '-') && ($teamdirectorave === '-')) {
    $teamgrandave = $teamvisionaryave;
    $teamgrandavefm = number_format($teamgrandave, 0);
}
if (($teamvisionaryave === '-') && ($teamguardianave != '-') && ($teampsychologistave === '-') && ($teamdirectorave === '-')) {
    $teamgrandave = $teamguardianave;
    $teamgrandavefm = number_format($teamgrandave, 0);
}
if (($teamvisionaryave === '-') && ($teamguardianave === '-') && ($teampsychologistave != '-') && ($teamdirectorave === '-')) {
    $teamgrandave = $teampsychologistave;
    $teamgrandavefm = number_format($teamgrandave, 0);
}
if (($teamvisionaryave === '-') && ($teamguardianave === '-') && ($teampsychologistave === '-') && ($teamdirectorave != '-')) {
    $teamgrandave = $teamdirectorave;
    $teamgrandavefm = number_format($teamgrandave, 0);
}

if (($teamvisionaryave != '-') && ($teamguardianave != '-') && ($teampsychologistave === '-') && ($teamdirectorave === '-')) {
    $teamgrandave = ($teamvisionaryave + $teamguardianave) / 2;
    $teamgrandavefm = number_format($teamgrandave, 0);
}
if (($teamvisionaryave != '-') && ($teamguardianave === '-') && ($teampsychologistave != '-') && ($teamdirectorave === '-')) {
    $teamgrandave = ($teamvisionaryave + $teampsychologistave) / 2;
    $teamgrandavefm = number_format($teamgrandave, 0);
}
if (($teamvisionaryave != '-') && ($teamguardianave === '-') && ($teampsychologistave === '-') && ($teamdirectorave != '-')) {
    $teamgrandave = ($teamvisionaryave + $teamdirectorave) / 2;
    $teamgrandavefm = number_format($teamgrandave, 0);
}
if (($teamvisionaryave === '-') && ($teamguardianave != '-') && ($teampsychologistave != '-') && ($teamdirectorave === '-')) {
    $teamgrandave = ($teamguardianave + $teampsychologistave) / 2;
    $teamgrandavefm = number_format($teamgrandave, 0);
}
if (($teamvisionaryave === '-') && ($teamguardianave != '-') && ($teampsychologistave === '-') && ($teamdirectorave != '-')) {
    $teamgrandave = ($teamguardianave + $teamdirectorave) / 2;
    $teamgrandavefm = number_format($teamgrandave, 0);
}
if (($teamvisionaryave === '-') && ($teamguardianave === '-') && ($teampsychologistave != '-') && ($teamdirectorave != '-')) {
    $teamgrandave = ($teampsychologistave + $teamdirectorave) / 2;
    $teamgrandavefm = number_format($teamgrandave, 0);
}

if (($teamvisionaryave != '-') && ($teamguardianave != '-') && ($teampsychologistave != '-') && ($teamdirectorave === '-')) {
    $teamgrandave = ($teamvisionaryave + $teamguardianave + $teampsychologistave) / 3;
    $teamgrandavefm = number_format($teamgrandave, 0);
}
if (($teamvisionaryave != '-') && ($teamguardianave != '-') && ($teampsychologistave === '-') && ($teamdirectorave != '-')) {
    $teamgrandave = ($teamvisionaryave + $teamguardianave + $teamdirectorave) / 3;
    $teamgrandavefm = number_format($teamgrandave, 0);
}
if (($teamvisionaryave != '-') && ($teamguardianave === '-') && ($teampsychologistave != '-') && ($teamdirectorave != '-')) {
    $teamgrandave = ($teamvisionaryave + $teampsychologistave + $teamdirectorave) / 3;
    $teamgrandavefm = number_format($teamgrandave, 0);
}
if (($teamvisionaryave === '-') && ($teamguardianave != '-') && ($teampsychologistave != '-') && ($teamdirectorave != '-')) {
    $teamgrandave = ($teamguardianave + $teampsychologistave + $teamdirectorave) / 3;
    $teamgrandavefm = number_format($teamgrandave, 0);
}


if (($stakevisionaryave === '-') && ($stakeguardianave === '-') && ($stakepsychologistave === '-') && ($stakedirectorave === '-')) {
    $stakegrandave = '-';
    $stakegrandavefm = '-';
}
if (($stakevisionaryave != '-') && ($stakeguardianave != '-') && ($stakepsychologistave != '-') && ($stakedirectorave != '-')) {
    $stakegrandave = ($stakevisionaryave + $stakeguardianave + $stakepsychologistave + $stakedirectorave) / 4;
    $stakegrandavefm = number_format($stakegrandave, 0);
}

if (($stakevisionaryave != '-') && ($stakeguardianave === '-') && ($stakepsychologistave === '-') && ($stakedirectorave === '-')) {
    $stakegrandave = $stakevisionaryave;
    $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave === '-') && ($stakeguardianave != '-') && ($stakepsychologistave === '-') && ($stakedirectorave === '-')) {
    $stakegrandave = $stakeguardianave;
    $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave === '-') && ($stakeguardianave === '-') && ($stakepsychologistave != '-') && ($stakedirectorave === '-')) {
    $stakegrandave = $stakepsychologistave;
    $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave === '-') && ($stakeguardianave === '-') && ($stakepsychologistave === '-') && ($stakedirectorave != '-')) {
    $stakegrandave = $stakedirectorave;
    $stakegrandavefm = number_format($stakegrandave, 0);
}

if (($stakevisionaryave != '-') && ($stakeguardianave != '-') && ($stakepsychologistave === '-') && ($stakedirectorave === '-')) {
    $stakegrandave = ($stakevisionaryave + $stakeguardianave) / 2;
    $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave != '-') && ($stakeguardianave === '-') && ($stakepsychologistave != '-') && ($stakedirectorave === '-')) {
    $stakegrandave = ($stakevisionaryave + $stakepsychologistave) / 2;
    $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave != '-') && ($stakeguardianave === '-') && ($stakepsychologistave === '-') && ($stakedirectorave != '-')) {
    $stakegrandave = ($stakevisionaryave + $stakedirectorave) / 2;
    $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave === '-') && ($stakeguardianave != '-') && ($stakepsychologistave != '-') && ($stakedirectorave === '-')) {
    $stakegrandave = ($stakeguardianave + $stakepsychologistave) / 2;
    $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave === '-') && ($stakeguardianave != '-') && ($stakepsychologistave === '-') && ($stakedirectorave != '-')) {
    $stakegrandave = ($stakeguardianave + $stakedirectorave) / 2;
    $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave === '-') && ($stakeguardianave === '-') && ($stakepsychologistave != '-') && ($stakedirectorave != '-')) {
    $stakegrandave = ($stakepsychologistave + $stakedirectorave) / 2;
    $stakegrandavefm = number_format($stakegrandave, 0);
}

if (($stakevisionaryave != '-') && ($stakeguardianave != '-') && ($stakepsychologistave != '-') && ($stakedirectorave === '-')) {
    $stakegrandave = ($stakevisionaryave + $stakeguardianave + $stakepsychologistave) / 3;
    $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave != '-') && ($stakeguardianave != '-') && ($stakepsychologistave === '-') && ($stakedirectorave != '-')) {
    $stakegrandave = ($stakevisionaryave + $stakeguardianave + $stakedirectorave) / 3;
    $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave != '-') && ($stakeguardianave === '-') && ($stakepsychologistave != '-') && ($stakedirectorave != '-')) {
    $stakegrandave = ($stakevisionaryave + $stakepsychologistave + $stakedirectorave) / 3;
    $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave === '-') && ($stakeguardianave != '-') && ($stakepsychologistave != '-') && ($stakedirectorave != '-')) {
    $stakegrandave = ($stakeguardianave + $stakepsychologistave + $stakedirectorave) / 3;
    $stakegrandavefm = number_format($stakegrandave, 0);
}
//echo "Stakegrandave= $stakegrandave ";

//Calculating tandsgrandave for ultimate star-rating

if ($teamgrandave === '-' && $stakegrandave === '-' ) {
$tandsgrandave = '-';
$tandsgrandavefm = $tandsgrandave;
$grandcomment = '';
} elseif ($teamgrandave != '-' && $stakegrandave === '-' ) {
$tandsgrandave = $teamgrandave;
$tandsgrandavefm = number_format($tandsgrandave, 0);
} elseif ($teamgrandave === '-' && $stakegrandave != '-' ) {
$tandsgrandave = $stakegrandave;
$tandsgrandavefm = number_format($tandsgrandave, 0);
} else {
$tandsgrandave = 0.5*($teamgrandave + $stakegrandave);
$tandsgrandavefm = number_format($tandsgrandave, 0);
}

if (($tandsgrandave != '-') && ($tandsgrandave >= 68.81)) {
    $grandcomment = "<img src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
} elseif (($tandsgrandave != '-') && ($tandsgrandave >= 63.52 && $tandsgrandave < 68.81)) {
    $grandcomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
} else {
    $grandcomment = '';
}


$totalhighs = $focushigh + $communicatehigh + $securehigh + $organizehigh + $unitehigh + $empowerhigh + $deliverhigh + $improvehigh;

//echo "tandsgrandave= $tandsgrandave ";

// Database average calculations
// Make these inoperable and use initialization values if program runs too slow
/*
$d1db = mysqli_query($dbc, "SELECT  AVG(d1) FROM teamdata WHERE  (((d1>=0) AND (d1<=4)))");
$d1dbave = mysqli_fetch_array($d1db) or die(mysqli_error());
if (!$d1dbave['AVG(d1)']) {
    $dbavgd1 = '-';
} else {
    $dbavgd1 = number_format(25 * $d1dbave['AVG(d1)'], 0);
    $dbfocus = $dbfocus + $d1dbave['AVG(d1)'];
    $dbfocusdenom++;
}
//echo "After d1 db focus = $dbfocus and dbfocusdenom = $dbfocusdenom<br /><br />"  ;

$d2db = mysqli_query($dbc, "SELECT AVG(d2) FROM teamdata WHERE  ((d2>=0) AND (d2<=4))");
$d2dbave = mysqli_fetch_array($d2db) or die(mysqli_error());
if (!$d2dbave['AVG(d2)']) {
    $dbavgd2 = '-';
} else {
    $dbavgd2 = number_format(25 * $d2dbave['AVG(d2)'], 0);
    $dbfocus = $dbfocus + $d2dbave['AVG(d2)'];
    $dbfocusdenom++;
}
//echo "After d2 db focus = $dbfocus and dbfocusdenom = $dbfocusdenom<br /><br />"  ;

$d3db = mysqli_query($dbc, "SELECT  AVG(d3) FROM teamdata WHERE  ((d3>=0) AND (d3<=4))");
$d3dbave = mysqli_fetch_array($d3db) or die(mysqli_error());
if (!$d3dbave['AVG(d3)']) {
    $dbavgd3 = '-';
} else {
    $dbavgd3 = number_format(25 * $d3dbave['AVG(d3)'], 0);
    $dbfocus = $dbfocus + $d3dbave['AVG(d3)'];
    $dbfocusdenom++;
}
//echo "After d3 db focus = $dbfocus and dbfocusdenom = $dbfocusdenom<br /><br />"  ;

// Calculating focus summary stats - averages
if ($dbfocusdenom === 0) {
    $dbfocusave = '-';
    $dbfocusavefm = '-';
} else {
    $dbfocusave = 25 * $dbfocus / $dbfocusdenom;
    $dbfocusavefm = number_format($dbfocusave, 0);
}
//echo" dbfocusave = $dbfocusave" ;


$d4db = mysqli_query($dbc, "SELECT  AVG(d4) FROM teamdata WHERE  ((d4>=0) AND (d4<=4))");
$d4dbave = mysqli_fetch_array($d4db) or die(mysqli_error());
if (!$d4dbave['AVG(d4)']) {
    $dbavgd4 = '-';
} else {
    $dbavgd4 = number_format(25 * $d4dbave['AVG(d4)'], 0);
    $dbcommunicate = $dbcommunicate + $d4dbave['AVG(d4)'];
    $dbcommunicatedenom++;
}
//echo "After d4 db communicate = $dbcommunicate and dbcommunicatedenom = $dbcommunicatedenom<br /><br />"  ;


$d5db = mysqli_query($dbc, "SELECT  AVG(d5) FROM teamdata WHERE  ((d5>=0) AND (d5<=4))");
$d5dbave = mysqli_fetch_array($d5db) or die(mysqli_error());
if (!$d5dbave['AVG(d5)']) {
    $dbavgd5 = '-';
} else {
    $dbavgd5 = number_format(25 * $d5dbave['AVG(d5)'], 0);
    $dbcommunicate = $dbcommunicate + $d5dbave['AVG(d5)'];
    $dbcommunicatedenom++;
}
//echo "After d5 db communicate = $dbcommunicate and dbcommunicatedenom = $dbcommunicatedenom<br /><br />"  ;


$d6db = mysqli_query($dbc, "SELECT AVG(d6) FROM teamdata WHERE  ((d6>=0) AND (d6<=4))");
$d6dbave = mysqli_fetch_array($d6db) or die(mysqli_error());
if (!$d6dbave['AVG(d6)']) {
    $dbavgd6 = '-';
} else {
    $dbavgd6 = number_format(25 * $d6dbave['AVG(d6)'], 0);
    $dbcommunicate = $dbcommunicate + $d6dbave['AVG(d6)'];
    $dbcommunicatedenom++;
}
//echo "After d6 db communicate = $dbcommunicate and dbcommunicatedenom = $dbcommunicatedenom<br /><br />"  ;


// Calculating communicate summary stats - averages
if ($dbcommunicatedenom === 0) {
    $dbcommunicateave = '-';
    $dbcommunicateavefm = '-';
} else {
    $dbcommunicateave = 25 * $dbcommunicate / $dbcommunicatedenom;
    $dbcommunicateavefm = number_format($dbcommunicateave, 0);
}
//echo" dbcommunicateave = $dbcommunicateave" ;


if (($dbfocusdenom === 0) && ($dbcommunicatedenom === 0)) {
    $dbvisionaryave = '-';
    $dbvisionaryavefm = '-';
}
if (($dbfocusdenom != 0) && ($dbcommunicatedenom === 0)) {
    $dbvisionaryave = 25 * $dbfocus / $dbfocusdenom;
    $dbvisionaryavefm = number_format($dbvisionaryave, 0);
}
if (($dbfocusdenom === 0) && ($dbcommunicatedenom != 0)) {
    $dbvisionaryave = 25 * $dbcommunicate / $dbcommunicatedenom;
    $dbvisionaryavefm = number_format($dbvisionaryave, 0);
}
if (($dbfocusdenom != 0) && ($dbcommunicatedenom != 0)) {
    $dbvisionaryave = 12.5 * (($dbfocus / $dbfocusdenom) + ($dbcommunicate / $dbcommunicatedenom));
    $dbvisionaryavefm = number_format($dbvisionaryave, 0);
}
//echo "dbvisionaryave = $dbvisionaryave dbvisionaryavefm = $dbvisionaryavefm<br /><br />" ;

$d7db = mysqli_query($dbc, "SELECT AVG(d7) FROM teamdata WHERE ((d7>=0) AND (d7<=4))");
$d7dbave = mysqli_fetch_array($d7db) or die(mysqli_error());
if (!$d7dbave['AVG(d7)']) {
    $dbavgd7 = '-';
} else {
    $dbavgd7 = number_format(25 * $d7dbave['AVG(d7)'], 0);
    $dbsecure = $dbsecure + $d7dbave['AVG(d7)'];
    $dbsecuredenom++;
}
//echo "After d7 db secure = $dbsecure and dbsecuredenom = $dbsecuredenom<br /><br />"  ;


$d8db = mysqli_query($dbc, "SELECT  AVG(d8) FROM teamdata WHERE ((d8>=0) AND (d8<=4))");
$d8dbave = mysqli_fetch_array($d8db) or die(mysqli_error());
if (!$d8dbave['AVG(d8)']) {
    $dbavgd8 = '-';
} else {
    $dbavgd8 = number_format(25 * $d8dbave['AVG(d8)'], 0);
    $dbsecure = $dbsecure + $d8dbave['AVG(d8)'];
    $dbsecuredenom++;
}
//echo "After d8 db secure = $dbsecure and dbsecuredenom = $dbsecuredenom<br /><br />"  ;


$d9db = mysqli_query($dbc, "SELECT AVG(d9) FROM teamdata WHERE  ((d9>=0) AND (d9<=4))");
$d9dbave = mysqli_fetch_array($d9db) or die(mysqli_error());
if (!$d9dbave['AVG(d9)']) {
    $dbavgd9 = '-';
} else {
    $dbavgd9 = number_format(25 * $d9dbave['AVG(d9)'], 0);
    $dbsecure = $dbsecure + $d9dbave['AVG(d9)'];
    $dbsecuredenom++;
}
//echo "After d9 db secure = $dbsecure and dbsecuredenom = $dbsecuredenom<br /><br />"  ;

// Calculating secure summary stats - averages
if ($dbsecuredenom === 0) {
    $dbsecureave = '-';
    $dbsecureavefm = '-';
} else {
    $dbsecureave = 25 * $dbsecure / $dbsecuredenom;
    $dbsecureavefm = number_format($dbsecureave, 0);
}
//echo" dbsecureave = $dbsecureave" ;

$d10db = mysqli_query($dbc, "SELECT AVG(d10) FROM teamdata WHERE ((d10>=0) AND (d10<=4))");
$d10dbave = mysqli_fetch_array($d10db) or die(mysqli_error());
if (!$d10dbave['AVG(d10)']) {
    $dbavgd10 = '-';
} else {
    $dbavgd10 = number_format(25 * $d10dbave['AVG(d10)'], 0);
    $dborganize = $dborganize + $d10dbave['AVG(d10)'];
    $dborganizedenom++;
}
//echo "After d10 db organize = $dborganize and dborganizedenom = $dborganizedenom<br /><br />"  ;


$d11db = mysqli_query($dbc, "SELECT AVG(d11) FROM teamdata WHERE ((d11>=0) AND (d11<=4))");
$d11dbave = mysqli_fetch_array($d11db) or die(mysqli_error());
if (!$d11dbave['AVG(d11)']) {
    $dbavgd11 = '-';
} else {
    $dbavgd11 = number_format(25 * $d11dbave['AVG(d11)'], 0);
    $dborganize = $dborganize + $d11dbave['AVG(d11)'];
    $dborganizedenom++;
}
//echo "After d11 db organize = $dborganize and dborganizedenom = $dborganizedenom<br /><br />"  ;


$d12db = mysqli_query($dbc, "SELECT  AVG(d12) FROM teamdata WHERE  ((d12>=0) AND (d12<=4))");
$d12dbave = mysqli_fetch_array($d12db) or die(mysqli_error());
if (!$d12dbave['AVG(d12)']) {
    $dbavgd12 = '-';
} else {
    $dbavgd12 = number_format(25 * $d12dbave['AVG(d12)'], 0);
    $dborganize = $dborganize + $d12dbave['AVG(d12)'];
    $dborganizedenom++;
}
//echo "After d12 db organize = $dborganize and dborganizedenom = $dborganizedenom<br /><br />"  ;


// Calculating organize summary stats - averages
if ($dborganizedenom === 0) {
    $dborganizeave = '-';
    $dborganizeavefm = '-';
} else {
    $dborganizeave = 25 * $dborganize / $dborganizedenom;
    $dborganizeavefm = number_format($dborganizeave, 0);
}
//echo" dborganizeave = $dborganizeave" ;

if (($dbsecuredenom === 0) && ($dborganizedenom === 0)) {
    $dbguardianave = '-';
    $dbguardianavefm = '-';
}
if (($dbsecuredenom != 0) && ($dborganizedenom === 0)) {
    $dbguardianave = 25 * $dbsecure / $dbsecuredenom;
    $dbguardianavefm = number_format($dbguardianave, 0);
}
if (($dbsecuredenom === 0) && ($dborganizedenom != 0)) {
    $dbguardianave = 25 * $dborganize / $dborganizedenom;
    $dbguardianavefm = number_format($dbguardianave, 0);
}
if (($dbsecuredenom != 0) && ($dborganizedenom != 0)) {
    $dbguardianave = 12.5 * (($dbsecure / $dbsecuredenom) + ($dborganize / $dborganizedenom));
    $dbguardianavefm = number_format($dbguardianave, 0);
}
//echo "dbguardianave = $dbguardianave dbguardianavefm = $dbguardianavefm<br /><br />" ;


$d13db = mysqli_query($dbc, "SELECT AVG(d13) FROM teamdata WHERE ((d13>=0) AND (d13<=4))");
$d13dbave = mysqli_fetch_array($d13db) or die(mysqli_error());
if (!$d13dbave['AVG(d13)']) {
    $dbavgd13 = '-';
} else {
    $dbavgd13 = number_format(25 * $d13dbave['AVG(d13)'], 0);
    $dbunite = $dbunite + $d13dbave['AVG(d13)'];
    $dbunitedenom++;
}
//echo "After d13 db unite = $dbunite and dbunitedenom = $dbunitedenom<br /><br />"  ;


$d14db = mysqli_query($dbc, "SELECT  AVG(d14) FROM teamdata WHERE  ((d14>=0) AND (d14<=4))");
$d14dbave = mysqli_fetch_array($d14db) or die(mysqli_error());
if (!$d14dbave['AVG(d14)']) {
    $dbavgd14 = '-';
} else {
    $dbavgd14 = number_format(25 * $d14dbave['AVG(d14)'], 0);
    $dbunite = $dbunite + $d14dbave['AVG(d14)'];
    $dbunitedenom++;
}
//echo "After d14 db unite = $dbunite and dbunitedenom = $dbunitedenom<br /><br />"  ;


$d15db = mysqli_query($dbc, "SELECT AVG(d15) FROM teamdata WHERE  ((d15>=0) AND (d15<=4))");
$d15dbave = mysqli_fetch_array($d15db) or die(mysqli_error());
if (!$d15dbave['AVG(d15)']) {
    $dbavgd15 = '-';
} else {
    $dbavgd15 = number_format(25 * $d15dbave['AVG(d15)'], 0);
    $dbunite = $dbunite + $d15dbave['AVG(d15)'];
    $dbunitedenom++;
}
//echo "After d15 db unite = $dbunite and dbunitedenom = $dbunitedenom<br /><br />"  ;


// Calculating unite summary stats - averages
if ($dbunitedenom === 0) {
    $dbuniteave = '-';
    $dbuniteavefm = '-';
} else {
    $dbuniteave = 25 * $dbunite / $dbunitedenom;
    $dbuniteavefm = number_format($dbuniteave, 0);
}
//echo" dbuniteave = $dbuniteave" ;


$d16db = mysqli_query($dbc, "SELECT  AVG(d16) FROM teamdata WHERE  ((d16>=0) AND (d16<=4))");
$d16dbave = mysqli_fetch_array($d16db) or die(mysqli_error());
if (!$d16dbave['AVG(d16)']) {
    $dbavgd16 = '-';
} else {
    $dbavgd16 = number_format(25 * $d16dbave['AVG(d16)'], 0);
    $dbempower = $dbempower + $d16dbave['AVG(d16)'];
    $dbempowerdenom++;
}
//echo "After d16 db empower = $dbempower and dbempowerdenom = $dbempowerdenom<br /><br />"  ;


$d17db = mysqli_query($dbc, "SELECT  AVG(d17) FROM teamdata WHERE  ((d17>=0) AND (d17<=4))");
$d17dbave = mysqli_fetch_array($d17db) or die(mysqli_error());
if (!$d17dbave['AVG(d17)']) {
    $dbavgd17 = '-';
} else {
    $dbavgd17 = number_format(25 * $d17dbave['AVG(d17)'], 0);
    $dbempower = $dbempower + $d17dbave['AVG(d17)'];
    $dbempowerdenom++;
}
//echo "After d17 db empower = $dbempower and dbempowerdenom = $dbempowerdenom<br /><br />"  ;


$d18db = mysqli_query($dbc, "SELECT   AVG(d18) FROM teamdata WHERE  ((d18>=0) AND (d18<=4))");
$d18dbave = mysqli_fetch_array($d18db) or die(mysqli_error());
if (!$d18dbave['AVG(d18)']) {
    $dbavgd18 = '-';
} else {
    $dbavgd18 = number_format(25 * $d18dbave['AVG(d18)'], 0);
    $dbempower = $dbempower + $d18dbave['AVG(d18)'];
    $dbempowerdenom++;
}
//echo "After d18 db empower = $dbempower and dbempowerdenom = $dbempowerdenom<br /><br />"  ;


$d19db = mysqli_query($dbc, "SELECT AVG(d19) FROM teamdata WHERE  ((d19>=0) AND (d19<=4))");
$d19dbave = mysqli_fetch_array($d19db) or die(mysqli_error());
if (!$d19dbave['AVG(d19)']) {
    $dbavgd19 = '-';
} else {
    $dbavgd19 = number_format(25 * $d19dbave['AVG(d19)'], 0);
    $dbdeliver = $dbdeliver + $d19dbave['AVG(d19)'];
    $dbdeliverdenom++;
}
//echo "After d19 db deliver = $dbdeliver and dbdeliverdenom = $dbdeliverdenom<br /><br />"  ;


$d20db = mysqli_query($dbc, "SELECT  AVG(d20) FROM teamdata WHERE  ((d20>=0) AND (d20<=4))");
$d20dbave = mysqli_fetch_array($d20db) or die(mysqli_error());
if (!$d20dbave['AVG(d20)']) {
    $dbavgd20 = '-';
} else {
    $dbavgd20 = number_format(25 * $d20dbave['AVG(d20)'], 0);
    $dbdeliver = $dbdeliver + $d20dbave['AVG(d20)'];
    $dbdeliverdenom++;
}
//echo "After d20 db deliver = $dbdeliver and dbdeliverdenom = $dbdeliverdenom<br /><br />"  ;


$d21db = mysqli_query($dbc, "SELECT  AVG(d21) FROM teamdata WHERE  ((d21>=0) AND (d21<=4))");
$d21dbave = mysqli_fetch_array($d21db) or die(mysqli_error());
if (!$d21dbave['AVG(d21)']) {
    $dbavgd21 = '-';
} else {
    $dbavgd21 = number_format(25 * $d21dbave['AVG(d21)'], 0);
    $dbdeliver = $dbdeliver + $d21dbave['AVG(d21)'];
    $dbdeliverdenom++;
}
//echo "After d21 db deliver = $dbdeliver and dbdeliverdenom = $dbdeliverdenom<br /><br />"  ;


$d22db = mysqli_query($dbc, "SELECT  AVG(d22) FROM teamdata WHERE ((d22>=0) AND (d22<=4))");
$d22dbave = mysqli_fetch_array($d22db) or die(mysqli_error());
if (!$d22dbave['AVG(d22)']) {
    $dbavgd22 = '-';
} else {
    $dbavgd22 = number_format(25 * $d22dbave['AVG(d22)'], 0);
    $dbimprove = $dbimprove + $d22dbave['AVG(d22)'];
    $dbimprovedenom++;
}
//echo "After d22 db improve = $dbimprove and dbimprovedenom = $dbimprovedenom<br /><br />"  ;


$d23db = mysqli_query($dbc, "SELECT  AVG(d23) FROM teamdata WHERE  ((d23>=0) AND (d23<=4))");
$d23dbave = mysqli_fetch_array($d23db) or die(mysqli_error());
if (!$d23dbave['AVG(d23)']) {
    $dbavgd23 = '-';
} else {
    $dbavgd23 = number_format(25 * $d23dbave['AVG(d23)'], 0);
    $dbimprove = $dbimprove + $d23dbave['AVG(d23)'];
    $dbimprovedenom++;
}
//echo "After d23 db improve = $dbimprove and dbimprovedenom = $dbimprovedenom<br /><br />"  ;


$d24db = mysqli_query($dbc, "SELECT  AVG(d24) FROM teamdata WHERE  ((d24>=0) AND (d24<=4))");
$d24dbave = mysqli_fetch_array($d24db) or die(mysqli_error());
if (!$d24dbave['AVG(d24)']) {
    $dbavgd24 = '-';
} else {
    $dbavgd24 = number_format(25 * $d24dbave['AVG(d24)'], 0);
    $dbimprove = $dbimprove + $d24dbave['AVG(d24)'];
    $dbimprovedenom++;
}
//echo "After d24 db improve = $dbimprove and dbimprovedenom = $dbimprovedenom<br /><br />"  ;


// Calculating improve summary stats - averages
if ($dbimprovedenom === 0) {
    $dbimproveave = '-';
    $dbimproveavefm = '-';
} else {
    $dbimproveave = 25 * $dbimprove / $dbimprovedenom;
    $dbimproveavefm = number_format($dbimproveave, 0);
}
//echo" dbimproveave = $dbimproveave" ;


$d25db = mysqli_query($dbc, "SELECT  AVG(d25) FROM teamdata WHERE ((d25>=0) AND (d25<=4)) ");
$d25dbave = mysqli_fetch_array($d25db) or die(mysqli_error());
if (!$d25dbave['AVG(d25)']) {
    $dbavgd25 = '-';
} else {
    $dbavgd25 = number_format(25 * $d25dbave['AVG(d25)'], 0);
    $dbdeliver = $dbdeliver + $d25dbave['AVG(d25)'];
    $dbdeliverdenom++;
}
//echo "After d25 db deliver = $dbdeliver and dbdeliverdenom = $dbdeliverdenom<br /><br />"  ;

// Calculating deliver summary stats - averages
if ($dbdeliverdenom === 0) {
    $dbdeliverave = '-';
    $dbdeliveravefm = '-';
} else {
    $dbdeliverave = 25 * $dbdeliver / $dbdeliverdenom;
    $dbdeliveravefm = number_format($dbdeliverave, 0);
}
//echo" dbdeliverave = $dbdeliverave" ;


if (($dbdeliverdenom === 0) && ($dbimprovedenom === 0)) {
    $dbdirectorave = '-';
    $dbdirectoravefm = '-';
}
if (($dbdeliverdenom != 0) && ($dbimprovedenom === 0)) {
    $dbdirectorave = 25 * $dbdeliver / $dbdeliverdenom;
    $dbdirectoravefm = number_format($dbdirectorave, 0);
}
if (($dbdeliverdenom === 0) && ($dbimprovedenom != 0)) {
    $dbdirectorave = 25 * $dbimprove / $dbimprovedenom;
    $dbdirectoravefm = number_format($dbdirectorave, 0);
}
if (($dbdeliverdenom != 0) && ($dbimprovedenom != 0)) {
    $dbdirectorave = 12.5 * (($dbdeliver / $dbdeliverdenom) + ($dbimprove / $dbimprovedenom));
    $dbdirectoravefm = number_format($dbdirectorave, 0);
}
//echo "dbdirectorave = $dbdirectorave dbdirectoravefm = $dbdirectoravefm<br /><br />" ;


$d26db = mysqli_query($dbc, "SELECT  AVG(d26) FROM teamdata WHERE  ((d26>=0) AND (d26<=4))");
$d26dbave = mysqli_fetch_array($d26db) or die(mysqli_error());
if (!$d26dbave['AVG(d26)']) {
    $dbavgd26 = '-';
} else {
    $dbavgd26 = number_format(25 * $d26dbave['AVG(d26)'], 0);
    $dbempower = $dbempower + $d26dbave['AVG(d26)'];
    $dbempowerdenom++;
}
//echo "After d26 db empower = $dbempower and dbempowerdenom = $dbempowerdenom<br /><br />"  ;

// Calculating empower summary stats - averages
if ($dbempowerdenom === 0) {
    $dbempowerave = '-';
    $dbempoweravefm = '-';
} else {
    $dbempowerave = 25 * $dbempower / $dbempowerdenom;
    $dbempoweravefm = number_format($dbempowerave, 0);
}
//echo" dbempowerave = $dbempowerave" ;


if (($dbunitedenom === 0) && ($dbempowerdenom === 0)) {
    $dbpsychologistave = '-';
    $dbpsychologistavefm = '-';
}
if (($dbunitedenom != 0) && ($dbempowerdenom === 0)) {
    $dbpsychologistave = 25 * $dbunite / $dbunitedenom;
    $dbpsychologistavefm = number_format($dbpsychologistave, 0);
}
if (($dbunitedenom === 0) && ($dbempowerdenom != 0)) {
    $dbpsychologistave = 25 * $dbempower / $dbempowerdenom;
    $dbpsychologistavefm = number_format($dbpsychologistave, 0);
}
if (($dbunitedenom != 0) && ($dbempowerdenom != 0)) {
    $dbpsychologistave = 12.5 * (($dbunite / $dbunitedenom) + ($dbempower / $dbempowerdenom));
    $dbpsychologistavefm = number_format($dbpsychologistave, 0);
}
//echo "dbpsychologistave = $dbpsychologistave dbpsychologistavefm = $dbpsychologistavefm<br /><br />" ;



if (($dbvisionaryave === '-') && ($dbguardianave === '-') && ($dbpsychologistave === '-') && ($dbdirectorave === '-')) {
    $dbgrandave = '-';
    $dbgrandavefm = '-';
}
if (($dbvisionaryave != '-') && ($dbguardianave != '-') && ($dbpsychologistave != '-') && ($dbdirectorave != '-')) {
    $dbgrandave = ($dbvisionaryave + $dbguardianave + $dbpsychologistave + $dbdirectorave) / 4;
    $dbgrandavefm = number_format($dbgrandave, 0);
}

if (($dbvisionaryave != '-') && ($dbguardianave === '-') && ($dbpsychologistave === '-') && ($dbdirectorave === '-')) {
    $dbgrandave = $dbvisionaryave;
    $dbgrandavefm = number_format($dbgrandave, 0);
}
if (($dbvisionaryave === '-') && ($dbguardianave != '-') && ($dbpsychologistave === '-') && ($dbdirectorave === '-')) {
    $dbgrandave = $dbguardianave;
    $dbgrandavefm = number_format($dbgrandave, 0);
}
if (($dbvisionaryave === '-') && ($dbguardianave === '-') && ($dbpsychologistave != '-') && ($dbdirectorave === '-')) {
    $dbgrandave = $dbpsychologistave;
    $dbgrandavefm = number_format($dbgrandave, 0);
}
if (($dbvisionaryave === '-') && ($dbguardianave === '-') && ($dbpsychologistave === '-') && ($dbdirectorave != '-')) {
    $dbgrandave = $dbdirectorave;
    $dbgrandavefm = number_format($dbgrandave, 0);
}

if (($dbvisionaryave != '-') && ($dbguardianave != '-') && ($dbpsychologistave === '-') && ($dbdirectorave === '-')) {
    $dbgrandave = ($dbvisionaryave + $dbguardianave) / 2;
    $dbgrandavefm = number_format($dbgrandave, 0);
}
if (($dbvisionaryave != '-') && ($dbguardianave === '-') && ($dbpsychologistave != '-') && ($dbdirectorave === '-')) {
    $dbgrandave = ($dbvisionaryave + $dbpsychologistave) / 2;
    $dbgrandavefm = number_format($dbgrandave, 0);
}
if (($dbvisionaryave != '-') && ($dbguardianave === '-') && ($dbpsychologistave === '-') && ($dbdirectorave != '-')) {
    $dbgrandave = ($dbvisionaryave + $dbdirectorave) / 2;
    $dbgrandavefm = number_format($dbgrandave, 0);
}
if (($dbvisionaryave === '-') && ($dbguardianave != '-') && ($dbpsychologistave != '-') && ($dbdirectorave === '-')) {
    $dbgrandave = ($dbguardianave + $dbpsychologistave) / 2;
    $dbgrandavefm = number_format($dbgrandave, 0);
}
if (($dbvisionaryave === '-') && ($dbguardianave != '-') && ($dbpsychologistave === '-') && ($dbdirectorave != '-')) {
    $dbgrandave = ($dbguardianave + $dbdirectorave) / 2;
    $dbgrandavefm = number_format($dbgrandave, 0);
}
if (($dbvisionaryave === '-') && ($dbguardianave === '-') && ($dbpsychologistave != '-') && ($dbdirectorave != '-')) {
    $dbgrandave = ($dbpsychologistave + $dbdirectorave) / 2;
    $dbgrandavefm = number_format($dbgrandave, 0);
}

if (($dbvisionaryave != '-') && ($dbguardianave != '-') && ($dbpsychologistave != '-') && ($dbdirectorave === '-')) {
    $dbgrandave = ($dbvisionaryave + $dbguardianave + $dbpsychologistave) / 3;
    $dbgrandavefm = number_format($dbgrandave, 0);
}
if (($dbvisionaryave != '-') && ($dbguardianave != '-') && ($dbpsychologistave === '-') && ($dbdirectorave != '-')) {
    $dbgrandave = ($dbvisionaryave + $dbguardianave + $dbdirectorave) / 3;
    $dbgrandavefm = number_format($dbgrandave, 0);
}
if (($dbvisionaryave != '-') && ($dbguardianave === '-') && ($dbpsychologistave != '-') && ($dbdirectorave != '-')) {
    $dbgrandave = ($dbvisionaryave + $dbpsychologistave + $dbdirectorave) / 3;
    $dbgrandavefm = number_format($dbgrandave, 0);
}
if (($dbvisionaryave === '-') && ($dbguardianave != '-') && ($dbpsychologistave != '-') && ($dbdirectorave != '-')) {
    $dbgrandave = ($dbguardianave + $dbpsychologistave + $dbdirectorave) / 3;
    $dbgrandavefm = number_format($dbgrandave, 0);
}

//echo "Dbgrandave= $dbgrandave ";
// end dbase calcs

 */

//Extracting team votes for development
$tvotedev1 = number_format($getteamvotes['SUM(dev1)'], 0);
$tvotedev2 = number_format($getteamvotes['SUM(dev2)'], 0);
$tvotedev3 = number_format($getteamvotes['SUM(dev3)'], 0);
$tvotedev4 = number_format($getteamvotes['SUM(dev4)'], 0);
$tvotedev5 = number_format($getteamvotes['SUM(dev5)'], 0);
$tvotedev6 = number_format($getteamvotes['SUM(dev6)'], 0);
$tvotedev7 = number_format($getteamvotes['SUM(dev7)'], 0);
$tvotedev8 = number_format($getteamvotes['SUM(dev8)'], 0);
$tvotedev9 = number_format($getteamvotes['SUM(dev9)'], 0);
$tvotedev10 = number_format($getteamvotes['SUM(dev10)'], 0);
$tvotedev11 = number_format($getteamvotes['SUM(dev11)'], 0);
$tvotedev12 = number_format($getteamvotes['SUM(dev12)'], 0);
$tvotedev13 = number_format($getteamvotes['SUM(dev13)'], 0);
$tvotedev14 = number_format($getteamvotes['SUM(dev14)'], 0);
$tvotedev15 = number_format($getteamvotes['SUM(dev15)'], 0);
$tvotedev16 = number_format($getteamvotes['SUM(dev16)'], 0);
$tvotedev17 = number_format($getteamvotes['SUM(dev17)'], 0);
$tvotedev18 = number_format($getteamvotes['SUM(dev18)'], 0);
$tvotedev19 = number_format($getteamvotes['SUM(dev19)'], 0);
$tvotedev20 = number_format($getteamvotes['SUM(dev20)'], 0);
$tvotedev21 = number_format($getteamvotes['SUM(dev21)'], 0);
$tvotedev22 = number_format($getteamvotes['SUM(dev22)'], 0);
$tvotedev23 = number_format($getteamvotes['SUM(dev23)'], 0);
$tvotedev24 = number_format($getteamvotes['SUM(dev24)'], 0);
$tvotedev25 = number_format($getteamvotes['SUM(dev25)'], 0);
$tvotedev26 = number_format($getteamvotes['SUM(dev26)'], 0);

//Extracting stakeholder votes for development
$svotedev1 = number_format($getstakevotes['SUM(dev1)'], 0);
$svotedev2 = number_format($getstakevotes['SUM(dev2)'], 0);
$svotedev3 = number_format($getstakevotes['SUM(dev3)'], 0);
$svotedev4 = number_format($getstakevotes['SUM(dev4)'], 0);
$svotedev5 = number_format($getstakevotes['SUM(dev5)'], 0);
$svotedev6 = number_format($getstakevotes['SUM(dev6)'], 0);
$svotedev7 = number_format($getstakevotes['SUM(dev7)'], 0);
$svotedev8 = number_format($getstakevotes['SUM(dev8)'], 0);
$svotedev9 = number_format($getstakevotes['SUM(dev9)'], 0);
$svotedev10 = number_format($getstakevotes['SUM(dev10)'], 0);
$svotedev11 = number_format($getstakevotes['SUM(dev11)'], 0);
$svotedev12 = number_format($getstakevotes['SUM(dev12)'], 0);
$svotedev13 = number_format($getstakevotes['SUM(dev13)'], 0);
$svotedev14 = number_format($getstakevotes['SUM(dev14)'], 0);
$svotedev15 = number_format($getstakevotes['SUM(dev15)'], 0);
$svotedev16 = number_format($getstakevotes['SUM(dev16)'], 0);
$svotedev17 = number_format($getstakevotes['SUM(dev17)'], 0);
$svotedev18 = number_format($getstakevotes['SUM(dev18)'], 0);
$svotedev19 = number_format($getstakevotes['SUM(dev19)'], 0);
$svotedev20 = number_format($getstakevotes['SUM(dev20)'], 0);
$svotedev21 = number_format($getstakevotes['SUM(dev21)'], 0);
$svotedev22 = number_format($getstakevotes['SUM(dev22)'], 0);
$svotedev23 = number_format($getstakevotes['SUM(dev23)'], 0);
$svotedev24 = number_format($getstakevotes['SUM(dev24)'], 0);
$svotedev25 = number_format($getstakevotes['SUM(dev25)'], 0);
$svotedev26 = number_format($getstakevotes['SUM(dev26)'], 0);

// Calculating graph line lengths for summary stats

$ltgrave = 1 + (1.8 * $teamgrandave);
$lsgrave = 1 + (1.8 * $stakegrandave);
$ldgrave = 1 + (1.8 * $dbgrandave);

$ltvisave = 1 + (1.8 * $teamvisionaryave);
$ldvisave = 1 + (1.8 * $dbvisionaryave);
$lsvisave = 1 + (1.8 * $stakevisionaryave);

$ltguaave = 1 + (1.8 * $teamguardianave);
$ldguaave = 1 + (1.8 * $dbguardianave);
$lsguaave = 1 + (1.8 * $stakeguardianave);

$ltpsyave = 1 + (1.8 * $teampsychologistave);
$ldpsyave = 1 + (1.8 * $dbpsychologistave);
$lspsyave = 1 + (1.8 * $stakepsychologistave);

$ltdirave = 1 + (1.8 * $teamdirectorave);
$lddirave = 1 + (1.8 * $dbdirectorave);
$lsdirave = 1 + (1.8 * $stakedirectorave);

$ltfocusave = 1 + (1.8 * $teamfocusave);
$ldfocusave = 1 + (1.8 * $dbfocusave);
$lsfocusave = 1 + (1.8 * $stakefocusave);

$ltcommunicateave = 1 + (1.8 * $teamcommunicateave);
$ldcommunicateave = 1 + (1.8 * $dbcommunicateave);
$lscommunicateave = 1 + (1.8 * $stakecommunicateave);

$ltsecureave = 1 + (1.8 * $teamsecureave);
$ldsecureave = 1 + (1.8 * $dbsecureave);
$lssecureave = 1 + (1.8 * $stakesecureave);

$ltorganizeave = 1 + (1.8 * $teamorganizeave);
$ldorganizeave = 1 + (1.8 * $dborganizeave);
$lsorganizeave = 1 + (1.8 * $stakeorganizeave);

$ltuniteave = 1 + (1.8 * $teamuniteave);
$lduniteave = 1 + (1.8 * $dbuniteave);
$lsuniteave = 1 + (1.8 * $stakeuniteave);

$ltempowerave = 1 + (1.8 * $teamempowerave);
$ldempowerave = 1 + (1.8 * $dbempowerave);
$lsempowerave = 1 + (1.8 * $stakeempowerave);

$ltdeliverave = 1 + (1.8 * $teamdeliverave);
$lddeliverave = 1 + (1.8 * $dbdeliverave);
$lsdeliverave = 1 + (1.8 * $stakedeliverave);

$ltimproveave = 1 + (1.8 * $teamimproveave);
$ldimproveave = 1 + (1.8 * $dbimproveave);
$lsimproveave = 1 + (1.8 * $stakeimproveave);

// Calculating graph line lengths for detailed report p5

$lt1 = 1 + (1.8 * $teamavgd1);
$lt2 = 1 + (1.8 * $teamavgd2);
$lt3 = 1 + (1.8 * $teamavgd3);
$lt4 = 1 + (1.8 * $teamavgd4);
$lt5 = 1 + (1.8 * $teamavgd5);
$lt6 = 1 + (1.8 * $teamavgd6);
$lt7 = 1 + (1.8 * $teamavgd7);
$lt8 = 1 + (1.8 * $teamavgd8);
$lt9 = 1 + (1.8 * $teamavgd9);
$lt10 = 1 + (1.8 * $teamavgd10);
$lt11 = 1 + (1.8 * $teamavgd11);
$lt12 = 1 + (1.8 * $teamavgd12);
$lt13 = 1 + (1.8 * $teamavgd13);
$lt14 = 1 + (1.8 * $teamavgd14);
$lt15 = 1 + (1.8 * $teamavgd15);
$lt16 = 1 + (1.8 * $teamavgd16);
$lt17 = 1 + (1.8 * $teamavgd17);
$lt18 = 1 + (1.8 * $teamavgd18);
$lt19 = 1 + (1.8 * $teamavgd19);
$lt20 = 1 + (1.8 * $teamavgd20);
$lt21 = 1 + (1.8 * $teamavgd21);
$lt22 = 1 + (1.8 * $teamavgd22);
$lt23 = 1 + (1.8 * $teamavgd23);
$lt24 = 1 + (1.8 * $teamavgd24);
$lt25 = 1 + (1.8 * $teamavgd25);
$lt26 = 1 + (1.8 * $teamavgd26);

$ldb1 = 1 + (1.8 * $dbavgd1);
$ldb2 = 1 + (1.8 * $dbavgd2);
$ldb3 = 1 + (1.8 * $dbavgd3);
$ldb4 = 1 + (1.8 * $dbavgd4);
$ldb5 = 1 + (1.8 * $dbavgd5);
$ldb6 = 1 + (1.8 * $dbavgd6);
$ldb7 = 1 + (1.8 * $dbavgd7);
$ldb8 = 1 + (1.8 * $dbavgd8);
$ldb9 = 1 + (1.8 * $dbavgd9);
$ldb10 = 1 + (1.8 * $dbavgd10);
$ldb11 = 1 + (1.8 * $dbavgd11);
$ldb12 = 1 + (1.8 * $dbavgd12);
$ldb13 = 1 + (1.8 * $dbavgd13);
$ldb14 = 1 + (1.8 * $dbavgd14);
$ldb15 = 1 + (1.8 * $dbavgd15);
$ldb16 = 1 + (1.8 * $dbavgd16);
$ldb17 = 1 + (1.8 * $dbavgd17);
$ldb18 = 1 + (1.8 * $dbavgd18);
$ldb19 = 1 + (1.8 * $dbavgd19);
$ldb20 = 1 + (1.8 * $dbavgd20);
$ldb21 = 1 + (1.8 * $dbavgd21);
$ldb22 = 1 + (1.8 * $dbavgd22);
$ldb23 = 1 + (1.8 * $dbavgd23);
$ldb24 = 1 + (1.8 * $dbavgd24);
$ldb25 = 1 + (1.8 * $dbavgd25);
$ldb26 = 1 + (1.8 * $dbavgd26);

$lstake1 = 1 + (1.8 * $stakeavgd1);
$lstake2 = 1 + (1.8 * $stakeavgd2);
$lstake3 = 1 + (1.8 * $stakeavgd3);
$lstake4 = 1 + (1.8 * $stakeavgd4);
$lstake5 = 1 + (1.8 * $stakeavgd5);
$lstake6 = 1 + (1.8 * $stakeavgd6);
$lstake7 = 1 + (1.8 * $stakeavgd7);
$lstake8 = 1 + (1.8 * $stakeavgd8);
$lstake9 = 1 + (1.8 * $stakeavgd9);
$lstake10 = 1 + (1.8 * $stakeavgd10);
$lstake11 = 1 + (1.8 * $stakeavgd11);
$lstake12 = 1 + (1.8 * $stakeavgd12);
$lstake13 = 1 + (1.8 * $stakeavgd13);
$lstake14 = 1 + (1.8 * $stakeavgd14);
$lstake15 = 1 + (1.8 * $stakeavgd15);
$lstake16 = 1 + (1.8 * $stakeavgd16);
$lstake17 = 1 + (1.8 * $stakeavgd17);
$lstake18 = 1 + (1.8 * $stakeavgd18);
$lstake19 = 1 + (1.8 * $stakeavgd19);
$lstake20 = 1 + (1.8 * $stakeavgd20);
$lstake21 = 1 + (1.8 * $stakeavgd21);
$lstake22 = 1 + (1.8 * $stakeavgd22);
$lstake23 = 1 + (1.8 * $stakeavgd23);
$lstake24 = 1 + (1.8 * $stakeavgd24);
$lstake25 = 1 + (1.8 * $stakeavgd25);
$lstake26 = 1 + (1.8 * $stakeavgd26);
?>