<?php

// Sorting development votes from team members into popularity order and printing out topics with >1 vote

$ivote_dev = array();
$ivote_dev[0] = 0;
$ivote_dev[1] = intval($ivotedev1);
$ivote_dev[2] = intval($ivotedev2);
$ivote_dev[3] = intval($ivotedev3);
$ivote_dev[4] = intval($ivotedev4);
$ivote_dev[5] = intval($ivotedev5);
$ivote_dev[6] = intval($ivotedev6);
$ivote_dev[7] = intval($ivotedev7);
$ivote_dev[8] = intval($ivotedev8);
$ivote_dev[9] = intval($ivotedev9);
$ivote_dev[10] = intval($ivotedev10);
$ivote_dev[11] = intval($ivotedev11);
$ivote_dev[12] = intval($ivotedev12);
$ivote_dev[13] = intval($ivotedev13);
$ivote_dev[14] = intval($ivotedev14);
$ivote_dev[15] = intval($ivotedev15);
$ivote_dev[16] = intval($ivotedev16);
$ivote_dev[17] = intval($ivotedev17);
$ivote_dev[18] = intval($ivotedev18);
$ivote_dev[19] = intval($ivotedev19);
$ivote_dev[20] = intval($ivotedev20);
$ivote_dev[21] = intval($ivotedev21);
$ivote_dev[22] = intval($ivotedev22);
$ivote_dev[23] = intval($ivotedev23);
$ivote_dev[24] = intval($ivotedev24);
$ivote_dev[25] = intval($ivotedev25);
$ivote_dev[26] = intval($ivotedev26);

/* Test self array
$ivote_dev[1] = 1;
$ivote_dev[2] = 1;
$ivote_dev[3] = 1;
$ivote_dev[4] = 1;
$ivote_dev[5] = 1;
$ivote_dev[6] = 1;
$ivote_dev[7] = 1;
$ivote_dev[8] = 1;
$ivote_dev[9] = 1;
$ivote_dev[10] = 1;
$ivote_dev[11] = 1;
$ivote_dev[12] = 1;
$ivote_dev[13] = 1;
$ivote_dev[14] = 1;
$ivote_dev[15] = 1;
$ivote_dev[16] = 1;
$ivote_dev[17] = 1;
$ivote_dev[18] = 1;
$ivote_dev[19] = 1;
$ivote_dev[20] = 1;
$ivote_dev[21] = 1;
$ivote_dev[22] = 1;
$ivote_dev[23] = 1;
$ivote_dev[24] = 1;
$ivote_dev[25] = 1;
$ivote_dev[26] = 1;
*/

arsort($ivote_dev);

// Testing zero votes for development

if (($surveytype === 9) || ($surveytype === 10) || ($surveytype === 11)) {
  $pdf->SetFont('Helvetica', '', 12);
  $pdf->Write(8, 'Your own suggestions for development: ' . "\n");
  if (($ivote_dev[1] === 0) && ($ivote_dev[2] === 0) && ($ivote_dev[3] === 0) && ($ivote_dev[4] === 0) && ($ivote_dev[5] === 0) && ($ivote_dev[6] === 0) && ($ivote_dev[7] === 0) && ($ivote_dev[8] === 0) && ($ivote_dev[9] === 0) && ($ivote_dev[10] === 0) && ($ivote_dev[11] === 0) && ($ivote_dev[12] === 0) && ($ivote_dev[13] === 0) && ($ivote_dev[14] === 0) && ($ivote_dev[15] === 0) && ($ivote_dev[16] === 0) && ($ivote_dev[17] === 0) && ($ivote_dev[18] === 0) && ($ivote_dev[19] === 0) && ($ivote_dev[20] === 0) && ($ivote_dev[21] === 0) && ($ivote_dev[22] === 0) && ($ivote_dev[23] === 0) && ($ivote_dev[24] === 0) && ($ivote_dev[25] === 0) && ($ivote_dev[26] === 0)) {
    $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, "\n" . '       You made no suggestions for development. ' . "\n");
  }
  else {
    foreach ($ivote_dev as $key => $value) {
      if ($value != 0) { // Making sure header row in the array does not get printed
        $pdf->SetFont('Helvetica', 'B', 9);
        $pdf->Write(5, '    ' . $value . '  votes  ' . $topics_ind_review[$key][2] . "\n");
        $pdf->SetFont('Helvetica', '', 9);
        $pdf->Write(5, '                      Recommended development exercise link:   ');
        $pdf->SetFont('', 'U');
        $link = $pdf->AddLink();
        $pdf->Write(5, $topics_ind_review[$key][1], $customised_link . $topics_ind_review[$key][5]);
        $pdf->Write(5, "\n" . "\n");
      }
    }
  }
}

// Sorting development votes from stakeholders into popularity order and printing out topics with >1 vote

$svote_dev = array();
$svote_dev[0] = 0;
$svote_dev[1] = intval($svotedev1);
$svote_dev[2] = intval($svotedev2);
$svote_dev[3] = intval($svotedev3);
$svote_dev[4] = intval($svotedev4);
$svote_dev[5] = intval($svotedev5);
$svote_dev[6] = intval($svotedev6);
$svote_dev[7] = intval($svotedev7);
$svote_dev[8] = intval($svotedev8);
$svote_dev[9] = intval($svotedev9);
$svote_dev[10] = intval($svotedev10);
$svote_dev[11] = intval($svotedev11);
$svote_dev[12] = intval($svotedev12);
$svote_dev[13] = intval($svotedev13);
$svote_dev[14] = intval($svotedev14);
$svote_dev[15] = intval($svotedev15);
$svote_dev[16] = intval($svotedev16);
$svote_dev[17] = intval($svotedev17);
$svote_dev[18] = intval($svotedev18);
$svote_dev[19] = intval($svotedev19);
$svote_dev[20] = intval($svotedev20);
$svote_dev[21] = intval($svotedev21);
$svote_dev[22] = intval($svotedev22);
$svote_dev[23] = intval($svotedev23);
$svote_dev[24] = intval($svotedev24);
$svote_dev[25] = intval($svotedev25);
$svote_dev[26] = intval($svotedev26);

/* Test contributor array
$svote_dev[1] = 4;
$svote_dev[2] = 4;
$svote_dev[3] = 4;
$svote_dev[4] = 4;
$svote_dev[5] = 3;
$svote_dev[6] = 3;
$svote_dev[7] = 3;
$svote_dev[8] = 3;
$svote_dev[9] = 3;
$svote_dev[10] = 3;
$svote_dev[11] = 3;
$svote_dev[12] = 3;
$svote_dev[13] = 3;
$svote_dev[14] = 3;
$svote_dev[15] = 3;
$svote_dev[16] = 3;
$svote_dev[17] = 3;
$svote_dev[18] = 3;
$svote_dev[19] = 3;
$svote_dev[20] = 3;
$svote_dev[21] = 3;
$svote_dev[22] = 3;
$svote_dev[23] = 3;
$svote_dev[24] = 3;
$svote_dev[25] = 3;
$svote_dev[26] = 3;
*/

arsort($svote_dev);

// Testing zero votes for development

if (($surveytype === 10) || ($surveytype === 11)) {
  $pdf->SetFont('Helvetica', '', 12);
  $pdf->Write(8, "\n" . "\n" . 'Suggestions that were picked by more than one contributor: ' . "\n" . "\n");
  if (($svote_dev[1] === 0) && ($svote_dev[2] === 0) && ($svote_dev[3] === 0) && ($svote_dev[4] === 0) && ($svote_dev[5] === 0) && ($svote_dev[6] === 0) && ($svote_dev[7] === 0) && ($svote_dev[8] === 0) && ($svote_dev[9] === 0) && ($svote_dev[10] === 0) && ($svote_dev[11] === 0) && ($svote_dev[12] === 0) && ($svote_dev[13] === 0) && ($svote_dev[14] === 0) && ($svote_dev[15] === 0) && ($svote_dev[16] === 0) && ($svote_dev[17] === 0) && ($svote_dev[18] === 0) && ($svote_dev[19] === 0) && ($svote_dev[20] === 0) && ($svote_dev[21] === 0) && ($svote_dev[22] === 0) && ($svote_dev[23] === 0) && ($svote_dev[24] === 0) && ($svote_dev[25] === 0) && ($svote_dev[26] === 0)) {
    $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, '       There were no suggestions for development. ' . "\n");
  }
  else {
    $count = 0;
    foreach ($svote_dev as $key => $value) {
      if ($value > 1) {  // Producing items that had more than a single vote
        $count++;
        $pdf->SetFont('Helvetica', 'B', 9);
        $pdf->Write(5, '    ' . $value . '  votes  ' . $topics_ind_review[$key][2] . "\n");
        $pdf->SetFont('Helvetica', '', 9);
        $pdf->Write(5, '                      Recommended development exercise link:   ');
        $pdf->SetFont('', 'U');
        $link = $pdf->AddLink();
        $pdf->Write(5, $topics_ind_review[$key][1], $customised_link . $topics_ind_review[$key][5]);
        $pdf->Write(5, "\n" . "\n");
      }
    }
    if ($count === 0) {
      $pdf->SetFont('Helvetica', '', 9);
      $pdf->Write(5, "\n" . '       There were no suggestions with more than one vote. ' . "\n");
    }
  }
}
/*
  $pdf->SetFont('Helvetica', '', 8);
  $pdf->Write(5, ''. "\n". "\n". "\n" .'No. of self-rating respondents: ' . $numindrespondents . "\n");
  $pdf->SetFont('Helvetica', '', 8);
  $pdf->Write(5, 'No. of 360 contributors: ' . $numstakerespondents . "\n" . "\n");
 */
?>
