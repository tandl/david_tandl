<?php

// Sorting development votes into popularity order and printing out highest-voted topics with >1 vote

$tvote_dev = array();
$tvote_dev[0] = 0;
$tvote_dev[1] = intval($tvotedev1);
$tvote_dev[2] = intval($tvotedev2);
$tvote_dev[3] = intval($tvotedev3);
$tvote_dev[4] = intval($tvotedev4);
$tvote_dev[5] = intval($tvotedev5);
$tvote_dev[6] = intval($tvotedev6);
$tvote_dev[7] = intval($tvotedev7);
$tvote_dev[8] = intval($tvotedev8);
$tvote_dev[9] = intval($tvotedev9);
$tvote_dev[10] = intval($tvotedev10);
$tvote_dev[11] = intval($tvotedev11);
$tvote_dev[12] = intval($tvotedev12);
$tvote_dev[13] = intval($tvotedev13);
$tvote_dev[14] = intval($tvotedev14);
$tvote_dev[15] = intval($tvotedev15);
$tvote_dev[16] = intval($tvotedev16);
$tvote_dev[17] = intval($tvotedev17);
$tvote_dev[18] = intval($tvotedev18);
$tvote_dev[19] = intval($tvotedev19);
$tvote_dev[20] = intval($tvotedev20);
$tvote_dev[21] = intval($tvotedev21);
$tvote_dev[22] = intval($tvotedev22);
$tvote_dev[23] = intval($tvotedev23);
$tvote_dev[24] = intval($tvotedev24);
$tvote_dev[25] = intval($tvotedev25);
$tvote_dev[26] = intval($tvotedev26);

/*
$tvote_dev[1] = 1;
$tvote_dev[2] =1;
$tvote_dev[3] =1;
$tvote_dev[4] =1;
$tvote_dev[5] = 1;
$tvote_dev[6] =1;
$tvote_dev[7] =1;
$tvote_dev[8] = 1;
$tvote_dev[9] = 1;
$tvote_dev[10] = 1;
$tvote_dev[11] = 1;
$tvote_dev[12] = 1;
$tvote_dev[13] = 1;
$tvote_dev[14] = 1;
$tvote_dev[15] = 1;
$tvote_dev[16] = 1;
$tvote_dev[17] = 1;
$tvote_dev[18] = 1;
$tvote_dev[19] = 1;
$tvote_dev[20] = 1;
$tvote_dev[21] = 1;
$tvote_dev[22] = 1;
$tvote_dev[23] = 1;
$tvote_dev[24] = 1;
$tvote_dev[25] = 2;
$tvote_dev[26] = 1;
*/
$pdf->SetFont('Helvetica', '', 9);
$solution = 'Got through to here';
$largest_vote = 0;
$largest_vote = max($tvote_dev);
if ($largest_vote > 0) {
  $pdf->Write(5, 'The area(s) your team would most like to work on together: ' . "\n");
 $pdf->SetLeftMargin(30);
  for ($i = 1; $i <= 26; $i++) {
    if ($tvote_dev[$i] === $largest_vote) {
      $pdf->SetFont('Helvetica', 'B', 9);
      $pdf->Write(5, "\n" . $topics_team_review[$i][2] . "\n");
      $pdf->SetFont('Helvetica', '', 9);
      $pdf->Write(5, "Recommended team exercise link: ");
      $pdf->SetFont('','U');
      $link = $pdf->AddLink();
$pdf->Write(5,$topics_team_review[$i][1], $topics_team_review[$i][5]);
$pdf->Write(5, "\n");
$pdf->SetFont('');
    }
  }
}
else {
  $pdf->Write(5, 'This report will help you work out just how to do it.' . "\n" . "\n");
}
  $pdf->SetLeftMargin(20);
  $pdf->Write(5, '  ' . "\n" . "\n");

$pdf->SetTextColor(0, 0, 0);
?>
