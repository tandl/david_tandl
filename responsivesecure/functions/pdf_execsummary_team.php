<?php

/* include ('functions/pdf_toprank_team.php');
  top_devvote($tvotedev1, $tvotedev2, $tvotedev3, $tvotedev4, $tvotedev5, $tvotedev6, $tvotedev7, $tvotedev8, $tvotedev9, $tvotedev10, $tvotedev11, $tvotedev12, $tvotedev13, $tvotedev14, $tvotedev15, $tvotedev16, $tvotedev17, $tvotedev18, $tvotedev19, $tvotedev20, $tvotedev21, $tvotedev22, $tvotedev23, $tvotedev24, $tvotedev25, $tvotedev26); */

// Testing: $tandsgrandave = 45;

if (($tandsgrandave != '-') && ($tandsgrandave >= 72.34)) {
// Top 10% Exec summary
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'An excellent result.' . "\n" . "\n");
    $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, 'Your overall score was ');
    $pdf->SetFont('Helvetica', 'B', 9);
    $pdf->Write(5, $tandsgrandavefm);
    $pdf->SetFont('Helvetica', '', 9);
   // $pdf->Write(5, '%.' . "\n");
    $pdf->Write(5, '%. This is the average of your self-assessment and your stakeholders\' assessment (if you included them in the review) and puts you in the top 10% of our database.' . "\n" . "\n");
  $pdf->Write(5, 'You can always improve, though! Use the recommended exercises based on your report  - and push yourself to improve on even this excellent review.');
}
elseif (($tandsgrandave != '-') && ($tandsgrandave >= 64.99 && $tandsgrandave < 72.34)) {
  // Top quartile
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Your team received a very good review.' . "\n" . "\n");
     $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, 'Your overall score was ');
    $pdf->SetFont('Helvetica', 'B', 9);
    $pdf->Write(5, $tandsgrandavefm);
    $pdf->SetFont('Helvetica', '', 9);
   // $pdf->Write(5, '%.' . "\n");
    $pdf->Write(5, '%. This is the average of your self-assessment and your stakeholders\' assessment (if you included them in the review) and puts you in the top quartile of our database.' . "\n" . "\n");
  $pdf->Write(5, 'You can always improve, though. Use the recommended exercises based on your report  - and push yourself to get above the top 10% benchmark. ');
}
elseif (($tandsgrandave != '-') && ($tandsgrandave >= 56.80 && $tandsgrandave < 64.99)) {
// Above average
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Your team received a good review.' . "\n" . "\n");
       $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, 'Your overall score was ');
    $pdf->SetFont('Helvetica', 'B', 9);
    $pdf->Write(5, $tandsgrandavefm);
    $pdf->SetFont('Helvetica', '', 9);
   // $pdf->Write(5, '%.' . "\n");
    $pdf->Write(5, '%. This is the average of your self-assessment and your stakeholders\' assessment (if you included them in the review) and puts you in the top half of our database.' . "\n" . "\n");
  $pdf->Write(5, 'You can always improve, though. Use the recommended exercises based on your report - and push yourself to get into the top quarter or top 10%. ');
}
elseif (($tandsgrandave != '-') && ($tandsgrandave >= 48.61 && $tandsgrandave < 56.80)) {
// Lower quartile
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Your team received a fairly good review.' . "\n" . "\n");
        $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, 'Your overall score was ');
    $pdf->SetFont('Helvetica', 'B', 9);
    $pdf->Write(5, $tandsgrandavefm);
    $pdf->SetFont('Helvetica', '', 9);
   // $pdf->Write(5, '%.' . "\n");
    $pdf->Write(5, '%. This is the average of your self-assessment and your stakeholders\' assessment (if you included them in the review). This puts you just below the mid-point score in our research database, so there\'s plenty that you can work on, but you\'re starting from a solid base.' . "\n" . "\n");
  $pdf->Write(5, 'You can always improve, though - and push yourself to get into the top quarter or top 10%.  ');
}
else {
// Bottom quartile
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Your team needs its confidence building' . "\n" . "\n");
     $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, 'Your overall score was ');
    $pdf->SetFont('Helvetica', 'B', 9);
    $pdf->Write(5, $tandsgrandavefm);
    $pdf->SetFont('Helvetica', '', 9);
   // $pdf->Write(5, '%.' . "\n");
    $pdf->Write(5, '%. This is the average of your self-assessment and your stakeholders\' assessment (if you included them in the review).  It puts you in the lower quartile of our database, and building your confidence as a team is an essential first step to rising up the database ratings. ' . "\n" . "\n");
  $pdf->Write(5, 'The key to building confidence is to work on improving step by step and get a few successes under your belt - much like an athlete would get back to fitness after an injury. ' );
}
?>
