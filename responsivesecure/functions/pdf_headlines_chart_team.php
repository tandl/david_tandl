
<?php

//Producing the headlines report bar charts for the Team Review
//Specifying the labels and values used in the reports
//Each line on the report shows data for three variables - the self score, database average, and stakeholder score (if requested in report set up)
//Items are the individual questions from the review
//Grand average is the average of all scored items on the questionnaire

//NB database averages and stakeholder averages have same variable names as the corresponding variables for teams.
//This is OK (but should be changed at some stage) because the data is stored in different tables
//and the report generation queries the team table for the team report and the in table for the ind report

//Specifying labels for the bar charts in the report

$topics = array('Clarity of team objectives', 'Focus on what customers want', 'Ambitious and measurable team targets', 'Inspiring communication', 'Generating optimism', 'Leading by example', 'Managing stakeholders', 'Securing skills and resources', 'Negotiating skill', 'Effective team meetings', 'Clear roles and responsibilities', 'Effective delegation', 'Full contribution to team discussions', 'Disagreements in the open and resolved', 'Collective achievement put above individual priorities', 'Can-do culture', 'Emotional intelligence', 'Appreciation and support', 'Collective performance management', 'Quality of performance data and reports', 'Team decision-making', 'Creativity and innovation', 'Systematic performance improvement based on data', 'Leading and delivering change', 'Meeting targets', 'Personal and professional fulfilment');


//Specifying individual item values for the bar charts in the report
$teamavefm = array($teamavgd1, $teamavgd2, $teamavgd3, $teamavgd4, $teamavgd5, $teamavgd6, $teamavgd7, $teamavgd8, $teamavgd9, $teamavgd10, $teamavgd11, $teamavgd12, $teamavgd13, $teamavgd14, $teamavgd15, $teamavgd16, $teamavgd17, $teamavgd18, $teamavgd19, $teamavgd20, $teamavgd21, $teamavgd22, $teamavgd23, $teamavgd24, $teamavgd25, $teamavgd26);

$dbavefm = array($dbavgd1, $dbavgd2, $dbavgd3, $dbavgd4, $dbavgd5, $dbavgd6, $dbavgd7, $dbavgd8, $dbavgd9, $dbavgd10, $dbavgd11, $dbavgd12, $dbavgd13, $dbavgd14, $dbavgd15, $dbavgd16, $dbavgd17, $dbavgd18, $dbavgd19, $dbavgd20, $dbavgd21, $dbavgd22, $dbavgd23, $dbavgd24, $dbavgd25, $dbavgd26);

$stakeavefm = array($stakeavgd1, $stakeavgd2, $stakeavgd3, $stakeavgd4, $stakeavgd5, $stakeavgd6, $stakeavgd7, $stakeavgd8, $stakeavgd9, $stakeavgd10, $stakeavgd11, $stakeavgd12, $stakeavgd13, $stakeavgd14, $stakeavgd15, $stakeavgd16, $stakeavgd17, $stakeavgd18, $stakeavgd19, $stakeavgd20, $stakeavgd21, $stakeavgd22, $stakeavgd23, $stakeavgd24, $stakeavgd25, $stakeavgd26);

$tvotedev = array($tvotedev1, $tvotedev2, $tvotedev3, $tvotedev4, $tvotedev5, $tvotedev6, $tvotedev7, $tvotedev8, $tvotedev9, $tvotedev10, $tvotedev11, $tvotedev12, $tvotedev13, $tvotedev14, $tvotedev15, $tvotedev16, $tvotedev17, $tvotedev18, $tvotedev19, $tvotedev20, $tvotedev21, $tvotedev22, $tvotedev23, $tvotedev24, $tvotedev25, $tvotedev26);

//Specifying faces values for the bar charts in the report
$facesteamscores = array($teamvisionaryavefm, $teamguardianavefm, $teampsychologistavefm, $teamdirectoravefm);
$facesdbscores = array($dbvisionaryavefm, $dbguardianavefm, $dbpsychologistavefm, $dbdirectoravefm);
$facesstakescores = array($stakevisionaryavefm, $stakeguardianavefm, $stakepsychologistavefm, $stakedirectoravefm);

//Specifying aspects values for the bar charts in the report
$aspectsteamscores = array($teamfocusavefm, $teamcommunicateavefm, $teamsecureavefm, $teamorganizeavefm, $teamuniteavefm, $teamempoweravefm, $teamdeliveravefm, $teamimproveavefm);
$aspectsdbscores = array($dbfocusavefm, $dbcommunicateavefm, $dbsecureavefm, $dborganizeavefm, $dbuniteavefm, $dbempoweravefm, $dbdeliveravefm, $dbimproveavefm);
$aspectsstakescores = array($stakefocusavefm, $stakecommunicateavefm, $stakesecureavefm, $stakeorganizeavefm, $stakeuniteavefm, $stakeempoweravefm, $stakedeliveravefm, $stakeimproveavefm);

// Put database top and bottom 10 percentile figs for items, calculated periodically using SPSS in arrays for benchmarking
$itemteamtoptenpc = array(85, 84, 81, 76, 80, 89, 92, 86, 79, 82, 81, 80, 83, 79, 92, 93, 87, 94, 77, 79, 82, 87, 81, 84, 84, 92);
$itemteambottenpc = array(29, 37, 26, 24, 29, 36, 42, 26, 29, 30, 27, 27, 25, 26, 35, 35, 30, 40, 23, 20, 30, 32, 23, 31, 41, 31);
// Put database top and bottom 10 percentile figs for aspects, calculated periodically using SPSS in arrays for benchmarking
$aspectsteamtoptenpc = array(79, 77, 79, 74, 78, 84, 71, 76);
$aspectsteambottenpc = array(39, 34, 42, 34, 35, 42, 35, 35);
// Put database top and bottom 10 percentile figs for faces, calculated periodically using SPSS in arrays for benchmarking
$facesteamtoptenpc = array(76, 73, 77, 72);
$facesteambottenpc = array(38, 41, 40, 37);
// Put database top and bottom 10 percentile figs for grand average, calculated periodically using SPSS in arrays for benchmarking
$teamgrandtoptenpc = 72;
$teamgrandbottenpc = 41;

?>


