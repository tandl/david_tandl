<?php

    $pdf->SetFont('Helvetica', '', 12);
    $pdf->Write(5, 'Your Signature Strengths' . "\n" . "\n");

//Printing out top 5 scores identified by the ind - ie their "Signature Strengths"

$indss = array();

$indss['%      '.$topics_ind_review[1][3]] = $indavgd1;
$indss['%      '.$topics_ind_review[2][3]] = $indavgd2;
$indss['%      '.$topics_ind_review[3][3]] = $indavgd3;
$indss['%      '.$topics_ind_review[4][3]] = $indavgd4;
$indss['%      '.$topics_ind_review[5][3]] = $indavgd5;
$indss['%      '.$topics_ind_review[6][3]] = $indavgd6;
$indss['%      '.$topics_ind_review[7][3]] = $indavgd7;
$indss['%      '.$topics_ind_review[8][3]] = $indavgd8;
$indss['%      '.$topics_ind_review[9][3]] = $indavgd9;
$indss['%      '.$topics_ind_review[10][3]] = $indavgd10;
$indss['%      '.$topics_ind_review[11][3]] = $indavgd11;
$indss['%      '.$topics_ind_review[12][3]] = $indavgd12;
$indss['%      '.$topics_ind_review[13][3]] = $indavgd13;
$indss['%      '.$topics_ind_review[14][3]] = $indavgd14;
$indss['%      '.$topics_ind_review[15][3]] = $indavgd15;
$indss['%      '.$topics_ind_review[16][3]] = $indavgd16;
$indss['%      '.$topics_ind_review[17][3]] = $indavgd17;
$indss['%      '.$topics_ind_review[18][3]] = $indavgd18;
$indss['%      '.$topics_ind_review[19][3]] = $indavgd19;
$indss['%      '.$topics_ind_review[20][3]] = $indavgd20;
$indss['%      '.$topics_ind_review[21][3]] = $indavgd21;
$indss['%      '.$topics_ind_review[22][3]] = $indavgd22;
$indss['%      '.$topics_ind_review[23][3]] = $indavgd23;
$indss['%      '.$topics_ind_review[24][3]] = $indavgd24;
$indss['%      '.$topics_ind_review[25][3]] = $indavgd25;
$indss['%      '.$topics_ind_review[26][3]] = $indavgd26;

$pdf->SetFont('Helvetica', '', 10);
$pdf->Write(5, 'Your view - the top 4 scores (see Page 4) as rated by you: ' . "\n" ."\n");
$pdf->SetFont('Helvetica', '', 9);




//Printing out any Signature Strengths identified by ind members

if (($indavgd1 === '-') && ($indavgd2 === '-') && ($indavgd3 === '-') && ($indavgd4 === '-') && ($indavgd5 === '-') && ($indavgd6 === '-') && ($indavgd7 === '-') && ($indavgd8 === '-') && ($indavgd9 === '-') && ($indavgd10 === '-') && ($indavgd11 === '-') && ($indavgd12 === '-') && ($indavgd13 === '-') && ($indavgd14 === '-') && ($indavgd15 === '-') && ($indavgd16 === '-') && ($indavgd17 === '-') && ($indavgd18 === '-') && ($indavgd19 === '-') && ($indavgd20 === '-') && ($indavgd21 === '-') && ($indavgd22 === '-') && ($indavgd23 === '-') && ($indavgd24 === '-') && ($indavgd25 === '-') && ($indavgd26 === '-')) {
    $pdf->Write(3, '       You didn\'t identify any signature strengths ' . "\n" . "\n");
} else {
    arsort($indss);
    $top4 = array_slice($indss, 0, 4);
    foreach ($top4 as $key => $value) {
        if ($value != '-') {
            $pdf->Write(3, '     ' . $value . ' ' . $key . "\n" . "\n");
        }
    }
}


$stakess = array();

$stakess['%      '.$topics_ind_review[1][4]] = $stakeavgd1;
$stakess['%      '.$topics_ind_review[2][4]] = $stakeavgd2;
$stakess['%      '.$topics_ind_review[3][4]] = $stakeavgd3;
$stakess['%      '.$topics_ind_review[4][4]] = $stakeavgd4;
$stakess['%      '.$topics_ind_review[5][4]] = $stakeavgd5;
$stakess['%      '.$topics_ind_review[6][4]] = $stakeavgd6;
$stakess['%      '.$topics_ind_review[7][4]] = $stakeavgd7;
$stakess['%      '.$topics_ind_review[8][4]] = $stakeavgd8;
$stakess['%      '.$topics_ind_review[9][4]] = $stakeavgd9;
$stakess['%      '.$topics_ind_review[10][4]] = $stakeavgd10;
$stakess['%      '.$topics_ind_review[11][4]] = $stakeavgd11;
$stakess['%      '.$topics_ind_review[12][4]] = $stakeavgd12;
$stakess['%      '.$topics_ind_review[13][4]] = $stakeavgd13;
$stakess['%      '.$topics_ind_review[14][4]] = $stakeavgd14;
$stakess['%      '.$topics_ind_review[15][4]] = $stakeavgd15;
$stakess['%      '.$topics_ind_review[16][4]] = $stakeavgd16;
$stakess['%      '.$topics_ind_review[17][4]] = $stakeavgd17;
$stakess['%      '.$topics_ind_review[18][4]] = $stakeavgd18;
$stakess['%      '.$topics_ind_review[19][4]] = $stakeavgd19;
$stakess['%      '.$topics_ind_review[20][4]] = $stakeavgd20;
$stakess['%      '.$topics_ind_review[21][4]] = $stakeavgd21;
$stakess['%      '.$topics_ind_review[22][4]] = $stakeavgd22;
$stakess['%      '.$topics_ind_review[23][4]] = $stakeavgd23;
$stakess['%      '.$topics_ind_review[24][4]] = $stakeavgd24;
$stakess['%      '.$topics_ind_review[25][4]] = $stakeavgd25;
$stakess['%      '.$topics_ind_review[26][4]] = $stakeavgd26;

$pdf->SetFont('Helvetica', '', 10);
$pdf->Write(5, "\n" . 'Your 360 contributors\' view - the top 4 scores (see Page 4) as rated by your contributors: ' . "\n" ."\n");
$pdf->SetFont('Helvetica', '', 9);


//Printing out any Signature Strengths identified by stakeholders

if (($stakeavgd1 === '-') && ($stakeavgd2 === '-') && ($stakeavgd3 === '-') && ($stakeavgd4 === '-') && ($stakeavgd5 === '-') && ($stakeavgd6 === '-') && ($stakeavgd7 === '-') && ($stakeavgd8 === '-') && ($stakeavgd9 === '-') && ($stakeavgd10 === '-') && ($stakeavgd11 === '-') && ($stakeavgd12 === '-') && ($stakeavgd13 === '-') && ($stakeavgd14 === '-') && ($stakeavgd15 === '-') && ($stakeavgd16 === '-') && ($stakeavgd17 === '-') && ($stakeavgd18 === '-') && ($stakeavgd19 === '-') && ($stakeavgd20 === '-') && ($stakeavgd21 === '-') && ($stakeavgd22 === '-') && ($stakeavgd23 === '-') && ($stakeavgd24 === '-') && ($stakeavgd25 === '-') && ($stakeavgd26 === '-')) {
    $pdf->Write(3, '       No stakeholders contibuted to this section of the review ' . "\n" . "\n");
} else {
    arsort($stakess);
    $top4 = array_slice($stakess, 0, 4);
    foreach ($top4 as $key => $value) {
        if ($value != '-') {
            $pdf->Write(3, '     ' . $value . ' ' . $key . "\n" . "\n");
        }
    }
}

    $pdf->SetFont('Helvetica', '', 12);
    $pdf->Write(5,  "\n" . "\n" . 'Your Challenges' . "\n");

//Printing out bottom 5 scoresidentified by the ind - ie their "Potential Fatal Flaws"

$indff = array();

/* These were the items re-worded slightly for the report - an idea I reverted on in April 2017
$indff['%      Not always clear on purpose and direction'] = $indavgd1;
$indff['%      Not always focused on key business goals'] = $indavgd2;
$indff['%      Does not always show high expectations of self and others'] = $indavgd3;
$indff['%      Does not always inspire others'] = $indavgd4;
$indff['%      Does not create a climate of optimism and confidence'] = $indavgd5;
$indff['%      Doesn\'t always set a great example -  actions don\'t always match words'] = $indavgd6;
$indff['%      Doesn\'t always build productive relationships with others'] = $indavgd7;
$indff['%      Not always good at managing priorities and workload'] = $indavgd8;
$indff['%      Not always a good negotiator'] = $indavgd9;
$indff['%      Not always well organized'] = $indavgd10;
$indff['%      Not always clear about roles and responsibilities'] = $indavgd11;
$indff['%      Doesn\'t always delegate well to others'] = $indavgd12;
$indff['%      Doesn\'t always communicate well'] = $indavgd13;
$indff['%      Doesn\'t always handle disagreement constructively'] = $indavgd14;
$indff['%      Not always a good team player'] = $indavgd15;
$indff['%      Doesn\'t always have a can-do attitude'] = $indavgd16;
$indff['%      Doesn\'t always express trust and confidence in people'] = $indavgd17;
$indff['%      Doesn\'t always support and encourage others'] = $indavgd18;
$indff['%      Doesn\'t always deliver what they say they will'] = $indavgd19;
$indff['%      Doesn\'t always listen to feedback and take appropriate action'] = $indavgd20;
$indff['%      Doesn\'t always make decision well'] = $indavgd21;
$indff['%      Is not always adaptable and innovative'] = $indavgd22;
$indff['%      Doesn\'t always strive to improve their performance'] = $indavgd23;
$indff['%      Not always good at making change happen'] = $indavgd24;
$indff['%      Doesn\'t always get great results from themselves and others'] = $indavgd25;
$indff['%      Doesn\'t always empower people around them'] = $indavgd26;
*/
/*
	This is the actual wording of the stakeholder questions in the online leadership style review
*/

$indff['%      '.$topics_ind_review[1][3]] = $indavgd1;
$indff['%      '.$topics_ind_review[2][3]] = $indavgd2;
$indff['%      '.$topics_ind_review[3][3]] = $indavgd3;
$indff['%      '.$topics_ind_review[4][3]] = $indavgd4;
$indff['%      '.$topics_ind_review[5][3]] = $indavgd5;
$indff['%      '.$topics_ind_review[6][3]] = $indavgd6;
$indff['%      '.$topics_ind_review[7][3]] = $indavgd7;
$indff['%      '.$topics_ind_review[8][3]] = $indavgd8;
$indff['%      '.$topics_ind_review[9][3]] = $indavgd9;
$indff['%      '.$topics_ind_review[10][3]] = $indavgd10;
$indff['%      '.$topics_ind_review[11][3]] = $indavgd11;
$indff['%      '.$topics_ind_review[12][3]] = $indavgd12;
$indff['%      '.$topics_ind_review[13][3]] = $indavgd13;
$indff['%      '.$topics_ind_review[14][3]] = $indavgd14;
$indff['%      '.$topics_ind_review[15][3]] = $indavgd15;
$indff['%      '.$topics_ind_review[16][3]] = $indavgd16;
$indff['%      '.$topics_ind_review[17][3]] = $indavgd17;
$indff['%      '.$topics_ind_review[18][3]] = $indavgd18;
$indff['%      '.$topics_ind_review[19][3]] = $indavgd19;
$indff['%      '.$topics_ind_review[20][3]] = $indavgd20;
$indff['%      '.$topics_ind_review[21][3]] = $indavgd21;
$indff['%      '.$topics_ind_review[22][3]] = $indavgd22;
$indff['%      '.$topics_ind_review[23][3]] = $indavgd23;
$indff['%      '.$topics_ind_review[24][3]] = $indavgd24;
$indff['%      '.$topics_ind_review[25][3]] = $indavgd25;
$indff['%      '.$topics_ind_review[26][3]] = $indavgd26;


$pdf->SetFont('Helvetica', '', 10);
$pdf->Write(5, "\n" . 'Your view - the bottom 4 scores (see Page 4) as rated by you: ' . "\n" ."\n");
$pdf->SetFont('Helvetica', '', 9);


//Printing out the Fatal Flaws identified by ind members

if (($indavgd1 === '-') && ($indavgd2 === '-') && ($indavgd3 === '-') && ($indavgd4 === '-') && ($indavgd5 === '-') && ($indavgd6 === '-') && ($indavgd7 === '-') && ($indavgd8 === '-') && ($indavgd9 === '-') && ($indavgd10 === '-') && ($indavgd11 === '-') && ($indavgd12 === '-') && ($indavgd13 === '-') && ($indavgd14 === '-') && ($indavgd15 === '-') && ($indavgd16 === '-') && ($indavgd17 === '-') && ($indavgd18 === '-') && ($indavgd19 === '-') && ($indavgd20 === '-') && ($indavgd21 === '-') && ($indavgd22 === '-') && ($indavgd23 === '-') && ($indavgd24 === '-') && ($indavgd25 === '-') && ($indavgd26 === '-')) {
    $pdf->Write(3, '       You didn\'t identify any potential fatal flaws ' . "\n" . "\n");
} else {
    $indff = array_diff($indff, array('-'));
    asort($indff);
    $bottom4 = array_slice($indff, 0, 4);
    foreach ($bottom4 as $key => $value) {
        $pdf->Write(3, '     ' . $value . ' ' . $key . "\n" . "\n");
    }
}

$stakeff = array();

/* These were the items re-worded slightly for the report - an idea I reverted on in April 2017
$stakeff['%      Not always clear on purpose and direction'] = $stakeavgd1;
$stakeff['%      Not always focused on key business goals'] = $stakeavgd2;
$stakeff['%      Does not always show high expectations of self and others'] = $stakeavgd3;
$stakeff['%      Does not always inspire others'] = $stakeavgd4;
$stakeff['%      Does not create a climate of optimism and confidence'] = $stakeavgd5;
$stakeff['%      Doesn\'t always set a great example -  actions don\'t always match words'] = $stakeavgd6;
$stakeff['%      Doesn\'t always build productive relationships with others'] = $stakeavgd7;
$stakeff['%      Not always good at managing priorities and workload'] = $stakeavgd8;
$stakeff['%      Not always a good negotiator'] = $stakeavgd9;
$stakeff['%      Not always well organized'] = $stakeavgd10;
$stakeff['%      Not always clear about roles and responsibilities'] = $stakeavgd11;
$stakeff['%      Doesn\'t always delegate well to others'] = $stakeavgd12;
$stakeff['%      Doesn\'t always communicate well'] = $stakeavgd13;
$stakeff['%      Doesn\'t always handle disagreement constructively'] = $stakeavgd14;
$stakeff['%      Not always a good team player'] = $stakeavgd15;
$stakeff['%      Doesn\'t always have a can-do attitude'] = $stakeavgd16;
$stakeff['%      Doesn\'t always express trust and confidence in people'] = $stakeavgd17;
$stakeff['%      Doesn\'t always support and encourage others'] = $stakeavgd18;
$stakeff['%      Doesn\'t always deliver what they say they will'] = $stakeavgd19;
$stakeff['%      Doesn\'t always listen to feedback and take appropriate action'] = $stakeavgd20;
$stakeff['%      Doesn\'t always make decision well'] = $stakeavgd21;
$stakeff['%      Is not always adaptable and innovative'] = $stakeavgd22;
$stakeff['%      Doesn\'t always strive to improve their performance'] = $stakeavgd23;
$stakeff['%      Not always good at making change happen'] = $stakeavgd24;
$stakeff['%      Doesn\'t always get great results from themselves and others'] = $stakeavgd25;
$stakeff['%      Doesn\'t always empower people around them'] = $stakeavgd26;
*/

/*
This is the actual wording of the stakeholder questions in the online review
*/

$stakeff['%      '.$topics_ind_review[1][4]] = $stakeavgd1;
$stakeff['%      '.$topics_ind_review[2][4]] = $stakeavgd2;
$stakeff['%      '.$topics_ind_review[3][4]] = $stakeavgd3;
$stakeff['%      '.$topics_ind_review[4][4]] = $stakeavgd4;
$stakeff['%      '.$topics_ind_review[5][4]] = $stakeavgd5;
$stakeff['%      '.$topics_ind_review[6][4]] = $stakeavgd6;
$stakeff['%      '.$topics_ind_review[7][4]] = $stakeavgd7;
$stakeff['%      '.$topics_ind_review[8][4]] = $stakeavgd8;
$stakeff['%      '.$topics_ind_review[9][4]] = $stakeavgd9;
$stakeff['%      '.$topics_ind_review[10][4]] = $stakeavgd10;
$stakeff['%      '.$topics_ind_review[11][4]] = $stakeavgd11;
$stakeff['%      '.$topics_ind_review[12][4]] = $stakeavgd12;
$stakeff['%      '.$topics_ind_review[13][4]] = $stakeavgd13;
$stakeff['%      '.$topics_ind_review[14][4]] = $stakeavgd14;
$stakeff['%      '.$topics_ind_review[15][4]] = $stakeavgd15;
$stakeff['%      '.$topics_ind_review[16][4]] = $stakeavgd16;
$stakeff['%      '.$topics_ind_review[17][4]] = $stakeavgd17;
$stakeff['%      '.$topics_ind_review[18][4]] = $stakeavgd18;
$stakeff['%      '.$topics_ind_review[19][4]] = $stakeavgd19;
$stakeff['%      '.$topics_ind_review[20][4]] = $stakeavgd20;
$stakeff['%      '.$topics_ind_review[21][4]] = $stakeavgd21;
$stakeff['%      '.$topics_ind_review[22][4]] = $stakeavgd22;
$stakeff['%      '.$topics_ind_review[23][4]] = $stakeavgd23;
$stakeff['%      '.$topics_ind_review[24][4]] = $stakeavgd24;
$stakeff['%      '.$topics_ind_review[25][4]] = $stakeavgd25;
$stakeff['%      '.$topics_ind_review[26][4]] = $stakeavgd26;

$pdf->SetFont('Helvetica', '', 10);
$pdf->Write(5, "\n" . 'Your contributors\' view - the bottom 4 scores (see Page 4) as rated by your contributors: ' . "\n" ."\n");
$pdf->SetFont('Helvetica', '', 9);


//Printing out the Fatal Flaws identified by ind members

if (($stakeavgd1 === '-') && ($stakeavgd2 === '-') && ($stakeavgd3 === '-') && ($stakeavgd4 === '-') && ($stakeavgd5 === '-') && ($stakeavgd6 === '-') && ($stakeavgd7 === '-') && ($stakeavgd8 === '-') && ($stakeavgd9 === '-') && ($stakeavgd10 === '-') && ($stakeavgd11 === '-') && ($stakeavgd12 === '-') && ($stakeavgd13 === '-') && ($stakeavgd14 === '-') && ($stakeavgd15 === '-') && ($stakeavgd16 === '-') && ($stakeavgd17 === '-') && ($stakeavgd18 === '-') && ($stakeavgd19 === '-') && ($stakeavgd20 === '-') && ($stakeavgd21 === '-') && ($stakeavgd22 === '-') && ($stakeavgd23 === '-') && ($stakeavgd24 === '-') && ($stakeavgd25 === '-') && ($stakeavgd26 === '-')) {
    $pdf->Write(3, '       No stakeholders contibuted to this section of the review ' . "\n" . "\n");
} else {
    $stakeff = array_diff($stakeff, array('-'));
    asort($stakeff);
    $bottom4 = array_slice($stakeff, 0, 4);
    foreach ($bottom4 as $key => $value) {
        $pdf->Write(3, '     ' . $value . ' ' . $key . "\n" . "\n");
    }
}


?>