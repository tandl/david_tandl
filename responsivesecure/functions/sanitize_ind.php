<?php

// Function to sanitize data input from ind questionnaires OTC-quest-ind_self.php, OTC-quest-ind_boss.php, OTC-quest-ind_contributor.php
// Checking submitted data - all digits are valid ctype digits and in the right range for all question responses, indrole and development priorities



    // Trim all the incoming data:
    $trimmed = array_map('trim', $_POST);
		
		$sid = filter_input(INPUT_POST, 'sid', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
		
    if (ctype_digit($trimmed['indrole']) && ((intval($trimmed['indrole']) === 1) || (intval($trimmed['indrole']) === 2) || (intval($trimmed['indrole']) === 3) || (intval($trimmed['indrole']) === 4) || (intval($trimmed['indrole']) === 99))) {
        $tr = $trimmed['indrole'];
    } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d1']) && ((intval($trimmed['d1']) === 0) || (intval($trimmed['d1']) === 1) || (intval($trimmed['d1']) === 2) || (intval($trimmed['d1']) === 3) || (intval($trimmed['d1']) === 4) || (intval($trimmed['d1']) === 99))) {
        $d1 = $trimmed['d1'];
    } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d2']) && ((intval($trimmed['d2']) === 0) || (intval($trimmed['d2']) === 1) || (intval($trimmed['d2']) === 2) || (intval($trimmed['d2']) === 3) || (intval($trimmed['d2']) === 4) || (intval($trimmed['d2']) === 99))) {
        $d2 = $trimmed['d2'];
    } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d3']) && ((intval($trimmed['d3']) === 0) || (intval($trimmed['d3']) === 1) || (intval($trimmed['d3']) === 2) || (intval($trimmed['d3']) === 3) || (intval($trimmed['d3']) === 4) || (intval($trimmed['d3']) === 99))) {
        $d3 = $trimmed['d3'];
    } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d4']) && ((intval($trimmed['d4']) === 0) || (intval($trimmed['d4']) === 1) || (intval($trimmed['d4']) === 2) || (intval($trimmed['d4']) === 3) || (intval($trimmed['d4']) === 4) || (intval($trimmed['d4']) === 99))) {
        $d4 = $trimmed['d4'];
    } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d5']) && ((intval($trimmed['d5']) === 0) || (intval($trimmed['d5']) === 1) || (intval($trimmed['d5']) === 2) || (intval($trimmed['d5']) === 3) || (intval($trimmed['d5']) === 4) || (intval($trimmed['d5']) === 99))) {
        $d5 = $trimmed['d5'];
    } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d6']) && ((intval($trimmed['d6']) === 0) || (intval($trimmed['d6']) === 1) || (intval($trimmed['d6']) === 2) || (intval($trimmed['d6']) === 3) || (intval($trimmed['d6']) === 4) || (intval($trimmed['d6']) === 99))) {
        $d6 = $trimmed['d6'];
    } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d7']) && ((intval($trimmed['d7']) === 0) || (intval($trimmed['d7']) === 1) || (intval($trimmed['d7']) === 2) || (intval($trimmed['d7']) === 3) || (intval($trimmed['d7']) === 4) || (intval($trimmed['d7']) === 99))) {
        $d7 = $trimmed['d7'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d8']) && ((intval($trimmed['d8']) === 0) || (intval($trimmed['d8']) === 1) || (intval($trimmed['d8']) === 2) || (intval($trimmed['d8']) === 3) || (intval($trimmed['d8']) === 4) || (intval($trimmed['d8']) === 99))) {
        $d8 = $trimmed['d8'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d9']) && ((intval($trimmed['d9']) === 0) || (intval($trimmed['d9']) === 1) || (intval($trimmed['d9']) === 2) || (intval($trimmed['d9']) === 3) || (intval($trimmed['d9']) === 4) || (intval($trimmed['d9']) === 99))) {
        $d9 = $trimmed['d9'];
                    } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d10']) && ((intval($trimmed['d10']) === 0) || (intval($trimmed['d10']) === 1) || (intval($trimmed['d10']) === 2) || (intval($trimmed['d10']) === 3) || (intval($trimmed['d10']) === 4) || (intval($trimmed['d10']) === 99))) {
        $d10 = $trimmed['d10'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d11']) && ((intval($trimmed['d11']) === 0) || (intval($trimmed['d11']) === 1) || (intval($trimmed['d11']) === 2) || (intval($trimmed['d11']) === 3) || (intval($trimmed['d11']) === 4) || (intval($trimmed['d11']) === 99))) {
        $d11 = $trimmed['d11'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d12']) && ((intval($trimmed['d12']) === 0) || (intval($trimmed['d12']) === 1) || (intval($trimmed['d12']) === 2) || (intval($trimmed['d12']) === 3) || (intval($trimmed['d12']) === 4) || (intval($trimmed['d12']) === 99))) {
        $d12 = $trimmed['d12'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d13']) && ((intval($trimmed['d13']) === 0) || (intval($trimmed['d13']) === 1) || (intval($trimmed['d13']) === 2) || (intval($trimmed['d13']) === 3) || (intval($trimmed['d13']) === 4) || (intval($trimmed['d13']) === 99))) {
        $d13 = $trimmed['d13'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d14']) && ((intval($trimmed['d14']) === 0) || (intval($trimmed['d14']) === 1) || (intval($trimmed['d14']) === 2) || (intval($trimmed['d14']) === 3) || (intval($trimmed['d14']) === 4) || (intval($trimmed['d14']) === 99))) {
        $d14 = $trimmed['d14'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d15']) && ((intval($trimmed['d15']) === 0) || (intval($trimmed['d15']) === 1) || (intval($trimmed['d15']) === 2) || (intval($trimmed['d15']) === 3) || (intval($trimmed['d15']) === 4) || (intval($trimmed['d15']) === 99))) {
        $d15 = $trimmed['d15'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d16']) && ((intval($trimmed['d16']) === 0) || (intval($trimmed['d16']) === 1) || (intval($trimmed['d16']) === 2) || (intval($trimmed['d16']) === 3) || (intval($trimmed['d16']) === 4) || (intval($trimmed['d16']) === 99))) {
        $d16 = $trimmed['d16'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d17']) && ((intval($trimmed['d17']) === 0) || (intval($trimmed['d17']) === 1) || (intval($trimmed['d17']) === 2) || (intval($trimmed['d17']) === 3) || (intval($trimmed['d17']) === 4) || (intval($trimmed['d17']) === 99))) {
        $d17 = $trimmed['d17'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d18']) && ((intval($trimmed['d18']) === 0) || (intval($trimmed['d18']) === 1) || (intval($trimmed['d18']) === 2) || (intval($trimmed['d18']) === 3) || (intval($trimmed['d18']) === 4) || (intval($trimmed['d18']) === 99))) {
        $d18 = $trimmed['d18'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d19']) && ((intval($trimmed['d19']) === 0) || (intval($trimmed['d19']) === 1) || (intval($trimmed['d19']) === 2) || (intval($trimmed['d19']) === 3) || (intval($trimmed['d19']) === 4) || (intval($trimmed['d19']) === 99))) {
        $d19 = $trimmed['d19'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d20']) && ((intval($trimmed['d20']) === 0) || (intval($trimmed['d20']) === 1) || (intval($trimmed['d20']) === 2) || (intval($trimmed['d20']) === 3) || (intval($trimmed['d20']) === 4) || (intval($trimmed['d20']) === 99))) {
        $d20 = $trimmed['d20'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d21']) && ((intval($trimmed['d21']) === 0) || (intval($trimmed['d21']) === 1) || (intval($trimmed['d21']) === 2) || (intval($trimmed['d21']) === 3) || (intval($trimmed['d21']) === 4) || (intval($trimmed['d21']) === 99))) {
        $d21 = $trimmed['d21'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d22']) && ((intval($trimmed['d22']) === 0) || (intval($trimmed['d22']) === 1) || (intval($trimmed['d22']) === 2) || (intval($trimmed['d22']) === 3) || (intval($trimmed['d22']) === 4) || (intval($trimmed['d22']) === 99))) {
        $d22 = $trimmed['d22'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d23']) && ((intval($trimmed['d23']) === 0) || (intval($trimmed['d23']) === 1) || (intval($trimmed['d23']) === 2) || (intval($trimmed['d23']) === 3) || (intval($trimmed['d23']) === 4) || (intval($trimmed['d23']) === 99))) {
        $d23 = $trimmed['d23'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d24']) && ((intval($trimmed['d24']) === 0) || (intval($trimmed['d24']) === 1) || (intval($trimmed['d24']) === 2) || (intval($trimmed['d24']) === 3) || (intval($trimmed['d24']) === 4) || (intval($trimmed['d24']) === 99))) {
        $d24 = $trimmed['d24'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d25']) && ((intval($trimmed['d25']) === 0) || (intval($trimmed['d25']) === 1) || (intval($trimmed['d25']) === 2) || (intval($trimmed['d25']) === 3) || (intval($trimmed['d25']) === 4) || (intval($trimmed['d25']) === 99))) {
        $d25 = $trimmed['d25'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['d26']) && ((intval($trimmed['d26']) === 0) || (intval($trimmed['d26']) === 1) || (intval($trimmed['d26']) === 2) || (intval($trimmed['d26']) === 3) || (intval($trimmed['d26']) === 4) || (intval($trimmed['d26']) === 99))) {
        $d26 = $trimmed['d26'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }


    if (ctype_digit($trimmed['dev1']) && ( (intval($trimmed['dev1']) === 0) || (intval($trimmed['dev1']) === 1) )) {
        $dev1 = $trimmed['dev1'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev2']) && ( (intval($trimmed['dev2']) === 0) || (intval($trimmed['dev2']) === 1) )) {
        $dev2 = $trimmed['dev2'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev3']) && ( (intval($trimmed['dev3']) === 0) || (intval($trimmed['dev3']) === 1) )) {
        $dev3 = $trimmed['dev3'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev4']) && ( (intval($trimmed['dev4']) === 0) || (intval($trimmed['dev4']) === 1) )) {
        $dev4 = $trimmed['dev4'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev5']) && ( (intval($trimmed['dev5']) === 0) || (intval($trimmed['dev5']) === 1) )) {
        $dev5 = $trimmed['dev5'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev6']) && ( (intval($trimmed['dev6']) === 0) || (intval($trimmed['dev6']) === 1) )) {
        $dev6 = $trimmed['dev6'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev7']) && ( (intval($trimmed['dev7']) === 0) || (intval($trimmed['dev7']) === 1) )) {
        $dev7 = $trimmed['dev7'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev8']) && ( (intval($trimmed['dev8']) === 0) || (intval($trimmed['dev8']) === 1) )) {
        $dev8 = $trimmed['dev8'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev9']) && ( (intval($trimmed['dev9']) === 0) || (intval($trimmed['dev9']) === 1) )) {
        $dev9 = $trimmed['dev9'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev10']) && ( (intval($trimmed['dev10']) === 0) || (intval($trimmed['dev10']) === 1) )) {
        $dev10 = $trimmed['dev10'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev11']) && ( (intval($trimmed['dev11']) === 0) || (intval($trimmed['dev11']) === 1) )) {
        $dev11 = $trimmed['dev11'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev12']) && ( (intval($trimmed['dev12']) === 0) || (intval($trimmed['dev12']) === 1) )) {
        $dev12 = $trimmed['dev12'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev13']) && ( (intval($trimmed['dev13']) === 0) || (intval($trimmed['dev13']) === 1) )) {
        $dev13 = $trimmed['dev13'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev14']) && ( (intval($trimmed['dev14']) === 0) || (intval($trimmed['dev14']) === 1) )) {
        $dev14 = $trimmed['dev14'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev15']) && ( (intval($trimmed['dev15']) === 0) || (intval($trimmed['dev15']) === 1) )) {
        $dev15 = $trimmed['dev15'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev16']) && ( (intval($trimmed['dev16']) === 0) || (intval($trimmed['dev16']) === 1) )) {
        $dev16 = $trimmed['dev16'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev17']) && ( (intval($trimmed['dev17']) === 0) || (intval($trimmed['dev17']) === 1) )) {
        $dev17 = $trimmed['dev17'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev18']) && ( (intval($trimmed['dev18']) === 0) || (intval($trimmed['dev18']) === 1) )) {
        $dev18 = $trimmed['dev18'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev19']) && ( (intval($trimmed['dev19']) === 0) || (intval($trimmed['dev19']) === 1) )) {
        $dev19 = $trimmed['dev19'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev20']) && ( (intval($trimmed['dev20']) === 0) || (intval($trimmed['dev20']) === 1) )) {
        $dev20 = $trimmed['dev20'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev21']) && ( (intval($trimmed['dev21']) === 0) || (intval($trimmed['dev21']) === 1) )) {
        $dev21 = $trimmed['dev21'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev22']) && ( (intval($trimmed['dev22']) === 0) || (intval($trimmed['dev22']) === 1) )) {
        $dev22 = $trimmed['dev22'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev23']) && ( (intval($trimmed['dev23']) === 0) || (intval($trimmed['dev23']) === 1) )) {
        $dev23 = $trimmed['dev23'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev24']) && ( (intval($trimmed['dev24']) === 0) || (intval($trimmed['dev24']) === 1) )) {
        $dev24 = $trimmed['dev24'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev25']) && ( (intval($trimmed['dev25']) === 0) || (intval($trimmed['dev25']) === 1) )) {
        $dev25 = $trimmed['dev25'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }
    if (ctype_digit($trimmed['dev26']) && ( (intval($trimmed['dev26']) === 0) || (intval($trimmed['dev26']) === 1) )) {
        $dev26 = $trimmed['dev26'];
            } else { $url = BASE_URL;  ob_end_clean();  header("Location: $url"); exit(); 
    }

    // echo'<p> All inputs are valid ctype digits and integers of an appropriate value</p>';
// Tight security needed here

    $gc = $trimmed['goodcomments'];
    $bc = $trimmed['badcomments'];


// End of submission sanitization      
?>
