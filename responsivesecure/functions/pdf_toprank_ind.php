<?php

// Sorting development votes into popularity order and printing out highest-voted topics with >1 vote

$ivote_dev = array();
$ivote_dev[0] = 0;
$ivote_dev[1] = intval($ivotedev1);
$ivote_dev[2] = intval($ivotedev2);
$ivote_dev[3] = intval($ivotedev3);
$ivote_dev[4] = intval($ivotedev4);
$ivote_dev[5] = intval($ivotedev5);
$ivote_dev[6] = intval($ivotedev6);
$ivote_dev[7] = intval($ivotedev7);
$ivote_dev[8] = intval($ivotedev8);
$ivote_dev[9] = intval($ivotedev9);
$ivote_dev[10] = intval($ivotedev10);
$ivote_dev[11] = intval($ivotedev11);
$ivote_dev[12] = intval($ivotedev12);
$ivote_dev[13] = intval($ivotedev13);
$ivote_dev[14] = intval($ivotedev14);
$ivote_dev[15] = intval($ivotedev15);
$ivote_dev[16] = intval($ivotedev16);
$ivote_dev[17] = intval($ivotedev17);
$ivote_dev[18] = intval($ivotedev18);
$ivote_dev[19] = intval($ivotedev19);
$ivote_dev[20] = intval($ivotedev20);
$ivote_dev[21] = intval($ivotedev21);
$ivote_dev[22] = intval($ivotedev22);
$ivote_dev[23] = intval($ivotedev23);
$ivote_dev[24] = intval($ivotedev24);
$ivote_dev[25] = intval($ivotedev25);
$ivote_dev[26] = intval($ivotedev26);

/*
$ivote_dev[1] = 1;
$ivote_dev[2] =1;
$ivote_dev[3] =1;
$ivote_dev[4] =1;
$ivote_dev[5] = 1;
$ivote_dev[6] =1;
$ivote_dev[7] =1;
$ivote_dev[8] = 1;
$ivote_dev[9] = 1;
$ivote_dev[10] = 1;
$ivote_dev[11] = 1;
$ivote_dev[12] = 1;
$ivote_dev[13] = 1;
$ivote_dev[14] = 1;
$ivote_dev[15] = 1;
$ivote_dev[16] = 1;
$ivote_dev[17] = 1;
$ivote_dev[18] = 1;
$ivote_dev[19] = 1;
$ivote_dev[20] = 1;
$ivote_dev[21] = 1;
$ivote_dev[22] = 1;
$ivote_dev[23] = 1;
$ivote_dev[24] = 1;
$ivote_dev[25] = 2;
$ivote_dev[26] = 1;
*/
$pdf->SetFont('Helvetica', '', 9);
$solution = 'Got through to here';
$largest_vote = 0;
$largest_vote = max($ivote_dev);
if ($largest_vote > 0) {
  $pdf->Write(5, 'The area(s) you think you most need to work on: ' . "\n");
 $pdf->SetLeftMargin(30);
  for ($i = 1; $i <= 26; $i++) {
    if ($ivote_dev[$i] === $largest_vote) {
      $pdf->SetFont('Helvetica', 'B', 9);
      $pdf->Write(5, "\n" . $topics_ind_review[$i][2] . "\n");
      $pdf->SetFont('Helvetica', '', 9);
      $pdf->Write(5, "Recommended development exercise link: ");
      $pdf->SetFont('','U');
      $link = $pdf->AddLink();
      if ($subdomainprefix === 'default'){
        $customised_link = 'https://';
      } else {
        $customised_link = 'https://'.$subdomainprefix.'.';
      }
$pdf->Write(5,$topics_ind_review[$i][1],$customised_link.$topics_ind_review[$i][5]);
$pdf->Write(5, "\n");
$pdf->SetFont('');
    }
  }
}
else {
  $pdf->Write(5, 'This report will help you work out just how to do it.' . "\n" . "\n");
}
  $pdf->SetLeftMargin(20);
  $pdf->Write(5, '  ' . "\n" . "\n");

$pdf->SetTextColor(0, 0, 0);
?>

