<?php

// Sorting development votes from team members into popularity order and printing out topics with >1 vote

arsort($tvote_dev);
if ($surveytype != 4) {
  $pdf->SetFont('Helvetica', '', 10);
  $pdf->Write(8, 'Team member suggestions for development: ' . "\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(8, 'The list below show areas that were picked by more than one team member: ' . "\n");
  if (($tvote_dev[1] === 0) && ($tvote_dev[2] === 0) && ($tvote_dev[3] === 0) && ($tvote_dev[4] === 0) && ($tvote_dev[5] === 0) && ($tvote_dev[6] === 0) && ($tvote_dev[7] === 0) && ($tvote_dev[8] === 0) && ($tvote_dev[9] === 0) && ($tvote_dev[10] === 0) && ($tvote_dev[11] === 0) && ($tvote_dev[12] === 0) && ($tvote_dev[13] === 0) && ($tvote_dev[14] === 0) && ($tvote_dev[15] === 0) && ($tvote_dev[16] === 0) && ($tvote_dev[17] === 0) && ($tvote_dev[18] === 0) && ($tvote_dev[19] === 0) && ($tvote_dev[20] === 0) && ($tvote_dev[21] === 0) && ($tvote_dev[22] === 0) && ($tvote_dev[23] === 0) && ($tvote_dev[24] === 0) && ($tvote_dev[25] === 0) && ($tvote_dev[26] === 0)) {
    $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, '       There were no suggestions for development. ' . "\n");
  }
  else {
    $count = 0;
    foreach ($tvote_dev as $key => $value) {
      if ($value > 1) {   // Producing items that had more than a single vote
        $count++;
        $pdf->SetFont('Helvetica', 'B', 9);
        $pdf->Write(5, '    ' . $value . '  votes  ' . $topics_team_review[$key][2] . "\n");
        $pdf->SetFont('Helvetica', '', 9);
        $pdf->Write(5, '                      Recommended team exercise link:   ');
        $pdf->SetFont('', 'U');
        $link = $pdf->AddLink();
        $pdf->Write(5, $topics_team_review[$key][1], $topics_team_review[$key][5]);
        $pdf->Write(5, "\n" . "\n");
      }
    }
    if ($count === 0) {
      $pdf->SetFont('Helvetica', '', 9);
      $pdf->Write(5, '       There were no suggestions with more than one vote. ' . "\n");
    }
  }
}
elseif ($surveytype === 4) {
  $pdf->SetFont('Helvetica', '', 10);
  $pdf->Write(8, 'Team member suggestions for development: ' . "\n");
  if (($tvote_dev[1] === 0) && ($tvote_dev[2] === 0) && ($tvote_dev[3] === 0) && ($tvote_dev[4] === 0) && ($tvote_dev[5] === 0) && ($tvote_dev[6] === 0) && ($tvote_dev[7] === 0) && ($tvote_dev[8] === 0) && ($tvote_dev[9] === 0) && ($tvote_dev[10] === 0) && ($tvote_dev[11] === 0) && ($tvote_dev[12] === 0) && ($tvote_dev[13] === 0) && ($tvote_dev[14] === 0) && ($tvote_dev[15] === 0) && ($tvote_dev[16] === 0) && ($tvote_dev[17] === 0) && ($tvote_dev[18] === 0) && ($tvote_dev[19] === 0) && ($tvote_dev[20] === 0) && ($tvote_dev[21] === 0) && ($tvote_dev[22] === 0) && ($tvote_dev[23] === 0) && ($tvote_dev[24] === 0) && ($tvote_dev[25] === 0) && ($tvote_dev[26] === 0)) {
    $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, '       There were no suggestions for development. ' . "\n");
  }
  else {
    foreach ($tvote_dev as $key => $value) {
      if ($value > 0) {  // Producing items that had just a single vote as it is a single respondent review
        $pdf->SetFont('Helvetica', 'B', 9);
        $pdf->Write(5, '    ' . $value . '  votes  ' . $topics_team_review[$key][2] . "\n");
        $pdf->SetFont('Helvetica', '', 9);
        $pdf->Write(5, '                      Recommended team exercise link:   ');
        $pdf->SetFont('', 'U');
        $link = $pdf->AddLink();
        $pdf->Write(5, $topics_team_review[$key][1], $topics_team_review[$key][5]);
        $pdf->Write(5, "\n" . "\n");
      }
    }
  }
}

// Sorting development votes from stakeholders into popularity order and printing out topics with >1 vote

// Bring in the stakeholder development votes
$svote_dev = array();
$svote_dev[0] = 0;
$svote_dev[1] = intval($svotedev1);
$svote_dev[2] = intval($svotedev2);
$svote_dev[3] = intval($svotedev3);
$svote_dev[4] = intval($svotedev4);
$svote_dev[5] = intval($svotedev5);
$svote_dev[6] = intval($svotedev6);
$svote_dev[7] = intval($svotedev7);
$svote_dev[8] = intval($svotedev8);
$svote_dev[9] = intval($svotedev9);
$svote_dev[10] = intval($svotedev10);
$svote_dev[11] = intval($svotedev11);
$svote_dev[12] = intval($svotedev12);
$svote_dev[13] = intval($svotedev13);
$svote_dev[14] = intval($svotedev14);
$svote_dev[15] = intval($svotedev15);
$svote_dev[16] = intval($svotedev16);
$svote_dev[17] = intval($svotedev17);
$svote_dev[18] = intval($svotedev18);
$svote_dev[19] = intval($svotedev19);
$svote_dev[20] = intval($svotedev20);
$svote_dev[21] = intval($svotedev21);
$svote_dev[22] = intval($svotedev22);
$svote_dev[23] = intval($svotedev23);
$svote_dev[24] = intval($svotedev24);
$svote_dev[25] = intval($svotedev25);
$svote_dev[26] = intval($svotedev26);

// Sorting development votes from stakeholders into popularity order and printing out topics with >1 vote

arsort($svote_dev);

// Testing zero votes for development

if ($surveytype != 4) {
  $pdf->SetFont('Helvetica', '', 10);
  $pdf->Write(8, "\n" . "\n" . 'Stakeholder suggestions for development: ' . "\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(8, 'The list below show areas that were picked by more than one stakeholder. ' . "\n");
  if (($svote_dev[1] === 0) && ($svote_dev[2] === 0) && ($svote_dev[3] === 0) && ($svote_dev[4] === 0) && ($svote_dev[5] === 0) && ($svote - dev[6] === 0) && ($svote_dev[7] === 0) && ($svote_dev[8] === 0) && ($svote_dev[9] === 0) && ($svote_dev[10] === 0) && ($svote_dev[11] === 0) && ($svote_dev[12] === 0) && ($svote_dev[13] === 0) && ($svote_dev[14] === 0) && ($svote_dev[15] === 0) && ($svote_dev[16] === 0) && ($svote_dev[17] === 0) && ($svote_dev[18] === 0) && ($svote_dev[19] === 0) && ($svote_dev[20] === 0) && ($svote_dev[21] === 0) && ($svote_dev[22] === 0) && ($svote_dev[23] === 0) && ($svote_dev[24] === 0) && ($svote_dev[25] === 0) && ($svote_dev[26] === 0)) {
    $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, '       There were no suggestions for development. ' . "\n");
  }
  else {
    $count = 0;
    foreach ($svote_dev as $key => $value) {
      if ($value > 1) {  // Producing items that had more than a single vote
        $count++;
        $pdf->SetFont('Helvetica', 'B', 9);
        $pdf->Write(5, '    ' . $value . '  votes  ' . $topics_team_review[$key][2] . "\n");
        $pdf->SetFont('Helvetica', '', 9);
        $pdf->Write(5, '                      Recommended team exercise link:   ');
        $pdf->SetFont('', 'U');
        $link = $pdf->AddLink();
        $pdf->Write(5, $topics_team_review[$key][1], $topics_team_review[$key][5]);
        $pdf->Write(5, "\n" . "\n");
      }
    }
    if ($count === 0) {
      $pdf->SetFont('Helvetica', '', 9);
      $pdf->Write(5, '       There were no suggestions with more than one vote. ' . "\n");
    }
  }
}
elseif ($surveytype === 4) {
  // This section is just left in in case a 'single respondent' review later gets extended to include some stakeholder respondents
  $pdf->SetFont('Helvetica', '', 10);
  $pdf->Write(8, 'Stakeholder suggestions for development: ' . "\n");
  if (($svote_dev[1] === 0) && ($svote_dev[2] === 0) && ($svote_dev[3] === 0) && ($svote_dev[4] === 0) && ($svote_dev[5] === 0) && ($svote - dev[6] === 0) && ($svote_dev[7] === 0) && ($svote_dev[8] === 0) && ($svote_dev[9] === 0) && ($svote_dev[10] === 0) && ($svote_dev[11] === 0) && ($svote_dev[12] === 0) && ($svote_dev[13] === 0) && ($svote_dev[14] === 0) && ($svote_dev[15] === 0) && ($svote_dev[16] === 0) && ($svote_dev[17] === 0) && ($svote_dev[18] === 0) && ($svote_dev[19] === 0) && ($svote_dev[20] === 0) && ($svote_dev[21] === 0) && ($svote_dev[22] === 0) && ($svote_dev[23] === 0) && ($svote_dev[24] === 0) && ($svote_dev[25] === 0) && ($svote_dev[26] === 0)) {
    $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, '       There were no suggestions for development from stakeholders. ' . "\n");
  }
  else {
    $count = 0;
    foreach ($svote_dev as $key => $value) {
      if ($value > 1) {  // Producing items that had more than a single vote
        $count++;
        $pdf->SetFont('Helvetica', 'B', 9);
        $pdf->Write(5, '    ' . $value . '  votes  ' . $topics_team_review[$key][2] . "\n");
        $pdf->SetFont('Helvetica', '', 9);
        $pdf->Write(5, '                      Recommended team exercise link:   ');
        $pdf->SetFont('', 'U');
        $link = $pdf->AddLink();
        $pdf->Write(5, $topics_team_review[$key][1], $topics_team_review[$key][5]);
        $pdf->Write(5, "\n" . "\n");
      }
    }
    if ($count === 0) {
      $pdf->SetFont('Helvetica', '', 9);
      $pdf->Write(5, '       There were no suggestions with more than one vote. ' . "\n");
    }
  }
}


$pdf->SetFont('Helvetica', '', 8);
$pdf->Write(5, '' . "\n" . "\n" . "\n" . 'No. of team respondents: ' . $numteamrespondents . "\n");
$pdf->Write(5, 'No. of stakeholder respondents: ' . $numstakerespondents . "\n" . "\n");
?>
