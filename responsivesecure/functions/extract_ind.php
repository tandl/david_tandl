
<?php

//NB NB NB I have made the calculations highly restrictive - input must be between 0 and 4 inclusive. This can easily be changed by altering the SELECT criteria. Restriction then is just the SQL type in the database.
// Function to extract individual and database averages and votes from the individual online questionnaire. Is, and is desgned to be an exact mirror of the ind questionnaire but geared to the individual. Data evetually sent to the inddata database

/* Assume connect and disconnect details are on the including page. If not, add:

  require_once (MYSQLI);
  if (!$dbc)
  {
  die('Could not connect you up to the database: ');
  }

  before the script below, and:

  mysqli_close($dbc);

  at the end
 */

// Initializing variables

/* Array to store CLIENT VARIABLES for use in production of customised pdf report
 *  As we are constructing distinct programmes for each type of report, this will need to be duplicated in the 'extract' file of each type
 * A client can only have 1 prefix, as that is the 'name' of the sub-domain they have licensed.
 * A client may have several email domains associated with their prefix (eg in a business with several email domains).  Each email domain can have its own logo if preferred.
 * Where more than one email domain is used, enter a secondary set(s) of client variables for each email domain. This secondary set(s) MUST all have the same prefix as the prefix in the primary client set.
 * Each secondary set for a client can have its own logo if required. Just enter the approriate logo image name and put the image in the images folder.
 * The logo will be reduced to a height of 25 pixels for the report. Dimensions should be entered in pixels. Width dimension should not be more than 2x height. If it is it will need editing.
 *
 * DJM 18.04.18
 */
$client_details = array(
  array("client_name", "client_email_domain", "sub_domain_prefix", "client_logo", "client_logo_width", "client_logo_height"),
  array("PHE", "phe.gov.uk", "phe", "phe-logo3.png", 213, 127),
  array("Croydon", "croydon.gov.uk", "croydon", "Croydon for report.png", 317, 100),
  array("EST", "est.org.uk", "est", "EST_125.png", 125, 95),
  array("CHP", "communityhealthparnerships.co.uk", "chp", "CHP logo 100.jpg", 162, 100),
  array("Red Cross", "redcross.org.uk", "redcross", "BritishRedCross for report.png", 512, 512),
  //array("Change-fx", "change-fx.com", "cfx", "analytics-1.png", 512, 512)
);

//Individual Topics & Page links array
$topics_ind_review = array(
  array('Topic ID', 'Topic Heading', 'Topic Description', 'Individual Question', 'Contributor Question', 'Exercise Link', 'Bar Chart Heading'),
  array(1, 'Clarifying objectives', 'Being clearer about purpose and objectives', 'Has clarity of purpose and direction', 'Has clarity of purpose and direction', 'teamsandleadership.net/expert-guidance/setting-direction/clarifying-objectives', 'Clarity of purpose'),
  array(2, 'Delivering what customers want', 'Putting more emphasis on business goals', 'Focuses clearly on key business goals', 'Focuses clearly on key business goals', 'teamsandleadership.net/expert-guidance/setting-direction/delivering-what-customers-want', 'Focus on key business goals'),
  array(3, 'Developing ambitious targets', 'Having ambitious and measurable targets', 'Has high expectations of self and others', 'Has high expectations of self and others', 'teamsandleadership.net/expert-guidance/exercise-setting-smart-compelling-objectives/exercises', 'High expectation of self and others'),
  array(4, 'Inspiring people', 'Communicating in an inspiring way', 'Inspires others', 'Inspires others', 'teamsandleadership.net/expert-guidance/leading-example/inspiring-people', 'Inspires others'),
  array(5, 'Generating optimism', 'Generating optimism and confidence', 'Creates a climate of optimism and confidence', 'Creates a climate of optimism and confidence', 'teamsandleadership.net/expert-guidance/leading-example/generating-optimism', 'Generates optimism'),
  array(6, 'Leading by example', 'Leading by example', 'Sets a great example - actions match words', 'Sets a great example - actions match words', 'teamsandleadership.net/expert-guidance/leading-example/leading-example', 'Leads by example'),
  array(7, 'Building productive relationships', 'Building good relationships with key stakeholders', 'Builds good relationships with others', 'Builds good relationships with others', 'teamsandleadership.net/expert-guidance/leading-change/managing-stakeholders', 'Builds productive relationships'),
  array(8, 'Influencing people', 'Ensuring your resources match your workload', 'Manages priorities and workload well', 'Manages priorities and workload well', 'teamsandleadership.net/expert-guidance/leading-example/influencing-people', 'Manages workload and priorities well'),
  array(9, 'Improving negotiating skills', 'Negotiating', 'Is a good negotiator', 'Is a good negotiator', 'teamsandleadership.net/expert-guidance/leading-change/improving-negotiating-skills', 'Negotiates well'),
  array(10, 'Reflecting on how you work', 'Improving personal organisation', 'Is well organised', 'Is well organised', 'teamsandleadership.net/expert-guidance/exercise-reflecting-how-you-work/exercises', 'Is well organised'),
  array(11, 'Clarifying roles and responsibilities', 'Being clear about roles and responsibilities', 'Is clear about roles and responsibilities', 'Is clear about roles and responsibilities', 'teamsandleadership.net/expert-guidance/organising-team/clarifying-roles-and-responsibilities', 'Is clear about roles and responsibilities'),
  array(12, 'Improving delegation', 'Delegating well', 'Delegates well to others when necessary', 'Delegates well to others when necessary', 'teamsandleadership.net/expert-guidance/organising-team/consulting-and-delegating', 'Delegates well when necessary'),
  array(13, 'Communicating well', 'Communicating well with colleagues', 'Communicates well', 'Communicates well', 'teamsandleadership.net/expert-guidance/leading-example/influencing-people', 'Communicates well'),
  array(14, 'Handling disagreement constructively', 'Handling disagreement and conflict', 'Handles disagreement constructively', 'Handles disagreement constructively', 'teamsandleadership.net/expert-guidance/building-teamwork/handling-disagreements', 'Handles disagreement constructively'),
  array(15, 'Improving team dynamics', 'Being a good team player', 'Is a good team player', 'Is a good team player', 'teamsandleadership.net/expert-guidance/building-teamwork/improving-team-dynamics', 'Is a good team player'),
  array(16, 'Becoming more engaged', 'Has a can-do attitude', 'Has a can-do attitude', 'Has a can-do attitude', 'teamsandleadership.net/expert-guidance/setting-direction/increasing-engagement', 'Has a can-do attitude'),
  array(17, 'Building emotional intelligence', 'Expressing trust and confidence in people', 'Expresses trust and confidence in people', 'Expresses trust and confidence in people', 'teamsandleadership.net/expert-guidance/empowering-people/emotional-intelligence', 'Expresses trust and confidence in people'),
  array(18, 'Thanking and supporting others', 'Thanking and supporting people', 'Provides support and encouragement to others', 'Provides support and encouragement to others', 'teamsandleadership.net/expert-guidance/empowering-people/empowering-people', 'Encourages and supports others'),
  array(19, 'Delivering on agreements', 'Making agreements and delivering on them', 'Delivers what they say they will', 'Delivers what they say they will', 'teamsandleadership.net/expert-guidance/leading-example/building-trust', 'Delivers what they say they will'),
  array(20, 'Reviewing progress', 'Seeking feedback and acting on it', 'Listens to feedback and takes appropriate action', 'Listens to feedback and takes appropriate action', 'teamsandleadership.net/expert-guidance/managing-performance/reviewing-your-progress', 'Listens to feedback and acts on it'),
  array(21, 'Improving decision-making', 'Improving decision-making', 'Makes decisions well', 'Makes decisions well', 'teamsandleadership.net/expert-guidance/managing-performance/making-decisions', 'Makes decisions well'),
  array(22, 'Creativity and innovation', 'Being more creative and innovative', 'Is adaptable and innovative', 'Is adaptable and innovative', 'teamsandleadership.net/expert-guidance/leading-change/encouraging-creativity-and-innovation', 'Adaptable and innovative'),
  array(23, 'Delivering continuous improvement', 'Continually striving to improve performance', 'Continually strives to improve personal performance', 'Continually strives to improve personal performance', 'teamsandleadership.net/expert-guidance/leading-change/delivering-continuous-improvement', 'Continually strives to improve performance'),
  array(24, 'Making change happen', 'Delivering change', 'Is good at making change happen', 'Is good at making change happen', 'teamsandleadership.net/expert-guidance/leading-change/leading-change-programmes', 'Good at making change happen'),
  array(25, 'Reviewing progress', 'Achieving the results promised', 'Gets great results from themselves and others', 'Gets great results from themselves and others', 'teamsandleadership.net/expert-guidance/managing-performance/reviewing-your-progress', 'Gets great results from self and others'),
  array(26, 'Empowering people', 'Empowers people around them', 'Empowers people around them', 'Empowers people around them', 'teamsandleadership.net/expert-guidance/empowering-people/empowering-people', 'Empowers people around them')
);

$indavgd1 = 99;
$indavgd2 = 99;
$indavgd3 = 99;
$indavgd4 = 99;
$indavgd5 = 99;
$indavgd6 = 99;
$indavgd7 = 99;
$indavgd8 = 99;
$indavgd9 = 99;
$indavgd10 = 99;
$indavgd11 = 99;
$indavgd12 = 99;
$indavgd13 = 99;
$indavgd14 = 99;
$indavgd15 = 99;
$indavgd16 = 99;
$indavgd17 = 99;
$indavgd18 = 99;
$indavgd19 = 99;
$indavgd20 = 99;
$indavgd21 = 99;
$indavgd22 = 99;
$indavgd23 = 99;
$indavgd24 = 99;
$indavgd25 = 99;
$indavgd26 = 99;

$stakeavgd1 = 99;
$stakeavgd2 = 99;
$stakeavgd3 = 99;
$stakeavgd4 = 99;
$stakeavgd5 = 99;
$stakeavgd6 = 99;
$stakeavgd7 = 99;
$stakeavgd8 = 99;
$stakeavgd9 = 99;
$stakeavgd10 = 99;
$stakeavgd11 = 99;
$stakeavgd12 = 99;
$stakeavgd13 = 99;
$stakeavgd14 = 99;
$stakeavgd15 = 99;
$stakeavgd16 = 99;
$stakeavgd17 = 99;
$stakeavgd18 = 99;
$stakeavgd19 = 99;
$stakeavgd20 = 99;
$stakeavgd21 = 99;
$stakeavgd22 = 99;
$stakeavgd23 = 99;
$stakeavgd24 = 99;
$stakeavgd25 = 99;
$stakeavgd26 = 99;

// Use most likely figures as defaults for db vars which get printed

$dbavgd1 = 56;
$dbavgd2 = 57;
$dbavgd3 = 50;
$dbavgd4 = 51;
$dbavgd5 = 54;
$dbavgd6 = 72;
$dbavgd7 = 63;
$dbavgd8 = 55;
$dbavgd9 = 54;
$dbavgd10 = 57;
$dbavgd11 = 62;
$dbavgd12 = 53;
$dbavgd13 = 55;
$dbavgd14 = 56;
$dbavgd15 = 69;
$dbavgd16 = 62;
$dbavgd17 = 59;
$dbavgd18 = 64;
$dbavgd19 = 41;
$dbavgd20 = 48;
$dbavgd21 = 60;
$dbavgd22 = 60;
$dbavgd23 = 52;
$dbavgd24 = 63;
$dbavgd25 = 63;
$dbavgd26 = 64;

$dbfocus = 0;
$dbfocusdenom = 0;
$dbfocusave = 54;
$dbfocusavefm = 54;

$dbcommunicate = 0;
$dbcommunicatedenom = 0;
$dbcommunicateave = 59;
$dbcommunicateavefm = 59;

$dbsecure = 0;
$dbsecuredenom = 0;
$dbsecureave = 57;
$dbsecureavefm = 57;

$dborganize = 0;
$dborganizedenom = 0;
$dborganizeave = 57;
$dborganizeavefm = 57;

$dbunite = 0;
$dbunitedenom = 0;
$dbuniteave = 60;
$dbuniteavefm = 60;

$dbempower = 0;
$dbempowerdenom = 0;
$dbempowerave = 62;
$dbempoweravefm = 62;

$dbdeliver = 0;
$dbdeliverdenom = 0;
$dbdeliverave = 53;
$dbdeliveravefm = 53;

$dbimprove = 0;
$dbimprovedenom = 0;
$dbimproveave = 58;
$dbimproveavefm = 58;

$dbvisionaryave = 57;
$dbvisionaryavefm = 57;

$dbguardianave = 57;
$dbguardianavefm = 57;

$dbpsychologistave = 61;
$dbpsychologistavefm = 61;

$dbdirectorave = 56;
$dbdirectoravefm = 56;

$dbgrandave = 58;
$dbgrandavefm = 58;

$indfocus = 0;
$indfocusdenom = 0;
$indfocusave = 0;
$indfocusavefm = 0;

$indcommunicate = 0;
$indcommunicatedenom = 0;
$indcommunicateave = 0;
$indcommunicateavefm = 0;

$indsecure = 0;
$indsecuredenom = 0;
$indsecureave = 0;
$indsecureavefm = 0;

$indorganize = 0;
$indorganizedenom = 0;
$indorganizeave = 0;
$indorganizeavefm = 0;

$indunite = 0;
$indunitedenom = 0;
$induniteave = 0;
$induniteavefm = 0;

$indempower = 0;
$indempowerdenom = 0;
$indempowerave = 0;
$indempoweravefm = 0;

$inddeliver = 0;
$inddeliverdenom = 0;
$inddeliverave = 0;
$inddeliveravefm = 0;

$indimprove = 0;
$indimprovedenom = 0;
$indimproveave = 0;
$indimproveavefm = 0;

$indvisionaryave = 0;
$indvisionaryavefm = 0;

$indguardianave = 0;
$indguardianavefm = 0;

$indpsychologistave = 0;
$indpsychologistavefm = 0;

$inddirectorave = 0;
$inddirectoravefm = 0;

$indgrandave = 0;
$indgrandavefm = 0;

$stakefocus = 0;
$stakefocusdenom = 0;
$stakefocusave = 0;
$stakefocusavefm = 0;

$stakecommunicate = 0;
$stakecommunicatedenom = 0;
$stakecommunicateave = 0;
$stakecommunicateavefm = 0;

$stakesecure = 0;
$stakesecuredenom = 0;
$stakesecureave = 0;
$stakesecureavefm = 0;

$stakeorganize = 0;
$stakeorganizedenom = 0;
$stakeorganizeave = 0;
$stakeorganizeavefm = 0;

$stakeunite = 0;
$stakeunitedenom = 0;
$stakeuniteave = 0;
$stakeuniteavefm = 0;

$stakeempower = 0;
$stakeempowerdenom = 0;
$stakeempowerave = 0;
$stakeempoweravefm = 0;

$stakedeliver = 0;
$stakedeliverdenom = 0;
$stakedeliverave = 0;
$stakedeliveravefm = 0;

$stakeimprove = 0;
$stakeimprovedenom = 0;
$stakeimproveave = 0;
$stakeimproveavefm = 0;

$stakevisionaryave = 0;
$stakevisionaryavefm = 0;

$stakeguardianave = 0;
$stakeguardianavefm = 0;

$stakepsychologistave = 0;
$stakepsychologistavefm = 0;

$stakedirectorave = 0;
$stakedirectoravefm = 0;

$stakegrandave = 0;
$stakegrandavefm = 0;

$iandsfocusave = 0;
$iandscommunicateave = 0;
$iandssecureave = 0;
$iandsorganizeave = 0;
$iandsuniteave = 0;
$iandsempowerave = 0;
$iandsdeliverave = 0;
$iandsimproveave = 0;

$iandsvisionaryave = 0;
$iandsguardianave = 0;
$iandspsychologistave = 0;
$iandsdirectorave = 0;
$iandsgrandave = 0;

$focushigh = 0;
$communicatehigh = 0;
$securehigh = 0;
$organizehigh = 0;
$unitehigh = 0;
$empowerhigh = 0;
$deliverhigh = 0;
$improvehigh = 0;


$focuscomment = '';
$communicatecomment = '';
$securecomment = '';
$organizecomment = '';
$unitecomment = '';
$empowercomment = '';
$delivercomment = '';
$improvecomment = '';
$visionarycomment = '';
$guardiancomment = '';
$psychologistcomment = '';
$directorcomment = '';
$grandcomment = '';



//Query database for ind averages and votes  for development
//Getting  votes for development from the subject of the review
$indvotes = mysqli_query($dbc, "SELECT  indsurveyid, SUM(dev1), SUM(dev2), SUM(dev3), SUM(dev4), SUM(dev5) , SUM(dev6), SUM(dev7) ,SUM(dev8),SUM(dev9), SUM(dev10), SUM(dev11), SUM(dev12), SUM(dev13) , SUM(dev14), SUM(dev15) ,SUM(dev16),SUM(dev17), SUM(dev18), SUM(dev19), SUM(dev20), SUM(dev21) , SUM(dev22), SUM(dev23) ,SUM(dev24),SUM(dev25), SUM(dev26) FROM inddata  WHERE ((indsurveyid = '$reportrequested') AND (indrole=1))");
$getindvotes = mysqli_fetch_array($indvotes) or die(mysqli_error());

//Getting boss + contributor votes for development
$stakevotes = mysqli_query($dbc, "SELECT  indsurveyid, SUM(dev1), SUM(dev2), SUM(dev3), SUM(dev4), SUM(dev5) , SUM(dev6), SUM(dev7) ,SUM(dev8),SUM(dev9), SUM(dev10), SUM(dev11), SUM(dev12), SUM(dev13) , SUM(dev14), SUM(dev15) ,SUM(dev16),SUM(dev17), SUM(dev18), SUM(dev19), SUM(dev20), SUM(dev21) , SUM(dev22), SUM(dev23) ,SUM(dev24),SUM(dev25), SUM(dev26) FROM inddata  WHERE ((indsurveyid = '$reportrequested') AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$getstakevotes = mysqli_fetch_array($stakevotes) or die(mysqli_error());

//Extract ind averages and summary stats
//Query database for d1 averages
$d1result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d1) FROM inddata WHERE  (((d1>=0) AND (d1<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d1score = mysqli_fetch_array($d1result) or die(mysqli_error());
if (!$d1score['AVG(d1)']) {
  $indavgd1 = '-';
  $d1selfcount = 0;
}
else {
  $d1selfcount = 1;
  $indavgd1 = number_format(25 * $d1score['AVG(d1)'], 0);
  $indfocus = $indfocus + $d1score['AVG(d1)'];
  $indfocusdenom++;
}
//echo "After d1 ind focus = $indfocus and indfocusdenom = $indfocusdenom <br />"  ;

$d1stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d1) FROM inddata WHERE  (((d1>=0) AND (d1<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d1stakescore = mysqli_fetch_array($d1stake) or die(mysqli_error());
if (!$d1stakescore['AVG(d1)']) {
  $stakeavgd1 = '-';
  $d1stakecount = 0;
}
else {
  $d1stakecount = 1;
  $stakeavgd1 = number_format(25 * $d1stakescore['AVG(d1)'], 0);
  $stakefocus = $stakefocus + $d1stakescore['AVG(d1)'];
  $stakefocusdenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d1selfcount === 0) AND ($d1stakecount === 0)) {
  $d1ave = 99;
  } else if (($d1selfcount === 0) AND ($d1stakecount != 0)) {
  $d1ave = $d1stakescore['AVG(d1)'];
  } else if (($d1selfcount != 0) AND ($d1stakecount === 0)) {
  $d1ave = $d1score['AVG(d1)'];
  } else {
  $d1ave = ($d1score['AVG(d1)'] + $d1stakescore['AVG(d1)']) / 2;
  } */

//Query database for d2 averages
$d2result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d2) FROM inddata WHERE  (((d2>=0) AND (d2<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d2score = mysqli_fetch_array($d2result) or die(mysqli_error());
if (!$d2score['AVG(d2)']) {
  $indavgd2 = '-';
  $d2selfcount = 0;
}
else {
  $d2selfcount = 1;
  $indavgd2 = number_format(25 * $d2score['AVG(d2)'], 0);
  $indfocus = $indfocus + $d2score['AVG(d2)'];
  $indfocusdenom++;
}
//echo "After d2 ind focus = $indfocus and indfocusdenom = $indfocusdenom<br />"  ;

$d2stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d2) FROM inddata WHERE  (((d2>=0) AND (d2<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d2stakescore = mysqli_fetch_array($d2stake) or die(mysqli_error());
if (!$d2stakescore['AVG(d2)']) {
  $stakeavgd2 = '-';
  $d2stakecount = 0;
}
else {
  $d2stakecount = 1;
  $stakeavgd2 = number_format(25 * $d2stakescore['AVG(d2)'], 0);
  $stakefocus = $stakefocus + $d2stakescore['AVG(d2)'];
  $stakefocusdenom++;
}

/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d2selfcount === 0) AND ($d2stakecount === 0)) {
  $d2ave = 99;
  } else if (($d2selfcount === 0) AND ($d2stakecount != 0)) {
  $d2ave = $d2stakescore['AVG(d2)'];
  } else if (($d2selfcount != 0) AND ($d2stakecount === 0)) {
  $d2ave = $d2score['AVG(d2)'];
  } else {
  $d2ave = ($d2score['AVG(d2)'] + $d2stakescore['AVG(d2)']) / 2;
  } */


//Query database for d3 averages
$d3result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d3) FROM inddata WHERE  (((d3>=0) AND (d3<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d3score = mysqli_fetch_array($d3result) or die(mysqli_error());
if (!$d3score['AVG(d3)']) {
  $indavgd3 = '-';
  $d3selfcount = 0;
}
else {
  $d3selfcount = 1;
  $indavgd3 = number_format(25 * $d3score['AVG(d3)'], 0);
  $indfocus = $indfocus + $d3score['AVG(d3)'];
  $indfocusdenom++;
}
//echo "After d3 ind focus = $indfocus and indfocusdenom = $indfocusdenom<br />"  ;


$d3stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d3) FROM inddata WHERE  (((d3>=0) AND (d3<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d3stakescore = mysqli_fetch_array($d3stake) or die(mysqli_error());
if (!$d3stakescore['AVG(d3)']) {
  $stakeavgd3 = '-';
  $d3stakecount = 0;
}
else {
  $d3stakecount = 1;
  $stakeavgd3 = number_format(25 * $d3stakescore['AVG(d3)'], 0);
  $stakefocus = $stakefocus + $d3stakescore['AVG(d3)'];
  $stakefocusdenom++;
}

/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d3selfcount === 0) AND ($d3stakecount === 0)) {
  $d3ave = 99;
  } else if (($d3selfcount === 0) AND ($d3stakecount != 0)) {
  $d3ave = $d3stakescore['AVG(d3)'];
  } else if (($d3selfcount != 0) AND ($d3stakecount === 0)) {
  $d3ave = $d3score['AVG(d3)'];
  } else {
  $d3ave = ($d3score['AVG(d3)'] + $d3stakescore['AVG(d3)']) / 2;
  } */

//Calculating stakefocus
if ($stakefocusdenom === 0) {
  $stakefocusave = '-';
  $stakefocusavefm = '-';
}
else {
  $stakefocusave = 25 * $stakefocus / $stakefocusdenom;
  $stakefocusavefm = number_format($stakefocusave, 0);
}
//echo" stakefocusave = $stakefocusave" ;
//Calculating indfocus
if ($indfocusdenom === 0) {
  $indfocusave = '-';
  $indfocusavefm = '-';
}
else {
  $indfocusave = 25 * $indfocus / $indfocusdenom;
  $indfocusavefm = number_format($indfocusave, 0);
}

// Calculating tandsfocus for star-rating
$iandsfocusdenom = $indfocusdenom + $stakefocusdenom;
$iandsfocus = $indfocus + $stakefocus;
if ($iandsfocusdenom === 0) {
  $focuscomment = '';
}
else {
  $iandsfocusave = 25 * ($iandsfocus / $iandsfocusdenom);
  if ($iandsfocusave >= 69.64) {
    $focushigh = 1;
    $focuscomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
  }
  elseif ($iandsfocusave >= 62.33 && $iandsfocusave < 69.64) {
    $focushigh = 1;
    $focuscomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
  }
  else {
    $focuscomment = '';
  }
}

//Query database for d4 averages
$d4result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d4) FROM inddata WHERE  (((d4>=0) AND (d4<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d4score = mysqli_fetch_array($d4result) or die(mysqli_error());
if (!$d4score['AVG(d4)']) {
  $indavgd4 = '-';
  $d4selfcount = 0;
}
else {
  $d4selfcount = 1;
  $indavgd4 = number_format(25 * $d4score['AVG(d4)'], 0);
  $indcommunicate = $indcommunicate + $d4score['AVG(d4)'];
  $indcommunicatedenom++;
}
//echo "After d4 ind communicate = $indcommunicate and indcommunicatedenom = $indcommunicatedenom <br />"  ;

$d4stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d4) FROM inddata WHERE  (((d4>=0) AND (d4<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d4stakescore = mysqli_fetch_array($d4stake) or die(mysqli_error());
if (!$d4stakescore['AVG(d4)']) {
  $stakeavgd4 = '-';
  $d4stakecount = 0;
}
else {
  $d4stakecount = 1;
  $stakeavgd4 = number_format(25 * $d4stakescore['AVG(d4)'], 0);
  $stakecommunicate = $stakecommunicate + $d4stakescore['AVG(d4)'];
  $stakecommunicatedenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d4selfcount === 0) AND ($d4stakecount === 0)) {
  $d4ave = 99;
  } else if (($d4selfcount === 0) AND ($d4stakecount != 0)) {
  $d4ave = $d4stakescore['AVG(d4)'];
  } else if (($d4selfcount != 0) AND ($d4stakecount === 0)) {
  $d4ave = $d4score['AVG(d4)'];
  } else {
  $d4ave = ($d4score['AVG(d4)'] + $d4stakescore['AVG(d4)']) / 2;
  } */

//Query database for d5 averages
$d5result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d5) FROM inddata WHERE  (((d5>=0) AND (d5<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d5score = mysqli_fetch_array($d5result) or die(mysqli_error());
if (!$d5score['AVG(d5)']) {
  $indavgd5 = '-';
  $d5selfcount = 0;
}
else {
  $d5selfcount = 1;
  $indavgd5 = number_format(25 * $d5score['AVG(d5)'], 0);
  $indcommunicate = $indcommunicate + $d5score['AVG(d5)'];
  $indcommunicatedenom++;
}
//echo "After d5 ind communicate = $indcommunicate and indcommunicatedenom = $indcommunicatedenom <br />"  ;

$d5stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d5) FROM inddata WHERE  (((d5>=0) AND (d5<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d5stakescore = mysqli_fetch_array($d5stake) or die(mysqli_error());
if (!$d5stakescore['AVG(d5)']) {
  $stakeavgd5 = '-';
  $d5stakecount = 0;
}
else {
  $d5stakecount = 1;
  $stakeavgd5 = number_format(25 * $d5stakescore['AVG(d5)'], 0);
  $stakecommunicate = $stakecommunicate + $d5stakescore['AVG(d5)'];
  $stakecommunicatedenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d5selfcount === 0) AND ($d5stakecount === 0)) {
  $d5ave = 99;
  } else if (($d5selfcount === 0) AND ($d5stakecount != 0)) {
  $d5ave = $d5stakescore['AVG(d5)'];
  } else if (($d5selfcount != 0) AND ($d5stakecount === 0)) {
  $d5ave = $d5score['AVG(d5)'];
  } else {
  $d5ave = ($d5score['AVG(d5)'] + $d5stakescore['AVG(d5)']) / 2;
  } */

//Query database for d6 averages
$d6result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d6) FROM inddata WHERE  (((d6>=0) AND (d6<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d6score = mysqli_fetch_array($d6result) or die(mysqli_error());
if (!$d6score['AVG(d6)']) {
  $indavgd6 = '-';
  $d6selfcount = 0;
}
else {
  $d6selfcount = 1;
  $indavgd6 = number_format(25 * $d6score['AVG(d6)'], 0);
  $indcommunicate = $indcommunicate + $d6score['AVG(d6)'];
  $indcommunicatedenom++;
}
//echo "After d6 ind communicate = $indcommunicate and indcommunicatedenom = $indcommunicatedenom <br />"  ;

$d6stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d6) FROM inddata WHERE  (((d6>=0) AND (d6<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d6stakescore = mysqli_fetch_array($d6stake) or die(mysqli_error());
if (!$d6stakescore['AVG(d6)']) {
  $stakeavgd6 = '-';
  $d6stakecount = 0;
}
else {
  $d6stakecount = 1;
  $stakeavgd6 = number_format(25 * $d6stakescore['AVG(d6)'], 0);
  $stakecommunicate = $stakecommunicate + $d6stakescore['AVG(d6)'];
  $stakecommunicatedenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d6selfcount === 0) AND ($d6stakecount === 0)) {
  $d6ave = 99;
  } else if (($d6selfcount === 0) AND ($d6stakecount != 0)) {
  $d6ave = $d6stakescore['AVG(d6)'];
  } else if (($d6selfcount != 0) AND ($d6stakecount === 0)) {
  $d6ave = $d6score['AVG(d6)'];
  } else {
  $d6ave = ($d6score['AVG(d6)'] + $d6stakescore['AVG(d6)']) / 2;
  } */

//Calculating stakecommunicate
if ($stakecommunicatedenom === 0) {
  $stakecommunicateave = '-';
  $stakecommunicateavefm = '-';
}
else {
  $stakecommunicateave = 25 * $stakecommunicate / $stakecommunicatedenom;
  $stakecommunicateavefm = number_format($stakecommunicateave, 0);
}
//echo" stakecommunicateave = $stakecommunicateave" ;
//Calculating indcommunicate
if ($indcommunicatedenom === 0) {
  $indcommunicateave = '-';
  $indcommunicateavefm = '-';
}
else {
  $indcommunicateave = 25 * $indcommunicate / $indcommunicatedenom;
  $indcommunicateavefm = number_format($indcommunicateave, 0);
}

// Calculating tandscommunicate for star-rating
$iandscommunicatedenom = $indcommunicatedenom + $stakecommunicatedenom;
$iandscommunicate = $indcommunicate + $stakecommunicate;
if ($iandscommunicatedenom === 0) {
  $communicatecomment = '';
}
else {
  $iandscommunicateave = 25 * ($iandscommunicate / $iandscommunicatedenom);
  if ($iandscommunicateave >= 74.17) {
    $communicatehigh = 1;
    $communicatecomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
  }
  elseif ($iandscommunicateave >= 66.87 && $iandscommunicateave < 74.17) {
    $communicatehigh = 1;
    $communicatecomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
  }
  else {
    $communicatecomment = '';
  };
}

// Calculating indvisionary
if (($indfocusdenom === 0) && ($indcommunicatedenom === 0)) {
  $indvisionaryave = '-';
  $indvisionaryavefm = '-';
}
if (($indfocusdenom != 0) && ($indcommunicatedenom === 0)) {
  $indvisionaryave = $indfocusave;
  $indvisionaryavefm = $indfocusavefm;
}
if (($indfocusdenom === 0) && ($indcommunicatedenom != 0)) {
  $indvisionaryave = $indcommunicateave;
  $indvisionaryavefm = $indcommunicateavefm;
}
if (($indfocusdenom != 0) && ($indcommunicatedenom != 0)) {
  $indvisionaryave = 0.5 * ($indfocusave + $indcommunicateave);
  $indvisionaryavefm = number_format($indvisionaryave, 0);
}

// Calculating stakevisionary
if (($stakefocusdenom === 0) && ($stakecommunicatedenom === 0)) {
  $stakevisionaryave = '-';
  $stakevisionaryavefm = '-';
}
if (($stakefocusdenom != 0) && ($stakecommunicatedenom === 0)) {
  $stakevisionaryave = $stakefocusave;
  $stakevisionaryavefm = $stakefocusavefm;
}
if (($stakefocusdenom === 0) && ($stakecommunicatedenom != 0)) {
  $stakevisionaryave = $stakecommunicateave;
  $stakevisionaryavefm = $stakecommunicateavefm;
}
if (($stakefocusdenom != 0) && ($stakecommunicatedenom != 0)) {
  $stakevisionaryave = 0.5 * ($stakefocusave + $stakecommunicateave);
  $stakevisionaryavefm = number_format($stakevisionaryave, 0);
}

//Calculating tandsvisionary for star-rating
if (($iandsfocusdenom === 0) && ($iandscommunicatedenom === 0)) {

//just for the sake of calculation - actually it does not exist in reality
  $iandsvisionaryave = '-';
}
elseif (($iandsfocusdenom != 0) && ($iandscommunicatedenom === 0)) {
  $iandsvisionaryave = $iandsfocusave;
}
elseif (($iandsfocusdenom === 0) && ($iandscommunicatedenom != 0)) {
  $iandsvisionaryave = $iandscommunicateave;
}
else {
  $iandsvisionaryave = 0.5 * ($iandsfocusave + $iandscommunicateave);
}

if (($iandsvisionaryave != '-') && ($iandsvisionaryave >= 70.02)) {
  $visionarycomment = "<img src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
}
elseif (($iandsvisionaryave != '-') && ($iandsvisionaryave >= 63.60 && $iandsvisionaryave < 70.02)) {
  $visionarycomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
}
else {
  $visionarycomment = '';
}

//Query database for d7 averages
$d7result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d7) FROM inddata WHERE  (((d7>=0) AND (d7<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d7score = mysqli_fetch_array($d7result) or die(mysqli_error());
if (!$d7score['AVG(d7)']) {
  $indavgd7 = '-';
  $d7selfcount = 0;
}
else {
  $d7selfcount = 1;
  $indavgd7 = number_format(25 * $d7score['AVG(d7)'], 0);
  $indsecure = $indsecure + $d7score['AVG(d7)'];
  $indsecuredenom++;
}
//echo "After d7 ind secure = $indsecure and indsecuredenom = $indsecuredenom<br />"  ;

$d7stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d7) FROM inddata WHERE  (((d7>=0) AND (d7<=4))  AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d7stakescore = mysqli_fetch_array($d7stake) or die(mysqli_error());
if (!$d7stakescore['AVG(d7)']) {
  $stakeavgd7 = '-';
  $d7stakecount = 0;
}
else {
  $d7stakecount = 1;
  $stakeavgd7 = number_format(25 * $d7stakescore['AVG(d7)'], 0);
  $stakesecure = $stakesecure + $d7stakescore['AVG(d7)'];
  $stakesecuredenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d7selfcount === 0) AND ($d7stakecount === 0)) {
  $d7ave = 99;
  } else if (($d7selfcount === 0) AND ($d7stakecount != 0)) {
  $d7ave = $d7stakescore['AVG(d7)'];
  } else if (($d7selfcount != 0) AND ($d7stakecount === 0)) {
  $d7ave = $d7score['AVG(d7)'];
  } else {
  $d7ave = ($d7score['AVG(d7)'] + $d7stakescore['AVG(d7)']) / 2;
  } */

//Query database for d8 averages
$d8result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d8) FROM inddata WHERE  (((d8>=0) AND (d8<=4))  AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d8score = mysqli_fetch_array($d8result) or die(mysqli_error());
if (!$d8score['AVG(d8)']) {
  $indavgd8 = '-';
  $d8selfcount = 0;
}
else {
  $d8selfcount = 1;
  $indavgd8 = number_format(25 * $d8score['AVG(d8)'], 0);
  $indsecure = $indsecure + $d8score['AVG(d8)'];
  $indsecuredenom++;
}
//echo "After d8 ind secure = $indsecure and indsecuredenom = $indsecuredenom<br />"  ;

$d8stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d8) FROM inddata WHERE  (((d8>=0) AND (d8<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d8stakescore = mysqli_fetch_array($d8stake) or die(mysqli_error());
if (!$d8stakescore['AVG(d8)']) {
  $stakeavgd8 = '-';
  $d8stakecount = 0;
}
else {
  $d8stakecount = 1;
  $stakeavgd8 = number_format(25 * $d8stakescore['AVG(d8)'], 0);
  $stakesecure = $stakesecure + $d8stakescore['AVG(d8)'];
  $stakesecuredenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d8selfcount === 0) AND ($d8stakecount === 0)) {
  $d8ave = 99;
  } else if (($d8selfcount === 0) AND ($d8stakecount != 0)) {
  $d8ave = $d8stakescore['AVG(d8)'];
  } else if (($d8selfcount != 0) AND ($d8stakecount === 0)) {
  $d8ave = $d8score['AVG(d8)'];
  } else {
  $d8ave = ($d8score['AVG(d8)'] + $d8stakescore['AVG(d8)']) / 2;
  } */

//
//Query database for d9 averages
$d9result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d9) FROM inddata WHERE  (((d9>=0) AND (d9<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d9score = mysqli_fetch_array($d9result) or die(mysqli_error());
if (!$d9score['AVG(d9)']) {
  $indavgd9 = '-';
  $d9selfcount = 0;
}
else {
  $d9selfcount = 1;
  $indavgd9 = number_format(25 * $d9score['AVG(d9)'], 0);
  $indsecure = $indsecure + $d9score['AVG(d9)'];
  $indsecuredenom++;
}
//echo "After d9 ind secure = $indsecure and indsecuredenom = $indsecuredenom<br />"  ;

$d9stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d9) FROM inddata WHERE  (((d9>=0) AND (d9<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d9stakescore = mysqli_fetch_array($d9stake) or die(mysqli_error());
if (!$d9stakescore['AVG(d9)']) {
  $stakeavgd9 = '-';
  $d9stakecount = 0;
}
else {
  $d9stakecount = 1;
  $stakeavgd9 = number_format(25 * $d9stakescore['AVG(d9)'], 0);
  $stakesecure = $stakesecure + $d9stakescore['AVG(d9)'];
  $stakesecuredenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d9selfcount === 0) AND ($d9stakecount === 0)) {
  $d9ave = 99;
  } else if (($d9selfcount === 0) AND ($d9stakecount != 0)) {
  $d9ave = $d9stakescore['AVG(d9)'];
  } else if (($d9selfcount != 0) AND ($d9stakecount === 0)) {
  $d9ave = $d9score['AVG(d9)'];
  } else {
  $d9ave = ($d9score['AVG(d9)'] + $d9stakescore['AVG(d9)']) / 2;
  } */

//Calculating stakesecure
if ($stakesecuredenom === 0) {
  $stakesecureave = '-';
  $stakesecureavefm = '-';
}
else {
  $stakesecureave = 25 * $stakesecure / $stakesecuredenom;
  $stakesecureavefm = number_format($stakesecureave, 0);
}
//echo" stakesecureave = $stakesecureave" ;
//Calculating indsecure
if ($indsecuredenom === 0) {
  $indsecureave = '-';
  $indsecureavefm = '-';
}
else {
  $indsecureave = 25 * $indsecure / $indsecuredenom;
  $indsecureavefm = number_format($indsecureave, 0);
}

// Calculating tandssecure for star-rating
$iandssecuredenom = $indsecuredenom + $stakesecuredenom;
$iandssecure = $indsecure + $stakesecure;
if ($iandssecuredenom === 0) {
  $securecomment = '';
}
else {
  $iandssecureave = 25 * ($iandssecure / $iandssecuredenom);
  if ($iandssecureave >= 72.56) {
    $securehigh = 1;
    $securecomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
  }
  elseif ($iandssecureave >= 65.26 && $iandssecureave < 72.56) {
    $securehigh = 1;
    $securecomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
  }
  else {
    $securecomment = '';
  }
}

//Query database for d10 averages
$d10result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d10) FROM inddata WHERE  (((d10>=0) AND (d10<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d10score = mysqli_fetch_array($d10result) or die(mysqli_error());
if (!$d10score['AVG(d10)']) {
  $indavgd10 = '-';
  $d10selfcount = 0;
}
else {
  $d10selfcount = 1;
  $indavgd10 = number_format(25 * $d10score['AVG(d10)'], 0);
  $indorganize = $indorganize + $d10score['AVG(d10)'];
  $indorganizedenom++;
}
//echo "After d10 ind organize = $indorganize and indorganizedenom = $indorganizedenom<br />"  ;

$d10stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d10) FROM inddata WHERE  (((d10>=0) AND (d10<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d10stakescore = mysqli_fetch_array($d10stake) or die(mysqli_error());
if (!$d10stakescore['AVG(d10)']) {
  $stakeavgd10 = '-';
  $d10stakecount = 0;
}
else {
  $d10stakecount = 1;
  $stakeavgd10 = number_format(25 * $d10stakescore['AVG(d10)'], 0);
  $stakeorganize = $stakeorganize + $d10stakescore['AVG(d10)'];
  $stakeorganizedenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d10selfcount === 0) AND ($d10stakecount === 0)) {
  $d10ave = 99;
  } else if (($d10selfcount === 0) AND ($d10stakecount != 0)) {
  $d10ave = $d10stakescore['AVG(d10)'];
  } else if (($d10selfcount != 0) AND ($d10stakecount === 0)) {
  $d10ave = $d10score['AVG(d10)'];
  } else {
  $d10ave = ($d10score['AVG(d10)'] + $d10stakescore['AVG(d10)']) / 2;
  } */


//Query database for d11 averages
$d11result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d11) FROM inddata WHERE  (((d11>=0) AND (d11<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d11score = mysqli_fetch_array($d11result) or die(mysqli_error());
if (!$d11score['AVG(d11)']) {
  $indavgd11 = '-';
  $d11selfcount = 0;
}
else {
  $d11selfcount = 1;
  $indavgd11 = number_format(25 * $d11score['AVG(d11)'], 0);
  $indorganize = $indorganize + $d11score['AVG(d11)'];
  $indorganizedenom++;
}
//echo "After d11 ind organize = $indorganize and indorganizedenom = $indorganizedenom<br />"  ;

$d11stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d11) FROM inddata WHERE  (((d11>=0) AND (d11<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d11stakescore = mysqli_fetch_array($d11stake) or die(mysqli_error());
if (!$d11stakescore['AVG(d11)']) {
  $stakeavgd11 = '-';
  $d11stakecount = 0;
}
else {
  $d11stakecount = 1;
  $stakeavgd11 = number_format(25 * $d11stakescore['AVG(d11)'], 0);
  $stakeorganize = $stakeorganize + $d11stakescore['AVG(d11)'];
  $stakeorganizedenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d11selfcount === 0) AND ($d11stakecount === 0)) {
  $d11ave = 99;
  } else if (($d11selfcount === 0) AND ($d11stakecount != 0)) {
  $d11ave = $d11stakescore['AVG(d11)'];
  } else if (($d11selfcount != 0) AND ($d11stakecount === 0)) {
  $d11ave = $d11score['AVG(d11)'];
  } else {
  $d11ave = ($d11score['AVG(d11)'] + $d11stakescore['AVG(d11)']) / 2;
  } */

//Query database for d12 averages
$d12result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d12) FROM inddata WHERE  (((d12>=0) AND (d12<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d12score = mysqli_fetch_array($d12result) or die(mysqli_error());
if (!$d12score['AVG(d12)']) {
  $indavgd12 = '-';
  $d12selfcount = 0;
}
else {
  $d12selfcount = 1;
  $indavgd12 = number_format(25 * $d12score['AVG(d12)'], 0);
  $indorganize = $indorganize + $d12score['AVG(d12)'];
  $indorganizedenom++;
}
//echo "After d12 ind organize = $indorganize and indorganizedenom = $indorganizedenom<br />"  ;

$d12stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d12) FROM inddata WHERE  (((d12>=0) AND (d12<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d12stakescore = mysqli_fetch_array($d12stake) or die(mysqli_error());
if (!$d12stakescore['AVG(d12)']) {
  $stakeavgd12 = '-';
  $d12stakecount = 0;
}
else {
  $d12stakecount = 1;
  $stakeavgd12 = number_format(25 * $d12stakescore['AVG(d12)'], 0);
  $stakeorganize = $stakeorganize + $d12stakescore['AVG(d12)'];
  $stakeorganizedenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d12selfcount === 0) AND ($d12stakecount === 0)) {
  $d12ave = 99;
  } else if (($d12selfcount === 0) AND ($d12stakecount != 0)) {
  $d12ave = $d12stakescore['AVG(d12)'];
  } else if (($d12selfcount != 0) AND ($d12stakecount === 0)) {
  $d12ave = $d12score['AVG(d12)'];
  } else {
  $d12ave = ($d12score['AVG(d12)'] + $d12stakescore['AVG(d12)']) / 2;
  } */

//
//Calculating stakeorganize
if ($stakeorganizedenom === 0) {
  $stakeorganizeave = '-';
  $stakeorganizeavefm = '-';
}
else {
  $stakeorganizeave = 25 * $stakeorganize / $stakeorganizedenom;
  $stakeorganizeavefm = number_format($stakeorganizeave, 0);
}
//echo" stakeorganizeave = $stakeorganizeave" ;
//Calculating indorganize
if ($indorganizedenom === 0) {
  $indorganizeave = '-';
  $indorganizeavefm = '-';
}
else {
  $indorganizeave = 25 * $indorganize / $indorganizedenom;
  $indorganizeavefm = number_format($indorganizeave, 0);
}

// Calculating tandsorganize for star-rating
$iandsorganizedenom = $indorganizedenom + $stakeorganizedenom;
$iandsorganize = $indorganize + $stakeorganize;
if ($iandsorganizedenom === 0) {
  $organizecomment = '';
}
else {
  $iandsorganizeave = 25 * ($iandsorganize / $iandsorganizedenom);
  if ($iandsorganizeave >= 72.86) {
    $organizehigh = 1;
    $organizecomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
  }
  elseif ($iandsorganizeave >= 65.56 && $iandsorganizeave < 72.86) {
    $organizehigh = 1;
    $organizecomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
  }
  else {
    $organizecomment = '';
  };
}

// Calculating indguardian
if (($indsecuredenom === 0) && ($indorganizedenom === 0)) {
  $indguardianave = '-';
  $indguardianavefm = '-';
}
if (($indsecuredenom != 0) && ($indorganizedenom === 0)) {
  $indguardianave = $indsecureave;
  $indguardianavefm = $indsecureavefm;
}
if (($indsecuredenom === 0) && ($indorganizedenom != 0)) {
  $indguardianave = $indorganizeave;
  $indguardianavefm = $indorganizeavefm;
}
if (($indsecuredenom != 0) && ($indorganizedenom != 0)) {
  $indguardianave = 0.5 * ($indsecureave + $indorganizeave);
  $indguardianavefm = number_format($indguardianave, 0);
}

// Calculating stakeguardian
if (($stakesecuredenom === 0) && ($stakeorganizedenom === 0)) {
  $stakeguardianave = '-';
  $stakeguardianavefm = '-';
}
if (($stakesecuredenom != 0) && ($stakeorganizedenom === 0)) {
  $stakeguardianave = $stakesecureave;
  $stakeguardianavefm = $stakesecureavefm;
}
if (($stakesecuredenom === 0) && ($stakeorganizedenom != 0)) {
  $stakeguardianave = $stakeorganizeave;
  $stakeguardianavefm = $stakeorganizeavefm;
}
if (($stakesecuredenom != 0) && ($stakeorganizedenom != 0)) {
  $stakeguardianave = 0.5 * ($stakesecureave + $stakeorganizeave);
  $stakeguardianavefm = number_format($stakeguardianave, 0);
}

//Calculating tandsguardian for star-rating
if (($iandssecuredenom === 0) && ($iandsorganizedenom === 0)) {

//just for the sake of calculation - actually it does not exist in reality
  $iandsguardianave = '-';
}
elseif (($iandssecuredenom != 0) && ($iandsorganizedenom === 0)) {
  $iandsguardianave = $iandssecureave;
}
elseif (($iandssecuredenom === 0) && ($iandsorganizedenom != 0)) {
  $iandsguardianave = $iandsorganizeave;
}
else {
  $iandsguardianave = 0.5 * ($iandssecureave + $iandsorganizeave);
}

if (($iandsguardianave != '-') && ($iandsguardianave >= 70.82)) {
  $guardiancomment = "<img src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
}
elseif (($iandsguardianave != '-') && ($iandsguardianave >= 64.41 && $iandsguardianave < 70.82)) {
  $guardiancomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
}
else {
  $guardiancomment = '';
};

//Query database for d13 averages
$d13result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d13) FROM inddata WHERE  (((d13>=0) AND (d13<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d13score = mysqli_fetch_array($d13result) or die(mysqli_error());
if (!$d13score['AVG(d13)']) {
  $indavgd13 = '-';
  $d13selfcount = 0;
}
else {
  $d13selfcount = 1;
  $indavgd13 = number_format(25 * $d13score['AVG(d13)'], 0);
  $indunite = $indunite + $d13score['AVG(d13)'];
  $indunitedenom++;
}
//echo "After d13 ind unite = $indunite and indunitedenom = $indunitedenom<br />"  ;

$d13stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d13) FROM inddata WHERE  (((d13>=0) AND (d13<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d13stakescore = mysqli_fetch_array($d13stake) or die(mysqli_error());
if (!$d13stakescore['AVG(d13)']) {
  $stakeavgd13 = '-';
  $d13stakecount = 0;
}
else {
  $d13stakecount = 1;
  $stakeavgd13 = number_format(25 * $d13stakescore['AVG(d13)'], 0);
  $stakeunite = $stakeunite + $d13stakescore['AVG(d13)'];
  $stakeunitedenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d13selfcount === 0) AND ($d13stakecount === 0)) {
  $d13ave = 99;
  } else if (($d13selfcount === 0) AND ($d13stakecount != 0)) {
  $d13ave = $d13stakescore['AVG(d13)'];
  } else if (($d13selfcount != 0) AND ($d13stakecount === 0)) {
  $d13ave = $d13score['AVG(d13)'];
  } else {
  $d13ave = ($d13score['AVG(d13)'] + $d13stakescore['AVG(d13)']) / 2;
  } */


//Query database for d14 averages
$d14result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d14) FROM inddata WHERE  (((d14>=0) AND (d14<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d14score = mysqli_fetch_array($d14result) or die(mysqli_error());
if (!$d14score['AVG(d14)']) {
  $indavgd14 = '-';
  $d14selfcount = 0;
}
else {
  $d14selfcount = 1;
  $indavgd14 = number_format(25 * $d14score['AVG(d14)'], 0);
  $indunite = $indunite + $d14score['AVG(d14)'];
  $indunitedenom++;
}
//echo "After d14 ind unite = $indunite and indunitedenom = $indunitedenom<br />"  ;

$d14stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d14) FROM inddata WHERE  (((d14>=0) AND (d14<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d14stakescore = mysqli_fetch_array($d14stake) or die(mysqli_error());
if (!$d14stakescore['AVG(d14)']) {
  $stakeavgd14 = '-';
  $d14stakecount = 0;
}
else {
  $d14stakecount = 1;
  $stakeavgd14 = number_format(25 * $d14stakescore['AVG(d14)'], 0);
  $stakeunite = $stakeunite + $d14stakescore['AVG(d14)'];
  $stakeunitedenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d14selfcount === 0) AND ($d14stakecount === 0)) {
  $d14ave = 99;
  } else if (($d14selfcount === 0) AND ($d14stakecount != 0)) {
  $d14ave = $d14stakescore['AVG(d14)'];
  } else if (($d14selfcount != 0) AND ($d14stakecount === 0)) {
  $d14ave = $d14score['AVG(d14)'];
  } else {
  $d14ave = ($d14score['AVG(d14)'] + $d14stakescore['AVG(d14)']) / 2;
  } */


//Query database for d15 averages
$d15result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d15) FROM inddata WHERE  (((d15>=0) AND (d15<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d15score = mysqli_fetch_array($d15result) or die(mysqli_error());
if (!$d15score['AVG(d15)']) {
  $indavgd15 = '-';
  $d15selfcount = 0;
}
else {
  $d15selfcount = 1;
  $indavgd15 = number_format(25 * $d15score['AVG(d15)'], 0);
  $indunite = $indunite + $d15score['AVG(d15)'];
  $indunitedenom++;
}
//echo "After d15 ind unite = $indunite and indunitedenom = $indunitedenom<br />"  ;

$d15stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d15) FROM inddata WHERE  (((d15>=0) AND (d15<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d15stakescore = mysqli_fetch_array($d15stake) or die(mysqli_error());
if (!$d15stakescore['AVG(d15)']) {
  $stakeavgd15 = '-';
  $d15stakecount = 0;
}
else {
  $d15stakecount = 1;
  $stakeavgd15 = number_format(25 * $d15stakescore['AVG(d15)'], 0);
  $stakeunite = $stakeunite + $d15stakescore['AVG(d15)'];
  $stakeunitedenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d15selfcount === 0) AND ($d15stakecount === 0)) {
  $d15ave = 99;
  } else if (($d15selfcount === 0) AND ($d15stakecount != 0)) {
  $d15ave = $d15stakescore['AVG(d15)'];
  } else if (($d15selfcount != 0) AND ($d15stakecount === 0)) {
  $d15ave = $d15score['AVG(d15)'];
  } else {
  $d15ave = ($d15score['AVG(d15)'] + $d15stakescore['AVG(d15)']) / 2;
  } */


//Calculating stakeunite
if ($stakeunitedenom === 0) {
  $stakeuniteave = '-';
  $stakeuniteavefm = '-';
}
else {
  $stakeuniteave = 25 * $stakeunite / $stakeunitedenom;
  $stakeuniteavefm = number_format($stakeuniteave, 0);
}
//echo" stakeuniteave = $stakeuniteave" ;
//Calculating indunite
if ($indunitedenom === 0) {
  $induniteave = '-';
  $induniteavefm = '-';
}
else {
  $induniteave = 25 * $indunite / $indunitedenom;
  $induniteavefm = number_format($induniteave, 0);
}

// Calculating tandsunite for star-rating
$iandsunitedenom = $indunitedenom + $stakeunitedenom;
$iandsunite = $indunite + $stakeunite;
if ($iandsunitedenom === 0) {
  $unitecomment = '';
}
else {
  $iandsuniteave = 25 * ($iandsunite / $iandsunitedenom);
  if ($iandsuniteave >= 75.28) {
    $unitehigh = 1;
    $unitecomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
  }
  elseif ($iandsuniteave >= 67.98 && $iandsuniteave < 75.28) {
    $unitehigh = 1;
    $unitecomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
  }
  else {
    $unitecomment = '';
  };
}

//echo" induniteave = $induniteave<br /><br />" ;
//Query database for d16 averages
$d16result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d16) FROM inddata WHERE  (((d16>=0) AND (d16<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d16score = mysqli_fetch_array($d16result) or die(mysqli_error());
if (!$d16score['AVG(d16)']) {
  $indavgd16 = '-';
  $d16selfcount = 0;
}
else {
  $d16selfcount = 1;
  $indavgd16 = number_format(25 * $d16score['AVG(d16)'], 0);
  $indempower = $indempower + $d16score['AVG(d16)'];
  $indempowerdenom++;
}
//echo "After d16 ind empower = $indempower and indempowerdenom = $indempowerdenom<br />"  ;

$d16stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d16) FROM inddata WHERE  (((d16>=0) AND (d16<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d16stakescore = mysqli_fetch_array($d16stake) or die(mysqli_error());
if (!$d16stakescore['AVG(d16)']) {
  $stakeavgd16 = '-';
  $d16stakecount = 0;
}
else {
  $d16stakecount = 1;
  $stakeavgd16 = number_format(25 * $d16stakescore['AVG(d16)'], 0);
  $stakeempower = $stakeempower + $d16stakescore['AVG(d16)'];
  $stakeempowerdenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d16selfcount === 0) AND ($d16stakecount === 0)) {
  $d16ave = 99;
  } else if (($d16selfcount === 0) AND ($d16stakecount != 0)) {
  $d16ave = $d16stakescore['AVG(d16)'];
  } else if (($d16selfcount != 0) AND ($d16stakecount === 0)) {
  $d16ave = $d16score['AVG(d16)'];
  } else {
  $d16ave = ($d16score['AVG(d16)'] + $d16stakescore['AVG(d16)']) / 2;
  } */


//Query database for d17 averages
$d17result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d17) FROM inddata WHERE  (((d17>=0) AND (d17<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d17score = mysqli_fetch_array($d17result) or die(mysqli_error());
if (!$d17score['AVG(d17)']) {
  $indavgd17 = '-';
  $d17selfcount = 0;
}
else {
  $d17selfcount = 1;
  $indavgd17 = number_format(25 * $d17score['AVG(d17)'], 0);
  $indempower = $indempower + $d17score['AVG(d17)'];
  $indempowerdenom++;
}
//echo "After d17 ind empower = $indempower and indempowerdenom = $indempowerdenom<br />"  ;

$d17stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d17) FROM inddata WHERE  (((d17>=0) AND (d17<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d17stakescore = mysqli_fetch_array($d17stake) or die(mysqli_error());
if (!$d17stakescore['AVG(d17)']) {
  $stakeavgd17 = '-';
  $d17stakecount = 0;
}
else {
  $d17stakecount = 1;
  $stakeavgd17 = number_format(25 * $d17stakescore['AVG(d17)'], 0);
  $stakeempower = $stakeempower + $d17stakescore['AVG(d17)'];
  $stakeempowerdenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d17selfcount === 0) AND ($d17stakecount === 0)) {
  $d17ave = 99;
  } else if (($d17selfcount === 0) AND ($d17stakecount != 0)) {
  $d17ave = $d17stakescore['AVG(d17)'];
  } else if (($d17selfcount != 0) AND ($d17stakecount === 0)) {
  $d17ave = $d17score['AVG(d17)'];
  } else {
  $d17ave = ($d17score['AVG(d17)'] + $d17stakescore['AVG(d17)']) / 2;
  } */



//Query database for d18 averages
$d18result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d18) FROM inddata WHERE  (((d18>=0) AND (d18<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d18score = mysqli_fetch_array($d18result) or die(mysqli_error());
if (!$d18score['AVG(d18)']) {
  $indavgd18 = '-';
  $d18selfcount = 0;
}
else {
  $d18selfcount = 1;
  $indavgd18 = number_format(25 * $d18score['AVG(d18)'], 0);
  $indempower = $indempower + $d18score['AVG(d18)'];
  $indempowerdenom++;
}
//echo "After d18 ind empower = $indempower and indempowerdenom = $indempowerdenom<br />"  ;

$d18stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d18) FROM inddata WHERE  (((d18>=0) AND (d18<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d18stakescore = mysqli_fetch_array($d18stake) or die(mysqli_error());
if (!$d18stakescore['AVG(d18)']) {
  $stakeavgd18 = '-';
  $d18stakecount = 0;
}
else {
  $d18stakecount = 1;
  $stakeavgd18 = number_format(25 * $d18stakescore['AVG(d18)'], 0);
  $stakeempower = $stakeempower + $d18stakescore['AVG(d18)'];
  $stakeempowerdenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d18selfcount === 0) AND ($d18stakecount === 0)) {
  $d18ave = 99;
  } else if (($d18selfcount === 0) AND ($d18stakecount != 0)) {
  $d18ave = $d18stakescore['AVG(d18)'];
  } else if (($d18selfcount != 0) AND ($d18stakecount === 0)) {
  $d18ave = $d18score['AVG(d18)'];
  } else {
  $d18ave = ($d18score['AVG(d18)'] + $d18stakescore['AVG(d18)']) / 2;
  } */



//Query database for d19 averages
$d19result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d19) FROM inddata WHERE  (((d19>=0) AND (d19<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d19score = mysqli_fetch_array($d19result) or die(mysqli_error());
if (!$d19score['AVG(d19)']) {
  $indavgd19 = '-';
  $d19selfcount = 0;
}
else {
  $d19selfcount = 1;
  $indavgd19 = number_format(25 * $d19score['AVG(d19)'], 0);
  $inddeliver = $inddeliver + $d19score['AVG(d19)'];
  $inddeliverdenom++;
}
//echo "After d19 ind deliver = $inddeliver and inddeliverdenom = $inddeliverdenom<br />"  ;

$d19stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d19) FROM inddata WHERE  (((d19>=0) AND (d19<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d19stakescore = mysqli_fetch_array($d19stake) or die(mysqli_error());
if (!$d19stakescore['AVG(d19)']) {
  $stakeavgd19 = '-';
  $d19stakecount = 0;
}
else {
  $d19stakecount = 1;
  $stakeavgd19 = number_format(25 * $d19stakescore['AVG(d19)'], 0);
  $stakedeliver = $stakedeliver + $d19stakescore['AVG(d19)'];
  $stakedeliverdenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d19selfcount === 0) AND ($d19stakecount === 0)) {
  $d19ave = 99;
  } else if (($d19selfcount === 0) AND ($d19stakecount != 0)) {
  $d19ave = $d19stakescore['AVG(d19)'];
  } else if (($d19selfcount != 0) AND ($d19stakecount === 0)) {
  $d19ave = $d19score['AVG(d19)'];
  } else {
  $d19ave = ($d19score['AVG(d19)'] + $d19stakescore['AVG(d19)']) / 2;
  } */


//Query database for d20 averages
$d20result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d20) FROM inddata WHERE  (((d20>=0) AND (d20<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d20score = mysqli_fetch_array($d20result) or die(mysqli_error());
if (!$d20score['AVG(d20)']) {
  $indavgd20 = '-';
  $d20selfcount = 0;
}
else {
  $d20selfcount = 1;
  $indavgd20 = number_format(25 * $d20score['AVG(d20)'], 0);
  $inddeliver = $inddeliver + $d20score['AVG(d20)'];
  $inddeliverdenom++;
}
//echo "After d20 ind deliver = $inddeliver and inddeliverdenom = $inddeliverdenom<br />"  ;

$d20stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d20) FROM inddata WHERE  (((d20>=0) AND (d20<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d20stakescore = mysqli_fetch_array($d20stake) or die(mysqli_error());
if (!$d20stakescore['AVG(d20)']) {
  $stakeavgd20 = '-';
  $d20stakecount = 0;
}
else {
  $d20stakecount = 1;
  $stakeavgd20 = number_format(25 * $d20stakescore['AVG(d20)'], 0);
  $stakedeliver = $stakedeliver + $d20stakescore['AVG(d20)'];
  $stakedeliverdenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d20selfcount === 0) AND ($d20stakecount === 0)) {
  $d20ave = 99;
  } else if (($d20selfcount === 0) AND ($d20stakecount != 0)) {
  $d20ave = $d20stakescore['AVG(d20)'];
  } else if (($d20selfcount != 0) AND ($d20stakecount === 0)) {
  $d20ave = $d20score['AVG(d20)'];
  } else {
  $d20ave = ($d20score['AVG(d20)'] + $d20stakescore['AVG(d20)']) / 2;
  } */


//Query database for d21 averages
$d21result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d21) FROM inddata WHERE  (((d21>=0) AND (d21<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d21score = mysqli_fetch_array($d21result) or die(mysqli_error());
if (!$d21score['AVG(d21)']) {
  $indavgd21 = '-';
  $d21selfcount = 0;
}
else {
  $d21selfcount = 1;
  $indavgd21 = number_format(25 * $d21score['AVG(d21)'], 0);
  $inddeliver = $inddeliver + $d21score['AVG(d21)'];
  $inddeliverdenom++;
}
//echo "After d21 ind deliver = $inddeliver and inddeliverdenom = $inddeliverdenom<br />"  ;

$d21stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d21) FROM inddata WHERE  (((d21>=0) AND (d21<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d21stakescore = mysqli_fetch_array($d21stake) or die(mysqli_error());
if (!$d21stakescore['AVG(d21)']) {
  $stakeavgd21 = '-';
  $d21stakecount = 0;
}
else {
  $d21stakecount = 1;
  $stakeavgd21 = number_format(25 * $d21stakescore['AVG(d21)'], 0);
  $stakedeliver = $stakedeliver + $d21stakescore['AVG(d21)'];
  $stakedeliverdenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d21selfcount === 0) AND ($d21stakecount === 0)) {
  $d21ave = 99;
  } else if (($d21selfcount === 0) AND ($d21stakecount != 0)) {
  $d21ave = $d21stakescore['AVG(d21)'];
  } else if (($d21selfcount != 0) AND ($d21stakecount === 0)) {
  $d21ave = $d21score['AVG(d21)'];
  } else {
  $d21ave = ($d21score['AVG(d21)'] + $d21stakescore['AVG(d21)']) / 2;
  } */



//Query database for d22 averages
$d22result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d22) FROM inddata WHERE  (((d22>=0) AND (d22<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d22score = mysqli_fetch_array($d22result) or die(mysqli_error());
if (!$d22score['AVG(d22)']) {
  $indavgd22 = '-';
  $d22selfcount = 0;
}
else {
  $d22selfcount = 1;
  $indavgd22 = number_format(25 * $d22score['AVG(d22)'], 0);
  $indimprove = $indimprove + $d22score['AVG(d22)'];
  $indimprovedenom++;
}
//echo "After d22 ind improve = $indimprove and indimprovedenom = $indimprovedenom<br />"  ;

$d22stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d22) FROM inddata WHERE  (((d22>=0) AND (d22<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d22stakescore = mysqli_fetch_array($d22stake) or die(mysqli_error());
if (!$d22stakescore['AVG(d22)']) {
  $stakeavgd22 = '-';
  $d22stakecount = 0;
}
else {
  $d22stakecount = 1;
  $stakeavgd22 = number_format(25 * $d22stakescore['AVG(d22)'], 0);
  $stakeimprove = $stakeimprove + $d22stakescore['AVG(d22)'];
  $stakeimprovedenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d22selfcount === 0) AND ($d22stakecount === 0)) {
  $d22ave = 99;
  } else if (($d22selfcount === 0) AND ($d22stakecount != 0)) {
  $d22ave = $d22stakescore['AVG(d22)'];
  } else if (($d22selfcount != 0) AND ($d22stakecount === 0)) {
  $d22ave = $d22score['AVG(d22)'];
  } else {
  $d22ave = ($d22score['AVG(d22)'] + $d22stakescore['AVG(d22)']) / 2;
  } */

//Query database for d23 averages
$d23result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d23) FROM inddata WHERE  (((d23>=0) AND (d23<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d23score = mysqli_fetch_array($d23result) or die(mysqli_error());
if (!$d23score['AVG(d23)']) {
  $indavgd23 = '-';
  $d23selfcount = 0;
}
else {
  $d23selfcount = 1;
  $indavgd23 = number_format(25 * $d23score['AVG(d23)'], 0);
  $indimprove = $indimprove + $d23score['AVG(d23)'];
  $indimprovedenom++;
}
//echo "After d23 ind improve = $indimprove and indimprovedenom = $indimprovedenom<br />"  ;

$d23stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d23) FROM inddata WHERE  (((d23>=0) AND (d23<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d23stakescore = mysqli_fetch_array($d23stake) or die(mysqli_error());
if (!$d23stakescore['AVG(d23)']) {
  $stakeavgd23 = '-';
  $d23stakecount = 0;
}
else {
  $d23stakecount = 1;
  $stakeavgd23 = number_format(25 * $d23stakescore['AVG(d23)'], 0);
  $stakeimprove = $stakeimprove + $d23stakescore['AVG(d23)'];
  $stakeimprovedenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d23selfcount === 0) AND ($d23stakecount === 0)) {
  $d23ave = 99;
  } else if (($d23selfcount === 0) AND ($d23stakecount != 0)) {
  $d23ave = $d23stakescore['AVG(d23)'];
  } else if (($d23selfcount != 0) AND ($d23stakecount === 0)) {
  $d23ave = $d23score['AVG(d23)'];
  } else {
  $d23ave = ($d23score['AVG(d23)'] + $d23stakescore['AVG(d23)']) / 2;
  } */


//Query database for d24 averages
$d24result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d24) FROM inddata WHERE  (((d24>=0) AND (d24<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d24score = mysqli_fetch_array($d24result) or die(mysqli_error());
if (!$d24score['AVG(d24)']) {
  $indavgd24 = '-';
  $d24selfcount = 0;
}
else {
  $d24selfcount = 1;
  $indavgd24 = number_format(25 * $d24score['AVG(d24)'], 0);
  $indimprove = $indimprove + $d24score['AVG(d24)'];
  $indimprovedenom++;
}
//echo "After d24 ind improve = $indimprove and indimprovedenom = $indimprovedenom<br />"  ;

$d24stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d24) FROM inddata WHERE  (((d24>=0) AND (d24<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d24stakescore = mysqli_fetch_array($d24stake) or die(mysqli_error());
if (!$d24stakescore['AVG(d24)']) {
  $stakeavgd24 = '-';
  $d24stakecount = 0;
}
else {
  $d24stakecount = 1;
  $stakeavgd24 = number_format(25 * $d24stakescore['AVG(d24)'], 0);
  $stakeimprove = $stakeimprove + $d24stakescore['AVG(d24)'];
  $stakeimprovedenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d24selfcount === 0) AND ($d24stakecount === 0)) {
  $d24ave = 99;
  } else if (($d24selfcount === 0) AND ($d24stakecount != 0)) {
  $d24ave = $d24stakescore['AVG(d24)'];
  } else if (($d24selfcount != 0) AND ($d24stakecount === 0)) {
  $d24ave = $d24score['AVG(d24)'];
  } else {
  $d24ave = ($d24score['AVG(d24)'] + $d24stakescore['AVG(d24)']) / 2;
  } */

//Calculating stakeimprove
if ($stakeimprovedenom === 0) {
  $stakeimproveave = '-';
  $stakeimproveavefm = '-';
}
else {
  $stakeimproveave = 25 * $stakeimprove / $stakeimprovedenom;
  $stakeimproveavefm = number_format($stakeimproveave, 0);
}
//echo" stakeimproveave = $stakeimproveave" ;
//Calculating indimprove
if ($indimprovedenom === 0) {
  $indimproveave = '-';
  $indimproveavefm = '-';
}
else {
  $indimproveave = 25 * $indimprove / $indimprovedenom;
  $indimproveavefm = number_format($indimproveave, 0);
}

// Calculating tandsimprove for star-rating
$iandsimprovedenom = $indimprovedenom + $stakeimprovedenom;
$iandsimprove = $indimprove + $stakeimprove;
if ($iandsimprovedenom === 0) {
  $improvecomment = '';
}
else {
  $iandsimproveave = 25 * ($iandsimprove / $iandsimprovedenom);
  if ($iandsimproveave >= 73.52) {
    $improvehigh = 1;
    $improvecomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
  }
  elseif ($iandsimproveave >= 66.62 && $iandsimproveave < 73.52) {
    $improvehigh = 1;
    $improvecomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
  }
  else {
    $improvecomment = '';
  }
}

//Query database for d25 averages
$d25result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d25) FROM inddata WHERE  (((d25>=0) AND (d25<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d25score = mysqli_fetch_array($d25result) or die(mysqli_error());
if (!$d25score['AVG(d25)']) {
  $indavgd25 = '-';
  $d25selfcount = 0;
}
else {
  $d25selfcount = 1;
  $indavgd25 = number_format(25 * $d25score['AVG(d25)'], 0);
  $inddeliver = $inddeliver + $d25score['AVG(d25)'];
  $inddeliverdenom++;
}
//echo "After d25 ind deliver = $inddeliver and inddeliverdenom = $inddeliverdenom<br />"  ;

$d25stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d25) FROM inddata WHERE  (((d25>=0) AND (d25<=4))  AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d25stakescore = mysqli_fetch_array($d25stake) or die(mysqli_error());
if (!$d25stakescore['AVG(d25)']) {
  $stakeavgd25 = '-';
  $d25stakecount = 0;
}
else {
  $d25stakecount = 1;
  $stakeavgd25 = number_format(25 * $d25stakescore['AVG(d25)'], 0);
  $stakedeliver = $stakedeliver + $d25stakescore['AVG(d25)'];
  $stakedeliverdenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d25selfcount === 0) AND ($d25stakecount === 0)) {
  $d25ave = 99;
  } else if (($d25selfcount === 0) AND ($d25stakecount != 0)) {
  $d25ave = $d25stakescore['AVG(d25)'];
  } else if (($d25selfcount != 0) AND ($d25stakecount === 0)) {
  $d25ave = $d25score['AVG(d25)'];
  } else {
  $d25ave = ($d25score['AVG(d25)'] + $d25stakescore['AVG(d25)']) / 2;
  } */


//Calculating stakedeliver
if ($stakedeliverdenom === 0) {
  $stakedeliverave = '-';
  $stakedeliveravefm = '-';
}
else {
  $stakedeliverave = 25 * $stakedeliver / $stakedeliverdenom;
  $stakedeliveravefm = number_format($stakedeliverave, 0);
}
//echo" stakedeliverave = $stakedeliverave" ;
//Calculating inddeliver
if ($inddeliverdenom === 0) {
  $inddeliverave = '-';
  $inddeliveravefm = '-';
}
else {
  $inddeliverave = 25 * $inddeliver / $inddeliverdenom;
  $inddeliveravefm = number_format($inddeliverave, 0);
}

// Calculating tandsdeliver for star-rating
$iandsdeliverdenom = $inddeliverdenom + $stakedeliverdenom;
$iandsdeliver = $inddeliver + $stakedeliver;
if ($iandsdeliverdenom === 0) {
  $delivercomment = '';
}
else {
  $iandsdeliverave = 25 * ($iandsdeliver / $iandsdeliverdenom);
  if ($iandsdeliverave >= 68.42) {
    $deliverhigh = 1;
    $delivercomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
  }
  elseif ($iandsdeliverave >= 61.11 && $iandsdeliverave < 68.42) {
    $deliverhigh = 1;
    $delivercomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
  }
  else {
    $delivercomment = '';
  };
}

// Calculating inddirector
if (($inddeliverdenom === 0) && ($indimprovedenom === 0)) {
  $inddirectorave = '-';
  $inddirectoravefm = '-';
}
if (($inddeliverdenom != 0) && ($indimprovedenom === 0)) {
  $inddirectorave = $inddeliverave;
  $inddirectoravefm = $inddeliveravefm;
}
if (($inddeliverdenom === 0) && ($indimprovedenom != 0)) {
  $inddirectorave = $indimproveave;
  $inddirectoravefm = $indimproveavefm;
}
if (($inddeliverdenom != 0) && ($indimprovedenom != 0)) {
  $inddirectorave = 0.5 * ($inddeliverave + $indimproveave);
  $inddirectoravefm = number_format($inddirectorave, 0);
}

// Calculating stakedirector
if (($stakedeliverdenom === 0) && ($stakeimprovedenom === 0)) {
  $stakedirectorave = '-';
  $stakedirectoravefm = '-';
}
if (($stakedeliverdenom != 0) && ($stakeimprovedenom === 0)) {
  $stakedirectorave = $stakedeliverave;
  $stakedirectoravefm = $stakedeliveravefm;
}
if (($stakedeliverdenom === 0) && ($stakeimprovedenom != 0)) {
  $stakedirectorave = $stakeimproveave;
  $stakedirectoravefm = $stakeimproveavefm;
}
if (($stakedeliverdenom != 0) && ($stakeimprovedenom != 0)) {
  $stakedirectorave = 0.5 * ($stakedeliverave + $stakeimproveave);
  $stakedirectoravefm = number_format($stakedirectorave, 0);
}

//Calculating tandsdirector for star-rating
if (($iandsdeliverdenom === 0) && ($iandsimprovedenom === 0)) {

//just for the sake of calculation - actually it does not exist in reality
  $iandsdirectorave = '-';
}
elseif (($iandsdeliverdenom != 0) && ($iandsimprovedenom === 0)) {
  $iandsdirectorave = $iandsdeliverave;
}
elseif (($iandsdeliverdenom === 0) && ($iandsimprovedenom != 0)) {
  $iandsdirectorave = $iandsimproveave;
}
else {
  $iandsdirectorave = 0.5 * ($iandsdeliverave + $iandsimproveave);
}

if (($iandsdirectorave != '-') && ($iandsdirectorave >= 69.08)) {
  $directorcomment = "<img src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
}
elseif (($iandsdirectorave != '-') && ($iandsdirectorave >= 62.67 && $iandsdirectorave < 69.08)) {
  $directorcomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
}
else {
  $directorcomment = '';
};

//Query database for d26 averages
$d26result = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d26) FROM inddata WHERE  (((d26>=0) AND (d26<=4)) AND indsurveyid = '$reportrequested' AND (indrole=1))");
$d26score = mysqli_fetch_array($d26result) or die(mysqli_error());
if (!$d26score['AVG(d26)']) {
  $indavgd26 = '-';
  $d26selfcount = 0;
}
else {
  $d26selfcount = 1;
  $indavgd26 = number_format(25 * $d26score['AVG(d26)'], 0);
  $indempower = $indempower + $d26score['AVG(d26)'];
  $indempowerdenom++;
}
//echo "After d26 ind empower = $indempower and indempowerdenom = $indempowerdenom<br />"  ;

$d26stake = mysqli_query($dbc, "SELECT  indsurveyid, AVG(d26) FROM inddata WHERE  (((d26>=0) AND (d26<=4)) AND indsurveyid = '$reportrequested' AND ((indrole=2) OR (indrole=3)  OR (indrole=99)))");
$d26stakescore = mysqli_fetch_array($d26stake) or die(mysqli_error());
if (!$d26stakescore['AVG(d26)']) {
  $stakeavgd26 = '-';
  $d26stakecount = 0;
}
else {
  $d26stakecount = 1;
  $stakeavgd26 = number_format(25 * $d26stakescore['AVG(d26)'], 0);
  $stakeempower = $stakeempower + $d26stakescore['AVG(d26)'];
  $stakeempowerdenom++;
}
/* I was going to add individual and stakeholder scores to get a rating on a question but then decided to report them separately but have left this in to show the calc if I should need it again May 2014

  if (($d26selfcount === 0) AND ($d26stakecount === 0)) {
  $d26ave = 99;
  } else if (($d26selfcount === 0) AND ($d26stakecount != 0)) {
  $d26ave = $d26stakescore['AVG(d26)'];
  } else if (($d26selfcount != 0) AND ($d26stakecount === 0)) {
  $d26ave = $d26score['AVG(d26)'];
  } else {
  $d26ave = ($d26score['AVG(d26)'] + $d26stakescore['AVG(d26)']) / 2;
  } */



// Calculating 8 facet averages
//Calculating stakeempower
if ($stakeempowerdenom === 0) {
  $stakeempowerave = '-';
  $stakeempoweravefm = '-';
}
else {
  $stakeempowerave = 25 * $stakeempower / $stakeempowerdenom;
  $stakeempoweravefm = number_format($stakeempowerave, 0);
}
//echo" stakeempowerave = $stakeempowerave" ;
//Calculating indempower
if ($indempowerdenom === 0) {
  $indempowerave = '-';
  $indempoweravefm = '-';
}
else {
  $indempowerave = 25 * $indempower / $indempowerdenom;
  $indempoweravefm = number_format($indempowerave, 0);
}

// Calculating tandsempower for star-rating
$iandsempowerdenom = $indempowerdenom + $stakeempowerdenom;
$iandsempower = $indempower + $stakeempower;
if ($iandsempowerdenom === 0) {
  $empowercomment = '';
}
else {
  $iandsempowerave = 25 * ($iandsempower / $iandsempowerdenom);
  if ($iandsempowerave >= 78.07) {
    $empowerhigh = 1;
    $empowercomment = "<img  src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
  }
  elseif ($iandsempowerave >= 70.76 && $iandsempowerave < 78.07) {
    $empowerhigh = 1;
    $empowercomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
  }
  else {
    $empowercomment = '';
  };
}

// Calculating indpsychologist
if (($indunitedenom === 0) && ($indempowerdenom === 0)) {
  $indpsychologistave = '-';
  $indpsychologistavefm = '-';
}
if (($indunitedenom != 0) && ($indempowerdenom === 0)) {
  $indpsychologistave = $induniteave;
  $indpsychologistavefm = $induniteavefm;
}
if (($indunitedenom === 0) && ($indempowerdenom != 0)) {
  $indpsychologistave = $indempowerave;
  $indpsychologistavefm = $indempoweravefm;
}
if (($indunitedenom != 0) && ($indempowerdenom != 0)) {
  $indpsychologistave = 0.5 * ($induniteave + $indempowerave);
  $indpsychologistavefm = number_format($indpsychologistave, 0);
}

// Calculating stakepsychologist
if (($stakeunitedenom === 0) && ($stakeempowerdenom === 0)) {
  $stakepsychologistave = '-';
  $stakepsychologistavefm = '-';
}
if (($stakeunitedenom != 0) && ($stakeempowerdenom === 0)) {
  $stakepsychologistave = $stakeuniteave;
  $stakepsychologistavefm = $stakeuniteavefm;
}
if (($stakeunitedenom === 0) && ($stakeempowerdenom != 0)) {
  $stakepsychologistave = $stakeempowerave;
  $stakepsychologistavefm = $stakeempoweravefm;
}
if (($stakeunitedenom != 0) && ($stakeempowerdenom != 0)) {
  $stakepsychologistave = 0.5 * ($stakeuniteave + $stakeempowerave);
  $stakepsychologistavefm = number_format($stakepsychologistave, 0);
}

//Calculating tandspsychologist for star-rating
if (($iandsunitedenom === 0) && ($iandsempowerdenom === 0)) {

//just for the sake of calculation - actually it does not exist in reality
  $iandspsychologistave = '-';
}
elseif (($iandsunitedenom != 0) && ($iandsempowerdenom === 0)) {
  $iandspsychologistave = $iandsuniteave;
}
elseif (($iandsunitedenom === 0) && ($iandsempowerdenom != 0)) {
  $iandspsychologistave = $iandsempowerave;
}
else {
  $iandspsychologistave = 0.5 * ($iandsuniteave + $iandsempowerave);
}

if (($iandspsychologistave != '-') && ($iandspsychologistave >= 74.49)) {
  $psychologistcomment = "<img src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
}
elseif (($iandspsychologistave != '-') && ($iandspsychologistave >= 68.38 && $iandspsychologistave < 74.49)) {
  $psychologistcomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
}
else {
  $psychologistcomment = '';
};


// Calculating grand total averages for Headlines page


if (($indvisionaryave === '-') && ($indguardianave === '-') && ($indpsychologistave === '-') && ($inddirectorave === '-')) {
  $indgrandave = '-';
  $indgrandavefm = '-';
}
if (($indvisionaryave != '-') && ($indguardianave != '-') && ($indpsychologistave != '-') && ($inddirectorave != '-')) {
  $indgrandave = ($indvisionaryave + $indguardianave + $indpsychologistave + $inddirectorave) / 4;
  $indgrandavefm = number_format($indgrandave, 0);
}

if (($indvisionaryave != '-') && ($indguardianave === '-') && ($indpsychologistave === '-') && ($inddirectorave === '-')) {
  $indgrandave = $indvisionaryave;
  $indgrandavefm = number_format($indgrandave, 0);
}
if (($indvisionaryave === '-') && ($indguardianave != '-') && ($indpsychologistave === '-') && ($inddirectorave === '-')) {
  $indgrandave = $indguardianave;
  $indgrandavefm = number_format($indgrandave, 0);
}
if (($indvisionaryave === '-') && ($indguardianave === '-') && ($indpsychologistave != '-') && ($inddirectorave === '-')) {
  $indgrandave = $indpsychologistave;
  $indgrandavefm = number_format($indgrandave, 0);
}
if (($indvisionaryave === '-') && ($indguardianave === '-') && ($indpsychologistave === '-') && ($inddirectorave != '-')) {
  $indgrandave = $inddirectorave;
  $indgrandavefm = number_format($indgrandave, 0);
}

if (($indvisionaryave != '-') && ($indguardianave != '-') && ($indpsychologistave === '-') && ($inddirectorave === '-')) {
  $indgrandave = ($indvisionaryave + $indguardianave) / 2;
  $indgrandavefm = number_format($indgrandave, 0);
}
if (($indvisionaryave != '-') && ($indguardianave === '-') && ($indpsychologistave != '-') && ($inddirectorave === '-')) {
  $indgrandave = ($indvisionaryave + $indpsychologistave) / 2;
  $indgrandavefm = number_format($indgrandave, 0);
}
if (($indvisionaryave != '-') && ($indguardianave === '-') && ($indpsychologistave === '-') && ($inddirectorave != '-')) {
  $indgrandave = ($indvisionaryave + $inddirectorave) / 2;
  $indgrandavefm = number_format($indgrandave, 0);
}
if (($indvisionaryave === '-') && ($indguardianave != '-') && ($indpsychologistave != '-') && ($inddirectorave === '-')) {
  $indgrandave = ($indguardianave + $indpsychologistave) / 2;
  $indgrandavefm = number_format($indgrandave, 0);
}
if (($indvisionaryave === '-') && ($indguardianave != '-') && ($indpsychologistave === '-') && ($inddirectorave != '-')) {
  $indgrandave = ($indguardianave + $inddirectorave) / 2;
  $indgrandavefm = number_format($indgrandave, 0);
}
if (($indvisionaryave === '-') && ($indguardianave === '-') && ($indpsychologistave != '-') && ($inddirectorave != '-')) {
  $indgrandave = ($indpsychologistave + $inddirectorave) / 2;
  $indgrandavefm = number_format($indgrandave, 0);
}

if (($indvisionaryave != '-') && ($indguardianave != '-') && ($indpsychologistave != '-') && ($inddirectorave === '-')) {
  $indgrandave = ($indvisionaryave + $indguardianave + $indpsychologistave) / 3;
  $indgrandavefm = number_format($indgrandave, 0);
}
if (($indvisionaryave != '-') && ($indguardianave != '-') && ($indpsychologistave === '-') && ($inddirectorave != '-')) {
  $indgrandave = ($indvisionaryave + $indguardianave + $inddirectorave) / 3;
  $indgrandavefm = number_format($indgrandave, 0);
}
if (($indvisionaryave != '-') && ($indguardianave === '-') && ($indpsychologistave != '-') && ($inddirectorave != '-')) {
  $indgrandave = ($indvisionaryave + $indpsychologistave + $inddirectorave) / 3;
  $indgrandavefm = number_format($indgrandave, 0);
}
if (($indvisionaryave === '-') && ($indguardianave != '-') && ($indpsychologistave != '-') && ($inddirectorave != '-')) {
  $indgrandave = ($indguardianave + $indpsychologistave + $inddirectorave) / 3;
  $indgrandavefm = number_format($indgrandave, 0);
}


if (($stakevisionaryave === '-') && ($stakeguardianave === '-') && ($stakepsychologistave === '-') && ($stakedirectorave === '-')) {
  $stakegrandave = '-';
  $stakegrandavefm = '-';
}
if (($stakevisionaryave != '-') && ($stakeguardianave != '-') && ($stakepsychologistave != '-') && ($stakedirectorave != '-')) {
  $stakegrandave = ($stakevisionaryave + $stakeguardianave + $stakepsychologistave + $stakedirectorave) / 4;
  $stakegrandavefm = number_format($stakegrandave, 0);
}

if (($stakevisionaryave != '-') && ($stakeguardianave === '-') && ($stakepsychologistave === '-') && ($stakedirectorave === '-')) {
  $stakegrandave = $stakevisionaryave;
  $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave === '-') && ($stakeguardianave != '-') && ($stakepsychologistave === '-') && ($stakedirectorave === '-')) {
  $stakegrandave = $stakeguardianave;
  $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave === '-') && ($stakeguardianave === '-') && ($stakepsychologistave != '-') && ($stakedirectorave === '-')) {
  $stakegrandave = $stakepsychologistave;
  $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave === '-') && ($stakeguardianave === '-') && ($stakepsychologistave === '-') && ($stakedirectorave != '-')) {
  $stakegrandave = $stakedirectorave;
  $stakegrandavefm = number_format($stakegrandave, 0);
}

if (($stakevisionaryave != '-') && ($stakeguardianave != '-') && ($stakepsychologistave === '-') && ($stakedirectorave === '-')) {
  $stakegrandave = ($stakevisionaryave + $stakeguardianave) / 2;
  $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave != '-') && ($stakeguardianave === '-') && ($stakepsychologistave != '-') && ($stakedirectorave === '-')) {
  $stakegrandave = ($stakevisionaryave + $stakepsychologistave) / 2;
  $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave != '-') && ($stakeguardianave === '-') && ($stakepsychologistave === '-') && ($stakedirectorave != '-')) {
  $stakegrandave = ($stakevisionaryave + $stakedirectorave) / 2;
  $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave === '-') && ($stakeguardianave != '-') && ($stakepsychologistave != '-') && ($stakedirectorave === '-')) {
  $stakegrandave = ($stakeguardianave + $stakepsychologistave) / 2;
  $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave === '-') && ($stakeguardianave != '-') && ($stakepsychologistave === '-') && ($stakedirectorave != '-')) {
  $stakegrandave = ($stakeguardianave + $stakedirectorave) / 2;
  $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave === '-') && ($stakeguardianave === '-') && ($stakepsychologistave != '-') && ($stakedirectorave != '-')) {
  $stakegrandave = ($stakepsychologistave + $stakedirectorave) / 2;
  $stakegrandavefm = number_format($stakegrandave, 0);
}

if (($stakevisionaryave != '-') && ($stakeguardianave != '-') && ($stakepsychologistave != '-') && ($stakedirectorave === '-')) {
  $stakegrandave = ($stakevisionaryave + $stakeguardianave + $stakepsychologistave) / 3;
  $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave != '-') && ($stakeguardianave != '-') && ($stakepsychologistave === '-') && ($stakedirectorave != '-')) {
  $stakegrandave = ($stakevisionaryave + $stakeguardianave + $stakedirectorave) / 3;
  $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave != '-') && ($stakeguardianave === '-') && ($stakepsychologistave != '-') && ($stakedirectorave != '-')) {
  $stakegrandave = ($stakevisionaryave + $stakepsychologistave + $stakedirectorave) / 3;
  $stakegrandavefm = number_format($stakegrandave, 0);
}
if (($stakevisionaryave === '-') && ($stakeguardianave != '-') && ($stakepsychologistave != '-') && ($stakedirectorave != '-')) {
  $stakegrandave = ($stakeguardianave + $stakepsychologistave + $stakedirectorave) / 3;
  $stakegrandavefm = number_format($stakegrandave, 0);
}
//echo "Stakegrandave= $stakegrandave ";
//Calculating tandsgrandave for ultimate star-rating

if ($indgrandave === '-' && $stakegrandave === '-') {
  $iandsgrandave = '-';
  $iandsgrandavefm = $iandsgrandave;
  $grandcomment = '';
}
elseif ($indgrandave != '-' && $stakegrandave === '-') {
  $iandsgrandave = $indgrandave;
  $iandsgrandavefm = number_format($iandsgrandave, 0);
}
elseif ($indgrandave === '-' && $stakegrandave != '-') {
  $iandsgrandave = $stakegrandave;
  $iandsgrandavefm = number_format($iandsgrandave, 0);
}
else {
  $iandsgrandave = 0.5 * ($indgrandave + $stakegrandave);
  $iandsgrandavefm = number_format($iandsgrandave, 0);
}

if (($iandsgrandave != '-') && ($iandsgrandave >= 68.81)) {
  $grandcomment = "<img src=\"images/bookmark-4.png\" alt=\"Gold star award\" width=\"12\" height=\"12\" />";
}
elseif (($iandsgrandave != '-') && ($iandsgrandave >= 63.52 && $iandsgrandave < 68.81)) {
  $grandcomment = "<img src=\"images/bookmark-silver.png\" alt=\"Silver star award\" width=\"12\" height=\"12\" />";
}
else {
  $grandcomment = '';
};


$totalhighs = $focushigh + $communicatehigh + $securehigh + $organizehigh + $unitehigh + $empowerhigh + $deliverhigh + $improvehigh;

//echo "tandsgrandave= $iandsgrandave ";
// Database average calculations
// Make these inoperable and use initialization values if program runs too slow
/*
  $d1db = mysqli_query($dbc, "SELECT  AVG(d1) FROM inddata WHERE  (((d1>=0) AND (d1<=4)))");
  $d1dbave = mysqli_fetch_array($d1db) or die(mysqli_error());
  if (!$d1dbave['AVG(d1)']) {
  $dbavgd1 = '-';
  } else {
  $dbavgd1 = number_format(25 * $d1dbave['AVG(d1)'], 0);
  $dbfocus = $dbfocus + $d1dbave['AVG(d1)'];
  $dbfocusdenom++;
  }
  //echo "After d1 db focus = $dbfocus and dbfocusdenom = $dbfocusdenom<br /><br />"  ;

  $d2db = mysqli_query($dbc, "SELECT AVG(d2) FROM inddata WHERE  ((d2>=0) AND (d2<=4))");
  $d2dbave = mysqli_fetch_array($d2db) or die(mysqli_error());
  if (!$d2dbave['AVG(d2)']) {
  $dbavgd2 = '-';
  } else {
  $dbavgd2 = number_format(25 * $d2dbave['AVG(d2)'], 0);
  $dbfocus = $dbfocus + $d2dbave['AVG(d2)'];
  $dbfocusdenom++;
  }
  //echo "After d2 db focus = $dbfocus and dbfocusdenom = $dbfocusdenom<br /><br />"  ;

  $d3db = mysqli_query($dbc, "SELECT  AVG(d3) FROM inddata WHERE  ((d3>=0) AND (d3<=4))");
  $d3dbave = mysqli_fetch_array($d3db) or die(mysqli_error());
  if (!$d3dbave['AVG(d3)']) {
  $dbavgd3 = '-';
  } else {
  $dbavgd3 = number_format(25 * $d3dbave['AVG(d3)'], 0);
  $dbfocus = $dbfocus + $d3dbave['AVG(d3)'];
  $dbfocusdenom++;
  }
  //echo "After d3 db focus = $dbfocus and dbfocusdenom = $dbfocusdenom<br /><br />"  ;

  // Calculating focus summary stats - averages
  if ($dbfocusdenom === 0) {
  $dbfocusave = '-';
  $dbfocusavefm = '-';
  } else {
  $dbfocusave = 25 * $dbfocus / $dbfocusdenom;
  $dbfocusavefm = number_format($dbfocusave, 0);
  }
  //echo" dbfocusave = $dbfocusave" ;


  $d4db = mysqli_query($dbc, "SELECT  AVG(d4) FROM inddata WHERE  ((d4>=0) AND (d4<=4))");
  $d4dbave = mysqli_fetch_array($d4db) or die(mysqli_error());
  if (!$d4dbave['AVG(d4)']) {
  $dbavgd4 = '-';
  } else {
  $dbavgd4 = number_format(25 * $d4dbave['AVG(d4)'], 0);
  $dbcommunicate = $dbcommunicate + $d4dbave['AVG(d4)'];
  $dbcommunicatedenom++;
  }
  //echo "After d4 db communicate = $dbcommunicate and dbcommunicatedenom = $dbcommunicatedenom<br /><br />"  ;


  $d5db = mysqli_query($dbc, "SELECT  AVG(d5) FROM inddata WHERE  ((d5>=0) AND (d5<=4))");
  $d5dbave = mysqli_fetch_array($d5db) or die(mysqli_error());
  if (!$d5dbave['AVG(d5)']) {
  $dbavgd5 = '-';
  } else {
  $dbavgd5 = number_format(25 * $d5dbave['AVG(d5)'], 0);
  $dbcommunicate = $dbcommunicate + $d5dbave['AVG(d5)'];
  $dbcommunicatedenom++;
  }
  //echo "After d5 db communicate = $dbcommunicate and dbcommunicatedenom = $dbcommunicatedenom<br /><br />"  ;


  $d6db = mysqli_query($dbc, "SELECT AVG(d6) FROM inddata WHERE  ((d6>=0) AND (d6<=4))");
  $d6dbave = mysqli_fetch_array($d6db) or die(mysqli_error());
  if (!$d6dbave['AVG(d6)']) {
  $dbavgd6 = '-';
  } else {
  $dbavgd6 = number_format(25 * $d6dbave['AVG(d6)'], 0);
  $dbcommunicate = $dbcommunicate + $d6dbave['AVG(d6)'];
  $dbcommunicatedenom++;
  }
  //echo "After d6 db communicate = $dbcommunicate and dbcommunicatedenom = $dbcommunicatedenom<br /><br />"  ;


  // Calculating communicate summary stats - averages
  if ($dbcommunicatedenom === 0) {
  $dbcommunicateave = '-';
  $dbcommunicateavefm = '-';
  } else {
  $dbcommunicateave = 25 * $dbcommunicate / $dbcommunicatedenom;
  $dbcommunicateavefm = number_format($dbcommunicateave, 0);
  }
  //echo" dbcommunicateave = $dbcommunicateave" ;


  if (($dbfocusdenom === 0) && ($dbcommunicatedenom === 0)) {
  $dbvisionaryave = '-';
  $dbvisionaryavefm = '-';
  }
  if (($dbfocusdenom != 0) && ($dbcommunicatedenom === 0)) {
  $dbvisionaryave = 25 * $dbfocus / $dbfocusdenom;
  $dbvisionaryavefm = number_format($dbvisionaryave, 0);
  }
  if (($dbfocusdenom === 0) && ($dbcommunicatedenom != 0)) {
  $dbvisionaryave = 25 * $dbcommunicate / $dbcommunicatedenom;
  $dbvisionaryavefm = number_format($dbvisionaryave, 0);
  }
  if (($dbfocusdenom != 0) && ($dbcommunicatedenom != 0)) {
  $dbvisionaryave = 12.5 * (($dbfocus / $dbfocusdenom) + ($dbcommunicate / $dbcommunicatedenom));
  $dbvisionaryavefm = number_format($dbvisionaryave, 0);
  }
  //echo "dbvisionaryave = $dbvisionaryave dbvisionaryavefm = $dbvisionaryavefm<br /><br />" ;

  $d7db = mysqli_query($dbc, "SELECT AVG(d7) FROM inddata WHERE ((d7>=0) AND (d7<=4))");
  $d7dbave = mysqli_fetch_array($d7db) or die(mysqli_error());
  if (!$d7dbave['AVG(d7)']) {
  $dbavgd7 = '-';
  } else {
  $dbavgd7 = number_format(25 * $d7dbave['AVG(d7)'], 0);
  $dbsecure = $dbsecure + $d7dbave['AVG(d7)'];
  $dbsecuredenom++;
  }
  //echo "After d7 db secure = $dbsecure and dbsecuredenom = $dbsecuredenom<br /><br />"  ;


  $d8db = mysqli_query($dbc, "SELECT  AVG(d8) FROM inddata WHERE ((d8>=0) AND (d8<=4))");
  $d8dbave = mysqli_fetch_array($d8db) or die(mysqli_error());
  if (!$d8dbave['AVG(d8)']) {
  $dbavgd8 = '-';
  } else {
  $dbavgd8 = number_format(25 * $d8dbave['AVG(d8)'], 0);
  $dbsecure = $dbsecure + $d8dbave['AVG(d8)'];
  $dbsecuredenom++;
  }
  //echo "After d8 db secure = $dbsecure and dbsecuredenom = $dbsecuredenom<br /><br />"  ;


  $d9db = mysqli_query($dbc, "SELECT AVG(d9) FROM inddata WHERE  ((d9>=0) AND (d9<=4))");
  $d9dbave = mysqli_fetch_array($d9db) or die(mysqli_error());
  if (!$d9dbave['AVG(d9)']) {
  $dbavgd9 = '-';
  } else {
  $dbavgd9 = number_format(25 * $d9dbave['AVG(d9)'], 0);
  $dbsecure = $dbsecure + $d9dbave['AVG(d9)'];
  $dbsecuredenom++;
  }
  //echo "After d9 db secure = $dbsecure and dbsecuredenom = $dbsecuredenom<br /><br />"  ;

  // Calculating secure summary stats - averages
  if ($dbsecuredenom === 0) {
  $dbsecureave = '-';
  $dbsecureavefm = '-';
  } else {
  $dbsecureave = 25 * $dbsecure / $dbsecuredenom;
  $dbsecureavefm = number_format($dbsecureave, 0);
  }
  //echo" dbsecureave = $dbsecureave" ;

  $d10db = mysqli_query($dbc, "SELECT AVG(d10) FROM inddata WHERE ((d10>=0) AND (d10<=4))");
  $d10dbave = mysqli_fetch_array($d10db) or die(mysqli_error());
  if (!$d10dbave['AVG(d10)']) {
  $dbavgd10 = '-';
  } else {
  $dbavgd10 = number_format(25 * $d10dbave['AVG(d10)'], 0);
  $dborganize = $dborganize + $d10dbave['AVG(d10)'];
  $dborganizedenom++;
  }
  //echo "After d10 db organize = $dborganize and dborganizedenom = $dborganizedenom<br /><br />"  ;


  $d11db = mysqli_query($dbc, "SELECT AVG(d11) FROM inddata WHERE ((d11>=0) AND (d11<=4))");
  $d11dbave = mysqli_fetch_array($d11db) or die(mysqli_error());
  if (!$d11dbave['AVG(d11)']) {
  $dbavgd11 = '-';
  } else {
  $dbavgd11 = number_format(25 * $d11dbave['AVG(d11)'], 0);
  $dborganize = $dborganize + $d11dbave['AVG(d11)'];
  $dborganizedenom++;
  }
  //echo "After d11 db organize = $dborganize and dborganizedenom = $dborganizedenom<br /><br />"  ;


  $d12db = mysqli_query($dbc, "SELECT  AVG(d12) FROM inddata WHERE  ((d12>=0) AND (d12<=4))");
  $d12dbave = mysqli_fetch_array($d12db) or die(mysqli_error());
  if (!$d12dbave['AVG(d12)']) {
  $dbavgd12 = '-';
  } else {
  $dbavgd12 = number_format(25 * $d12dbave['AVG(d12)'], 0);
  $dborganize = $dborganize + $d12dbave['AVG(d12)'];
  $dborganizedenom++;
  }
  //echo "After d12 db organize = $dborganize and dborganizedenom = $dborganizedenom<br /><br />"  ;


  // Calculating organize summary stats - averages
  if ($dborganizedenom === 0) {
  $dborganizeave = '-';
  $dborganizeavefm = '-';
  } else {
  $dborganizeave = 25 * $dborganize / $dborganizedenom;
  $dborganizeavefm = number_format($dborganizeave, 0);
  }
  //echo" dborganizeave = $dborganizeave" ;

  if (($dbsecuredenom === 0) && ($dborganizedenom === 0)) {
  $dbguardianave = '-';
  $dbguardianavefm = '-';
  }
  if (($dbsecuredenom != 0) && ($dborganizedenom === 0)) {
  $dbguardianave = 25 * $dbsecure / $dbsecuredenom;
  $dbguardianavefm = number_format($dbguardianave, 0);
  }
  if (($dbsecuredenom === 0) && ($dborganizedenom != 0)) {
  $dbguardianave = 25 * $dborganize / $dborganizedenom;
  $dbguardianavefm = number_format($dbguardianave, 0);
  }
  if (($dbsecuredenom != 0) && ($dborganizedenom != 0)) {
  $dbguardianave = 12.5 * (($dbsecure / $dbsecuredenom) + ($dborganize / $dborganizedenom));
  $dbguardianavefm = number_format($dbguardianave, 0);
  }
  //echo "dbguardianave = $dbguardianave dbguardianavefm = $dbguardianavefm<br /><br />" ;


  $d13db = mysqli_query($dbc, "SELECT AVG(d13) FROM inddata WHERE ((d13>=0) AND (d13<=4))");
  $d13dbave = mysqli_fetch_array($d13db) or die(mysqli_error());
  if (!$d13dbave['AVG(d13)']) {
  $dbavgd13 = '-';
  } else {
  $dbavgd13 = number_format(25 * $d13dbave['AVG(d13)'], 0);
  $dbunite = $dbunite + $d13dbave['AVG(d13)'];
  $dbunitedenom++;
  }
  //echo "After d13 db unite = $dbunite and dbunitedenom = $dbunitedenom<br /><br />"  ;


  $d14db = mysqli_query($dbc, "SELECT  AVG(d14) FROM inddata WHERE  ((d14>=0) AND (d14<=4))");
  $d14dbave = mysqli_fetch_array($d14db) or die(mysqli_error());
  if (!$d14dbave['AVG(d14)']) {
  $dbavgd14 = '-';
  } else {
  $dbavgd14 = number_format(25 * $d14dbave['AVG(d14)'], 0);
  $dbunite = $dbunite + $d14dbave['AVG(d14)'];
  $dbunitedenom++;
  }
  //echo "After d14 db unite = $dbunite and dbunitedenom = $dbunitedenom<br /><br />"  ;


  $d15db = mysqli_query($dbc, "SELECT AVG(d15) FROM inddata WHERE  ((d15>=0) AND (d15<=4))");
  $d15dbave = mysqli_fetch_array($d15db) or die(mysqli_error());
  if (!$d15dbave['AVG(d15)']) {
  $dbavgd15 = '-';
  } else {
  $dbavgd15 = number_format(25 * $d15dbave['AVG(d15)'], 0);
  $dbunite = $dbunite + $d15dbave['AVG(d15)'];
  $dbunitedenom++;
  }
  //echo "After d15 db unite = $dbunite and dbunitedenom = $dbunitedenom<br /><br />"  ;


  // Calculating unite summary stats - averages
  if ($dbunitedenom === 0) {
  $dbuniteave = '-';
  $dbuniteavefm = '-';
  } else {
  $dbuniteave = 25 * $dbunite / $dbunitedenom;
  $dbuniteavefm = number_format($dbuniteave, 0);
  }
  //echo" dbuniteave = $dbuniteave" ;


  $d16db = mysqli_query($dbc, "SELECT  AVG(d16) FROM inddata WHERE  ((d16>=0) AND (d16<=4))");
  $d16dbave = mysqli_fetch_array($d16db) or die(mysqli_error());
  if (!$d16dbave['AVG(d16)']) {
  $dbavgd16 = '-';
  } else {
  $dbavgd16 = number_format(25 * $d16dbave['AVG(d16)'], 0);
  $dbempower = $dbempower + $d16dbave['AVG(d16)'];
  $dbempowerdenom++;
  }
  //echo "After d16 db empower = $dbempower and dbempowerdenom = $dbempowerdenom<br /><br />"  ;


  $d17db = mysqli_query($dbc, "SELECT  AVG(d17) FROM inddata WHERE  ((d17>=0) AND (d17<=4))");
  $d17dbave = mysqli_fetch_array($d17db) or die(mysqli_error());
  if (!$d17dbave['AVG(d17)']) {
  $dbavgd17 = '-';
  } else {
  $dbavgd17 = number_format(25 * $d17dbave['AVG(d17)'], 0);
  $dbempower = $dbempower + $d17dbave['AVG(d17)'];
  $dbempowerdenom++;
  }
  //echo "After d17 db empower = $dbempower and dbempowerdenom = $dbempowerdenom<br /><br />"  ;


  $d18db = mysqli_query($dbc, "SELECT   AVG(d18) FROM inddata WHERE  ((d18>=0) AND (d18<=4))");
  $d18dbave = mysqli_fetch_array($d18db) or die(mysqli_error());
  if (!$d18dbave['AVG(d18)']) {
  $dbavgd18 = '-';
  } else {
  $dbavgd18 = number_format(25 * $d18dbave['AVG(d18)'], 0);
  $dbempower = $dbempower + $d18dbave['AVG(d18)'];
  $dbempowerdenom++;
  }
  //echo "After d18 db empower = $dbempower and dbempowerdenom = $dbempowerdenom<br /><br />"  ;


  $d19db = mysqli_query($dbc, "SELECT AVG(d19) FROM inddata WHERE  ((d19>=0) AND (d19<=4))");
  $d19dbave = mysqli_fetch_array($d19db) or die(mysqli_error());
  if (!$d19dbave['AVG(d19)']) {
  $dbavgd19 = '-';
  } else {
  $dbavgd19 = number_format(25 * $d19dbave['AVG(d19)'], 0);
  $dbdeliver = $dbdeliver + $d19dbave['AVG(d19)'];
  $dbdeliverdenom++;
  }
  //echo "After d19 db deliver = $dbdeliver and dbdeliverdenom = $dbdeliverdenom<br /><br />"  ;


  $d20db = mysqli_query($dbc, "SELECT  AVG(d20) FROM inddata WHERE  ((d20>=0) AND (d20<=4))");
  $d20dbave = mysqli_fetch_array($d20db) or die(mysqli_error());
  if (!$d20dbave['AVG(d20)']) {
  $dbavgd20 = '-';
  } else {
  $dbavgd20 = number_format(25 * $d20dbave['AVG(d20)'], 0);
  $dbdeliver = $dbdeliver + $d20dbave['AVG(d20)'];
  $dbdeliverdenom++;
  }
  //echo "After d20 db deliver = $dbdeliver and dbdeliverdenom = $dbdeliverdenom<br /><br />"  ;


  $d21db = mysqli_query($dbc, "SELECT  AVG(d21) FROM inddata WHERE  ((d21>=0) AND (d21<=4))");
  $d21dbave = mysqli_fetch_array($d21db) or die(mysqli_error());
  if (!$d21dbave['AVG(d21)']) {
  $dbavgd21 = '-';
  } else {
  $dbavgd21 = number_format(25 * $d21dbave['AVG(d21)'], 0);
  $dbdeliver = $dbdeliver + $d21dbave['AVG(d21)'];
  $dbdeliverdenom++;
  }
  //echo "After d21 db deliver = $dbdeliver and dbdeliverdenom = $dbdeliverdenom<br /><br />"  ;


  $d22db = mysqli_query($dbc, "SELECT  AVG(d22) FROM inddata WHERE ((d22>=0) AND (d22<=4))");
  $d22dbave = mysqli_fetch_array($d22db) or die(mysqli_error());
  if (!$d22dbave['AVG(d22)']) {
  $dbavgd22 = '-';
  } else {
  $dbavgd22 = number_format(25 * $d22dbave['AVG(d22)'], 0);
  $dbimprove = $dbimprove + $d22dbave['AVG(d22)'];
  $dbimprovedenom++;
  }
  //echo "After d22 db improve = $dbimprove and dbimprovedenom = $dbimprovedenom<br /><br />"  ;


  $d23db = mysqli_query($dbc, "SELECT  AVG(d23) FROM inddata WHERE  ((d23>=0) AND (d23<=4))");
  $d23dbave = mysqli_fetch_array($d23db) or die(mysqli_error());
  if (!$d23dbave['AVG(d23)']) {
  $dbavgd23 = '-';
  } else {
  $dbavgd23 = number_format(25 * $d23dbave['AVG(d23)'], 0);
  $dbimprove = $dbimprove + $d23dbave['AVG(d23)'];
  $dbimprovedenom++;
  }
  //echo "After d23 db improve = $dbimprove and dbimprovedenom = $dbimprovedenom<br /><br />"  ;


  $d24db = mysqli_query($dbc, "SELECT  AVG(d24) FROM inddata WHERE  ((d24>=0) AND (d24<=4))");
  $d24dbave = mysqli_fetch_array($d24db) or die(mysqli_error());
  if (!$d24dbave['AVG(d24)']) {
  $dbavgd24 = '-';
  } else {
  $dbavgd24 = number_format(25 * $d24dbave['AVG(d24)'], 0);
  $dbimprove = $dbimprove + $d24dbave['AVG(d24)'];
  $dbimprovedenom++;
  }
  //echo "After d24 db improve = $dbimprove and dbimprovedenom = $dbimprovedenom<br /><br />"  ;


  // Calculating improve summary stats - averages
  if ($dbimprovedenom === 0) {
  $dbimproveave = '-';
  $dbimproveavefm = '-';
  } else {
  $dbimproveave = 25 * $dbimprove / $dbimprovedenom;
  $dbimproveavefm = number_format($dbimproveave, 0);
  }
  //echo" dbimproveave = $dbimproveave" ;


  $d25db = mysqli_query($dbc, "SELECT  AVG(d25) FROM inddata WHERE ((d25>=0) AND (d25<=4)) ");
  $d25dbave = mysqli_fetch_array($d25db) or die(mysqli_error());
  if (!$d25dbave['AVG(d25)']) {
  $dbavgd25 = '-';
  } else {
  $dbavgd25 = number_format(25 * $d25dbave['AVG(d25)'], 0);
  $dbdeliver = $dbdeliver + $d25dbave['AVG(d25)'];
  $dbdeliverdenom++;
  }
  //echo "After d25 db deliver = $dbdeliver and dbdeliverdenom = $dbdeliverdenom<br /><br />"  ;

  // Calculating deliver summary stats - averages
  if ($dbdeliverdenom === 0) {
  $dbdeliverave = '-';
  $dbdeliveravefm = '-';
  } else {
  $dbdeliverave = 25 * $dbdeliver / $dbdeliverdenom;
  $dbdeliveravefm = number_format($dbdeliverave, 0);
  }
  //echo" dbdeliverave = $dbdeliverave" ;


  if (($dbdeliverdenom === 0) && ($dbimprovedenom === 0)) {
  $dbdirectorave = '-';
  $dbdirectoravefm = '-';
  }
  if (($dbdeliverdenom != 0) && ($dbimprovedenom === 0)) {
  $dbdirectorave = 25 * $dbdeliver / $dbdeliverdenom;
  $dbdirectoravefm = number_format($dbdirectorave, 0);
  }
  if (($dbdeliverdenom === 0) && ($dbimprovedenom != 0)) {
  $dbdirectorave = 25 * $dbimprove / $dbimprovedenom;
  $dbdirectoravefm = number_format($dbdirectorave, 0);
  }
  if (($dbdeliverdenom != 0) && ($dbimprovedenom != 0)) {
  $dbdirectorave = 12.5 * (($dbdeliver / $dbdeliverdenom) + ($dbimprove / $dbimprovedenom));
  $dbdirectoravefm = number_format($dbdirectorave, 0);
  }
  //echo "dbdirectorave = $dbdirectorave dbdirectoravefm = $dbdirectoravefm<br /><br />" ;


  $d26db = mysqli_query($dbc, "SELECT  AVG(d26) FROM inddata WHERE  ((d26>=0) AND (d26<=4))");
  $d26dbave = mysqli_fetch_array($d26db) or die(mysqli_error());
  if (!$d26dbave['AVG(d26)']) {
  $dbavgd26 = '-';
  } else {
  $dbavgd26 = number_format(25 * $d26dbave['AVG(d26)'], 0);
  $dbempower = $dbempower + $d26dbave['AVG(d26)'];
  $dbempowerdenom++;
  }
  //echo "After d26 db empower = $dbempower and dbempowerdenom = $dbempowerdenom<br /><br />"  ;

  // Calculating empower summary stats - averages
  if ($dbempowerdenom === 0) {
  $dbempowerave = '-';
  $dbempoweravefm = '-';
  } else {
  $dbempowerave = 25 * $dbempower / $dbempowerdenom;
  $dbempoweravefm = number_format($dbempowerave, 0);
  }
  //echo" dbempowerave = $dbempowerave" ;


  if (($dbunitedenom === 0) && ($dbempowerdenom === 0)) {
  $dbpsychologistave = '-';
  $dbpsychologistavefm = '-';
  }
  if (($dbunitedenom != 0) && ($dbempowerdenom === 0)) {
  $dbpsychologistave = 25 * $dbunite / $dbunitedenom;
  $dbpsychologistavefm = number_format($dbpsychologistave, 0);
  }
  if (($dbunitedenom === 0) && ($dbempowerdenom != 0)) {
  $dbpsychologistave = 25 * $dbempower / $dbempowerdenom;
  $dbpsychologistavefm = number_format($dbpsychologistave, 0);
  }
  if (($dbunitedenom != 0) && ($dbempowerdenom != 0)) {
  $dbpsychologistave = 12.5 * (($dbunite / $dbunitedenom) + ($dbempower / $dbempowerdenom));
  $dbpsychologistavefm = number_format($dbpsychologistave, 0);
  }
  //echo "dbpsychologistave = $dbpsychologistave dbpsychologistavefm = $dbpsychologistavefm<br /><br />" ;



  if (($dbvisionaryave === '-') && ($dbguardianave === '-') && ($dbpsychologistave === '-') && ($dbdirectorave === '-')) {
  $dbgrandave = '-';
  $dbgrandavefm = '-';
  }
  if (($dbvisionaryave != '-') && ($dbguardianave != '-') && ($dbpsychologistave != '-') && ($dbdirectorave != '-')) {
  $dbgrandave = ($dbvisionaryave + $dbguardianave + $dbpsychologistave + $dbdirectorave) / 4;
  $dbgrandavefm = number_format($dbgrandave, 0);
  }

  if (($dbvisionaryave != '-') && ($dbguardianave === '-') && ($dbpsychologistave === '-') && ($dbdirectorave === '-')) {
  $dbgrandave = $dbvisionaryave;
  $dbgrandavefm = number_format($dbgrandave, 0);
  }
  if (($dbvisionaryave === '-') && ($dbguardianave != '-') && ($dbpsychologistave === '-') && ($dbdirectorave === '-')) {
  $dbgrandave = $dbguardianave;
  $dbgrandavefm = number_format($dbgrandave, 0);
  }
  if (($dbvisionaryave === '-') && ($dbguardianave === '-') && ($dbpsychologistave != '-') && ($dbdirectorave === '-')) {
  $dbgrandave = $dbpsychologistave;
  $dbgrandavefm = number_format($dbgrandave, 0);
  }
  if (($dbvisionaryave === '-') && ($dbguardianave === '-') && ($dbpsychologistave === '-') && ($dbdirectorave != '-')) {
  $dbgrandave = $dbdirectorave;
  $dbgrandavefm = number_format($dbgrandave, 0);
  }

  if (($dbvisionaryave != '-') && ($dbguardianave != '-') && ($dbpsychologistave === '-') && ($dbdirectorave === '-')) {
  $dbgrandave = ($dbvisionaryave + $dbguardianave) / 2;
  $dbgrandavefm = number_format($dbgrandave, 0);
  }
  if (($dbvisionaryave != '-') && ($dbguardianave === '-') && ($dbpsychologistave != '-') && ($dbdirectorave === '-')) {
  $dbgrandave = ($dbvisionaryave + $dbpsychologistave) / 2;
  $dbgrandavefm = number_format($dbgrandave, 0);
  }
  if (($dbvisionaryave != '-') && ($dbguardianave === '-') && ($dbpsychologistave === '-') && ($dbdirectorave != '-')) {
  $dbgrandave = ($dbvisionaryave + $dbdirectorave) / 2;
  $dbgrandavefm = number_format($dbgrandave, 0);
  }
  if (($dbvisionaryave === '-') && ($dbguardianave != '-') && ($dbpsychologistave != '-') && ($dbdirectorave === '-')) {
  $dbgrandave = ($dbguardianave + $dbpsychologistave) / 2;
  $dbgrandavefm = number_format($dbgrandave, 0);
  }
  if (($dbvisionaryave === '-') && ($dbguardianave != '-') && ($dbpsychologistave === '-') && ($dbdirectorave != '-')) {
  $dbgrandave = ($dbguardianave + $dbdirectorave) / 2;
  $dbgrandavefm = number_format($dbgrandave, 0);
  }
  if (($dbvisionaryave === '-') && ($dbguardianave === '-') && ($dbpsychologistave != '-') && ($dbdirectorave != '-')) {
  $dbgrandave = ($dbpsychologistave + $dbdirectorave) / 2;
  $dbgrandavefm = number_format($dbgrandave, 0);
  }

  if (($dbvisionaryave != '-') && ($dbguardianave != '-') && ($dbpsychologistave != '-') && ($dbdirectorave === '-')) {
  $dbgrandave = ($dbvisionaryave + $dbguardianave + $dbpsychologistave) / 3;
  $dbgrandavefm = number_format($dbgrandave, 0);
  }
  if (($dbvisionaryave != '-') && ($dbguardianave != '-') && ($dbpsychologistave === '-') && ($dbdirectorave != '-')) {
  $dbgrandave = ($dbvisionaryave + $dbguardianave + $dbdirectorave) / 3;
  $dbgrandavefm = number_format($dbgrandave, 0);
  }
  if (($dbvisionaryave != '-') && ($dbguardianave === '-') && ($dbpsychologistave != '-') && ($dbdirectorave != '-')) {
  $dbgrandave = ($dbvisionaryave + $dbpsychologistave + $dbdirectorave) / 3;
  $dbgrandavefm = number_format($dbgrandave, 0);
  }
  if (($dbvisionaryave === '-') && ($dbguardianave != '-') && ($dbpsychologistave != '-') && ($dbdirectorave != '-')) {
  $dbgrandave = ($dbguardianave + $dbpsychologistave + $dbdirectorave) / 3;
  $dbgrandavefm = number_format($dbgrandave, 0);
  }

  //echo "Dbgrandave= $dbgrandave ";
  // end dbase calcs

 */

//Extracting subject of the review's votes for development
$ivotedev1 = number_format($getindvotes['SUM(dev1)'], 0);
$ivotedev2 = number_format($getindvotes['SUM(dev2)'], 0);
$ivotedev3 = number_format($getindvotes['SUM(dev3)'], 0);
$ivotedev4 = number_format($getindvotes['SUM(dev4)'], 0);
$ivotedev5 = number_format($getindvotes['SUM(dev5)'], 0);
$ivotedev6 = number_format($getindvotes['SUM(dev6)'], 0);
$ivotedev7 = number_format($getindvotes['SUM(dev7)'], 0);
$ivotedev8 = number_format($getindvotes['SUM(dev8)'], 0);
$ivotedev9 = number_format($getindvotes['SUM(dev9)'], 0);
$ivotedev10 = number_format($getindvotes['SUM(dev10)'], 0);
$ivotedev11 = number_format($getindvotes['SUM(dev11)'], 0);
$ivotedev12 = number_format($getindvotes['SUM(dev12)'], 0);
$ivotedev13 = number_format($getindvotes['SUM(dev13)'], 0);
$ivotedev14 = number_format($getindvotes['SUM(dev14)'], 0);
$ivotedev15 = number_format($getindvotes['SUM(dev15)'], 0);
$ivotedev16 = number_format($getindvotes['SUM(dev16)'], 0);
$ivotedev17 = number_format($getindvotes['SUM(dev17)'], 0);
$ivotedev18 = number_format($getindvotes['SUM(dev18)'], 0);
$ivotedev19 = number_format($getindvotes['SUM(dev19)'], 0);
$ivotedev20 = number_format($getindvotes['SUM(dev20)'], 0);
$ivotedev21 = number_format($getindvotes['SUM(dev21)'], 0);
$ivotedev22 = number_format($getindvotes['SUM(dev22)'], 0);
$ivotedev23 = number_format($getindvotes['SUM(dev23)'], 0);
$ivotedev24 = number_format($getindvotes['SUM(dev24)'], 0);
$ivotedev25 = number_format($getindvotes['SUM(dev25)'], 0);
$ivotedev26 = number_format($getindvotes['SUM(dev26)'], 0);

//Extracting boss + contributor votes for development
$svotedev1 = number_format($getstakevotes['SUM(dev1)'], 0);
$svotedev2 = number_format($getstakevotes['SUM(dev2)'], 0);
$svotedev3 = number_format($getstakevotes['SUM(dev3)'], 0);
$svotedev4 = number_format($getstakevotes['SUM(dev4)'], 0);
$svotedev5 = number_format($getstakevotes['SUM(dev5)'], 0);
$svotedev6 = number_format($getstakevotes['SUM(dev6)'], 0);
$svotedev7 = number_format($getstakevotes['SUM(dev7)'], 0);
$svotedev8 = number_format($getstakevotes['SUM(dev8)'], 0);
$svotedev9 = number_format($getstakevotes['SUM(dev9)'], 0);
$svotedev10 = number_format($getstakevotes['SUM(dev10)'], 0);
$svotedev11 = number_format($getstakevotes['SUM(dev11)'], 0);
$svotedev12 = number_format($getstakevotes['SUM(dev12)'], 0);
$svotedev13 = number_format($getstakevotes['SUM(dev13)'], 0);
$svotedev14 = number_format($getstakevotes['SUM(dev14)'], 0);
$svotedev15 = number_format($getstakevotes['SUM(dev15)'], 0);
$svotedev16 = number_format($getstakevotes['SUM(dev16)'], 0);
$svotedev17 = number_format($getstakevotes['SUM(dev17)'], 0);
$svotedev18 = number_format($getstakevotes['SUM(dev18)'], 0);
$svotedev19 = number_format($getstakevotes['SUM(dev19)'], 0);
$svotedev20 = number_format($getstakevotes['SUM(dev20)'], 0);
$svotedev21 = number_format($getstakevotes['SUM(dev21)'], 0);
$svotedev22 = number_format($getstakevotes['SUM(dev22)'], 0);
$svotedev23 = number_format($getstakevotes['SUM(dev23)'], 0);
$svotedev24 = number_format($getstakevotes['SUM(dev24)'], 0);
$svotedev25 = number_format($getstakevotes['SUM(dev25)'], 0);
$svotedev26 = number_format($getstakevotes['SUM(dev26)'], 0);

// Calculating total votes for development = subject + boss + contributor

$totivotedev1 = $ivotedev1 + $svotedev1;
$totivotedev2 = $ivotedev2 + $svotedev2;
$totivotedev3 = $ivotedev3 + $svotedev3;
$totivotedev4 = $ivotedev4 + $svotedev4;
$totivotedev5 = $ivotedev5 + $svotedev5;
$totivotedev6 = $ivotedev6 + $svotedev6;
$totivotedev7 = $ivotedev7 + $svotedev7;
$totivotedev8 = $ivotedev8 + $svotedev8;
$totivotedev9 = $ivotedev9 + $svotedev9;
$totivotedev10 = $ivotedev10 + $svotedev10;
$totivotedev11 = $ivotedev11 + $svotedev11;
$totivotedev12 = $ivotedev12 + $svotedev12;
$totivotedev13 = $ivotedev13 + $svotedev13;
$totivotedev14 = $ivotedev14 + $svotedev14;
$totivotedev15 = $ivotedev15 + $svotedev15;
$totivotedev16 = $ivotedev16 + $svotedev16;
$totivotedev17 = $ivotedev17 + $svotedev17;
$totivotedev18 = $ivotedev18 + $svotedev18;
$totivotedev19 = $ivotedev19 + $svotedev19;
$totivotedev20 = $ivotedev20 + $svotedev20;
$totivotedev21 = $ivotedev21 + $svotedev21;
$totivotedev22 = $ivotedev22 + $svotedev22;
$totivotedev23 = $ivotedev23 + $svotedev23;
$totivotedev24 = $ivotedev24 + $svotedev24;
$totivotedev25 = $ivotedev25 + $svotedev25;
$totivotedev26 = $ivotedev26 + $svotedev26;

// Calculating graph line lengths for summary stats

$ligrave = 1 + (1.8 * $indgrandave);
$lsgrave = 1 + (1.8 * $stakegrandave);
$ldgrave = 1 + (1.8 * $dbgrandave);

$livisave = 1 + (1.8 * $indvisionaryave);
$ldvisave = 1 + (1.8 * $dbvisionaryave);
$lsvisave = 1 + (1.8 * $stakevisionaryave);

$liguaave = 1 + (1.8 * $indguardianave);
$ldguaave = 1 + (1.8 * $dbguardianave);
$lsguaave = 1 + (1.8 * $stakeguardianave);

$lipsyave = 1 + (1.8 * $indpsychologistave);
$ldpsyave = 1 + (1.8 * $dbpsychologistave);
$lspsyave = 1 + (1.8 * $stakepsychologistave);

$lidirave = 1 + (1.8 * $inddirectorave);
$lddirave = 1 + (1.8 * $dbdirectorave);
$lsdirave = 1 + (1.8 * $stakedirectorave);

$lifocusave = 1 + (1.8 * $indfocusave);
$ldfocusave = 1 + (1.8 * $dbfocusave);
$lsfocusave = 1 + (1.8 * $stakefocusave);

$licommunicateave = 1 + (1.8 * $indcommunicateave);
$ldcommunicateave = 1 + (1.8 * $dbcommunicateave);
$lscommunicateave = 1 + (1.8 * $stakecommunicateave);

$lisecureave = 1 + (1.8 * $indsecureave);
$ldsecureave = 1 + (1.8 * $dbsecureave);
$lssecureave = 1 + (1.8 * $stakesecureave);

$liorganizeave = 1 + (1.8 * $indorganizeave);
$ldorganizeave = 1 + (1.8 * $dborganizeave);
$lsorganizeave = 1 + (1.8 * $stakeorganizeave);

$liuniteave = 1 + (1.8 * $induniteave);
$lduniteave = 1 + (1.8 * $dbuniteave);
$lsuniteave = 1 + (1.8 * $stakeuniteave);

$liempowerave = 1 + (1.8 * $indempowerave);
$ldempowerave = 1 + (1.8 * $dbempowerave);
$lsempowerave = 1 + (1.8 * $stakeempowerave);

$lideliverave = 1 + (1.8 * $inddeliverave);
$lddeliverave = 1 + (1.8 * $dbdeliverave);
$lsdeliverave = 1 + (1.8 * $stakedeliverave);

$liimproveave = 1 + (1.8 * $indimproveave);
$ldimproveave = 1 + (1.8 * $dbimproveave);
$lsimproveave = 1 + (1.8 * $stakeimproveave);

// Calculating graph line lengths for detailed report p5

$li1 = 1 + (1.8 * $indavgd1);
$li2 = 1 + (1.8 * $indavgd2);
$li3 = 1 + (1.8 * $indavgd3);
$li4 = 1 + (1.8 * $indavgd4);
$li5 = 1 + (1.8 * $indavgd5);
$li6 = 1 + (1.8 * $indavgd6);
$li7 = 1 + (1.8 * $indavgd7);
$li8 = 1 + (1.8 * $indavgd8);
$li9 = 1 + (1.8 * $indavgd9);
$li10 = 1 + (1.8 * $indavgd10);
$li11 = 1 + (1.8 * $indavgd11);
$li12 = 1 + (1.8 * $indavgd12);
$li13 = 1 + (1.8 * $indavgd13);
$li14 = 1 + (1.8 * $indavgd14);
$li15 = 1 + (1.8 * $indavgd15);
$li16 = 1 + (1.8 * $indavgd16);
$li17 = 1 + (1.8 * $indavgd17);
$li18 = 1 + (1.8 * $indavgd18);
$li19 = 1 + (1.8 * $indavgd19);
$li20 = 1 + (1.8 * $indavgd20);
$li21 = 1 + (1.8 * $indavgd21);
$li22 = 1 + (1.8 * $indavgd22);
$li23 = 1 + (1.8 * $indavgd23);
$li24 = 1 + (1.8 * $indavgd24);
$li25 = 1 + (1.8 * $indavgd25);
$li26 = 1 + (1.8 * $indavgd26);

$ldb1 = 1 + (1.8 * $dbavgd1);
$ldb2 = 1 + (1.8 * $dbavgd2);
$ldb3 = 1 + (1.8 * $dbavgd3);
$ldb4 = 1 + (1.8 * $dbavgd4);
$ldb5 = 1 + (1.8 * $dbavgd5);
$ldb6 = 1 + (1.8 * $dbavgd6);
$ldb7 = 1 + (1.8 * $dbavgd7);
$ldb8 = 1 + (1.8 * $dbavgd8);
$ldb9 = 1 + (1.8 * $dbavgd9);
$ldb10 = 1 + (1.8 * $dbavgd10);
$ldb11 = 1 + (1.8 * $dbavgd11);
$ldb12 = 1 + (1.8 * $dbavgd12);
$ldb13 = 1 + (1.8 * $dbavgd13);
$ldb14 = 1 + (1.8 * $dbavgd14);
$ldb15 = 1 + (1.8 * $dbavgd15);
$ldb16 = 1 + (1.8 * $dbavgd16);
$ldb17 = 1 + (1.8 * $dbavgd17);
$ldb18 = 1 + (1.8 * $dbavgd18);
$ldb19 = 1 + (1.8 * $dbavgd19);
$ldb20 = 1 + (1.8 * $dbavgd20);
$ldb21 = 1 + (1.8 * $dbavgd21);
$ldb22 = 1 + (1.8 * $dbavgd22);
$ldb23 = 1 + (1.8 * $dbavgd23);
$ldb24 = 1 + (1.8 * $dbavgd24);
$ldb25 = 1 + (1.8 * $dbavgd25);
$ldb26 = 1 + (1.8 * $dbavgd26);

$lstake1 = 1 + (1.8 * $stakeavgd1);
$lstake2 = 1 + (1.8 * $stakeavgd2);
$lstake3 = 1 + (1.8 * $stakeavgd3);
$lstake4 = 1 + (1.8 * $stakeavgd4);
$lstake5 = 1 + (1.8 * $stakeavgd5);
$lstake6 = 1 + (1.8 * $stakeavgd6);
$lstake7 = 1 + (1.8 * $stakeavgd7);
$lstake8 = 1 + (1.8 * $stakeavgd8);
$lstake9 = 1 + (1.8 * $stakeavgd9);
$lstake10 = 1 + (1.8 * $stakeavgd10);
$lstake11 = 1 + (1.8 * $stakeavgd11);
$lstake12 = 1 + (1.8 * $stakeavgd12);
$lstake13 = 1 + (1.8 * $stakeavgd13);
$lstake14 = 1 + (1.8 * $stakeavgd14);
$lstake15 = 1 + (1.8 * $stakeavgd15);
$lstake16 = 1 + (1.8 * $stakeavgd16);
$lstake17 = 1 + (1.8 * $stakeavgd17);
$lstake18 = 1 + (1.8 * $stakeavgd18);
$lstake19 = 1 + (1.8 * $stakeavgd19);
$lstake20 = 1 + (1.8 * $stakeavgd20);
$lstake21 = 1 + (1.8 * $stakeavgd21);
$lstake22 = 1 + (1.8 * $stakeavgd22);
$lstake23 = 1 + (1.8 * $stakeavgd23);
$lstake24 = 1 + (1.8 * $stakeavgd24);
$lstake25 = 1 + (1.8 * $stakeavgd25);
$lstake26 = 1 + (1.8 * $stakeavgd26);
?>