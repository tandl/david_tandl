<?php

/* include ('functions/pdf_toprank_ind.php');
  top_devvote($ivotedev1, $ivotedev2, $ivotedev3, $ivotedev4, $ivotedev5, $ivotedev6, $ivotedev7, $ivotedev8, $ivotedev9, $ivotedev10, $ivotedev11, $ivotedev12, $ivotedev13, $ivotedev14, $ivotedev15, $ivotedev16, $ivotedev17, $ivotedev18, $ivotedev19, $ivotedev20, $ivotedev21, $ivotedev22, $ivotedev23, $ivotedev24, $ivotedev25, $ivotedev26); */

//Test $iandsgrandave = 55;

if (($iandsgrandave != '-') && ($iandsgrandave >= 68.81)) {
// Top 10% Exec summary
    $pdf->SetFont('Helvetica', 'B', 9);
    $pdf->Write(5, 'An excellent result.' . "\n" . "\n");
    $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, 'Your overall score was ');
    $pdf->SetFont('Helvetica', 'B', 9);
    $pdf->Write(5, $iandsgrandavefm);
    $pdf->SetFont('Helvetica', '', 9);
   // $pdf->Write(5, '%.' . "\n");
    $pdf->Write(5, '%. This is the average of your self-assessment and your contributors\' assessment (if you included them in the review) and puts you in the top 10% of our database.' . "\n" . "\n");
    $pdf->Write(5, 'You can always improve, though! Use the recommended exercises based on your report  - and push yourself to improve on even this excellent review. ');
} elseif (($iandsgrandave != '-') && ($iandsgrandave >= 63.52 && $iandsgrandave < 68.81)) {
    // Top quartile
    $pdf->SetFont('Helvetica', 'B', 9);
    $pdf->Write(5, 'You received a very good review.' . "\n" . "\n");
     $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, 'Your overall score was ');
    $pdf->SetFont('Helvetica', 'B', 9);
    $pdf->Write(5, $iandsgrandavefm);
    $pdf->SetFont('Helvetica', '', 9);
   // $pdf->Write(5, '%.' . "\n");
    $pdf->Write(5, '%. This is the average of your self-assessment and your contributors\'  assessment (if you included them in the review) and puts you in the top quartile of our database.' . "\n" . "\n");
    $pdf->Write(5, 'You can always improve, though. Use the recommended exercises based on your report  - and push yourself to get above the top 10% benchmark. ');
} elseif (($iandsgrandave != '-') && ($iandsgrandave >= 57.61 && $iandsgrandave < 63.52)) {
// Above average
    $pdf->SetFont('Helvetica', 'B', 9);
    $pdf->Write(5, 'You  received a good review.' . "\n" . "\n");
       $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, 'Your overall score was ');
    $pdf->SetFont('Helvetica', 'B', 9);
    $pdf->Write(5, $iandsgrandavefm);
    $pdf->SetFont('Helvetica', '', 9);
   // $pdf->Write(5, '%.' . "\n");
    $pdf->Write(5, '%. This is the average of your self-assessment and your contributors\'  assessment (if you included them in the review) and puts you in the top half of our database.' . "\n" . "\n");
    $pdf->Write(5, 'You can always improve, though - and push yourself to get into the top quarter or top 10%. ');
} elseif (($iandsgrandave != '-') && ($iandsgrandave >= 51.71 && $iandsgrandave < 57.61)) {
// Lower quartile
    $pdf->SetFont('Helvetica', 'B', 9);
    $pdf->Write(5, 'You received a fairly good review.' . "\n" . "\n");
        $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, 'Your overall score was ');
    $pdf->SetFont('Helvetica', 'B', 9);
    $pdf->Write(5, $iandsgrandavefm);
    $pdf->SetFont('Helvetica', '', 9);
   // $pdf->Write(5, '%.' . "\n");
    $pdf->Write(5, '%. This is the average of your self-assessment and your contributors\'  assessment (if you included them in the review). This puts you just below the mid-point score in our research database, so there\'s plenty that you can work on, but you\'re starting from a solid base.' . "\n" . "\n");
    $pdf->Write(5, 'You can always improve, though - and push yourself to get into the top quarter or top 10%. ');
} else {
// Bottom quartile
    $pdf->SetFont('Helvetica', 'B', 9);
    $pdf->Write(5, 'Your need help to build up your confidence.' . "\n" . "\n");
     $pdf->SetFont('Helvetica', '', 9);
    $pdf->Write(5, 'Your overall score was ');
    $pdf->SetFont('Helvetica', 'B', 9);
    $pdf->Write(5, $iandsgrandavefm);
    $pdf->SetFont('Helvetica', '', 9);
   // $pdf->Write(5, '%.' . "\n");
    $pdf->Write(5, '%. This is the average of your self-assessment and your contributors\'  assessment (if you included them in the review).  It puts you in the lower quartile of our database, and building your confidence as a team is an essential first step to rising up the database ratings. ' . "\n" . "\n");
    $pdf->Write(5, 'The key to building confidence is to work on improving step by step and get a few successes under your belt - much like an athlete would get back to fitness after an injury. ');
}
?>
