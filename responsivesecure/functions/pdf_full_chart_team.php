
<?php

// Creating generic loop to print bar charts, where sections can have a variable number of topics underneath them
// First, array sections and topics in the order of printing - order of section titles, then order of topic Bar Chart headings
$sections_2018 = array("Setting direction", "Leading by example", "Organising the team", "Building teamwork", "Empowering people", "Managing performance", "Leading change", "Results achieved");
$sections_2018_image = array('images/v3/Aligned Direction.png', 'images/v3/Builds Trust.png', 'images/v3/Clear Organisation.png', 'images/v3/Effective Teamwork.png', 'images/v3/Empowers Team Members.png', 'images/v3/Effective Decision making.png', 'images/v3/Leads change.png', 'images/v3/analytics-1.png');

// All you need to do to change the order the questions appear on the report page is (1) get the questions in the right order in $topics_2018_dnumber and (2) indicate whether the next line is a Section heading or a topic in $print_order

// For the specific questions, in order of their appearance on the full report sheet, specifying the 'database variable id' for each topic, corresponding to the topic - eg 'Clarity of objectives' is d1 in the database, 'Focus on what customers want' is d2 etc
$topics_2018_dnumber = array(1, 2, 3, 15, 4, 5, 6, 8, 10, 11, 12, 13, 14, 16, 17, 18, 19, 20, 21, 22, 23, 24, 7, 9, 25, 26);

// Specifying order to print on the page line by line - section (1), or topic (2) eg 1,2,2 means 1 section heading then 2 topic headings
$print_order = array(1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 1, 2, 2, 1, 2, 2, 2, 1, 2, 2, 2, 1, 2, 2, 2, 2, 2, 1, 2, 2);

$x = 15; //x  axis start point for chart as a whole
$y = 41; //y  axis start point for chart as a whole
$xbar = 143; //x  axis start point for bars
$ybar = 53.5; //y  axis start point for bars
$lw = 1.1; // width of graph bar line
$n = 1;
$gridline = 0.1;

$pdf->SetlineWidth($gridline);
$pdf->SetDrawColor(255, 191, 0);
$pdf->Line($xbar - 0.5, 53.5, $xbar - 0.5, 267);
$pdf->Line($xbar + 12.5, 53.5, $xbar + 12.5, 267);
$pdf->Line($xbar + 25, 53.5, $xbar + 25, 267);
$pdf->Line($xbar + 37.5, 53.5, $xbar + 37.5, 267);
$pdf->Line($xbar + 50, 53.5, $xbar + 50, 267);

$pdf->Image("images/face-sad.png", $xbar - 4.3, $y + 2, 4, 4);
$pdf->Image("images/face-sad.png", $xbar - 0.3, $y + 2, 4, 4);
$pdf->Image("images/face-sad.png", $xbar + 10.5, $y + 2, 4, 4);
$pdf->SetXY($xbar + 22, $y + 2);
$pdf->SetFont('Helvetica', 'B', 7);
$pdf->Write(4, 'OK');
$pdf->Image("images/face-smile.png", $xbar + 35.5, $y + 2, 4, 4);
$pdf->Image("images/face-smile.png", $xbar + 45.5, $y + 2, 4, 4);
$pdf->Image("images/face-smile.png", $xbar + 49.5, $y + 2, 4, 4);

$pdf->SetXY($xbar - 2.1, $y + 8);
$pdf->SetRightMargin(0);
$pdf->SetFont('Helvetica', '', 7);
$pdf->Write(4, '0');
$pdf->SetXY($xbar + 10, $y + 8);
$pdf->Write(4, '25');
$pdf->SetXY($xbar + 22.5, $y + 8);
$pdf->Write(4, '50');
$pdf->SetXY($xbar + 35, $y + 8);
$pdf->Write(4, '75');
$pdf->SetXY($xbar + 46.5, $y + 8);
$pdf->Write(4, '100');
$pdf->SetRightMargin(25);

$pdf->SetFont('Helvetica', '', 7);
$pdf->SetXY($x + 78, $y + 8);
//$pdf->Write(4, 'Team   Database   Stakeholders  Develop?'); this is not used as the extra "Develop?" column made the table too confusing - see line 108-ish below
$pdf->Write(4, 'Team');
$pdf->SetXY($x + 87, $y + 8);
$pdf->Write(4, 'Database');
$pdf->SetXY($x + 99, $y + 8);
$pdf->Write(4, 'Stakeholders');

$pdf->SetlineWidth($lw - 0.3);
$pdf->SetDrawColor(13, 100, 183);

$pdf->Line($x + 79.8, $y + 12, $x + 85, $y + 12);
$pdf->SetDrawColor(217, 217, 217);
$pdf->Line($x + 88.5, $y + 12, $x + 98, $y + 12);
$pdf->SetDrawColor(239, 0, 209);
$pdf->Line($x + 100.8, $y + 12, $x + 114, $y + 12);

////////Testing Grand total overall bar
//Drawing elipses round high and low grand ave scores
if ((is_numeric($teamgrandavefm)) && ($teamgrandavefm >= $teamgrandtoptenpc)) {
  $pdf->Image("images/ellipsegood.png", $x + 81.5, $y + 15, 12, 6);
}
elseif ((is_numeric($teamgrandavefm)) && ($teamgrandavefm <= $teamgrandbottenpc)) {
  $pdf->Image("images/ellipsebad.png", $x + 81.5, $y + 15, 12, 6);
}

if ((is_numeric($stakegrandavefm)) && ($stakegrandavefm >= $teamgrandtoptenpc)) {
  $pdf->Image("images/ellipsegood.png", $x + 106, $y + 15, 12, 6);
}
elseif ((is_numeric($stakegrandavefm)) && ($stakegrandavefm <= $teamgrandbottenpc)) {
  $pdf->Image("images/ellipsebad.png", $x + 106, $y + 15, 12, 6);
}

$pdf->SetXY((20), $y + 16);
$pdf->SetFont('Helvetica', 'B', 9);
$pdf->Write(4, 'Your overall rating');

$pdf->SetXY(($x + 85), $y + 16);
$pdf->SetFont('Helvetica', 'B', 8.5);
$pdf->Write(4, $teamgrandavefm);
$pdf->SetXY(($x + 96), $y + 16);
$pdf->Write(4, $dbgrandavefm);
$pdf->SetXY(($x + 109), $y + 16);
$pdf->Write(4, $stakegrandavefm);

$pdf->SetlineWidth($lw);
$pdf->SetDrawColor(13, 100, 183);
$pdf->Line($xbar, ($y + 16.5), $xbar + (($teamgrandavefm) / 2), ($y + 16.5));
$pdf->SetDrawColor(217, 217, 217);
$pdf->Line($xbar, ($y + 16.5) + ($lw + 0.3), $xbar + ($dbgrandavefm / 2), ($y + 16.5) + ($lw + 0.3));
$pdf->SetDrawColor(239, 0, 209);
$pdf->Line($xbar, (($y + 16.5) + (2 * ($lw + 0.3))), $xbar + (($stakegrandavefm) / 2), (($y + 16.5) + (2 * ($lw + 0.3))));

///////End of Testing Grand total overall bar

$x = 20;
$y = 65;
$pdf->SetXY($x, $y);
$title_count = 0;
$topics_2018_count = 0;
for ($i = 0; $i <= 33; $i++) {
  if ($print_order[$i] == 1) {
    $pdf->SetFont('Helvetica', '', 9);
    $pdf->Image("$sections_2018_image[$title_count]", $x - 6, ($y + (6 * $i)), 5, 5);
    $pdf->Write(5, $sections_2018[$title_count]);
    $pdf->SetXY($x, $y + (6 * ($i + 1)));
    $title_count++;
  }
  else {
    // Drawing elipses round high and low item scores
    // Shorthand for easier code. The topic number above (n) will be stored in $teamavefm array in the (n-1)th position as the array index starts from 0.
    $sh = $topics_2018_dnumber[$topics_2018_count] - 1;
    if ((is_numeric($teamavefm[$sh])) && ($teamavefm[$sh] >= $itemteamtoptenpc[$sh])) {
      $pdf->Image("images/ellipsegood.png", $x + 76.5, ($y + (6 * $i) - 1), 12, 6);
    }
    elseif ((is_numeric($teamavefm[$sh])) && ($teamavefm[$sh] <= $itemteambottenpc[$sh])) {
      $pdf->Image("images/ellipsebad.png", $x + 76.5, ($y + (6 * $i) - 1), 12, 6);
    }

    if ((is_numeric($stakeavefm[$sh])) && ($stakeavefm[$sh] >= $itemteamtoptenpc[$sh])) {
      $pdf->Image("images/ellipsegood.png", $x + 101, ($y + (6 * $i) - 1), 12, 6);
    }
    elseif ((is_numeric($stakeavefm[$sh])) && ($stakeavefm[$sh] <= $itemteambottenpc[$sh])) {
      $pdf->Image("images/ellipsebad.png", $x + 101, ($y + (6 * $i) - 1), 12, 6);
    }

    // Printing bar charts and figures
    $pdf->SetFont('Helvetica', '', 8.5);
    $pdf->SetX($x + 3);
    // Setting vertical start point of bars
    $ybar = ($y + (6 * $i) + 1);

    // The $topics_team_review array has a first line (ie an index of 0) of 'headers' , so need to pick up $sh+1
    $sh_plus = $sh + 1;
    $pdf->Write(4, $topics_team_review[$sh_plus][6]);
    //$pdf->Write(4, $topics_2018[$topics_2018_count]);
    $pdf->SetX(($x + 80));
    $pdf->SetFont('Helvetica', '', 8.5);
    //$pdf->Write(4, $teamavefm[$k] . '         ' . $dbavefm[$k] . '           ' . $stakeavefm[$k] . '              ' . $tvotedev[$k]);

    $pdf->Write(4, $teamavefm[$sh]);
    $pdf->SetX(($x + 91));
    $pdf->Write(4, $dbavefm[$sh]);
    $pdf->SetX(($x + 104));
    $pdf->Write(4, $stakeavefm[$sh]);

    $pdf->SetlineWidth($lw);
    $pdf->SetDrawColor(13, 100, 183);
    $pdf->Line($xbar, $ybar, $xbar + (($teamavefm[$sh]) / 2), $ybar);
    $pdf->SetDrawColor(217, 217, 217);
    $pdf->Line($xbar, $ybar + ($lw + 0.3), $xbar + ($dbavefm[$sh] / 2), $ybar + ($lw + 0.3));
    $pdf->SetDrawColor(239, 0, 209);
    $pdf->Line($xbar, ($ybar + (2 * ($lw + 0.3))), $xbar + (($stakeavefm[$sh]) / 2), ($ybar + (2 * ($lw + 0.3))));

    $pdf->SetXY($x, $y + (6 * ($i + 1)));
    $topics_2018_count++;
  }
}

$pdf->SetXY(22, 272);
$pdf->SetRightMargin(0);
$pdf->SetFont('Helvetica', '', 8);
$pdf->Write(4, 'This score is in top 10% of team scores in the database                    This score is in bottom 10% of team scores in the database ');
$pdf->Image("images/ellipsegood.png", 15, 272, 8, 4);
$pdf->Image("images/ellipsebad.png", 100, 272, 8, 4);


$pdf->SetDrawColor(35, 134, 243);
// End of new page 2018
?>


