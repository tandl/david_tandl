<?php

    $pdf->SetFont('Helvetica', '', 12);
    $pdf->Write(5, 'Your Signature Strengths' . "\n" . "\n");

//Printing out top 5 scores identified by the team - ie their "Signature Strengths"

$teamss = array();

/* These were the items re-worded slightly for the report - an idea I reverted on in April 2017
$teamss['%      The team is very clear about what it is trying to achieve'] = $teamavgd1;
$teamss['%      The team puts great emphasis on delivering what customers want'] = $teamavgd2;
$teamss['%      The team has ambitious and measurable team targets'] = $teamavgd3;
$teamss['%      The team communicates its vision and priorities in an inspiring way'] = $teamavgd4;
$teamss['%      The team generates optimism and confidence internally and externally'] = $teamavgd5;
$teamss['%      The team leads by example - actions match words'] = $teamavgd6;
$teamss['%      The team builds good relationships with key stakeholders'] = $teamavgd7;
$teamss['%      The team has all the skills and resources it needs to achieve its goals'] = $teamavgd8;
$teamss['%      The team is good at negotiating to get what it needs'] = $teamavgd9;
$teamss['%      Team meetings are effective and their purpose is clear'] = $teamavgd10;
$teamss['%      The team is clear about everyone\'s role and responsibilities'] = $teamavgd11;
$teamss['%      Authority and responsibilities are delegated to the right people in the team'] = $teamavgd12;
$teamss['%      In team discussions no-one is too dominant or too quiet'] = $teamavgd13;
$teamss['%      The team talks about everything it needs to and disagreements get resolved'] = $teamavgd14;
$teamss['%      People put collective achievement above their individual priorities'] = $teamavgd15;
$teamss['%      The team has a very can-do culture'] = $teamavgd16;
$teamss['%      Individuals in the team feel understood and accepted by each other'] = $teamavgd17;
$teamss['%      Team members openly thank and support each other'] = $teamavgd18;
$teamss['%      Team members hold each other to account for delivering on agreements'] = $teamavgd19;
$teamss['%      The team has accurate and comprehensive performance data and reports'] = $teamavgd20;
$teamss['%      The team makes decisions well together'] = $teamavgd21;
$teamss['%      The team is very strong in creativity and innovation'] = $teamavgd22;
$teamss['%      The team systematically analyzes data to improve results'] = $teamavgd23;
$teamss['%      The team has a great track record in delivering change and improvements'] = $teamavgd24;
$teamss['%      The team actually achieves the results it sets itself'] = $teamavgd25;
$teamss['%      It is personally and professionally rewarding working in the team'] = $teamavgd26;
*/

/*
This is the actual wording of the stakeholder questions in the online review
*/
$teamss['%      We are all very clear about what the team is trying to achieve'] = $teamavgd1;
$teamss['%      Our team puts great emphasis on delivering what customers want'] = $teamavgd2;
$teamss['%      We have ambitious and measurable team targets'] = $teamavgd3;
$teamss['%      Our team communicates its vision and priorities in an inspiring way'] = $teamavgd4;
$teamss['%      The team generates optimism and confidence internally and externally'] = $teamavgd5;
$teamss['%      The team leads by example - we do what we say we will do'] = $teamavgd6;
$teamss['%      The team builds good relationships with key stakeholders'] = $teamavgd7;
$teamss['%      Our team has all the skills and resources it needs to achieve its goals'] = $teamavgd8;
$teamss['%      The team is good at negotiating to get what it needs'] = $teamavgd9;
$teamss['%      Team meetings are effective and their purpose is clear'] = $teamavgd10;
$teamss['%      The team is clear about everyone\'s role and responsibilities'] = $teamavgd11;
$teamss['%      Authority and responsibilities are delegated to the right people in the team'] = $teamavgd12;
$teamss['%      In team discussions no-one is too dominant or too quiet'] = $teamavgd13;
$teamss['%      The team talks about everything it needs to and disagreements get resolved'] = $teamavgd14;
$teamss['%      People put collective achievement above their individual priorities'] = $teamavgd15;
$teamss['%      The team has a very can-do culture'] = $teamavgd16;
$teamss['%      Individuals in our team feel understood and accepted by each other'] = $teamavgd17;
$teamss['%      Team members openly thank and support each other'] = $teamavgd18;
$teamss['%      Team members hold each other to account for delivering on agreements'] = $teamavgd19;
$teamss['%      The team has accurate and comprehensive performance data and reports'] = $teamavgd20;
$teamss['%      We make decisions well as a team'] = $teamavgd21;
$teamss['%      Our team is very strong in creativity and innovation'] = $teamavgd22;
$teamss['%      Our team systematically analyzes data to improve results'] = $teamavgd23;
$teamss['%      The team has a great track record in delivering change and improvements'] = $teamavgd24;
$teamss['%      How well does your team actually achieve the results it sets itself?'] = $teamavgd25;
$teamss['%      How personally and professionally rewarding is working in the team?'] = $teamavgd26;

$pdf->SetFont('Helvetica', '', 10);
$pdf->Write(5, 'Your team\'s view - top 4 scores (see Page 4) as rated by team members: ' . "\n" ."\n");
$pdf->SetFont('Helvetica', '', 9);




//Printing out any Signature Strengths identified by team members

if (($teamavgd1 === '-') && ($teamavgd2 === '-') && ($teamavgd3 === '-') && ($teamavgd4 === '-') && ($teamavgd5 === '-') && ($teamavgd6 === '-') && ($teamavgd7 === '-') && ($teamavgd8 === '-') && ($teamavgd9 === '-') && ($teamavgd10 === '-') && ($teamavgd11 === '-') && ($teamavgd12 === '-') && ($teamavgd13 === '-') && ($teamavgd14 === '-') && ($teamavgd15 === '-') && ($teamavgd16 === '-') && ($teamavgd17 === '-') && ($teamavgd18 === '-') && ($teamavgd19 === '-') && ($teamavgd20 === '-') && ($teamavgd21 === '-') && ($teamavgd22 === '-') && ($teamavgd23 === '-') && ($teamavgd24 === '-') && ($teamavgd25 === '-') && ($teamavgd26 === '-')) {
    $pdf->Write(3, '       Your team didn\'t identify any signature strengths ' . "\n" . "\n");
} else {
    arsort($teamss);
    $top4 = array_slice($teamss, 0, 4);
    foreach ($top4 as $key => $value) {
        if ($value != '-') {
            $pdf->Write(3, '     ' . $value . ' ' . $key . "\n" . "\n");
        }
    }
}


$stakess = array();
/* These were the items re-worded slightly for the report - an idea I reverted on in April 2017
$stakess['%      The team is very clear about what it is trying to achieve'] = $stakeavgd1;
$stakess['%      The team puts great emphasis on delivering what customers want'] = $stakeavgd2;
$stakess['%      The team has ambitious and measurable team targets'] = $stakeavgd3;
$stakess['%      The team communicates its vision and priorities in an inspiring way'] = $stakeavgd4;
$stakess['%      The team generates optimism and confidence internally and externally'] = $stakeavgd5;
$stakess['%      The team leads by example - actions match words'] = $stakeavgd6;
$stakess['%      The team builds good relationships with key stakeholders'] = $stakeavgd7;
$stakess['%      The team has all the skills and resources it needs to achieve its goals'] = $stakeavgd8;
$stakess['%      The team is good at negotiating to get what it needs'] = $stakeavgd9;
$stakess['%      Team meetings are effective and their purpose is clear'] = $stakeavgd10;
$stakess['%      The team is clear about everyone\'s role and responsibilities'] = $stakeavgd11;
$stakess['%      Authority and responsibilities are delegated to the right people in the team'] = $stakeavgd12;
$stakess['%      In team discussions no-one is too dominant or too quiet'] = $stakeavgd13;
$stakess['%      The team talks about everything it needs to and disagreements get resolved'] = $stakeavgd14;
$stakess['%      People put collective achievement above their individual priorities'] = $stakeavgd15;
$stakess['%      The team has a very can-do culture'] = $stakeavgd16;
$stakess['%      Individuals in the team feel understood and accepted by each other'] = $stakeavgd17;
$stakess['%      Team members openly thank and support each other'] = $stakeavgd18;
$stakess['%      Team members hold each other to account for delivering on agreements'] = $stakeavgd19;
$stakess['%      The team has accurate and comprehensive performance data and reports'] = $stakeavgd20;
$stakess['%      The team makes decisions well together'] = $stakeavgd21;
$stakess['%      The team is very strong in creativity and innovation'] = $stakeavgd22;
$stakess['%      The team systematically analyzes data to improve results'] = $stakeavgd23;
$stakess['%      The team has a great track record in delivering change and improvements'] = $stakeavgd24;
$stakess['%      The team actually achieves the results it sets itself'] = $stakeavgd25;
$stakess['%      It is personally and professionally rewarding working in the team'] = $stakeavgd26;
*/
/*
This is the actual wording of the stakeholder questions in the online review
*/
$stakess['%      Team members seem very clear about what they are trying to achieve together'] = $stakeavgd1;
$stakess['%      The team puts great emphasis on delivering what customers want'] = $stakeavgd2;
$stakess['%      The team has an ambitious vision'] = $stakeavgd3;
$stakess['%      The team communicates its vision and priorities in an inspiring way'] = $stakeavgd4;
$stakess['%      The team generates optimism and confidence'] = $stakeavgd5;
$stakess['%      The team leads by example - they do what they say they will do'] = $stakeavgd6;
$stakess['%      Team members build good relationships with key stakeholders'] = $stakeavgd7;
$stakess['%      The team seems skilful and well-resourced'] = $stakeavgd8;
$stakess['%      The team is good at negotiating'] = $stakeavgd9;
$stakess['%      The team seems very well organized'] = $stakeavgd10;
$stakess['%      People\'s roles and responsibilities in the team are very clear'] = $stakeavgd11;
$stakess['%      Team members can make decisions without having to refer back to the boss'] = $stakeavgd12;
$stakess['%      Communication between team members seems excellent'] = $stakeavgd13;
$stakess['%      Team members speak with one voice – they are very consistent'] = $stakeavgd14;
$stakess['%      The team seems to have an all-for-one and one-for-all attitude'] = $stakeavgd15;
$stakess['%      People in the team seem very motivated to get things done'] = $stakeavgd16;
$stakess['%      There seem to be very good relationships between team members'] = $stakeavgd17;
$stakess['%      People in the team seem to support each other very well'] = $stakeavgd18;
$stakess['%      Team members take responsibility if things go wrong and try to sort out problems'] = $stakeavgd19;
$stakess['%      Team members always seem to have the right information at their fingertips'] = $stakeavgd20;
$stakess['%      We can always get decisions out of the team when we need them'] = $stakeavgd21;
$stakess['%      The team comes up with very creative solutions'] = $stakeavgd22;
$stakess['%      They are very good at analyzing where exactly where things need improving'] = $stakeavgd23;
$stakess['%      Team members are very good at making change happen'] = $stakeavgd24;
$stakess['%      The team delivers great results for us'] = $stakeavgd25;
$stakess['%      Team members seem to enjoy their work'] = $stakeavgd26;

$pdf->SetFont('Helvetica', '', 10);
$pdf->Write(5, "\n" . 'Your stakeholders\' view - top 4 scores (see Page 4) as rated by your stakeholders: ' . "\n" ."\n");
$pdf->SetFont('Helvetica', '', 9);


//Printing out any Signature Strengths identified by stakeholders

if (($stakeavgd1 === '-') && ($stakeavgd2 === '-') && ($stakeavgd3 === '-') && ($stakeavgd4 === '-') && ($stakeavgd5 === '-') && ($stakeavgd6 === '-') && ($stakeavgd7 === '-') && ($stakeavgd8 === '-') && ($stakeavgd9 === '-') && ($stakeavgd10 === '-') && ($stakeavgd11 === '-') && ($stakeavgd12 === '-') && ($stakeavgd13 === '-') && ($stakeavgd14 === '-') && ($stakeavgd15 === '-') && ($stakeavgd16 === '-') && ($stakeavgd17 === '-') && ($stakeavgd18 === '-') && ($stakeavgd19 === '-') && ($stakeavgd20 === '-') && ($stakeavgd21 === '-') && ($stakeavgd22 === '-') && ($stakeavgd23 === '-') && ($stakeavgd24 === '-') && ($stakeavgd25 === '-') && ($stakeavgd26 === '-')) {
    $pdf->Write(3, '       No stakeholders contibuted to this section of the review ' . "\n" . "\n");
} else {
    arsort($stakess);
    $top4 = array_slice($stakess, 0, 4);
    foreach ($top4 as $key => $value) {
        if ($value != '-') {
            $pdf->Write(3, '     ' . $value . ' ' . $key . "\n" . "\n");
        }
    }
}

    $pdf->SetFont('Helvetica', '', 12);
    $pdf->Write(5,  "\n" . "\n" . 'Your Challenges' . "\n");

//Printing out bottom 5 scoresidentified by the team - ie their "Challenges"

$teamff = array();
/* These were the items re-worded slightly for the report - an idea I reverted on in April 2017
$teamff['%      The team is not very clear what it is trying to achieve'] = $teamavgd1;
$teamff['%      The team does not put enough emphasis on delivering what customers want'] = $teamavgd2;
$teamff['%      The team doesn\'t have ambitious and measurable team targets'] = $teamavgd3;
$teamff['%      The team doesn\'t communicate its vision and priorities in an inspiring way'] = $teamavgd4;
$teamff['%      The team doesn\'t generate optimism and confidence internally and externally'] = $teamavgd5;
$teamff['%      The team doesn\'t lead by example - actions don\'t match words'] = $teamavgd6;
$teamff['%      The team doesn\'t build good enough relationships with key stakeholders'] = $teamavgd7;
$teamff['%      The team doesn\'t have all the skills and resources it needs to achieve its goals'] = $teamavgd8;
$teamff['%      The team is not good at negotiating to get what it needs'] = $teamavgd9;
$teamff['%      Team meetings are not effective with a clear purpose'] = $teamavgd10;
$teamff['%      The team is not clear about everyone\'s role and responsibilities'] = $teamavgd11;
$teamff['%      Authority and responsibilities are not delegated to the right people in the team'] = $teamavgd12;
$teamff['%      In team discussions some people are too dominant or too quiet'] = $teamavgd13;
$teamff['%      The team doesn\'t talk about everything it needs to and resolve disagreements'] = $teamavgd14;
$teamff['%      People don\'t put collective achievement above their individual priorities'] = $teamavgd15;
$teamff['%      The team doesn\'t have a very can-do culture'] = $teamavgd16;
$teamff['%      Individuals in our team don\'t feel understood and accepted by each other'] = $teamavgd17;
$teamff['%      Team members don\'t openly thank and support each other'] = $teamavgd18;
$teamff['%      Team members don\'t hold each other to account for delivering on agreements'] = $teamavgd19;
$teamff['%      The team doesn\'t have accurate and comprehensive performance data and reports'] = $teamavgd20;
$teamff['%      The team doesn\'t make decisions well together'] = $teamavgd21;
$teamff['%      The team is not very strong in creativity and innovation'] = $teamavgd22;
$teamff['%      The team doesn\'t systematically analyze data to improve results'] = $teamavgd23;
$teamff['%      The team doesn\'t have a great track record in delivering change and improvements'] = $teamavgd24;
$teamff['%      The team doesn\'t achieve the results it sets itself'] = $teamavgd25;
$teamff['%      It is not personally and professionally rewarding working in the team'] = $teamavgd26;
*/

$teamff['%      We are all very clear about what the team is trying to achieve'] = $teamavgd1;
$teamff['%      Our team puts great emphasis on delivering what customers want'] = $teamavgd2;
$teamff['%      We have ambitious and measurable team targets'] = $teamavgd3;
$teamff['%      Our team communicates its vision and priorities in an inspiring way'] = $teamavgd4;
$teamff['%      The team generates optimism and confidence internally and externally'] = $teamavgd5;
$teamff['%      The team leads by example - we do what we say we will do'] = $teamavgd6;
$teamff['%      The team builds good relationships with key stakeholders'] = $teamavgd7;
$teamff['%      Our team has all the skills and resources it needs to achieve its goals'] = $teamavgd8;
$teamff['%      The team is good at negotiating to get what it needs'] = $teamavgd9;
$teamff['%      Team meetings are effective and their purpose is clear'] = $teamavgd10;
$teamff['%      The team is clear about everyone\'s role and responsibilities'] = $teamavgd11;
$teamff['%      Authority and responsibilities are delegated to the right people in the team'] = $teamavgd12;
$teamff['%      In team discussions no-one is too dominant or too quiet'] = $teamavgd13;
$teamff['%      The team talks about everything it needs to and disagreements get resolved'] = $teamavgd14;
$teamff['%      People put collective achievement above their individual priorities'] = $teamavgd15;
$teamff['%      The team has a very can-do culture'] = $teamavgd16;
$teamff['%      Individuals in our team feel understood and accepted by each other'] = $teamavgd17;
$teamff['%      Team members openly thank and support each other'] = $teamavgd18;
$teamff['%      Team members hold each other to account for delivering on agreements'] = $teamavgd19;
$teamff['%      The team has accurate and comprehensive performance data and reports'] = $teamavgd20;
$teamff['%      We make decisions well as a team'] = $teamavgd21;
$teamff['%      Our team is very strong in creativity and innovation'] = $teamavgd22;
$teamff['%      Our team systematically analyzes data to improve results'] = $teamavgd23;
$teamff['%      The team has a great track record in delivering change and improvements'] = $teamavgd24;
$teamff['%      How well does your team actually achieve the results it sets itself?'] = $teamavgd25;
$teamff['%      How personally and professionally rewarding is working in the team?'] = $teamavgd26;

$pdf->SetFont('Helvetica', '', 10);
$pdf->Write(5, "\n" . 'Your team\'s view - lowest 4 scores (see Page 4) as rated by team members: ' . "\n" ."\n");
$pdf->SetFont('Helvetica', '', 9);


//Printing out the Challenges identified by team members

if (($teamavgd1 === '-') && ($teamavgd2 === '-') && ($teamavgd3 === '-') && ($teamavgd4 === '-') && ($teamavgd5 === '-') && ($teamavgd6 === '-') && ($teamavgd7 === '-') && ($teamavgd8 === '-') && ($teamavgd9 === '-') && ($teamavgd10 === '-') && ($teamavgd11 === '-') && ($teamavgd12 === '-') && ($teamavgd13 === '-') && ($teamavgd14 === '-') && ($teamavgd15 === '-') && ($teamavgd16 === '-') && ($teamavgd17 === '-') && ($teamavgd18 === '-') && ($teamavgd19 === '-') && ($teamavgd20 === '-') && ($teamavgd21 === '-') && ($teamavgd22 === '-') && ($teamavgd23 === '-') && ($teamavgd24 === '-') && ($teamavgd25 === '-') && ($teamavgd26 === '-')) {
    $pdf->Write(3, '       Your team didn\'t identify any challenges ' . "\n" . "\n");
} else {
    $teamff = array_diff($teamff, array('-'));
    asort($teamff);
    $bottom4 = array_slice($teamff, 0, 4);
	arsort($bottom4);
    foreach ($bottom4 as $key => $value) {
        $pdf->Write(3, '     ' . $value . ' ' . $key . "\n" . "\n");
    }
}

$stakeff = array();

/* These were the items re-worded slightly for the report - an idea I reverted on in April 2017
$stakeff['%      The team is not very clear what it is trying to achieve'] = $stakeavgd1;
$stakeff['%      The team does not put enough emphasis on delivering what customers want'] = $stakeavgd2;
$stakeff['%      The team doesn\'t have ambitious and measurable team targets'] = $stakeavgd3;
$stakeff['%      The team doesn\'t communicate its vision and priorities in an inspiring way'] = $stakeavgd4;
$stakeff['%      The team doesn\'t generate optimism and confidence internally and externally'] = $stakeavgd5;
$stakeff['%      The team doesn\'t lead by example - actions don\'t match words'] = $stakeavgd6;
$stakeff['%      The team doesn\'t build good enough relationships with key stakeholders'] = $stakeavgd7;
$stakeff['%      The team doesn\'t have all the skills and resources it needs to achieve its goals'] = $stakeavgd8;
$stakeff['%      The team is not good at negotiating to get what it needs'] = $stakeavgd9;
$stakeff['%      Team meetings are not effective with a clear purpose'] = $stakeavgd10;
$stakeff['%      The team is not clear about everyone\'s role and responsibilities'] = $stakeavgd11;
$stakeff['%      Authority and responsibilities are not delegated to the right people in the team'] = $stakeavgd12;
$stakeff['%      In team discussions some people are too dominant or too quiet'] = $stakeavgd13;
$stakeff['%      The team doesn\'t talk about everything it needs to and resolve disagreements'] = $stakeavgd14;
$stakeff['%      People don\'t put collective achievement above their individual priorities'] = $stakeavgd15;
$stakeff['%      The team doesn\'t have a very can-do culture'] = $stakeavgd16;
$stakeff['%      Individuals in our team don\'t feel understood and accepted by each other'] = $stakeavgd17;
$stakeff['%      Team members don\'t openly thank and support each other'] = $stakeavgd18;
$stakeff['%      Team members don\'t hold each other to account for delivering on agreements'] = $stakeavgd19;
$stakeff['%      The team doesn\'t have accurate and comprehensive performance data and reports'] = $stakeavgd20;
$stakeff['%      The team doesn\'t make decisions well together'] = $stakeavgd21;
$stakeff['%      The team is not very strong in creativity and innovation'] = $stakeavgd22;
$stakeff['%      The team doesn\'t systematically analyze data to improve results'] = $stakeavgd23;
$stakeff['%      The team doesn\'t have a great track record in delivering change and improvements'] = $stakeavgd24;
$stakeff['%      The team doesn\'t achieve the results it sets itself'] = $stakeavgd25;
$stakeff['%      It is not personally and professionally rewarding working in the team'] = $stakeavgd26;
*/

$stakeff['%      Team members seem very clear about what they are trying to achieve together'] = $stakeavgd1;
$stakeff['%      The team puts great emphasis on delivering what customers want'] = $stakeavgd2;
$stakeff['%      The team has an ambitious vision'] = $stakeavgd3;
$stakeff['%      The team communicates its vision and priorities in an inspiring way'] = $stakeavgd4;
$stakeff['%      The team generates optimism and confidence'] = $stakeavgd5;
$stakeff['%      The team leads by example – they do what they say they will do'] = $stakeavgd6;
$stakeff['%      Team members build good relationships with key stakeholders'] = $stakeavgd7;
$stakeff['%      The team seems skilful and well-resourced'] = $stakeavgd8;
$stakeff['%      The team is good at negotiating'] = $stakeavgd9;
$stakeff['%      The team seems very well organized'] = $stakeavgd10;
$stakeff['%      People\'s roles and responsibilities in the team are very clear'] = $stakeavgd11;
$stakeff['%      Team members can make decisions without having to refer back to the boss'] = $stakeavgd12;
$stakeff['%      Communication between team members seems excellent'] = $stakeavgd13;
$stakeff['%      Team members speak with one voice – they are very consistent'] = $stakeavgd14;
$stakeff['%      The team seems to have an all-for-one and one-for-all attitude'] = $stakeavgd15;
$stakeff['%      People in the team seem very motivated to get things done'] = $stakeavgd16;
$stakeff['%      There seem to be very good relationships between team members'] = $stakeavgd17;
$stakeff['%      People in the team seem to support each other very well'] = $stakeavgd18;
$stakeff['%      Team members take responsibility if things go wrong and try to sort out problems'] = $stakeavgd19;
$stakeff['%      Team members always seem to have the right information at their fingertips'] = $stakeavgd20;
$stakeff['%      We can always get decisions out of the team when we need them'] = $stakeavgd21;
$stakeff['%      The team comes up with very creative solutions'] = $stakeavgd22;
$stakeff['%      They are very good at analyzing where exactly where things need improving'] = $stakeavgd23;
$stakeff['%      Team members are very good at making change happen'] = $stakeavgd24;
$stakeff['%      The team delivers great results for us'] = $stakeavgd25;
$stakeff['%      Team members seem to enjoy their work'] = $stakeavgd26;

$pdf->SetFont('Helvetica', '', 10);
$pdf->Write(5, "\n" . 'Your stakeholders\' view - lowest 4 scores (see Page 4) as rated by your stakeholders: ' . "\n" ."\n");
$pdf->SetFont('Helvetica', '', 9);


//Printing out the Challenges identified by team members

if (($stakeavgd1 === '-') && ($stakeavgd2 === '-') && ($stakeavgd3 === '-') && ($stakeavgd4 === '-') && ($stakeavgd5 === '-') && ($stakeavgd6 === '-') && ($stakeavgd7 === '-') && ($stakeavgd8 === '-') && ($stakeavgd9 === '-') && ($stakeavgd10 === '-') && ($stakeavgd11 === '-') && ($stakeavgd12 === '-') && ($stakeavgd13 === '-') && ($stakeavgd14 === '-') && ($stakeavgd15 === '-') && ($stakeavgd16 === '-') && ($stakeavgd17 === '-') && ($stakeavgd18 === '-') && ($stakeavgd19 === '-') && ($stakeavgd20 === '-') && ($stakeavgd21 === '-') && ($stakeavgd22 === '-') && ($stakeavgd23 === '-') && ($stakeavgd24 === '-') && ($stakeavgd25 === '-') && ($stakeavgd26 === '-')) {
    $pdf->Write(3, '       No stakeholders contibuted to this section of the review ' . "\n" . "\n");
} else {
    $stakeff = array_diff($stakeff, array('-'));
    asort($stakeff);
    $bottom4 = array_slice($stakeff, 0, 4);
	arsort($bottom4);
    foreach ($bottom4 as $key => $value) {
        $pdf->Write(3, '     ' . $value . ' ' . $key . "\n" . "\n");
    }
}


$pdf->SetXY(20,240);
$pdf->SetFont('Helvetica', '', 8);
$pdf->Write(5, 'No. of team respondents: ' . $numteamrespondents . "\n");
$pdf->Write(5, 'No. of stakeholder respondents: ' . $numstakerespondents . "\n" . "\n");


?>