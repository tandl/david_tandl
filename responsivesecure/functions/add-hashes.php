<?php
error_reporting(E_ALL);
require_once ('../includes/configuration-responsivesecure.php');

function addIndHashes() {
	require_once (MYSQLI);	
	
	$sql = "SELECT indsurveyid, url_hash FROM indsurveys WHERE url_hash = ''";

	foreach ($dbc->query($sql) as $row) :
    
    if($row['url_hash'] == ''):
    
    	$new_url_hash  = bin2hex(mcrypt_create_iv(22, MCRYPT_DEV_URANDOM));
    	$update_sql = "UPDATE indsurveys SET url_hash= '" . $new_url_hash . "' WHERE indsurveyid = '" . $row['indsurveyid'] . "'";
	    $dbc->query($update_sql);
    	
    endif; 
  endforeach;

  $stmt->free_result();
  $stmt->close();
}

function addTeamHashes() {
	require_once (MYSQLI);	
	
	$sql = "SELECT teamsurveyid, url_hash FROM teamsurveys WHERE url_hash = ''";
	$row = '';

	foreach ($dbc->query($sql) as $row) :
    
    if($row['url_hash'] == ''):
    
    	$new_url_hash  = bin2hex(mcrypt_create_iv(22, MCRYPT_DEV_URANDOM));
    	$update_sql = "UPDATE teamsurveys SET url_hash= '" . $new_url_hash . "' WHERE teamsurveyid = '" . $row['teamsurveyid'] . "'";
			//print $update_sql . '<br />';
	    $dbc->query($update_sql);
    	
    endif; 
  endforeach;
  
  $stmt->free_result();
  $stmt->close();
}

addIndHashes();
addTeamHashes();
