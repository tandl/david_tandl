
<?php

//Producing the headlines report bar charts for Your Leadership Quartet Team Review
//Specifying the labels and values used in the reports
//Each line on the report shows data for three variables - the team score, database average, and stakeholder score (if requested in report set up)
//Items are the individual questions from the review
//Faces are the averages of the four main groupings - Visionary, Guardian, Psychologist, Director
//Each face has two aspects (see website), so eight aspects in all. Aspects scores are averages of these.
//Grand average is the average of all scored items on the questionnaire

//NB database averages and stakeholder averages have same variable names as the corresponding variables for teams.
//This is OK (but should be changed at some stage) because the data is stored in different tables
//and the report generation queries the team table for the team report and the in table for the ind report

//Specifying labels for the bar charts in the report

$faces = array("The Visionary", "The Guardian", "The Psychologist", "The Director");

$facesimage = array('images/templatev140.png', 'images/templatev140.png', 'images/templateg140.png', 'images/templateg140.png', 'images/templatep140.png', 'images/templatep140.png', 'images/templated140.png', 'images/templated140.png', 'images/arrow-right-2.png');

$aspects = array("Setting vision & values", "Leading by example", "Working with stakeholders", "Agreeing responsibilities", "Building teamwork", "Empowering individuals", "Managing performance", "Leading change", "Results achieved");

$topics = array('Clarity of team objectives', 'Focus on what customers want', 'Ambitious and measurable team targets', 'Inspiring communication', 'Generating optimism', 'Leading by example', 'Managing stakeholders', 'Securing skills and resources', 'Negotiating skill', 'Effective team meetings', 'Clear roles and responsibilities', 'Effective delegation', 'Full contribution to team discussions', 'Disagreements in the open and resolved', 'Collective achievement put above individual priorities', 'Can-do culture', 'Emotional intelligence', 'Appreciation and support', 'Collective performance management', 'Quality of performance data and reports', 'Team decision-making', 'Creativity and innovation', 'Systematic performance improvement based on data', 'Leading and delivering change', 'Meeting targets', 'Personal and professional fulfilment');

//Specifying individual item values for the bar charts in the report
$teamavefm = array($teamavgd1, $teamavgd2, $teamavgd3, $teamavgd4, $teamavgd5, $teamavgd6, $teamavgd7, $teamavgd8, $teamavgd9, $teamavgd10, $teamavgd11, $teamavgd12, $teamavgd13, $teamavgd14, $teamavgd15, $teamavgd16, $teamavgd17, $teamavgd18, $teamavgd19, $teamavgd20, $teamavgd21, $teamavgd22, $teamavgd23, $teamavgd24, $teamavgd25, $teamavgd26);

$dbavefm = array($dbavgd1, $dbavgd2, $dbavgd3, $dbavgd4, $dbavgd5, $dbavgd6, $dbavgd7, $dbavgd8, $dbavgd9, $dbavgd10, $dbavgd11, $dbavgd12, $dbavgd13, $dbavgd14, $dbavgd15, $dbavgd16, $dbavgd17, $dbavgd18, $dbavgd19, $dbavgd20, $dbavgd21, $dbavgd22, $dbavgd23, $dbavgd24, $dbavgd25, $dbavgd26);

$stakeavefm = array($stakeavgd1, $stakeavgd2, $stakeavgd3, $stakeavgd4, $stakeavgd5, $stakeavgd6, $stakeavgd7, $stakeavgd8, $stakeavgd9, $stakeavgd10, $stakeavgd11, $stakeavgd12, $stakeavgd13, $stakeavgd14, $stakeavgd15, $stakeavgd16, $stakeavgd17, $stakeavgd18, $stakeavgd19, $stakeavgd20, $stakeavgd21, $stakeavgd22, $stakeavgd23, $stakeavgd24, $stakeavgd25, $stakeavgd26);

$tvotedev = array($tvotedev1, $tvotedev2, $tvotedev3, $tvotedev4, $tvotedev5, $tvotedev6, $tvotedev7, $tvotedev8, $tvotedev9, $tvotedev10, $tvotedev11, $tvotedev12, $tvotedev13, $tvotedev14, $tvotedev15, $tvotedev16, $tvotedev17, $tvotedev18, $tvotedev19, $tvotedev20, $tvotedev21, $tvotedev22, $tvotedev23, $tvotedev24, $tvotedev25, $tvotedev26);

//Specifying faces values for the bar charts in the report
$facesteamscores = array($teamvisionaryavefm, $teamguardianavefm, $teampsychologistavefm, $teamdirectoravefm);
$facesdbscores = array($dbvisionaryavefm, $dbguardianavefm, $dbpsychologistavefm, $dbdirectoravefm);
$facesstakescores = array($stakevisionaryavefm, $stakeguardianavefm, $stakepsychologistavefm, $stakedirectoravefm);

//Specifying aspects values for the bar charts in the report
$aspectsteamscores = array($teamfocusavefm, $teamcommunicateavefm, $teamsecureavefm, $teamorganizeavefm, $teamuniteavefm, $teamempoweravefm, $teamdeliveravefm, $teamimproveavefm);
$aspectsdbscores = array($dbfocusavefm, $dbcommunicateavefm, $dbsecureavefm, $dborganizeavefm, $dbuniteavefm, $dbempoweravefm, $dbdeliveravefm, $dbimproveavefm);
$aspectsstakescores = array($stakefocusavefm, $stakecommunicateavefm, $stakesecureavefm, $stakeorganizeavefm, $stakeuniteavefm, $stakeempoweravefm, $stakedeliveravefm, $stakeimproveavefm);

// Put database top and bottom 10 percentile figs for items, calculated periodically using SPSS in arrays for benchmarking
$itemteamtoptenpc = array(85, 84, 81, 76, 80, 89, 92, 86, 79, 82, 81, 80, 83, 79, 92, 93, 87, 94, 77, 79, 82, 87, 81, 84, 84, 92);
$itemteambottenpc = array(29, 37, 26, 24, 29, 36, 42, 26, 29, 30, 27, 27, 25, 26, 35, 35, 30, 40, 23, 20, 30, 32, 23, 31, 41, 31);
// Put database top and bottom 10 percentile figs for aspects, calculated periodically using SPSS in arrays for benchmarking
$aspectsteamtoptenpc = array(79, 77, 79, 74, 78, 84, 71, 76);
$aspectsteambottenpc = array(39, 34, 42, 34, 35, 42, 35, 35);
// Put database top and bottom 10 percentile figs for faces, calculated periodically using SPSS in arrays for benchmarking
$facesteamtoptenpc = array(76, 73, 77, 72);
$facesteambottenpc = array(38, 41, 40, 37);
// Put database top and bottom 10 percentile figs for grand average, calculated periodically using SPSS in arrays for benchmarking
$teamgrandtoptenpc = 72;
$teamgrandbottenpc = 41;

$x = 20; //x  axis start point for chart as a whole
$y = 46; //y  axis start point for chart as a whole
$xbar = 143; //x  axis start point for bars
$ybar = 53.5; //y  axis start point for bars
$lw = 1.1; // width of graph bar line
$n = 1;
$gridline = 0.1;

$pdf->SetlineWidth($gridline);
$pdf->SetDrawColor(255, 191, 0);
$pdf->Line($xbar - 0.5, 68.5, $xbar - 0.5, 210);
$pdf->Line($xbar + 12.5, 68.5, $xbar + 12.5, 210);
$pdf->Line($xbar + 25, 68.5, $xbar + 25, 210);
$pdf->Line($xbar + 37.5, 68.5, $xbar + 37.5, 210);
$pdf->Line($xbar + 50, 68.5, $xbar + 50, 210);

$pdf->Image("images/face-sad.png", $xbar - 4.3, $y + 12, 4, 4);
$pdf->Image("images/face-sad.png", $xbar - 0.3, $y + 12, 4, 4);
$pdf->Image("images/face-sad.png", $xbar + 10.5, $y + 12, 4, 4);
$pdf->SetXY($xbar + 22, $y + 12);
$pdf->SetFont('Helvetica', 'B', 7);
$pdf->Write(4, 'OK');
$pdf->Image("images/face-smile.png", $xbar + 35.5, $y + 12, 4, 4);
$pdf->Image("images/face-smile.png", $xbar + 45.5, $y + 12, 4, 4);
$pdf->Image("images/face-smile.png", $xbar + 49.5, $y + 12, 4, 4);

$pdf->SetXY($xbar - 2.1, $y + 18);
$pdf->SetRightMargin(0);
$pdf->SetFont('Helvetica', '', 7);
$pdf->Write(4, '0');
$pdf->SetXY($xbar + 10, $y + 18);
$pdf->Write(4, '25');
$pdf->SetXY($xbar + 22.5, $y + 18);
$pdf->Write(4, '50');
$pdf->SetXY($xbar + 35, $y + 18);
$pdf->Write(4, '75');
$pdf->SetXY($xbar + 46.5, $y + 18);
$pdf->Write(4, '100');
$pdf->SetRightMargin(25);

$pdf->SetFont('Helvetica', '', 7);
$pdf->SetXY($x + 78, $y + 18);
//$pdf->Write(4, 'Team   Database   Stakeholders  Develop?'); this is not used as the extra "Develop?" column made the table too confusing - see line 108-ish below
$pdf->Write(4, 'Team');
$pdf->SetXY($x + 87, $y + 18);
$pdf->Write(4, 'Database');
$pdf->SetXY($x + 99, $y + 18);
$pdf->Write(4, 'Stakeholders');

$pdf->SetlineWidth($lw - 0.3);
$pdf->SetDrawColor(13, 100, 183);

$pdf->Line($x + 79.8, $y + 22, $x + 85, $y + 22);
$pdf->SetDrawColor(217, 217, 217);
$pdf->Line($x + 88.5, $y + 22, $x + 98, $y + 22);
$pdf->SetDrawColor(239, 0, 209);
$pdf->Line($x + 100.8, $y + 22, $x + 114, $y + 22);

$pdf->SetXY($x, $y + 32);
$pdf->SetFont('Helvetica', '', 10);
$pdf->Write(5, 'Overall score:');
$pdf->SetFont('Helvetica', '', 9);
$pdf->SetXY($x, $y + 42);
$pdf->Write(5, 'Your Leadership Quartet Summary score');

// Grand total bar

$yy = $y + 42;
$yybar = $y + 43;

//Drawing elipses round high and low grand ave scores
if ((is_numeric($teamgrandavefm)) && ($teamgrandavefm >= $teamgrandtoptenpc)) {
    $pdf->Image("images/ellipsegood.png", $x + 76.5, $yy - 1, 12, 6);
}
elseif ((is_numeric($teamgrandavefm)) && ($teamgrandavefm <= $teamgrandbottenpc)) {
    $pdf->Image("images/ellipsebad.png", $x + 76.5, $yy - 1, 12, 6);
}

if ((is_numeric($stakegrandavefm)) && ($stakegrandavefm >= $teamgrandtoptenpc)) {
    $pdf->Image("images/ellipsegood.png", $x + 101, $yy - 1, 12, 6);
}
elseif ((is_numeric($stakegrandavefm)) && ($stakegrandavefm <= $teamgrandbottenpc)) {
    $pdf->Image("images/ellipsebad.png", $x + 101, $yy - 1, 12, 6);
}


$pdf->SetXY(($x + 80), $yy);
$pdf->SetFont('Helvetica', 'B', 8.5);
$pdf->Write(4, $teamgrandavefm);
$pdf->SetXY(($x + 91), $yy);
$pdf->Write(4, $dbgrandavefm);
$pdf->SetXY(($x + 104), $yy);
$pdf->Write(4, $stakegrandavefm);

$pdf->SetlineWidth($lw);
$pdf->SetDrawColor(13, 100, 183);
$pdf->Line($xbar, $yybar, $xbar + (($teamgrandavefm) / 2), $yybar);
$pdf->SetDrawColor(217, 217, 217);
$pdf->Line($xbar, $yybar + ($lw + 0.3), $xbar + ($dbgrandavefm / 2), $yybar + ($lw + 0.3));
$pdf->SetDrawColor(239, 0, 209);
$pdf->Line($xbar, ($yybar + (2 * ($lw + 0.3))), $xbar + (($stakegrandavefm) / 2), ($yybar + (2 * ($lw + 0.3))));


$pdf->SetXY(20, $y + 60);
$pdf->SetFont('Helvetica', '', 10);
$pdf->Write(5, 'Your team\'s Leadership Quartet scores:');


for ($i = 0; $i <= 3; $i++) {
    $n+=1;
    $image = (2 * $i);
    $yy = ($y + 70 + ($i * 25)); //reset height of scores down page
    $yybar = ($y + 70 + ($i * 25)); //reset height of bars down page

    $pdf->SetFont('Helvetica', '', 9);
    $pdf->Image("$facesimage[$image]", $x, $yy, 4, 4);
    $pdf->SetXY($x + 6, $yy);
    $pdf->Write(5, $faces[$i]);

    //Drawing elipses round high and low faces scores
    if ((is_numeric($facesteamscores[$i])) && ($facesteamscores[$i] >= $facesteamtoptenpc[$i])) {
        $pdf->Image("images/ellipsegood.png", $x + 76.5, $yy - 1, 12, 6);
    }
    elseif ((is_numeric($facesteamscores[$i])) && ($facesteamscores[$i] <= $facesteambottenpc[$i])) {
        $pdf->Image("images/ellipsebad.png", $x + 76.5, $yy - 1, 12, 6);
    }

    if ((is_numeric($facesstakescores[$i])) && ($facesstakescores[$i] >= $facesteamtoptenpc[$i])) {
        $pdf->Image("images/ellipsegood.png", $x + 101, $yy - 1, 12, 6);
    }
    elseif ((is_numeric($facesstakescores[$i])) && ($facesstakescores[$i] <= $facesteambottenpc[$i])) {
        $pdf->Image("images/ellipsebad.png", $x + 101, $yy - 1, 12, 6);
    }


    $pdf->SetXY(($x + 80), $yy);
    $pdf->SetFont('Helvetica', '', 8.5);
    $pdf->Write(4, $facesteamscores[$i]);
    $pdf->SetXY(($x + 91), $yy);
    $pdf->Write(4, $facesdbscores[$i]);
    $pdf->SetXY(($x + 104), $yy);
    $pdf->Write(4, $facesstakescores[$i]);

    $pdf->SetlineWidth($lw);
    $pdf->SetDrawColor(13, 100, 183);
    $pdf->Line($xbar, $yybar, $xbar + (($facesteamscores[$i]) / 2), $yybar);
    $pdf->SetDrawColor(217, 217, 217);
    $pdf->Line($xbar, $yybar + ($lw + 0.3), $xbar + ($facesdbscores[$i] / 2), $yybar + ($lw + 0.3));
    $pdf->SetDrawColor(239, 0, 209);
    $pdf->Line($xbar, ($yybar + (2 * ($lw + 0.3))), $xbar + (($facesstakescores[$i]) / 2), ($yybar + (2 * ($lw + 0.3))));


    for ($j = 0; $j <= 1; $j++) {
        $k = (($i * 2) + ($j)); //pick up next aspect headline
        $yy = ($yy + 7); //reset height down page
        $yybar = ($yy + 1); //reset height down page

        $pdf->SetXY($x + 10, $yy);
        $pdf->SetFont('Helvetica', '', 8.5);
        $pdf->Write(5, $aspects[$k]);

        /*   if ($teamavefm[$k] === '-') {    //formatting '-' to line up with numbers. Don't change this as value is used later, but should have used SetX
          ($teamavefm[$k] = '  - ');
          }

          if ($stakeavefm[$k] === '-') {    //formatting '-' to line up with numbers. Don't change this as value is used later, but should have used SetX
          ($stakeavefm[$k] = '  - ');
          } */

        //Drawing elipses round high and low aspects scores
        if ((is_numeric($aspectsteamscores[$k])) && ($aspectsteamscores[$k] >= $aspectsteamtoptenpc[$k])) {
            $pdf->Image("images/ellipsegood.png", $x + 76.5, $yy - 1, 12, 6);
        }
        elseif ((is_numeric($aspectsteamscores[$k])) && ($aspectsteamscores[$k] <= $aspectsteambottenpc[$k])) {
            $pdf->Image("images/ellipsebad.png", $x + 76.5, $yy - 1, 12, 6);
        }

        if ((is_numeric($aspectsstakescores[$k])) && ($aspectsstakescores[$k] >= $aspectsteamtoptenpc[$k])) {
            $pdf->Image("images/ellipsegood.png", $x + 101, $yy - 1, 12, 6);
        }
        elseif ((is_numeric($aspectsstakescores[$k])) && ($aspectsstakescores[$k] <= $aspectsteambottenpc[$k])) {
            $pdf->Image("images/ellipsebad.png", $x + 101, $yy - 1, 12, 6);
        }

        $pdf->SetXY(($x + 80), $yy);
        $pdf->SetFont('Helvetica', '', 8.5);
        $pdf->Write(4, $aspectsteamscores[$k]);
        $pdf->SetXY(($x + 91), $yy);
        $pdf->Write(4, $aspectsdbscores[$k]);
        $pdf->SetXY(($x + 104), $yy);
        $pdf->Write(4, $aspectsstakescores[$k]);

        $pdf->SetlineWidth($lw);
        $pdf->SetDrawColor(13, 100, 183);
        $pdf->Line($xbar, $yybar, $xbar + (($aspectsteamscores[$k]) / 2), $yybar);
        $pdf->SetDrawColor(217, 217, 217);
        $pdf->Line($xbar, $yybar + ($lw + 0.3), $xbar + ($aspectsdbscores[$k] / 2), $yybar + ($lw + 0.3));
        $pdf->SetDrawColor(239, 0, 209);
        $pdf->Line($xbar, ($yybar + (2 * ($lw + 0.3))), $xbar + (($aspectsstakescores[$k]) / 2), ($yybar + (2 * ($lw + 0.3))));
    }
}


$pdf->SetXY(($x), $yy + 20);
$pdf->SetFont('Helvetica', '', 11);
$pdf->Write(6, 'Legend for tables on this and following pages:');
$pdf->Image("images/ellipsegood.png", $x, $yy + 29, 12, 6);
$pdf->SetXY(($x + 12), $yy + 30);
$pdf->SetFont('Helvetica', '', 8);
$pdf->Write(4, 'This score is in the top 10% of team scores for this item in the database');

$pdf->Image("images/ellipsebad.png", $x, $yy + 37, 12, 6);
$pdf->SetXY(($x + 12), $yy + 38);
$pdf->SetFont('Helvetica', '', 8);
$pdf->Write(4, 'This score is in the bottom 10% of team scores for this item in the database');


$pdf->SetXY(($x), $yy + 50);
$pdf->SetFont('Helvetica', '', 8);
$pdf->Write(5, 'No. of team respondents: ' . $numteamrespondents . "\n" . "\n");
$pdf->SetXY(($x), $yy + 55);
$pdf->SetFont('Helvetica', '', 8);
$pdf->Write(5, 'No. of stakeholder respondents: ' . $numstakerespondents . "\n" . "\n");

$pdf->SetDrawColor(35, 134, 243);
?>


