<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<html>
    <head>
        
        <title><?php echo $page_title; ?></title>
        <link href="../sites/all/themes/tandl_zen/css/styles.css" rel="stylesheet" type="text/css"/>
        <?php /*
        <!--[if gte IE 9]>
          <link rel="stylesheet" href="../sites/all/themes/tandl_zen/css/ie.css">
        <![endif]-->
        */ ?>
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
        	@import url("../sites/all/libraries/fontawesome/css/font-awesome.css");
        </style>
        
        
    </head>
    <body class="rs <?php print $body_classes;?>">
			<div class="above-header">
				<div class="limiter clearfix">

				</div>
			</div>

			<header class="header" role="banner">

				<div class="limiter">
						<a href="/" title="Home" rel="home" class="header__logo"><img src="<?php print $logo_path; ?>" alt="Home" class="header__logo-image" /></a>
				</div>
	
			</header>
			<main>
    
