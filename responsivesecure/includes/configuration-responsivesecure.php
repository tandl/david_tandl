<?php
ini_set('session.cookie_domain', substr($_SERVER['SERVER_NAME'], strpos($_SERVER['SERVER_NAME'], "."), 100));
//Location of baseline configuration variables depending on local or remote host:

$host = substr($_SERVER['HTTP_HOST'], 0, 5);

//localhost
if (in_array($host, array('local', '127.0', '192.1'))) {

    define('SERVER_PATH', 'R:/htdocs/dev/tandl/');
    define('CONFIGVARS', SERVER_PATH . 'responsivesecure/includes/config/configvars-responsivesecure.php');
    define('DRUPAL_ROOT', 'R:/htdocs/dev/tandl');    

//dev
} elseif (in_array($host, array('tandl'))) { 

    define('SERVER_PATH', '/var/www/html/www.teamsandleadership.net/public/');
    define('CONFIGVARS', SERVER_PATH . 'responsivesecure/includes/config/configvars-responsivesecure.php');
    define('DRUPAL_ROOT', SERVER_PATH);    

//production   
} else {

		//live server	
    define('SERVER_PATH', '/var/www/private.teamsandleadership.net/');	
    define('CONFIGVARS', SERVER_PATH . 'configvars-responsivesecure.php');
    define('DRUPAL_ROOT', '/var/www/teamsandleadership.net');
    
    
		//here for testing live subdomains on dev only
    //define('SERVER_PATH', '/var/www/html/www.teamsandleadership.net/public/');
    //define('CONFIGVARS', SERVER_PATH . 'responsivesecure/includes/config/configvars-responsivesecure.php');
    //define('DRUPAL_ROOT', SERVER_PATH);     
		
		//re-direct to https:
    if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "") {
        $redirect = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header("Location: $redirect");
    }    	
}

require_once(CONFIGVARS);

define('MYSQLI', SERVER_PATH . 'responsivesecure/includes/db/mysqli_connect_tal_reviews.php');

require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_SESSION);
drupal_session_start();


//## load domain specific settings from Drupal ##
module_load_all();
module_load_include('inc', null, 'includes/file');

global $_domain;
global $base_url;

//retain the original base_url
$orig_base_url = $base_url;

//lookup current theme settings to get the logo path
$theme = domain_theme_lookup($_domain['domain_id']);
$theme_settings = unserialize($theme['settings']);

//need to set the base_url to the DRUPAL_ROOT
$protocol = ($_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
$base_url = $protocol . $_domain['subdomain'];
$logo_path = file_create_url($theme_settings['logo_path']);

//add domain access machine name to body class so domain specific styles kick in
$machine_name = domain_load_machine_name($_domain['domain_id']);
$body_classes = 'domain-' . str_replace('_','-',$machine_name);

//reset the base url to the responsive secure directory
$base_url = $orig_base_url;

?>
