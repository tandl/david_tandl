<?php
require_once (MYSQLI);

$hash = $_GET['a'];
$id 	= filter_input(INPUT_GET, 'b', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
$type = filter_var($_GET['c'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);

if(is_int($id) && is_string($type) && strlen($type)==1):

	switch($type):
		case 't':

			$stmt = $dbc->prepare("SELECT teamsurveyname FROM teamsurveys WHERE url_hash=? AND teamsurveyid=?");
			$stmt->bind_param('si', $hash, $id);
			$stmt->execute();
			$stmt->bind_result($res);
			$stmt->store_result();
			while ($stmt->fetch()) {
				$tsn = $res;
			}
	
		break;
	
		case 'i':
			$stmt = $dbc->prepare("SELECT indsurveyname FROM indsurveys WHERE url_hash=? AND indsurveyid=?");
			$stmt->bind_param('si', $hash, $id);
			$stmt->execute();
			$stmt->bind_result($res);
			$stmt->store_result();
			while ($stmt->fetch()) {
				$isn = $res;
			}
		break;
	
		default:
			header('Location: /');
			//print 'no switch';	
		break;
		
	endswitch;
	
	if(!$stmt->num_rows > 0):
		header('Location: /');	
		//print 'not found';
	else:
		//set the survey id
		$sid = $id;
	endif;

	$stmt->close();		
	
else:
	header('Location: /');
	//print 'no numeric';
endif;
