<?php

/* This script:
 * - define constants and settings depending on local or remote-host deployment
 * - dictates how errors are handled
 * - defines useful functions
 */

// Created DJM 28.10.2012
// Modified for Drupal 15.07.2016

// ************ SETTINGS ************ //

//Universal settings:

ini_set('error_reporting', E_ALL | E_STRICT);

// Adjust the time zone for PHP 5.1 and greater:
date_default_timezone_set('Europe/London');

//define('SALT', 'tandl');

//Teams and leadership contact details
	define('WEBSITE_TECH', 'david.mathew@teams-and-leadership.com');
	define('WEBSITE_LEGAL', 'david.mathew@change-fx.com');

// Client strapline and image details:	
	define ('CLIENT_NAME', "Public Health England");
	define ('STRAPLINE', "Protecting and improving the nation's health");
	define ('TEAMDEV_COPYRIGHT', 'xxxxxx');		
	define ('LEADDEV_COPYRIGHT', 'xxxxxx');		

//Location of baseline configuration variables depending on local or remote host:

$host = substr($_SERVER['HTTP_HOST'], 0, 5);
if (in_array($host, array('local', '127.0', '192.1'))) {
    $local = TRUE;
} else {
    $local = FALSE;
}



//Universal constants
// Location of the MySQL connection script:    
//define('MYSQLI', '/var/www/vhosts/future-fx.com/private/mysqli_connect_tal_reviews.php');
//define('MYSQLI', '/var/www/private.teamsandleadership.net/mysqli_connect_tal_reviews.php');

// Client-independent URLs (base for all redirections):
//define('BASE_URL',     'https://future-fx.com/');
define('BASE_URL',     'https://teamsandleadership.net/');
//define('REVIEWS_URL',  'https://future-fx.com/responsivesecure/');	
define('REVIEWS_URL',  'https://teamsandleadership.net/responsivesecure/');	
//define('SECURITY_URL', 'https://future-fx.com/responsivesecure/');
define('SECURITY_URL', 'https://teamsandleadership.net/responsivesecure/');
	
//Client constants:	
//Client-dependent URLs		
//define('SITE_URL', 'https://future-fx.com/');	
define('SITE_URL', 'https://teams-and-leadership.net/');
// Client contact addresses:
// Client admin contact addresses:
define('WEBSITE_ADMIN', 'david.mathew@teams-and-leadership.net');
define ('ODHR_CONTACT', 'phe@change-fx.com');



// ************ ERROR MANAGEMENT ************ //
// Create the error handler:
function my_error_handler($e_number, $e_message, $e_file, $e_line, $e_vars) {

    global $local;

    // Build the error message.
    $message = "<p>An error occurred in script '$e_file' on line $e_line: $e_message\n<br />";

    // Add the date and time:
    $message .= "Date/Time: " . date('n-j-Y H:i:s') . "\n<br />";

    // Append $e_vars to the $message:
    $message .= "<pre>" . print_r($e_vars, 1) . "</pre>\n</p>";

    if ($local == TRUE) { // We're on a development server (print the error).
        echo '<div class="error">' . $message . '</div><br />';
         if (($e_number != E_NOTICE) && ($e_number < 2048)) {
            echo '<p>Message to client on live site would be: <br />A system error has occurred. We apologize for the inconvenience.</p>';
            echo '<p>$local is ' . $local . ' and server = ' . $_SERVER['HTTP_HOST'] . '</p>';
        }
    } else { // Don't show the error:
        // Send an email to the admin:
        mail(EMAIL, 'Site Error!', $message, 'From: david.mathew@teams-and-leadership.com');

        // Only print an error message if the error isn't a notice:
        if (($e_number != E_NOTICE) && ($e_number < 2048)) {
            echo '<p> Live site: A system error occurred. We apologize for the inconvenience.</p>';
        } 
    } // End of $local IF.
}

// End of my_error_handler() definition.
// Use my error handler.
set_error_handler('my_error_handler');

?>
