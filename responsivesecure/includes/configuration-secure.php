<?php

ini_set('session.cookie_domain', substr($_SERVER['SERVER_NAME'], strpos($_SERVER['SERVER_NAME'], "."), 100));
//Location of baseline configuration variables depending on local or remote host:

$host = substr($_SERVER['HTTP_HOST'], 0, 5);
if (in_array($host, array('local', '127.0', '192.1'))) {
    $local = TRUE;
} else {
    $local = FALSE;
}

if ($local) {
    define('CONFIGVARS', 'R:/Secure/private/configvars-secure.php');
} else {
    define('CONFIGVARS', '/var/www/vhosts/teams-and-leadership.net/private/configvars-secure.php');
//     re-direct to https:  
    if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "") {
        $redirect = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header("Location: $redirect");
    }
}

require_once(CONFIGVARS);
?>
