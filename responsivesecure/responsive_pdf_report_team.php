<?php

// Include the configuration file ffx:
require_once ('includes/configuration-responsivesecure.php');
//include_once("includes/analyticstracking.php");
//require('includes/db_sessions.inc.php');
require ('fpdf/fpdf.php');

$memberid = $user->uid;
if ($memberid <= 0) {
  echo "Access denied: not logged in";
  exit();
}

$reportrequested = $_GET["teamsurveyid"];

require_once (MYSQLI);
if (!$dbc) {
  die('Could not connect you up to the database: ');
}

/*  Testing pick up of registering person using Andrew Cooper's reports
 *   Comment out block below if we are using this test
  $memberid = 175;
  $reportrequested = 590412;
  $reportrequested = 283716;
  $result1 = $dbc->prepare("SELECT teamsurveyname,surveytype FROM teamsurveys WHERE teamsurveyid=? AND memberid=?");
  $result1->bind_param("ii", $reportrequested, $memberid);
 *
 */

// Beginning of block to be commented out if we are testing pick up of registered person
// if we are admin, allow viewing any report
if ($memberid == 1) {
  $result1 = $dbc->prepare("SELECT teamsurveyname,surveytype FROM teamsurveys WHERE teamsurveyid=?");
  $result1->bind_param("i", $reportrequested);
}
else {
  // limit report to match current member id
  $result1 = $dbc->prepare("SELECT teamsurveyname,surveytype FROM teamsurveys WHERE teamsurveyid=? AND memberid=?");
  $result1->bind_param("ii", $reportrequested, $memberid);
}
// End of block to be commented out if we are testing pick up of registered person

$result1->execute();
//number of rows returned from query
$result1->store_result();
$rows = $result1->num_rows;

//populates teamsurveyname and surveytype variables
$result1->bind_result($teamsurveyname, $surveytype);
$result1->fetch();

if ($rows != 1) {
  echo "Access denied: no rows returned for survey and member";
  exit();
}

$_SESSION['reportsurveyname'] = $teamsurveyname;
// Check if 3 people have responded to multi-reviewer questionnaire, or 1 person to single-reviewer questionnaire

$result = mysqli_query($dbc, "SELECT teamsurveyid, COUNT(teamrespno) FROM teamdata WHERE teamsurveyid = '$reportrequested' && (teamrole = '2' || teamrole = '1') ");
$row = mysqli_fetch_array($result, MYSQLI_BOTH);
$numteamrespondents = $row['COUNT(teamrespno)'];

$result2 = mysqli_query($dbc, "SELECT teamsurveyid, COUNT(teamrespno) FROM teamdata WHERE teamsurveyid = '$reportrequested'  && (teamrole = '4' || teamrole = '3') ");
$row2 = mysqli_fetch_array($result2, MYSQLI_BOTH);
$numstakerespondents = $row2['COUNT(teamrespno)'];

$numrespondents = $numteamrespondents + $numstakerespondents;

$_SESSION['numrespondents'] = $numrespondents;

if ((($numrespondents < 3) && ($surveytype != 4)) || (($numrespondents === 0) && ($surveytype === 4))) {
  $url = 'resp-too-few-responses.php';
  header("Location: $url");
}
elseif (($numrespondents > 1) && ($surveytype === 4)) {
  // Define the URL for too many team responses
  // Folder structure is different on local. This for local:
  //
    $url = 'resp-too-many-team-responses.php';
  header("Location: $url");
}
else {


  include ('functions/extract.php');

  //Finding commissioner's  (ie member's) Drupal sub-domain prefix from their email address to enable report links to take them to their branded website rather than the default teamsandleadership.net
  /* create a prepared statement */
  if ($stmt = $dbc->prepare("SELECT memberemail FROM members WHERE memberid=?")) {

    /* bind parameters for markers */
    $stmt->bind_param("i", $memberid);

    /* execute query */
    $stmt->execute();

    /* bind result variables */
    $stmt->bind_result($client_email);

    /* fetch value and extract email domain */
    $stmt->fetch();

   // Initialising CLIENT VARIABLES to default
    $subdomainprefix = 'default';
    $client_logo = 'Jigsaw table.png';
    $client_logo_width = 25;
    $client_logo_height = 25;
    $client_email_domain = 'change-fx.com';

    // Generating client variable for report customisation
    $client_email_domain = substr(strrchr($client_email, "@"), 1);

    // Testing
    //$client_email_domain = 'redcross.org.uk';

    for ($i = 0; $i < count($client_details); $i++) {
      if ($client_details[$i][1] == $client_email_domain) {
        $subdomainprefix = $client_details[$i][2];
        $client_logo = $client_details[$i][3];
        $client_logo_width = ($client_details[$i][4] * 25 / $client_details[$i][5]);
      }
    }

    // Reducing logo size if too big at 25 high, to balance T&L logo
    if ($client_logo_width > 40) {
      $new_client_logo_width = 40;
      $new_client_logo_height = (40 / $client_logo_width) * 25;
      $client_logo_height = $new_client_logo_height;
      $client_logo_width = $new_client_logo_width;
    }

    // Setting customised client link, retruning always to their sub-domain
    if ($subdomainprefix === 'default') {
      $customised_link = 'https://';
    }
    else {
      $customised_link = 'https://' . $subdomainprefix . '.';
    }

//echo '<br/>Client\'s subdomain prefix after the loop is: '.$subdomainprefix;

    /* close statement */
    $stmt->close();
  }
  else {
    echo "ERROR: Could not prepare query:";
  }

//End of setting  commissioner's  (ie member's) Drupal sub-domain prefix

  class PDF extends FPDF {

    function Footer() {
      $this->SetX(-1); // negative arg means relative to right of page
      //  $page_width = $this->GetX() + 1;
      $this->SetY(-1); // negative arg means relative to bottom of page
      $page_height = $this->GetY() + 1;
      $this->SetLineWidth(.1);
      $this->SetDrawColor(0, 0, 0);
      $this->Line(20, $page_height - 12, 190, $page_height - 12);
      $this->SetFont('Helvetica', 'I', 8);
      $this->Text(96, $page_height - 6, 'Page ' .
          $this->PageNo() . ' of {nb}');
      $this->Text(20, $page_height - 6, 'Report generated: ' . date('d.m.Y'));
      $this->Text(127, $page_height - 6, chr(169) . '  2018 david.mathew@teamsandleadership.com');
    }

    function Circle($x, $y, $r, $style = 'D') {
      $this->Ellipse($x, $y, $r, $r, $style);
    }

    function Ellipse($x, $y, $rx, $ry, $style = 'D') {
      if ($style == 'F')
        $op = 'f';
      elseif ($style == 'FD' || $style == 'DF')
        $op = 'B';
      else
        $op = 'S';
      $lx = 4 / 3 * (M_SQRT2 - 1) * $rx;
      $ly = 4 / 3 * (M_SQRT2 - 1) * $ry;
      $k = $this->k;
      $h = $this->h;
      $this->_out(sprintf('%.2F %.2F m %.2F %.2F %.2F %.2F %.2F %.2F c', ($x + $rx) * $k, ($h - $y) * $k, ($x + $rx) * $k, ($h - ($y - $ly)) * $k, ($x + $lx) * $k, ($h - ($y - $ry)) * $k, $x * $k, ($h - ($y - $ry)) * $k));
      $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c', ($x - $lx) * $k, ($h - ($y - $ry)) * $k, ($x - $rx) * $k, ($h - ($y - $ly)) * $k, ($x - $rx) * $k, ($h - $y) * $k));
      $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c', ($x - $rx) * $k, ($h - ($y + $ly)) * $k, ($x - $lx) * $k, ($h - ($y + $ry)) * $k, $x * $k, ($h - ($y + $ry)) * $k));
      $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c %s', ($x + $lx) * $k, ($h - ($y + $ry)) * $k, ($x + $rx) * $k, ($h - ($y + $ly)) * $k, ($x + $rx) * $k, ($h - $y) * $k, $op));
    }

  }

  $pdf = new PDF();
  $pdf->SetTitle("Teams and Leadership Review");
  $pdf->AliasNbPages();
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->SetLeftMargin(20);
  $pdf->SetRightMargin(20);
  $pdf->SetTopMargin(15);

  /* FRONT PAGE ---------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();

  // Front page header
  // Add client logo
  $client_logo_top = 18 - (0.5 * $client_logo_height);
  $pdf->Image("images/v3/" . $client_logo, 20, $client_logo_top, $client_logo_width, $client_logo_height);

  // Add T&L logo
  $pdf->Image("images/v3/T&L Logo - Navy.png", 145, 6, 44, 25);

  // Draw Report Title box and Overall score highlight circle
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(224, 230, 255);
  $pdf->Line(33, 50, 180, 50);
  $pdf->SetFillColor(255, 249, 82);
  $pdf->Circle(174, 49, 10, 'F');

  // Formatting for different length Team Name so as not to upset page formatting
  // Test names
  // $teamsurveyname = '14 characters ';
  // $teamsurveyname = '84 characters 84 characters 84 characters 84 characters 84 characters 84 characters ';

  $teamsurveyname_strlen = strlen($teamsurveyname);
  $pdf->SetRightMargin(50);
  if (strlen($teamsurveyname) > 80) {
    $teamsurveyname = substr($teamsurveyname, 0, 77) . '...';
  }
  if ($teamsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 39);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $teamsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 39);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $teamsurveyname . "\n");
  }
  // End of Formatting for different length Team Name so as not to upset page formatting
  // Write this report details: Overall score and responses to date
  $pdf->SetRightMargin(20);
  $pdf->SetFont('Helvetica', '', 24);
  $pdf->SetXY(165.5, 47);
  $pdf->Write(5, $tandsgrandavefm);
  $pdf->SetFont('Helvetica', '', 20);
  $pdf->Write(5, "%");
  $pdf->SetXY(20, 56);
  $pdf->SetFont('Helvetica', 'I', 9);
  // Next line checks the sub-domain data has got through OK
  //  $pdf->Write(5, 'Client email: ' . $client_email . '  Member ID: ' . $memberid . '  Subdomain prefix: ' . $subdomainprefix . '  Email domain: ' . $client_email_domain . ' Client logo: ' .$client_logo .' ');
  $pdf->Write(5, 'Responses to date: ' . $numteamrespondents . ' team member and ' . $numstakerespondents . ' stakeholder responses' . "\n" . "\n" . "\n");

  //   $pdf->SetXY(160, 50);
  // End of Front page header


  $pdf->SetLineWidth(65);
  $pdf->SetDrawColor(240, 240, 240);
  $pdf->Line(51, 103, 163, 103);
  $pdf->Image("images/v3/analytics-1.png", 24, 88, 30, 30);
  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, "\n" . 'Executive summary' . "\n" . "\n");
  $pdf->SetLeftMargin(60);

  include ('functions/pdf_execsummary_team.php');

  $pdf->SetLeftMargin(20);
  $pdf->SetRightMargin(20);
  $pdf->SetFont('Helvetica', '', 16);
  $pdf->SetXY(38, 133);
  $pdf->Write(5, "\n" . "\n" . 'What\'s in this report' . "\n" . "\n");
  $pdf->Image("images/v3/Folders.png", 24, 150, 30, 30);
  $pdf->SetLeftMargin(60);
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, '* A detailed team analytic, benchmarking your team against 2000+ team reviews.' . "\n");
  $pdf->Write(5, '* Your signature strengths and challenges as a team.' . "\n");
  $pdf->Write(5, '* A list of development needs pinpointed by team members and stakeholders.' . "\n");
  $pdf->Write(5, '* Links to our recommended exercises based on your report.' . "\n");
  $pdf->Write(5, '* Comments from people who completed the questionnaire.' . "\n" . "\n");

  $pdf->SetLineWidth(70);
  $pdf->SetDrawColor(240, 240, 240);
  $pdf->Line(55, 240, 68, 240);
  $pdf->SetLineWidth(70);
  $pdf->SetDrawColor(240, 240, 240);
  $pdf->Line(142, 240, 155, 240);
  $pdf->SetLeftMargin(20);
  $pdf->SetRightMargin(20);
  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, "\n" . "\n" . 'Resources to use alongside this report' . "\n" . "\n");
  $pdf->SetFont('Helvetica', '', 11);

 $pdf->Image("images/v3/Asset 2 copy 2 (1).png", 52, 212, 20, 20);
  $pdf->SetXY(37, 236);

  $pdf->SetFont('Helvetica', 'B', 14);
  $pdf->Write(5, "Team Leader's Guide" . "\n" . "\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->SetLeftMargin(33);
  $pdf->SetRightMargin(118);
  $Title = 'A practical guide with all you need to feel confident about running a \'review & improve\' session with your team.';
  $pdf->Write(5, $Title . "\n"."\n");
  $pdf->SetX(46);
  $pdf->SetFont('', 'U');
  $link = $pdf->AddLink();
  $pdf->Write(5, 'Team Leader\'s Guide', $customised_link . 'teamsandleadership.net/system/files/related-files/Team Leader\'s Guide v3.pdf');
  //$pdf->SetXY(48, 242);
//  $pdf->Cell(0,10,$Title,0,0,'C');

  $pdf->Image("images/v3/Expert Guidance.png", 137, 212, 20, 20);
  $pdf->SetXY(127, 236);
  $pdf->SetLeftMargin(20);
  $pdf->SetRightMargin(20);
  $pdf->SetFont('Helvetica', 'B', 14);
  $pdf->Write(5, "Expert Guidance" . "\n" . "\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->SetLeftMargin(120);
  $pdf->SetRightMargin(33);
  $Title = 'Comprehensive briefings on the skills you need to transform your leadership skills and your team\'s performance.';
  $pdf->Write(5, $Title . "\n" . "\n");
  $pdf->SetX(134);
  $pdf->SetFont('', 'U');
  $link = $pdf->AddLink();

  $pdf->Write(5, 'Expert Guidance', $customised_link . 'teamsandleadership.net/expert-guidance');

  $pdf->SetLeftMargin(20);
  $pdf->SetRightMargin(20);


  /* PAGE 2 ------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();

  // Add client logo
  $client_logo_top = 18 - (0.5 * $client_logo_height);
  $pdf->Image("images/v3/" . $client_logo, 20, $client_logo_top, $client_logo_width, $client_logo_height);

  // Add T&L logo
  $pdf->Image("images/v3/T&L Logo - Navy.png", 145, 6, 44, 25);

  // Page 2 header
  // Draw Report Title box and Overall score highlight circle
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(224, 230, 255);
  $pdf->Line(33, 50, 180, 50);
  $pdf->SetFillColor(255, 249, 82);
  $pdf->Circle(174, 49, 10, 'F');

  // Adapting Front page header for this page
  $pdf->SetRightMargin(50);
  if ($teamsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 39);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $teamsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 39);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $teamsurveyname . "\n");
  }
  // End of Formatting for different length Team Name so as not to upset page formatting
  // Write this report details: Overall score and responses to date
  $pdf->SetRightMargin(20);
  $pdf->SetFont('Helvetica', '', 24);
  $pdf->SetXY(165.5, 47);
  $pdf->Write(5, $tandsgrandavefm);
  $pdf->SetFont('Helvetica', '', 20);
  $pdf->Write(5, "%");
  $pdf->SetXY(20, 56);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numteamrespondents . ' team member and ' . $numstakerespondents . ' stakeholder responses' . "\n" . "\n" . "\n");

// End of Page 2 header


  $pdf->SetLeftMargin(20);
  $pdf->SetRightMargin(25);
  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, "\n" . 'How to use this report' . "\n" . "\n" . "\n");
  $pdf->Image("images/v3/Step 1.png", 24, 89, 20, 20);
  $pdf->SetLeftMargin(55);
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Step 1: Review the report before meeting with your team. '."\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, 'What themes have been brought out? What do the team indicate that they would like to work on? Think about where you\'d like to get to by the end of the meeting.' . "\n" . "\n" . "\n" . "\n");
  $pdf->Image("images/v3/Step 2.png", 24, 119, 20, 20);
  $pdf->SetLeftMargin(55);
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Step 2: Click the links in the report to the recommended exercises,'."\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, 'or browse the ');
  $pdf->SetFont('', 'U');
  $link = $pdf->AddLink();
  $pdf->Write(5, 'Expert Guidance', $customised_link . 'teamsandleadership.net/expert-guidance');
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, ' section to find out more about your suggested areas of development.' . "\n" . "\n" . "\n" . "\n");
  $pdf->Image("images/v3/Step 3.png", 24, 149, 20, 20);
  $pdf->SetLeftMargin(55);
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Step 3: Meet as a team, and talk together about your report.'."\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, 'Review and agree together what you would like to work on. ' . "\n" . "\n" . "\n" . "\n"."\n");
  $pdf->Image("images/v3/Step 4.png", 24, 179, 20, 20);
  $pdf->SetLeftMargin(55);
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Step 4: Encourage dialogue.'."\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, 'Use the ');
  $pdf->SetFont('', 'U');
  $link = $pdf->AddLink();
  $pdf->Write(5, 'Team Leader\'s Guide', $customised_link . 'teamsandleadership.net/system/files/related-files/Team Leader\'s Guide v3.pdf');
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, ' for support to help you have a great discussion together. Your organisation may have access to trained facilitators too.' . "\n" . "\n" . "\n" . "\n");
  $pdf->Image("images/v3/Step 5.png", 24, 209, 20, 20);
  $pdf->SetLeftMargin(55);
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Step 5: Work on the recommended exercise'."\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, 'or see where the session takes you. Listen carefully to each other. Remember to keep an open and constructive tone to your discussions. ' . "\n" . "\n" . "\n"."\n");
  $pdf->Image("images/v3/Step 6.png", 24, 239, 20, 20);
  $pdf->SetLeftMargin(55);
  $pdf->SetFont('Helvetica', 'B', 9);
  $pdf->Write(5, 'Step 6: Agree actions.'."\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, 'Finally, conclude by agreeing who will do what, by when, to improve your team processes. Agree a date to check you have followed through. The more effective your team, the more engaged and fulfilled you and your colleagues will feel.');
  $pdf->SetLeftMargin(20);
  $pdf->SetRightMargin(20);

  include ('functions/pdf_headlines_chart_team.php');

  /* PAGE 3 ----------------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();

  // Front page header adapted for this page
  // Draw Report Title box and Overall score highlight circle
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(224, 230, 255);
  $pdf->Line(33, 20, 180, 20);
  $pdf->SetFillColor(255, 249, 82);
  $pdf->Circle(174, 20, 10, 'F');

  // Print Team Report details
  $pdf->SetRightMargin(50);
  if ($teamsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $teamsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $teamsurveyname . "\n");
  }
  // End of Page 3 header
  // Write this report details: Overall score and responses to date
  $pdf->SetRightMargin(20);
  $pdf->SetFont('Helvetica', '', 24);
  $pdf->SetXY(165.5, 18);
  $pdf->Write(5, $tandsgrandavefm);
  $pdf->SetFont('Helvetica', '', 20);
  $pdf->Write(5, "%");
  $pdf->SetXY(20, 27);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numteamrespondents . ' team member and ' . $numstakerespondents . ' stakeholder responses' . "\n" . "\n" . "\n");

  $pdf->SetFont('Helvetica', '', 16);
  $pdf->setXY(20, 45);
  $pdf->Write(5, 'Your Detailed Team Profile' . "\n" . "\n");

  include ('functions/pdf_full_chart_team.php');

  /* Original page -add back in to check calculations are right
    $pdf->AddPage();
    $pdf->SetFont('Helvetica', '', 14);
    $pdf->Write(10, $teamsurveyname . "\n");
    $pdf->Write(10, 'Your Leadership Quartet Team Review ORIGINAL' . "\n" . "\n");
    $pdf->SetlineWidth(0);
    $pdf->SetDrawColor(35, 134, 243);
    $pdf->Line(20, 40, 182, 40);
    $pdf->SetFont('Helvetica', '', 12);
    $pdf->setXY(20, 45);
    $pdf->Write(5, 'Your Detailed Team Profile - how do you perform?' . "\n" . "\n");

    include ('functions/ok_pdf_full_chart_team.php');
   */

  /* PAGE 4 --------------------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();
// Front page header adapted for this page
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(255, 255, 255);
  $pdf->Line(33, 20, 190, 20);
  // Print Team Report details
  $pdf->SetRightMargin(50);
  if ($teamsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $teamsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 13);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $teamsurveyname . "\n");
  }
  $pdf->SetRightMargin(20);
  $pdf->SetXY(20, 27);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numteamrespondents . ' team member and ' . $numstakerespondents . ' stakeholder responses' . "\n" . "\n" . "\n");
  $pdf->SetLineWidth(.1);
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->Line(20, 35, 190, 35);

  // End of Page 4 header

  include ('functions/pdf_sandw_team.php');

  /* PAGE 5 -------------------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();
// Front page header adapted for this page
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(255, 255, 255);
  $pdf->Line(33, 20, 190, 20);
  // Print Team Report details
  $pdf->SetRightMargin(50);
  if ($teamsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $teamsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 13);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $teamsurveyname . "\n");
  }
  $pdf->SetRightMargin(20);
  $pdf->SetXY(20, 27);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numteamrespondents . ' team member and ' . $numstakerespondents . ' stakeholder responses' . "\n" . "\n" . "\n");
  $pdf->SetLineWidth(.1);
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->Line(20, 35, 190, 35);

  // End of Page 5 header

  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, 'Suggestions for Development' . "\n" . "\n");
  $pdf->SetFont('Helvetica', '', 9);
  $pdf->Write(5, 'The questionnaire asked each respondent to pick up to three areas for the team to prioritise for development.' . "\n" . "\n");

  include ('functions/pdf_voterank_team.php');

  /* PAGE 6 --------------------------------------------------------------------------------------------------------------------------------- */

  $pdf->AddPage();
// Front page header adapted for this page
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(255, 255, 255);
  $pdf->Line(33, 20, 190, 20);
  // Print Team Report details
  $pdf->SetRightMargin(50);
  if ($teamsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $teamsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 13);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $teamsurveyname . "\n");
  }
  $pdf->SetRightMargin(20);
  $pdf->SetXY(20, 27);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numteamrespondents . ' team member and ' . $numstakerespondents . ' stakeholder responses' . "\n" . "\n" . "\n");
  $pdf->SetLineWidth(.1);
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->Line(20, 35, 190, 35);

  // End of Page 6 header

  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, 'Team Comments: What team members say you do well');

  $result = mysqli_query($dbc, "SELECT  goodcomments FROM teamdata  WHERE teamsurveyid = '$reportrequested' && (teamrole = '2' || teamrole = '1') ");
// Print out comments
  $pdf->SetXY(20, 55);
  $pdf->SetFont('Helvetica', '', 9);
  while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
    $goodc = $row['goodcomments'];
    if ($goodc != '') {    // stripping out nil comments
      $pdf->Write(5, '"' . $goodc . '"' . "\n" . "\n");
    }
  }

  /* PAGE 7 -------------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();
// Front page header adapted for this page
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(255, 255, 255);
  $pdf->Line(33, 20, 190, 20);
  // Print Team Report details
  $pdf->SetRightMargin(50);
  if ($teamsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $teamsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 13);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $teamsurveyname . "\n");
  }
  $pdf->SetRightMargin(20);
  $pdf->SetXY(20, 27);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numteamrespondents . ' team member and ' . $numstakerespondents . ' stakeholder responses' . "\n" . "\n" . "\n");
  $pdf->SetLineWidth(.1);
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->Line(20, 35, 190, 35);

  // End of Page 7 header

  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, 'Team Comments: What team members say you could improve ');

  $result1 = mysqli_query($dbc, "SELECT  badcomments FROM teamdata  WHERE teamsurveyid = '$reportrequested' && (teamrole = '2' || teamrole = '1') ");
// Print out comments
  $pdf->SetXY(20, 55);
  $pdf->SetFont('Helvetica', '', 9);
  while ($row = mysqli_fetch_array($result1, MYSQLI_BOTH)) {
    $badc = $row['badcomments'];
    if ($badc != '') {  // stripping out nil comments
      $pdf->Write(5, '"' . $badc . '"' . "\n" . "\n");
    }
  }

  /* PAGE 8 -------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();
// Front page header adapted for this page
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(255, 255, 255);
  $pdf->Line(33, 20, 190, 20);
  // Print Team Report details
  $pdf->SetRightMargin(50);
  if ($teamsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $teamsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 13);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $teamsurveyname . "\n");
  }
  $pdf->SetRightMargin(20);
  $pdf->SetXY(20, 27);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numteamrespondents . ' team member and ' . $numstakerespondents . ' stakeholder responses' . "\n" . "\n" . "\n");
  $pdf->SetLineWidth(.1);
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->Line(20, 35, 190, 35);

  // End of Front page header adapted for this page

  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, 'Stakeholder Comments: What stakeholders say you do well');

  $result2 = mysqli_query($dbc, "SELECT  goodcomments FROM teamdata  WHERE teamsurveyid = '$reportrequested'  && (teamrole = '4' || teamrole = '3')");
// Print out comments
  $pdf->SetXY(20, 55);
  $pdf->SetFont('Helvetica', '', 9);
  while ($row = mysqli_fetch_array($result2, MYSQLI_BOTH)) {
    $goodc = $row['goodcomments'];
    if ($goodc != '') {    // stripping out nil comments
      $pdf->Write(5, '"' . $goodc . '"' . "\n" . "\n");
    }
  }

  /* PAGE 9 -------------------------------------------------------------------------------------------------------------- */
  $pdf->AddPage();
// Front page header adapted for this page
  $pdf->SetLineWidth(30);
  $pdf->SetDrawColor(255, 255, 255);
  $pdf->Line(33, 20, 190, 20);
  // Print Team Report details
  $pdf->SetRightMargin(50);
  if ($teamsurveyname_strlen >= 50) {
    $pdf->SetXY(20, 9);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(8, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(8, $teamsurveyname . "\n");
  }
  else {
    $pdf->SetXY(20, 13);
    $pdf->SetFont('Helvetica', 'B', 16);
    $pdf->Write(10, 'Team Review: ');
    $pdf->SetFont('Helvetica', '', 16);
    $pdf->Write(10, $teamsurveyname . "\n");
  }
  $pdf->SetRightMargin(20);
  $pdf->SetXY(20, 27);
  $pdf->SetFont('Helvetica', 'I', 9);
  $pdf->Write(5, 'Responses to date: ' . $numteamrespondents . ' team member and ' . $numstakerespondents . ' stakeholder responses' . "\n" . "\n" . "\n");
  $pdf->SetLineWidth(.1);
  $pdf->SetDrawColor(0, 0, 0);
  $pdf->Line(20, 35, 190, 35);

  // End of Front page header adapted for this page

  $pdf->SetFont('Helvetica', '', 16);
  $pdf->Write(5, 'Stakeholder Comments: What stakeholders say you could improve');
  $result3 = mysqli_query($dbc, "SELECT  badcomments FROM teamdata  WHERE teamsurveyid = '$reportrequested'  && (teamrole = '4' || teamrole = '3')");
// Print out comments
  $pdf->SetXY(20, 55);
  $pdf->SetFont('Helvetica', '', 9);
  while ($row = mysqli_fetch_array($result3, MYSQLI_BOTH)) {
    $badc = $row['badcomments'];
    if ($badc != '') {  // stripping out nil comments
      $pdf->Write(5, '"' . $badc . '"' . "\n" . "\n");
    }
  }

  $reportname = $teamsurveyname . ' Team Review .pdf';
  $pdf->Output($reportname, 'I');
  include("includes/analyticstracking.php");
  include("includes/downloadstracking.php");
}
mysqli_close($dbc);

// Include the HTML footer file:
// Write and close the session:
//session_write_close();
//include ('includes/footercfx.php');
?>
