<?php 
$markup = '

	<div class="box--review-set-up-ok">
	<div class="review-report-info">
		<div><div style="font-size: 1.5rem;">'. $num_responses .'</div> ' . $responses_text . '</div>	
		<div>'. $report_link .'</div>
	</div>
	<h2><span class="review-title">' . $surveyname . '</span></h2>
	<div class="review-type"><b>' . $surveytype . '</b></div>
	<div class="divider"></div>
	
	' . $loginpara . '
	</div>
';

print $markup;
print $footer_markup;
?>
