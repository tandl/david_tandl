<?php

/**
	* @file
 * Interrogates TandLbase and produces table showing reports this authenticated user has commissioned, with link to the reports
 *
 * Implements hook_menu().
 *
 */

function your_reviews_block_info() {
  $blocks['block_your_reviews'] = array(
    'info' => t('Your Reviews'),
  );
  
  return $blocks;
}

function your_reviews_block_view($delta='') {
	$block = array();

	switch ($delta) :
		case 'block_your_reviews':
			if(user_is_logged_in()):
			  $block['content'] = your_reviews_block();
			else:
				$path = 'review/dashboard';
				header('Location: ' . url('user/login', array('query' => array('destination' => $path), 'absolute' => TRUE)), TRUE, 302);
				drupal_exit();
			endif;
		break;
	endswitch;
	
	return $block;
}

function your_reviews_block() {

		$output = '
		<div class="row">
		<div class="span9">
		';

		//Connect to TandLbase database...
		db_set_active('TandLbase');

		//...and extract commissioned report details
		global $user;


		$memberemail = ' ';

		$uid =  $user->uid;
		
		//Building the table of team reviews
		$output .= $reports_message . '
		<p>Except for single reviewer reports, reports are only available to view when at least 3 people have contributed, for confidentiality reasons.<br />
		Your report is updated when you view it. View it now - and if more people contribute later, view it again to see an updated report.</p>		
		<h2>Your team reviews</h2>
		<a href="/review/add/team" class="icon-add">Add new team review</a>
		<table class="tablesaw tablesaw-stack table-team-reviews" data-tablesaw-mode="stack">
		<thead>
		<tr>
		<th>Review name</th>
		<th>Review type</th>
		<th>Setup date</th>
		<th>Responses</th>		
		<th>Check report</th>
		</tr>
		</thead>
		<tbody>
		';
		$numteamreviews = 0;

		// identify reports user has commissioned
		if ($uid == 1) {
		  $query = db_select('teamsurveys', 'r');
		  $query->condition('showhide', 1)
		      	->fields('r', array('teamsurveyid', 'teamsurveyname', 'surveytype', 'opendate', 'showhide','url_hash'))
						->orderBy('teamsurveynumber','DESC');
		  $result = $query->execute();
		}
		else {
		  $query = db_select('teamsurveys', 'r');
		  $query->condition('memberid', $uid)
		      	->condition('showhide', 1)
		      	->fields('r', array('teamsurveyid', 'teamsurveyname', 'surveytype', 'opendate', 'showhide','url_hash'))
						->orderBy('teamsurveynumber','DESC');
		  $result = $query->execute();
		}
		// and print table
		foreach ($result as $row) {
		  $numteamreviews++;

		  //get number of responses
		  $query = db_select('teamdata', 'i');
		  $query
		      ->condition('teamsurveyid', $row->teamsurveyid)
		      ->fields('i', array('teamrespno'));
		  $t_results = $query->execute()->fetchAll();		
			$num_responses = count($t_results);

		  if($num_responses < 3) {
		  	$link = 'More responses required';
		  } else {
		  	$link = "<a href='" . $base_url . "/responsivesecure/responsive_pdf_report_team.php?teamsurveyid=$row->teamsurveyid' target=\'_blank\' class='icon-pdf'><i class='fa fa-file-pdf'></i>View report</a>";
			}
			
		  if ($row->surveytype == 4) {
		    $stype = 'Team review by team leader only';
		  }
		  elseif ($row->surveytype == 5) {
		    $stype = 'Team review by whole team';
		  }
		  elseif ($row->surveytype == 6) {
		    $stype = 'Team review by whole team plus stakeholders';
		  }
		  else {
		    $stype = 'Unknown';
		  }

		 //$odate = strtotime($row->opendate);
		 $newdate = date('d M Y', strtotime($row->opendate));
		
		 global $base_url;
		 $output .= '
			<tr>
				 	<td><b class="tablesaw-cell-label">Review name</b><span class="tablesaw-cell-content"><a href="/review/' .$row->teamsurveyid .'/' . $row->url_hash  . '">' . $row->teamsurveyname . '</a></span></td>
				 	<td class="review-type"><b class="tablesaw-cell-label">Review type</b><span class="tablesaw-cell-content">' . $stype . '</span></td>
				 	<td class="review-date"><b class="tablesaw-cell-label">Setup date</b><span class="tablesaw-cell-content">' . $newdate . '</span></td>
				 	<td><b class="tablesaw-cell-label">Responses</b><span class="tablesaw-cell-content">'. $num_responses . '</span></td>
				 	<td><b class="tablesaw-cell-label">Check report</b><span class="tablesaw-cell-content">' . $link . '</span></td>		 
		 </tr>';
		 }


		if ($numteamreviews == 0) {
			$output .=  '<tr><td>None set up yet</td><td><a href="../sites/default/files/pdfs/team-sample-report.pdf" target="_blank">See a sample report here</a></td><td></td><td></td></tr>';
		}


		
		$output .= '
		</tbody>
		</table>
		<div class="showing showing-team">Showing 1-5 of ' . $numteamreviews . ' - <a href="" class="table-show-all-team">Show all</a></div>
		<br/>
		';

		//Building the table of individual reviews
		$output .= '<h2>Your individual reviews</h2>
		<a href="/review/add/individual" class="icon-add">Add new individual review</a>
		<table class="tablesaw tablesaw-stack table-ind-reviews" data-tablesaw-mode="stack">
		<thead>
		<tr>
		<th>Review name</th>
		<th>Review type</th>
		<th>Setup date</th>
		<th>Responses</th>
		<th>Check report</th>
		</tr>
		</thead>
		<tbody>
		';
		$numindreviews = 0;

		// identify reports user has commissioned
		// uid == 1 means the administrator can see all reports
		if ($uid == 1) {
		  $query = db_select('indsurveys', 'r');
		  $query
		      ->condition('showhide', 1)
		      ->fields('r', array('indsurveyid', 'indsurveyname', 'surveytype', 'opendate', 'showhide','url_hash'));
		$query
			->orderBy('indsurveynumber','DESC');
		  $result = $query->execute();
		}
		else {
		  $query = db_select('indsurveys', 'r');
		  $query
		      ->condition('memberid', $uid)
		      ->condition('showhide', 1)
		      ->fields('r', array('indsurveyid', 'indsurveyname', 'surveytype', 'opendate', 'showhide','url_hash'));
		$query
			->orderBy('indsurveynumber','DESC');
		  $result = $query->execute();
		}
		// and print table
		foreach ($result as $row) {
		  $numindreviews++;

		  //get number of responses
		  $query = db_select('inddata', 'i');
		  $query
		      ->condition('indsurveyid', $row->indsurveyid)
		      ->fields('i', array('indrespno'));
		  $i_results = $query->execute()->fetchAll();		
			$num_responses = count($i_results);
		  
		  if($num_responses < 3) {
		  	$link = 'More responses required';
		  } else {
		  	$link = "<a href='" . $base_url . "/responsivesecure/responsive_pdf_report_ind.php?indsurveyid=$row->indsurveyid' target='_blank' class='icon-pdf'>View report</a>";
		  }
		  
		  if ($row->surveytype == 9) {
		    $stype = 'Individual profile - self-assessment';
		  }
		  elseif ($row->surveytype == 10) {
		    $stype = 'Individual profile - 360 review';
		  }
		  else {
		    $stype = 'Unknown';
		  }

		   $newdate = date('d M Y', strtotime($row->opendate));
		   $output .= '<tr>
				 	<td><b class="tablesaw-cell-label">Review name</b><span class="tablesaw-cell-content"><a href="/review/' .$row->indsurveyid .'/' . $row->url_hash  . '">' . $row->indsurveyname . '</a></span></td>
				 	<td class="review-type"><b class="tablesaw-cell-label">Review type</b><span class="tablesaw-cell-content">' . $stype . '</span></td>
				 	<td class="review-date"><b class="tablesaw-cell-label">Setup date</b><span class="tablesaw-cell-content">' . $newdate . '</span></td>
				 	<td><b class="tablesaw-cell-label">Responses</b><span class="tablesaw-cell-content">'. $num_responses . '</span></td>
				 	<td><b class="tablesaw-cell-label">Check report</b><span class="tablesaw-cell-content">' . $link . '</span></td>
		   	</tr>';
		}

		// or send 'No reports yet' message
		if ($numindreviews == 0) {
		  $output .=  '<tr><td>None set up yet</td><td><a href="../sites/default/files/pdfs/individual-sample-report.pdf" target="_blank">See a sample report here</a></td><td></td><td></td></tr>';
		}

		$output .= '
		</tbody>
		</table>
		<div class="showing showing-team">Showing 1-5 of ' . $numindreviews . ' - <a href="" class="table-show-all-ind">Show all</a></div>
		</div>
		</div>
		';

		// ... and reset database to Drupal default
		db_set_active();

  return $output;
}


/* a page to display a single review */

function your_reviews_menu() {
  $items = array();

  $items['review/%/%'] = array(
  	'title' => 'Review Details', 
    'page callback' => 'review_page', 
    'page arguments' =>  array(1,2), 
    'access callback' => 'user_is_logged_in',
    'type' => MENU_CALLBACK
  );
  return $items;
} 

function review_page($id, $hash) { 

	global $base_url;

	db_set_active('TandLbase');
	
	//is it an individual survey
	$query = db_select('indsurveys', 'r')
				->condition('showhide', 1)
				->condition('url_hash', $hash)
				->condition('indsurveyid', $id)
				->fields('r', array('indsurveyid', 'indsurveyname', 'surveytype', 'opendate', 'showhide','url_hash'));
	$result = $query->execute()->fetch();	
	
	if(!empty($result)):
	
		//get number of responses
		$query = db_select('inddata', 'i');
		$query
		    ->condition('indsurveyid', $result->indsurveyid)
		    ->fields('i', array('indrespno'));
		$i_results = $query->execute()->fetchAll();		
		$num_responses = count($i_results);
		
		if($num_responses < 3) {
			//$report_link = 'More responses required';
		} else {
			$report_link = "<a href='" . $base_url . "/responsivesecure/responsive_pdf_report_ind.php?indsurveyid=$result->indsurveyid' target='_blank' class='icon-pdf' >View report</a>";
		}	
	
	
		$surveytype = $type;
		$surveyname = $result->indsurveyname;
		
		switch($result->surveytype) :

		  case '9' : 
		  	$stype = 'Individual profile - self-assessment only';
		  	$footer_markup = getOutputText('ind','screen');
		  	
		  	$iUrl = htmlspecialchars($base_url . '/responsivesecure/resp-quest-ind-self.php?a=' . $result->url_hash . '&b='. $result->indsurveyid . '&c=i');
		  	    	
		    $loginpara = '<p><div class="review-for">For the individual leadership review: </div><span class="icon-copy button clipboard" data-clipboard-text="'. $iUrl .'">Copy Link</span> <input value="' . $iUrl . '" class="clipboard" readonly="readonly" data-clipboard-text="'. $iUrl .'"  /></p>';

		    $mailpara = htmlspecialchars("The self-assessment link is: \nLink: " . $iUrl . "\n");
		    $mailpara .= "All the best \n\nDavid Mathew";
		    
		  break;

		  case '10' : 
		  	$stype = 'Individual profile - personal development 360 review';
		  	$footer_markup = getOutputText('ind','screen');

		  	$iUrl = htmlspecialchars($base_url . '/responsivesecure/resp-quest-ind-self.php?a=' . $result->url_hash . '&b='. $result->indsurveyid . '&c=i');
		  	$cUrl = htmlspecialchars($base_url . '/responsivesecure/resp-quest-ind-contrib.php?a=' . $result->url_hash . '&b='. $result->indsurveyid . '&c=i');       
		    
		    $loginpara = '<p class="icon-warn">Please note that the link to the "self-rating" questionnaire is unique to the individual reviewing themselves.<br /> Everyone else - ie all people asked as "contributors" to the 360 - shares the same link as each other to the questionnaire.</p>';
		    $loginpara .= '<p><div class="review-for">For self-rating - to be used only by the person reviewing themselves:</div><span class="icon-copy button clipboard" data-clipboard-text="'. $iUrl .'">Copy Link</span> <input value="' . $iUrl . '" class="clipboard" readonly="readonly" data-clipboard-text="'. $iUrl .'"  /></p>';
		    $loginpara .= '<p><div class="review-for">For all other "contributors":</div><span class="icon-copy button clipboard" data-clipboard-text="'. $cUrl .'">Copy Link</span> <input value="' . $cUrl . '" class="clipboard" readonly="readonly" data-clipboard-text="'. $cUrl .'"  /></p>';
		    
		    $mailpara = htmlspecialchars("Please note the different password for the self-rating by the person being reviewed and the password for everyone else who has been asked to contribute to the 360: \n\nFor the self-rating by the person being reviewed: \nLink: " . $iUrl . " \n");
		    $mailpara .= htmlspecialchars("For 360 contributors: \nLink: " . $cUrl . " \n");
		    $mailpara .= "All the best \n\nDavid Mathew";
	    break;

		endswitch;		
		
	endif;
	
	// if not, a team survey
	if(empty($result)) :

		$query = db_select('teamsurveys', 'r')
					->condition('showhide', 1)
					->condition('url_hash', $hash)
					->condition('teamsurveyid', $id)
					->fields('r', array('teamsurveyid', 'teamsurveyname', 'surveytype', 'opendate', 'showhide','url_hash'));
		$result = $query->execute()->fetch();	

		if(!empty($result)):
			$surveytype = $type;
			$surveyname = $result->teamsurveyname;	

			//get number of responses
			$query = db_select('teamdata', 't');
			$query
				  ->condition('teamsurveyid', $result->teamsurveyid)
				  ->fields('t', array('teamrespno'));
			$t_results = $query->execute()->fetchAll();		
			$num_responses = count($t_results);
		
			if($num_responses < 3) {
				//$report_link = 'More responses required';
			} else {
				$report_link = "<a href='" . $base_url . "/responsivesecure/responsive_pdf_report_ind.php?indsurveyid=$result->teamsurveyid' target='_blank' class='icon-pdf'>View report</a>";
			}	
			
			switch($result->surveytype) :
				case '4' : 
					$stype = 'Team review by single reviewer only';
					$footer_markup = getOutputText('team','screen');
					
					$lUrl = htmlspecialchars($base_url . '/responsivesecure/resp-quest-team-leader.php?a=' . $result->url_hash . '&b='. $result->teamsurveyid . '&c=t');
					
      		$loginpara = '<p><span class="icon-copy button clipboard" data-clipboard-text="'. $lUrl .'">Copy Link</span> <input value="' . $lUrl . '" class="clipboard" readonly="readonly" data-clipboard-text="'. $lUrl .'"  /></p>';
				break;

				case '5' : 
					$stype = 'Team review by all team members';
					$footer_markup = getOutputText('team','screen');
					
					$mUrl = htmlspecialchars($base_url . '/responsivesecure/resp-quest-team-member.php?a=' . $result->url_hash . '&b='. $result->teamsurveyid . '&c=t'); 
					
		      $loginpara .= '<p><span class="icon-copy button clipboard" data-clipboard-text="'. $mUrl .'">Copy Link</span> <input value="' . $mUrl . '" class="clipboard" readonly="readonly" data-clipboard-text="'. $mUrl .'"  /></p>';
				break;

				case '6' : 
					$stype = 'Team review by whole team plus stakeholders';
					$footer_markup = getOutputText('team','screen');
					
					$sUrl = htmlspecialchars($base_url . '/responsivesecure/resp-quest-team-stakeholder.php?a=' . $result->url_hash . '&b='. $result->teamsurveyid . '&c=t');
					$mUrl = htmlspecialchars($base_url . '/responsivesecure/resp-quest-team-member.php?a=' . $result->url_hash . '&b='. $result->teamsurveyid . '&c=t'); 				
			   	
				  $loginpara = '<p class="icon-warn">Please note the different links for the team members and stakeholders: </p>';		
				  $loginpara .= '<p><div class="review-for">For team members</div><span class="icon-copy button clipboard" data-clipboard-text="'. $mUrl .'">Copy Link</span> <input value="' . $mUrl . '" class="clipboard" readonly="readonly" data-clipboard-text="'. $mUrl .'"  /></p>';
				  $loginpara .= '<p><div class="review-for">For stakeholders</div><span class="icon-copy button clipboard" data-clipboard-text="'. $sUrl .'">Copy Link</span> <input value="' . $sUrl . '" class="clipboard" readonly="readonly" data-clipboard-text="'. $sUrl .'"  /></p>';
 				break;
				
			endswitch;			
			
		endif;
		
	endif;
	
	
	
	// reset database to Drupal
	db_set_active();
	
	
	
	if($num_responses == 1) :
		$responses_text = 'response';
	else:
		$responses_text = 'responses';
	endif;
	
	
	
	
	
	$markup = theme('your_reviews', array('surveytype' => $stype,
																				'surveyname' => $surveyname,
																				'loginpara' => $loginpara,
																				'num_responses' => $num_responses,
																				'responses_text' => $responses_text,
																				'report_link' => $report_link,																				
																				'footer_markup' => $footer_markup));

  $build = array(
    'body_text' => array(
      '#type' => 'markup',
      '#markup' => $markup,
    )  
  );
  
  return $build;

}

function your_reviews_theme() {
  return array(
    'your_reviews' => array(
      'arguments' => array('output' => NULL),
      'template' => 'your_reviews',
     ),
  );
}


