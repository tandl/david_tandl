/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document) {

  'use strict';

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.my_custom_behavior = {
    attach: function (context, settings) {

			$(document).ready(function(){
								
				//expert-guidance
				$('.view-expert-guidance .views-row .expert-guidance--header').on('click', function() {
					$(this).parent().toggleClass('open');
				});
				

				//off canvas mobile menu toggle	
				$('.menu-toggle').on('click', function() {
					$('.off-canvas, .off-canvas--close').toggleClass('open');
					$(this).toggleClass('open');
				});

				$('.off-canvas--close').on('click', function() {
					$('.off-canvas, .menu-toggle').toggleClass('open');
					$(this).toggleClass('open');
				});


				//tabs
				var hash = location.hash;

				if(hash) { 
					$('li[data-tab="'+ hash.substring(1) +'"]').addClass('tab-current');
					$('#' + hash.substring(1)).addClass('tab-current');
				} else {
					$('li[data-tab="overview"]').addClass('tab-current');
					$('#overview').addClass('tab-current');
				}
				
				$('ul.custom-tabs li').click(function(e) {
					e.preventDefault();
					e.stopPropagation();	
					
					var tab_id = $(this).attr('data-tab');

					$('ul.custom-tabs li').removeClass('tab-current');
					$('.tab-content').removeClass('tab-current');

					$(this).addClass('tab-current');
					$("#"+tab_id).addClass('tab-current');
					
					window.location = '#' + tab_id;
				})

        $(window).on('hashchange', function() {
            var hash = location.hash;
						
						if (hash) {
								var hash = location.hash;
								console.log(hash);
								$('ul.custom-tabs li').removeClass('tab-current');
								$('li[data-tab="'+ hash.substring(1) +'"]').addClass('tab-current');
                $('.tab-content').removeClass('tab-current');
                $('#' + hash.substring(1)).addClass('tab-current');
            }
        });				

			})
			
			//clickToCopy
			new ClipboardJS('.clipboard');

			//show all reviews
			$('.table-show-all-team').on('click', function(e) {
				e.preventDefault();
				$('.table-team-reviews').addClass('show-all');
				$(this).parent().hide();
			});
			
			$('.table-show-all-ind').on('click', function(e) {
				e.preventDefault();
				$('.table-ind-reviews').addClass('show-all');
				$(this).parent().hide();
			});			
    }
  };

})(jQuery, Drupal, this, this.document);
