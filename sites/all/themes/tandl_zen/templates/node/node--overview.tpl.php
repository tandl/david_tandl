<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
$entity_wrapper = entity_metadata_wrapper('node', $node);

if(!$entity_wrapper->field_related_files->value()):
	$class = 'two-tabs';
endif;
?>

<ul class="custom-tabs <?php print $class; ?>">
	<li data-tab="overview" class="">
		<img src="/<?php print path_to_theme(); ?>/images/icons/overview.png" />
		<div>Overview</div>
	</li>
	<li data-tab="exercises">
		<img src="/<?php print path_to_theme(); ?>/images/icons/exercises.png" />
		<div>Exercises</div>
	</li>
	
	<?php if ($entity_wrapper->field_related_files->value()): ?>
		<li data-tab="slides-and-pdfs">
			<img src="/<?php print path_to_theme(); ?>/images/icons/slides-and-pdfs.png" />
			<div>Slides &amp; PDF's</div>
		</li>	
	<?php endif; ?>
</ul>

<div class="">
	<div id="overview" class="tab-content">
		<h2<?php print $title_attributes; ?>>Overview</h2>
		<?php print render($content); ?>
	</div>
	<div id="exercises" class="tab-content">
		<h2>Exercises</h2>
		<?php
		foreach($entity_wrapper->field_overview_exercises->value() as $ex):
			$exercise_object = entity_metadata_wrapper('node', $ex->nid);	
			print '<div class="box">';
			print '<h3 class="box__title">' . l($exercise_object->title->value(), 'node/' . $ex->nid) . '</h3>';

			if($exercise_object->field_ex_length->value()): 
				print '<div>Time: ' . $exercise_object->field_ex_length->value() . '</div>';
			endif;
			
			print $exercise_object->field_ex_short_description->value()['value'];			
			print '</div>';
		endforeach;
		?>

		<?php 
		if($entity_wrapper->field_ex_related_exercises->value()):
			print '<h3>Related Exercises</h3>';
			print '<ul>';
			foreach($entity_wrapper->field_ex_related_exercises->value() as $ex):
				print '<li>' . l($ex->title, 'node/' . $ex->nid) . '</li>';
			endforeach;
			print '</ul>';
		endif;
		?>
	</div>
	
	<?php if ($entity_wrapper->field_related_files->value()): ?>
		<div id="slides-and-pdfs" class="tab-content">
			<h2>Slides &amp; PDF's</h2>
			<?php 
			print '<ul>';
			foreach($entity_wrapper->field_related_files->value() as $file):
			
				$file_object = entity_metadata_wrapper('node', $file->nid);	
				$file_url = file_create_url( $file_object->field_file_file->value()['uri']);
			
				print '<li>' . l($file->title, $file_url) . '</li>';
			endforeach; 
			print '</ul>';
			?>
		</div>	
	<?php endif; ?>	
</div>

<?php print render($content['links']); ?>
