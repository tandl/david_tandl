<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
$items = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_para_basic_link');
if($items):
	$url = $items[0]['url'];
endif;

print '<div class="paragraphs-item-basic-elements">';
print render($content['field_para_basic_image']);
if($url): 
	print '<h3 class=""><a href="' . $url . '">' . render($content['field_para_basic_title']) . '</a></h3>';
else:
	print '<h3 class="">' . render($content['field_para_basic_title']) . '</h3>'; 
endif;
print render($content['field_para_basic_copy']);
print render($content['field_para_basic_callout']);
print '</div>';
?>
