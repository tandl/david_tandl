<?php
/**
 * @file
 * page.vars.php
 */

/**
 * Implements hook_preprocess_page().
 *
 * @see page.tpl.php
 */
function tlbootstrap_preprocess_page(&$variables) {
  // Add information about the number of sidebars.
  if (!empty($variables['page']['sidebar_first']) && !empty($variables['page']['sidebar_second'])) {
    $variables['content_column_class'] = ' class="col-sm-6"';
  }
  elseif (!empty($variables['page']['sidebar_first']) || !empty($variables['page']['sidebar_second'])) {
    $variables['content_column_class'] = ' class="col-md-9"';
  }
  else {
    $variables['content_column_class'] = ' class="col-sm-12"';
  }
  
 /*DM070616 added to implement responsive images 
 */
 function tlbootstrap_preprocess_image(&$vars) {
$vars['attributes']['class'][] = "img-responsive";
}
  
 /*DM110416 added this to turn parent menu items into non-links. Works fine on main domain, but unfortunately it gets over-written by client css file so I need to find a work-around before I implement it.
  function tlbootstrap_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  if (strpos(url($element['#href']), 'nolink')) {
    $output = '' . $element['#title'] . '';
  } else {
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  }
  return '' . $output . $sub_menu . "\n";
}
End DM110416 */
 
 /* DM Added following https://www.drupal.org/node/1809322 to try to get custom css per domain */
  $domain = domain_get_domain();
  $filename = 'domain-' . $domain['machine_name'] . '.css';
  drupal_add_css(path_to_theme() . '/css/' . $filename, array('group' => CSS_THEME));
// or perhaps could have been: drupal_add_css(drupal_get_path('theme', 'tlbootstrap') . '/css/' . $filename);

  
/* DM Added this to try to resolve 2nd level menu problem 31012016 Will need to remove js file as well from sites/all/themes/tlbootstrap/js But leads to tricky scrolling */
//drupal_add_js(drupal_get_path('theme', 'tlbootstrap') .'/js/menu-hover.js');


  // Primary nav.
  $variables['primary_nav'] = FALSE;
  if ($variables['main_menu']) {
    // Build links.
    $variables['primary_nav'] = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
    // Provide default theme wrapper function.
    $variables['primary_nav']['#theme_wrappers'] = array('menu_tree__primary');
  }

  // Secondary nav.
  $variables['secondary_nav'] = FALSE;
  if ($variables['secondary_menu']) {
    // Build links.
    $variables['secondary_nav'] = menu_tree(variable_get('menu_secondary_links_source', 'user-menu'));
    // Provide default theme wrapper function.
    $variables['secondary_nav']['#theme_wrappers'] = array('menu_tree__secondary');
  }

  $variables['navbar_classes_array'] = array('navbar');

  if (theme_get_setting('bootstrap_navbar_position') !== '') {
    $variables['navbar_classes_array'][] = 'navbar-' . theme_get_setting('bootstrap_navbar_position');
  }
  else {
    $variables['navbar_classes_array'][] = 'container';
  }
  if (theme_get_setting('bootstrap_navbar_inverse')) {
    $variables['navbar_classes_array'][] = 'navbar-inverse';
  }
  else {
    $variables['navbar_classes_array'][] = 'navbar-default';
  }
}

//DM added 070716 to modify login pagess
function tlbootstrap_theme() {
  $items = array();
	
  $items['user_login'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'tlbootstrap') . '/templates',
    'template' => 'user-login',
    'preprocess functions' => array(
       'tlbootstrap_preprocess_user_login'
    ),
  );
  $items['user_register_form'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'tlbootstrap') . '/templates',
    'template' => 'user-register-form',
    'preprocess functions' => array(
      'tlbootstrap_preprocess_user_register_form'
    ),
  );
  $items['user_pass'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'tlbootstrap') . '/templates',
    'template' => 'user-pass',
    'preprocess functions' => array(
      'tlbootstrap_preprocess_user_pass'
    ),
  );
  return $items;
}

function tlbootstrap_preprocess_user_login(&$vars) {
  $vars['intro_text'] = t('');
}

function tlbootstrap_preprocess_user_register_form(&$vars) {
  $vars['intro_text'] = t('');
}

function tlbootstrap_preprocess_user_pass(&$vars) {	
  $vars['intro_text'] = t('If you have forgotten or want to change your password, complete your email address below and submit. You will receive an email with a link to the page where you can set your new password. If you can\'t see the email in your inbox, please check your junk folder.');
}

//End DM added 070716 to modify login page



function tlbootstrap_menu_alter(&$items) {
  $items['user/login']['title'] = 'Log in';  
  $items['user/password']['title'] = 'Re-set your password';
  $items['user/register']['title'] = 'Register';
}

function tlbootstrap_form_user_register_form_alter(&$form, $form_state, $form_id) {
     $form['actions']['submit']['#value'] = t('Register me please');
   }

function tlbootstrap_form_user_pass_alter(&$form, $form_state, $form_id) {
     $form['actions']['submit']['#value'] = t('Email me a link to re-set my password');
   }
